package br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.merging;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.UMLPlugin;
import org.eclipse.uml2.uml.resources.util.UMLResourcesUtil;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;


/**
 * Operation to import the elements of a OBO-RO UML model in another OBO-RO UML model.
 * 
 * @author cawal
 *
 */
public class OBOROUMLModelMergingOperation extends AbstractOperation {
  /**
   * The model which the elements are imported.
   */
  IResource sourceOBOROModel;
  /**
   * The model which the elements are imported.
   */
  IResource targetOBOROModel;

  /**
   * 
   * @param label
   * @param sourceOBOROModel The model which the elements are imported.
   * @param targetOBOROModel The model which the elements are imported.
   */
  public OBOROUMLModelMergingOperation(String label, IResource sourceOBOROModel,
      IResource targetOBOROModel) {
    super(label);
    this.sourceOBOROModel = sourceOBOROModel;
    this.targetOBOROModel = targetOBOROModel;
  }

  @Override
  public IStatus execute(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
    
    try {
      Model sourceModelElement = (Model) readEObjectsFromIResource(sourceOBOROModel).get(0);
      Model targetModelElement = (Model) readEObjectsFromIResource(targetOBOROModel).get(0);
      System.out.println("=========================================================================");
      System.out.println(sourceModelElement);
      System.out.println(targetModelElement);

      targetModelElement.allNamespaces().stream().forEach(System.out::println);
      
     sourceModelElement.getNestedPackages().forEach(p -> {
       targetModelElement.getNestedPackages().add(p);
     });
      
      
      targetModelElement.importMembers(sourceModelElement.getPackagedElements());

      targetModelElement.allOwnedElements().stream().forEach(System.out::println);
      sourceModelElement.allOwnedElements().stream().forEach(System.out::println);
      // Collection<org.eclipse.uml2.uml.PackageableElement> c
      // = EcoreUtil.copyAll(sourceModelElement.getPackagedElement());
      // targetModelElement.getPackagedElement().addAll(c);

      System.out.println("CHEGOU AQUI");
      targetModelElement.eResource().setModified(true);
      targetModelElement.eResource().save(null);
      System.out.println("DEVE TER SALVO");
      targetModelElement.allNamespaces().stream().forEach(System.out::println);

      return Status.OK_STATUS;

    } catch (IOException e) {
      // TODO Auto-generated catch block
      throw new ExecutionException(e.getLocalizedMessage());
    }

  }

  @Override
  public IStatus redo(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public IStatus undo(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
    // TODO Auto-generated method stub
    return null;
  }


  /**
   * Helper method to recover the model elements from a IResource.
   * 
   * @param resource
   * @return
   */
  protected EList<EObject> readEObjectsFromIResource(IResource resource) {
    // Register the XMI resource factory for the .xmi extension
    Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
    Map<String, Object> m = reg.getExtensionToFactoryMap();
    m.put(resource.getFileExtension(), new XMIResourceFactoryImpl());

    // Obtain a new resource set
    ResourceSet rs = new ResourceSetImpl();
    rs = UMLResourcesUtil.init(rs);
    
    rs.getURIConverter().getURIMap().forEach((k,v) -> System.out.println(k + " " + v));
    
    

    // Get the resource
    Resource file = rs.getResource(URI.createURI(resource.getLocationURI().toString()), true);

    // Get the model elements in my
    // example everything is hierarchical included in this first node
    return file.getContents();
  }

  /**
   * Register profile in all points of the ResourceSet, 
   * allowing then to be used within the resourceSet 
   * @param resourceSet
   * @param profile
   * @throws IOException
   */
  public static void registerProfiles(ResourceSet resourceSet, Profile profile) throws IOException {
    
    // registering package
    EPackage profilePackage = profile.getDefinition(); 
    resourceSet.getPackageRegistry().put(profilePackage.getNsURI(),profilePackage);
    
    // registering URI conversions
    resourceSet.getURIConverter().getURIMap().put(
        URI.createURI(profile.getURI()),
        URI.createURI("pathmap://UML_PROFILES/"+profilePackage.getNsPrefix()+".profile.uml")
        );
    
    // Registering profile maps
    Map<String,URI> epackageNSURI2profileLocationMap = UMLPlugin.getEPackageNsURIToProfileLocationMap();
    epackageNSURI2profileLocationMap.put(
        profilePackage.getNsURI(), 
        URI.createURI("pathmap://UML_PROFILES//"+profilePackage.getNsPrefix()+".profile.uml#_0")
        );
    
  
  }


}
