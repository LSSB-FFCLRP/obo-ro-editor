package br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.builder;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Map;

import javax.xml.parsers.SAXParserFactory;

import org.bbop.dataadapter.DataAdapterException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors.OBODatamodel2OBOAPITranslator;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors.OboFileSerializer;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors.OboFormatVersion;
import br.usp.ffclrp.dcm.lssb.oboroeditor.transformations.TransformationRunner;

/**
 * Instances of this class are created by the Eclipse Platform when some file
 * changes in the
 * workspace. Then, the build() method is called.
 * 
 * @author cawal
 *
 */
public class OboRoEditorBuilder extends IncrementalProjectBuilder {
    
    public static final String BUILDER_ID =
        "br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.oboroeditorBuilder";
    
    private static final String MARKER_TYPE =
        "br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.xmlProblem";
    
    @SuppressWarnings("unused")
    private SAXParserFactory parserFactory;
    
    private ProjectPropertiesManager propertiesManager;
    
    private IFile umlModelFile;
    private IFile importedOdmModelFile;
    private IFile initialExportationOdmModelFile;
    private IFile finalExportationOdmModelFile;
    private IFile exportedOboFile;
    private boolean isRoundTrip = false;
    
    /**
     * Marks a file with a error/message markers.
     * 
     * @param file
     * @param message
     * @param lineNumber
     * @param severity
     */
    private void addMarker(IFile file, String message, int lineNumber,
        int severity) {
        try {
            IMarker marker = file.createMarker(MARKER_TYPE);
            marker.setAttribute(IMarker.MESSAGE, message);
            marker.setAttribute(IMarker.SEVERITY, severity);
            if (lineNumber <= 0) {
                lineNumber = 1;
            }
            marker.setAttribute(IMarker.LINE_NUMBER, lineNumber);
        } catch (CoreException e) {
        }
    }
    
    /**
     * Builds the project fully.
     */
    @Override
    protected IProject[] build(int kind, Map<String, String> args,
        IProgressMonitor monitor)
        throws CoreException {
        
        System.out.println("CONSTRUINDO!");
        
        // get the information for the build
        IProject project = getProject();
        propertiesManager = new ProjectPropertiesManager(project);
        
        // get all important files and informations
        umlModelFile =
            project.getFile(propertiesManager.getProperty("main_uml_model"));
        
        importedOdmModelFile = project
            .getFile(propertiesManager.getProperty("imported_odm_model"));
        
        initialExportationOdmModelFile =
            project.getFile(
                propertiesManager.getProperty("initial_exportation_odm_model"));
        
        finalExportationOdmModelFile =
            project.getFile(
                propertiesManager.getProperty("final_exportation_odm_model"));
        
        exportedOboFile =
            project.getFile(propertiesManager.getProperty("exported_obo_file"));
        
        isRoundTrip = propertiesManager.getProperty("is_round_trip")
            .equalsIgnoreCase("true");
        
        // build accordingly
        try {
            fullBuild(monitor);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return null;
    }
    
    protected void fullBuild(final IProgressMonitor monitor)
        throws CoreException, IOException {
        
        // set the files as derived, to prevent git to include them
        setOutputFilesAsDerived();
        
        executeUml2OdmTransformation(monitor);
        
        // if (roundtrip/previousOntology), ODM enrichment
        if (isRoundTrip) {
            System.out.println("executando enriquecimento");
            executeOdmEnrichment(monitor);
        } else {
            finalExportationOdmModelFile = initialExportationOdmModelFile;
        }
        
        executeOdm2OboSerialization();
    }
    
    private void setOutputFilesAsDerived() {
        try {
            initialExportationOdmModelFile.setDerived(true, null);
            finalExportationOdmModelFile.setDerived(true, null);
            exportedOboFile.setDerived(true, null);
        } catch (CoreException e) {
            // do nothing... no problem!
        }
    }
    
    private void executeOdm2OboSerialization() throws CoreException {
        try {
            OBOSession odmSession = getOBODatamodelSession(
                finalExportationOdmModelFile.getLocationURI().toString());
            
            org.obo.datamodel.OBOSession oboSession;
            
            oboSession = OBODatamodel2OBOAPITranslator.translate(odmSession);
            serialize(oboSession, exportedOboFile);
            
        } catch (DataAdapterException | IOException e) {
            String message =
                "File not found: " + e.getMessage() + "\n"
                    + "Verify the ontology_build.properties file.";
            IStatus status = new Status(IStatus.ERROR,
                OboRoOntologyDevelopmentNature.NATURE_ID, message);
            addMarker(exportedOboFile, e.toString(), 0, 0);
            throw new CoreException(status);
        }
    }
    
    private void executeUml2OdmTransformation(final IProgressMonitor monitor)
        throws CoreException, IOException {
        
        if (initialExportationOdmModelFile.getLocalTimeStamp() > umlModelFile
            .getLocalTimeStamp()) {
            return;
        }
        
        System.out.println("Carregando uml");
        
        InputStream umlIS = umlModelFile.getContents();
        // create an empty InputStream
        PipedInputStream pipedIn = new PipedInputStream();
        // create an OutputStream with the InputStream from above as input
        PipedOutputStream pipedOS = new PipedOutputStream(pipedIn);
        
        // now work on the OutputStream
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Iniciando thread");
                TransformationRunner.transformUML2OboRoOdmFile(umlIS, pipedOS,
                    monitor);
            }
        });
        t.start();
        System.out.println("salvando arquivo");
        // now you have the OutputStream as InputStream, overwrite file contents
        if (!initialExportationOdmModelFile.exists()) {
            initialExportationOdmModelFile.create(pipedIn, true, monitor);
        } else {
            initialExportationOdmModelFile.setContents(pipedIn, true, true,
                monitor);
        }
    }
    
    private void executeOdmEnrichment(final IProgressMonitor monitor)
        throws CoreException, IOException {
        System.out.println("Executando enriquecimento");
        /*
         * if(finalExportationOdmModelFile.getLocalTimeStamp() >
         * initialExportationOdmModelFile.getLocalTimeStamp()) { return; }
         */
        
        System.out.println("Executando enriquecimento");
        
        InputStream newerOdmIS = initialExportationOdmModelFile.getContents();
        InputStream olderOdmIS = importedOdmModelFile.getContents();
        // create an empty InputStream
        PipedInputStream pipedIn = new PipedInputStream();
        // create an OutputStream with the InputStream from above as input
        PipedOutputStream pipedOS = new PipedOutputStream(pipedIn);
        
        // now work on the OutputStream
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                TransformationRunner.transformEnrichOdmFile(newerOdmIS,
                    olderOdmIS, pipedOS, monitor);;
            }
        });
        t.start();
        
        // now you have the OutputStream as InputStream, overwrite file contents
        if (!finalExportationOdmModelFile.exists()) {
            finalExportationOdmModelFile.create(pipedIn, true, monitor);
        
        } else {
            finalExportationOdmModelFile.setContents(pipedIn, true, true,
                monitor);
        }
    }
    
    /**
     * Convenience method. Similar to calling serialize(session, file,
     * OboFormatVersion.OBO1_2);
     * 
     * @param session
     *            the OBOSession to serialize
     * @param file
     *            the IFile to receive the session
     * @throws FileNotFoundException
     *             IFile does not present path information
     * @throws DataAdapterException
     *             problems during serialization
     * @throws CoreException 
     */
    public static void serialize(org.obo.datamodel.OBOSession session,
        IFile file)
        throws FileNotFoundException, DataAdapterException, CoreException {
        serialize(session, file, OboFormatVersion.OBO1_2);
    }
    
    
    /**
     * Serializes a OBOSession (OBO-API) in the IFile using the 
     * selected OBOFFF version.
     * @param session
     * @param file
     * @param oboFormatVersion
     * @throws DataAdapterException
     * @throws FileNotFoundException
     * @throws CoreException 
     */
    public static void serialize(org.obo.datamodel.OBOSession session,
        IFile file,
        OboFormatVersion oboFormatVersion)
        throws DataAdapterException, FileNotFoundException, CoreException {
        
        if (!file.exists()) {
                byte[] bytes = "File contents".getBytes();
                InputStream source = new ByteArrayInputStream(bytes);
                file.create(source, IResource.NONE, null);
        }
        OboFileSerializer.serialize(session, file.getLocation().toFile(),
            oboFormatVersion);
    }
    
    public static OBOSession getOBODatamodelSession(String filePath) throws IOException {
        // Initialize the model
        obodatamodelPackage.eINSTANCE.eClass();
        
        // Register the XMI resource factory for the .xmi extension
        Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
        Map<String, Object> m = reg.getExtensionToFactoryMap();
        m.put("obodatamodel", new XMIResourceFactoryImpl());
        
        // Obtain a new resource set
        ResourceSet resSet = new ResourceSetImpl();
        resSet.getLoadOptions().put(XMIResource.OPTION_DEFER_IDREF_RESOLUTION,
            Boolean.TRUE);
        resSet.getLoadOptions().put(XMIResource.OPTION_ENCODING, "UTF-8");
        // Get the resource
        Resource resource = resSet.getResource(URI.createURI(filePath), true);
        
        resource.load(resSet.getLoadOptions());
        
        // Get the first model element and cast it to the right type, in my
        // example everything is hierarchical included in this first node
        OBOSession ontology = (OBOSession) resource.getContents().get(0);
        return ontology;
    }
    
}
