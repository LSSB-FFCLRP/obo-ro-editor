package br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.builder;

import java.io.IOException;

import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;
import org.osgi.service.prefs.BackingStoreException;


public class OboRoOntologyDevelopmentNature implements IProjectNature {

	/**
	 * ID of this project nature
	 */
	public static final String NATURE_ID = "br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.oboroOntologyDevelopmentNature";

	private IProject project;

	@Override
	public void configure() throws CoreException {
		IProjectDescription desc = project.getDescription();
		ICommand[] commands = desc.getBuildSpec();


        try {
          addProjectStructure(this.project);
        } catch (BackingStoreException e) {
          e.printStackTrace();
        } catch (IOException e) {
          e.printStackTrace();
        }
		
		for (int i = 0; i < commands.length; ++i) {
			if (commands[i].getBuilderName().equals(OboRoEditorBuilder.BUILDER_ID)) {
				return;
			}
		}

		ICommand[] newCommands = new ICommand[commands.length + 1];
		System.arraycopy(commands, 0, newCommands, 0, commands.length);
		ICommand command = desc.newCommand();
		command.setBuilderName(OboRoEditorBuilder.BUILDER_ID);
		newCommands[newCommands.length - 1] = command;
		desc.setBuildSpec(newCommands);
		project.setDescription(desc, null);
	}

	@Override
	public void deconfigure() throws CoreException {
		IProjectDescription description = getProject().getDescription();
		ICommand[] commands = description.getBuildSpec();
		
		removeProjectStructure(project);
		
		for (int i = 0; i < commands.length; ++i) {
			if (commands[i].getBuilderName().equals(OboRoEditorBuilder.BUILDER_ID)) {
				ICommand[] newCommands = new ICommand[commands.length - 1];
				System.arraycopy(commands, 0, newCommands, 0, i);
				System.arraycopy(commands, i + 1, newCommands, i,
						commands.length - i - 1);
				description.setBuildSpec(newCommands);
				project.setDescription(description, null);
				return;
			}
		}
	}

	@Override
	public IProject getProject() {
		return project;
	}

	@Override
	public void setProject(IProject project) {
		this.project = project;
	}
	
	
	   /**
     * Adds the Ontology Development Project structure
     * 
     * @param project
     * @throws BackingStoreException
     * @throws CoreException
     * @throws IOException
     */
    private void addProjectStructure(IProject project)
            throws BackingStoreException, CoreException, IOException {

        // load the properties manager
        ProjectPropertiesManager propertyManager= new ProjectPropertiesManager(project);

        // create the files in workspace --------------------------

        // create all directories
        IFolder importsDirectory = project
                .getFolder(propertyManager.getProperty(
                        PropertiesKeyStrings.IMPORTED_ONTOLOGIES_DIRECTORY_KEY));

        // if not found, create it
        if (!importsDirectory.exists()) {
            importsDirectory.create(true, true, null);
        }

        IFolder buildDirectory = project.getFolder(propertyManager
                .getProperty(PropertiesKeyStrings.BUILD_DIRECTORY_KEY));

        // if not found, create it
        if (!buildDirectory.exists()) {
            buildDirectory.create(true, true, null);
        }

        IFolder projectResourcesDirectory = project
                .getFolder(propertyManager.getProperty(
                        PropertiesKeyStrings.PROJECT_RESOURCE_DIRECTORY_KEY));

        // if not found, create it
        if (!projectResourcesDirectory.exists()) {
            projectResourcesDirectory.create(true, true, null);
        }

    }

    
    /**
     * Remove the Ontology Development Project structure
     * Currently does nothing.
     * @param project
     */
    private void removeProjectStructure(IProject project) {
    }
}
