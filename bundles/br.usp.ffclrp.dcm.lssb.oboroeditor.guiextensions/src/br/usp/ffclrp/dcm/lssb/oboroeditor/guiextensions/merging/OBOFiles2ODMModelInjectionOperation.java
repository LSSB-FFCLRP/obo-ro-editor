package br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.merging;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.obo.datamodel.OBOSession;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors.OBOAPI2OBODatamodelTranslator;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors.OBODatamodelSerializer;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors.OboFileSerializer;

public class OBOFiles2ODMModelInjectionOperation extends AbstractOperation {

  List<IFile> sourceOBOFileList;
  IFile outputODMModelIFile;

  public OBOFiles2ODMModelInjectionOperation(String label, List<IFile> sourceOBOFileList,
      IFile outputODMModelIFile) {
    super(label);
    this.sourceOBOFileList = sourceOBOFileList;
    this.outputODMModelIFile = outputODMModelIFile;
  }

  @Override
  public IStatus execute(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {

    try {
      // inject the importing ontology alone and the transform it to OBO-RO
      List<String> importingPath = new ArrayList<String>();

      for (IFile ifile : sourceOBOFileList) {
        importingPath.add(ifile.getLocationURI().toString());
      }
      OBOSession session = OboFileSerializer.desserializeFromPaths(importingPath);
      OBOAPI2OBODatamodelTranslator translator = new OBOAPI2OBODatamodelTranslator(session);
      br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession translated =
          translator.translate();

      String outputPath = outputODMModelIFile.getFullPath().toFile().getAbsolutePath();
      OBODatamodelSerializer.serialize(translated, outputPath);
      
    } catch (Exception e) {
      throw new ExecutionException(e.getLocalizedMessage());
    }

    return Status.OK_STATUS;
  }

  @Override
  public IStatus redo(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
    // TODO Auto-generated method stub
    System.err.println("REDO");
    return execute(monitor, info);
  }

  @Override
  public IStatus undo(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
    // TODO Auto-generated method stub
    try {
      outputODMModelIFile.delete(true, monitor);
    } catch (CoreException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return Status.CANCEL_STATUS;
    }
    System.err.println("UNDO");
    return Status.OK_STATUS;
  }

}
