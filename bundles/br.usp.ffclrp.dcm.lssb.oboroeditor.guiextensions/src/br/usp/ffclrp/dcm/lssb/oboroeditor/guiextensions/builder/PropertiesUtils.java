package br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.builder;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Properties;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;

public class PropertiesUtils {
	
	/**
	 * Saves a java.util.Properties to an IFile.
	 * @param properties
	 * @param propertiesFile
	 * @throws IOException
	 * @throws CoreException
	 */
	public static void writeProperties(final Properties properties, final IFile propertiesFile)
			throws IOException, CoreException{
		//create an empty InputStream
		PipedInputStream in = new PipedInputStream();
		//create an OutputStream with the InputStream from above as input
		final PipedOutputStream out = new PipedOutputStream(in);
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					properties.store(out, propertiesFile.getName());
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();
		propertiesFile.setContents(in, true, true, null);
	}

}
