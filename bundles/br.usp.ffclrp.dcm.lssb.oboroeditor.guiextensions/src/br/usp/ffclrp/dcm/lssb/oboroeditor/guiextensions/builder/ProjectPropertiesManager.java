package br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.builder;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;



public class ProjectPropertiesManager {

  private static final String DEFAULT_PROPERTIES_RESOURCE_PATH =
      "/br/usp/ffclrp/dcm/lssb/oboroeditor/guiextensions/builder/default_ontology_build.properties";

  protected IProject project;
  protected final Properties properties = new Properties();
  protected IFile propertiesFile;

  public ProjectPropertiesManager(IProject project) {
    this.project = project;
    reloadProperties();
  }


  /**
   * Sets the project this Properties Manager manages
   * 
   * @param project
   */
  public void setProject(IProject project) {
    this.project = project;
    // checks if something changed in file
    reloadProperties();
  }

  public String getProperty(String name) {
    try {
      // if the properyFile was updated in file system
      if (!propertiesFile.isSynchronized(0)) {
        // update it in workspace
        propertiesFile.refreshLocal(0, null);
      }
      // load properties that the user changed in file
      properties.load(propertiesFile.getContents());
    } catch (CoreException | IOException e) {
      // TODO better error handling
      return null;
    }

    return properties.getProperty(name);
  }


  public void setProperty(String name, String value) {
    properties.setProperty(name, value);
    updatePropertyFile();
  }

  /**
   * Reloads the properties from file
   */
  private void reloadProperties() {
    // TODO adicionar o carregamento da ontologia aqui
    // remove olds
    properties.clear();

    // load all properties ----------------------------------
    try {
      InputStream defaultPropertiesSource =
          this.getClass().getResourceAsStream(DEFAULT_PROPERTIES_RESOURCE_PATH);
      properties.load(defaultPropertiesSource);

      // search for the property file.
      propertiesFile = project.getFile(PropertiesKeyStrings.ONTOLOGY_BUILD_PROPERTIES_FILE_PATH);

      // if not found, create it (copy the default)
      if (!propertiesFile.exists()) {
        propertiesFile.create(null, true, null);
        PropertiesUtils.writeProperties(properties, propertiesFile);
        return;
      } else {
        // overwrite the default properties with previous ones
        // in order to maintain user configured data
        properties.load(propertiesFile.getContents());
        return;
      }
    } catch (IOException | CoreException e) {
      System.err.println("Deu pau aqui");
      e.printStackTrace();
      // TODO Better error handling
    } 

  }

  private boolean updatePropertyFile() {
    if (null == propertiesFile) {
      return false;
    }
    try {
      // if not found, create it
      if (!propertiesFile.exists()) {
        propertiesFile.create(null, true, null);
      }
      // save properties to file in workspace
      PropertiesUtils.writeProperties(properties, propertiesFile);
    } catch (IOException e) {
      // TODO Better error handling
      return false;
    } catch (CoreException e) {
      // TODO Better error handling
      return false;
    }
    return true;
  }

}
