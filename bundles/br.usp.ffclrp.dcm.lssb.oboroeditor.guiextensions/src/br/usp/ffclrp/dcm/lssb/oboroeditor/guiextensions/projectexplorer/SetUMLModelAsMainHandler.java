package br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.projectexplorer;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

import br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.builder.ProjectPropertiesManager;
import br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.builder.PropertiesKeyStrings;

/**
 * This handler receives a UML model file and sets it as the main file for compiling an ontology.
 * 
 * @author cawal
 *
 */
public class SetUMLModelAsMainHandler extends AbstractHandler {

  public SetUMLModelAsMainHandler() {
    super();
  }

  @Override
  public Object execute(ExecutionEvent event) throws ExecutionException {

    // get the selected IResource
    ISelection sel = HandlerUtil.getActiveMenuSelection(event);
    IStructuredSelection selection = (IStructuredSelection) sel;
    Object selectedElement = selection.getFirstElement();

    if (selectedElement instanceof IAdaptable) {
      IFile model = (IFile) ((IAdaptable) selectedElement).getAdapter(IFile.class);

      // get the property manager
      ProjectPropertiesManager propertyManager =
          new ProjectPropertiesManager(model.getProject());

      // update the properties
      propertyManager.setProperty(PropertiesKeyStrings.MAIN_MODEL_KEY,
          model.getProjectRelativePath().toString());

    }
    return null;
  }

}
