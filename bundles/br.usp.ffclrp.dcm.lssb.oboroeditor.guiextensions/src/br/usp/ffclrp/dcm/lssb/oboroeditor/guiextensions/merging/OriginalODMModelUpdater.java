package br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.merging;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.bbop.dataadapter.DataAdapterException;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.obo.dataadapter.OBOParseException;
import org.obo.datamodel.OBOSession;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors.OBOAPI2OBODatamodelTranslator;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors.OBODatamodelSerializer;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors.OboFileSerializer;


public class OriginalODMModelUpdater extends AbstractOperation {

  private IFile originalODMModel;
  private List<IFile> importingOBOFiles;
  private IFile temporaryModel;

  public OriginalODMModelUpdater(String label, IFile originalModel, List<IFile> importingOBOFiles,
      IFile temporaryModel) {
    super(label);
    this.originalODMModel = originalModel;
    this.importingOBOFiles = importingOBOFiles;
    this.temporaryModel = temporaryModel;
  }

  @Override
  public IStatus execute(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
      // Updates the original ODM model with info of the imported ontology
    try {
      if (originalODMModel.exists()) {
        // open the original model and get current filenames
        br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession originalSession =
            (br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession) this
                .getModelElementsFromIResource(originalODMModel).get(0);

        // adds the new ontology
        List<String> currentFilenames = originalSession.getCurrentFilenames();
        for (IFile f : importingOBOFiles) {
          currentFilenames.add(f.getLocationURI().toString());
        }

        // inject the importing ontology with all current filenames
        OBOSession session = OboFileSerializer.desserializeFromPaths(currentFilenames);

        // injects with all ontologies
        OBOAPI2OBODatamodelTranslator translator = new OBOAPI2OBODatamodelTranslator(session);
        br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession newSession =
            translator.translate();

        OBODatamodelSerializer.serialize(newSession, temporaryModel.getLocationURI().toString());
        
        // substitute the old ODM file with the newly create
        originalODMModel.delete(true, null);
        temporaryModel.move(originalODMModel.getFullPath(), true, null);

      } else {
        // originalODMModel doesn't exist
        // it's needed to create it to maintain the info for the next import
        // so,, move the newly created ODM model to the default location
        temporaryModel.move(originalODMModel.getFullPath(), true, null);

      }
      return Status.OK_STATUS;
    } catch (CoreException | IOException | OBOParseException | DataAdapterException e) {
      throw new ExecutionException(e.getLocalizedMessage());
    }

  }

  @Override
  public IStatus redo(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public IStatus undo(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
    // TODO Auto-generated method stub
    return null;
  }

  protected List<EObject> getModelElementsFromIResource(IResource resource) {
    // Register the XMI resource factory for the .xmi extension
    Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
    Map<String, Object> m = reg.getExtensionToFactoryMap();
    m.put(resource.getFileExtension(), new XMIResourceFactoryImpl());

    // Obtain a new resource set
    ResourceSet resSet = new ResourceSetImpl();

    // Get the resource
    Resource file = resSet.getResource(URI.createURI(resource.getLocationURI().toString()), true);

    // Get the first model element and cast it to the right type, in my
    // example everything is hierarchical included in this first node
    return file.getContents();

  }

}
