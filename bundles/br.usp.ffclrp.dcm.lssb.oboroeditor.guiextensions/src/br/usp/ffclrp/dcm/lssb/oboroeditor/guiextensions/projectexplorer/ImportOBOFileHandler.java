package br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.projectexplorer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.IOperationHistory;
import org.eclipse.core.commands.operations.IUndoableOperation;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

import br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.builder.ProjectPropertiesManager;
import br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.builder.PropertiesKeyStrings;
import br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.merging.DesiredExtensionViewerFilter;
import br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.merging.OBOFiles2ODMModelInjectionOperation;
import br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.merging.OBOROUMLModelMergingOperation;
import br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.merging.ODMModel2UMLModelTransformationOperation;
import br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.merging.OriginalODMModelUpdater;


// TODO better handling of elements with same id
public class ImportOBOFileHandler extends AbstractHandler {
  private IFile temporaryUMLModel;
  private IFile originalODMModel;
  private IFile temporaryODMModel;
  private List<IFile> importingOBOFiles;
  private IFile mainUMLModel;
  
  /**
   * Asks the user for ontology files to import. Then, transforms then into ODM models and in an
   * OBO-RO model, adding the generated models to the current OBO-RO model.
   * This handler MUST be executed as a context menu for the project explorer view AND
   * after the user selected an IPROJECT!
   */
  @Override
  public Object execute(ExecutionEvent event) throws ExecutionException {

    IOperationHistory operationHistory = OperationHistoryFactory.getOperationHistory();


    // get the selected project and the project information
    IProject project = null;
    IStructuredSelection selection =
        (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
    Object firstElement = selection.getFirstElement();

    if(firstElement instanceof IProject) {
      project = (IProject) firstElement;
    } else {
      return null;
    }
        
    // get the property manager
    ProjectPropertiesManager propertyManager = new ProjectPropertiesManager(project);

    String uuid = java.util.UUID.randomUUID().toString();
    originalODMModel = project.getFile(propertyManager.getProperty(PropertiesKeyStrings.IMPORTED_ODM_MODEL));
    mainUMLModel = project.getFile(propertyManager.getProperty(PropertiesKeyStrings.MAIN_MODEL_KEY));;
    temporaryODMModel = project.getFile(uuid + ".obodatamodel");
    project.getFile(uuid + "-new.obodatamodel");;
    temporaryUMLModel = project.getFile(uuid + ".uml");
    

    // asks user for desired files
    System.err.println("Asking for files");
    importingOBOFiles = this.getOntologyFilesToImport();

    // if no file found, return
    if (importingOBOFiles.isEmpty()){ return null;}
    
    String label =  "importing: " + importingOBOFiles.stream()
        .map(IFile::getName)
        .collect(Collectors.joining(","));
       
    System.err.println("Starting operations");
    
    IUndoableOperation oboFiles2TempODMOperation = 
        new OBOFiles2ODMModelInjectionOperation(label, importingOBOFiles, temporaryODMModel);
    operationHistory.execute(oboFiles2TempODMOperation, null, null);
        
    IUndoableOperation tempODM2TempOBOROOperation = 
        new ODMModel2UMLModelTransformationOperation(label, temporaryODMModel, temporaryUMLModel);
    operationHistory.execute(tempODM2TempOBOROOperation, null, null);
    
    IUndoableOperation originalODMUpdateOperation = 
        new OriginalODMModelUpdater(label, originalODMModel, this.importingOBOFiles, this.temporaryODMModel);
    operationHistory.execute(originalODMUpdateOperation, null, null);
   
    IUndoableOperation mergeOBOROOperation = 
        new OBOROUMLModelMergingOperation(label, temporaryUMLModel, mainUMLModel);
    operationHistory.execute(mergeOBOROOperation, null, null);
    
    return null;
  }



  

  protected List<EObject> getModelElementsFromIResource(IResource resource) {
    // Register the XMI resource factory for the .xmi extension
    Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
    Map<String, Object> m = reg.getExtensionToFactoryMap();
    m.put(resource.getFileExtension(), new XMIResourceFactoryImpl());

    // Obtain a new resource set
    ResourceSet resSet = new ResourceSetImpl();

    // Get the resource
    Resource file = resSet.getResource(URI.createURI(resource.getLocationURI().toString()), true);

    // Get the first model element and cast it to the right type, in my
    // example everything is hierarchical included in this first node
    return file.getContents();

  }



  /**
   * Asks the user to choose ontology files.
   * 
   * @return a list of IFiles containing chosen files. An empty list if the operation was canceled
   *         by user.
   */
  protected List<IFile> getOntologyFilesToImport() {
    String dialogMessage = "Choose an ontology file:";
    final String[] desiredFileExtensions = new String[] {"obo",};

    // get elements to show the dialog correctly
    IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
    Assert.isNotNull(window);
    Shell shell = window.getShell();

    // Show dialog to get the IResources
    ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(shell,
        new WorkbenchLabelProvider(), new BaseWorkbenchContentProvider());
    dialog.setInput(ResourcesPlugin.getWorkspace().getRoot());
    dialog.setAllowMultiple(true);
    dialog.setMessage(dialogMessage);
    dialog.addFilter(new DesiredExtensionViewerFilter(desiredFileExtensions));
    
    // get the user selection, return a empty list if user canceled the dialog
    List<IFile> list = new ArrayList<IFile>();
    if (dialog.open() != Window.OK) {
      list = Collections.emptyList();
    } else {
      // cast instances and return list
      Object[] objects = dialog.getResult();
      IFile[] iFiles = (IFile[]) Arrays.asList(objects).toArray(new IFile[objects.length]);
      list.addAll(Arrays.asList(iFiles));
    }
    
    return list;
  }


}
