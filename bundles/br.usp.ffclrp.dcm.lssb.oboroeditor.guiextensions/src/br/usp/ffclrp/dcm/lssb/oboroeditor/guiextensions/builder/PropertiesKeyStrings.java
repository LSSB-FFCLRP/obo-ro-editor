package br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.builder;

public class PropertiesKeyStrings {

	/**
	 * The configuration file for the ontology build in the workspace.
	 */
	public static final String ONTOLOGY_BUILD_PROPERTIES_FILE_PATH = 
			 "ontology_build.properties";
	// property keys
	public static final String IMPORTED_ONTOLOGIES_DIRECTORY_KEY =  
	    "imported_ontologies_directory";
	public static final String BUILD_DIRECTORY_KEY =  
	    "build_directory";
    public static final String PROJECT_RESOURCE_DIRECTORY_KEY =  
        "resources_directory";
    public static final String ROUND_TRIP_KEY =  
        "is_round_trip";
    public static final String MAIN_MODEL_KEY = 
        "main_uml_model";
    public static final String INITIAL_EXPORTATION_ODM_KEY = 
        "initial_exportation_odm_model";
    public static final String FINAL_EXPORTATION_ODM_KEY = 
        "final_exportation_odm_model";
    public static final String EXPORTED_OBO_FILE_KEY = 
        "exported_obo_file";

    public static final String IMPORTED_ODM_MODEL = 
        "imported_odm_model";
    
    
}
