package br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.merging;

import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;

import br.usp.ffclrp.dcm.lssb.oboroeditor.transformations.TransformationRunner;

public class ODMModel2UMLModelTransformationOperation extends AbstractOperation {

  private IFile inputODMModel;
  private IFile outputUMLModel;

  public ODMModel2UMLModelTransformationOperation(String label, IFile inputODMModel,
      IFile outputOBOROModel) {
    super(label);

    this.inputODMModel = inputODMModel;
    this.outputUMLModel = outputOBOROModel;
  }

  @Override
  public IStatus execute(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {

    try {

      InputStream odmSource = inputODMModel.getContents();
      PipedInputStream pipedIS = new PipedInputStream();
      PipedOutputStream pipedOS = new PipedOutputStream(pipedIS);


      Thread t = new Thread(new Runnable() {
        @Override
        public void run() {
          TransformationRunner.transformOboRoOdm2UmlFile(odmSource, pipedOS);
        }
      });

      t.start();
      if (outputUMLModel.exists()) {
        outputUMLModel.setContents(pipedIS, true, true, monitor);
      } else {
        outputUMLModel.create(pipedIS, true, monitor);
      }

    } catch (CoreException | IOException e) {
      throw new ExecutionException(e.getLocalizedMessage());
    }

    return Status.OK_STATUS;
  }

  @Override
  public IStatus redo(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public IStatus undo(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
    // TODO Auto-generated method stub
    return null;
  }

}
