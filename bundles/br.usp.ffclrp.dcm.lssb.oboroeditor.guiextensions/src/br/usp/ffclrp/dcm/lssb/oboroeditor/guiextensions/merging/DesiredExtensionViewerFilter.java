package br.usp.ffclrp.dcm.lssb.oboroeditor.guiextensions.merging;

import java.util.Arrays;

import org.eclipse.core.resources.IResource;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

public class DesiredExtensionViewerFilter extends ViewerFilter {
  private String[] filterExtensions;

  public DesiredExtensionViewerFilter(String[] filterExtensions) {
    super();
    this.filterExtensions = filterExtensions;
  }

  // filtro para os tipos de arquivos desejados
  @Override
  public boolean select(Viewer viewer, Object parentElement, Object element) {

    IResource resource = (IResource) element;
    String fileExtension = resource.getFileExtension();
    return (fileExtension != null && Arrays.asList(filterExtensions).contains(fileExtension))
        || resource.getType() == IResource.FOLDER || resource.getType() == IResource.PROJECT;
  }
}