package br.usp.ffclrp.dcm.lssb.oboroeditor.papyrus.commands;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
//import org.eclipse.papyrus.uml.diagram.common.commands.CreateUMLModelCommand;
import org.eclipse.papyrus.uml.tools.utils.PackageUtil;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.UMLFactory;

import org.eclipse.papyrus.uml.diagram.common.commands.ModelCreationCommandBase;
import br.usp.ffclrp.dcm.lssb.uml.profiles.obodatamodel.OboDatamodelProfileUtil;


public class CreateOboRoModelCommand extends  ModelCreationCommandBase{

  /**
   * @see org.eclipse.papyrus.infra.core.extension.commands.ModelCreationCommandBase#createRootElement()
   *
   * @return
   */

  @Override
  protected EObject createRootElement() {
      return UMLFactory.eINSTANCE.createModel();
  }

  /**
   * A standard OboDatamodel model should have :
   *  - the OboDatamodel profile applied
   *  
   * @see org.eclipse.papyrus.infra.core.extension.commands.ModelCreationCommandBase#initializeModel(org.eclipse.emf.ecore.EObject)
   *
   * @param owner
   */

  @Override
  protected void initializeModel(EObject owner) {
      super.initializeModel(owner);
      Package packageOwner = (Package) owner;
      // Retrieve Library profile and apply it
      Profile oboDatamodelProfile = (Profile) PackageUtil.loadPackage(URI.createURI(OboDatamodelProfileUtil.PROFILE_PATH), owner.eResource().getResourceSet());
      if (oboDatamodelProfile != null) {
          PackageUtil.applyProfile(packageOwner, oboDatamodelProfile, true);
      }
  }
  
  
}
