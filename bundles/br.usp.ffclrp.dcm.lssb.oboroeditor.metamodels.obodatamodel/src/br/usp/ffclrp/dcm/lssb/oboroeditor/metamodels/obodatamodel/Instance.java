/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Instance#getOboPropertiesValues <em>Obo Properties Values</em>}</li>
 * </ul>
 *
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getInstance()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='TypeMustBeClass'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot TypeMustBeClass='self.type.oclIsKindOf(OBOClass)'"
 * @generated
 */
public interface Instance extends OBOObject {
  /**
   * Returns the value of the '<em><b>Obo Properties Values</b></em>' containment reference list.
   * The list contents are of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OboPropertyValue}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Obo Properties Values</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Obo Properties Values</em>' containment reference list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getInstance_OboPropertiesValues()
   * @model containment="true" ordered="false"
   * @generated
   */
  EList<OboPropertyValue> getOboPropertiesValues();

} // Instance
