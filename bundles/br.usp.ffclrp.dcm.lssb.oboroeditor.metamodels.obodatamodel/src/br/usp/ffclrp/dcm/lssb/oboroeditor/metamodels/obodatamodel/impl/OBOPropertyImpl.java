/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ListOfProperties;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Type;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>OBO Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl#isCyclic <em>Cyclic</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl#isSymmetric <em>Symmetric</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl#isTransitive <em>Transitive</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl#isReflexive <em>Reflexive</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl#isAlwaysImpliesInverse <em>Always Implies Inverse</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl#isDummy <em>Dummy</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl#isMetadataTag <em>Metadata Tag</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl#getCyclicExtension <em>Cyclic Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl#getSymmetricExtension <em>Symmetric Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl#getTransitiveExtension <em>Transitive Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl#getReflexiveExtension <em>Reflexive Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl#getAlwaysImpliesInverseExtension <em>Always Implies Inverse Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl#getDomainExtension <em>Domain Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl#getRangeExtension <em>Range Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl#getRange <em>Range</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl#getDomain <em>Domain</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl#isNonInheritable <em>Non Inheritable</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl#getDisjointOver <em>Disjoint Over</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl#getTransitiveOver <em>Transitive Over</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl#getHoldsOverChains <em>Holds Over Chains</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OBOPropertyImpl extends OBOObjectImpl implements OBOProperty {
  /**
   * The default value of the '{@link #isCyclic() <em>Cyclic</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isCyclic()
   * @generated
   * @ordered
   */
  protected static final boolean CYCLIC_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isCyclic() <em>Cyclic</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isCyclic()
   * @generated
   * @ordered
   */
  protected boolean cyclic = CYCLIC_EDEFAULT;

  /**
   * The default value of the '{@link #isSymmetric() <em>Symmetric</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSymmetric()
   * @generated
   * @ordered
   */
  protected static final boolean SYMMETRIC_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isSymmetric() <em>Symmetric</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSymmetric()
   * @generated
   * @ordered
   */
  protected boolean symmetric = SYMMETRIC_EDEFAULT;

  /**
   * The default value of the '{@link #isTransitive() <em>Transitive</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isTransitive()
   * @generated
   * @ordered
   */
  protected static final boolean TRANSITIVE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isTransitive() <em>Transitive</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isTransitive()
   * @generated
   * @ordered
   */
  protected boolean transitive = TRANSITIVE_EDEFAULT;

  /**
   * The default value of the '{@link #isReflexive() <em>Reflexive</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isReflexive()
   * @generated
   * @ordered
   */
  protected static final boolean REFLEXIVE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isReflexive() <em>Reflexive</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isReflexive()
   * @generated
   * @ordered
   */
  protected boolean reflexive = REFLEXIVE_EDEFAULT;

  /**
   * The default value of the '{@link #isAlwaysImpliesInverse() <em>Always Implies Inverse</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isAlwaysImpliesInverse()
   * @generated
   * @ordered
   */
  protected static final boolean ALWAYS_IMPLIES_INVERSE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isAlwaysImpliesInverse() <em>Always Implies Inverse</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isAlwaysImpliesInverse()
   * @generated
   * @ordered
   */
  protected boolean alwaysImpliesInverse = ALWAYS_IMPLIES_INVERSE_EDEFAULT;

  /**
   * The default value of the '{@link #isDummy() <em>Dummy</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isDummy()
   * @generated
   * @ordered
   */
  protected static final boolean DUMMY_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isDummy() <em>Dummy</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isDummy()
   * @generated
   * @ordered
   */
  protected boolean dummy = DUMMY_EDEFAULT;

  /**
   * The default value of the '{@link #isMetadataTag() <em>Metadata Tag</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isMetadataTag()
   * @generated
   * @ordered
   */
  protected static final boolean METADATA_TAG_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isMetadataTag() <em>Metadata Tag</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isMetadataTag()
   * @generated
   * @ordered
   */
  protected boolean metadataTag = METADATA_TAG_EDEFAULT;

  /**
   * The cached value of the '{@link #getCyclicExtension() <em>Cyclic Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCyclicExtension()
   * @generated
   * @ordered
   */
  protected NestedValue cyclicExtension;

  /**
   * The cached value of the '{@link #getSymmetricExtension() <em>Symmetric Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSymmetricExtension()
   * @generated
   * @ordered
   */
  protected NestedValue symmetricExtension;

  /**
   * The cached value of the '{@link #getTransitiveExtension() <em>Transitive Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTransitiveExtension()
   * @generated
   * @ordered
   */
  protected NestedValue transitiveExtension;

  /**
   * The cached value of the '{@link #getReflexiveExtension() <em>Reflexive Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReflexiveExtension()
   * @generated
   * @ordered
   */
  protected NestedValue reflexiveExtension;

  /**
   * The cached value of the '{@link #getAlwaysImpliesInverseExtension() <em>Always Implies Inverse Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAlwaysImpliesInverseExtension()
   * @generated
   * @ordered
   */
  protected NestedValue alwaysImpliesInverseExtension;

  /**
   * The cached value of the '{@link #getDomainExtension() <em>Domain Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDomainExtension()
   * @generated
   * @ordered
   */
  protected NestedValue domainExtension;

  /**
   * The cached value of the '{@link #getRangeExtension() <em>Range Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRangeExtension()
   * @generated
   * @ordered
   */
  protected NestedValue rangeExtension;

  /**
   * The cached value of the '{@link #getRange() <em>Range</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRange()
   * @generated
   * @ordered
   */
  protected Type range;

  /**
   * The cached value of the '{@link #getDomain() <em>Domain</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDomain()
   * @generated
   * @ordered
   */
  protected Type domain;

  /**
   * The default value of the '{@link #isNonInheritable() <em>Non Inheritable</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isNonInheritable()
   * @generated
   * @ordered
   */
  protected static final boolean NON_INHERITABLE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isNonInheritable() <em>Non Inheritable</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isNonInheritable()
   * @generated
   * @ordered
   */
  protected boolean nonInheritable = NON_INHERITABLE_EDEFAULT;

  /**
   * The cached value of the '{@link #getDisjointOver() <em>Disjoint Over</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDisjointOver()
   * @generated
   * @ordered
   */
  protected OBOProperty disjointOver;

  /**
   * The cached value of the '{@link #getTransitiveOver() <em>Transitive Over</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTransitiveOver()
   * @generated
   * @ordered
   */
  protected OBOProperty transitiveOver;

  /**
   * The cached value of the '{@link #getHoldsOverChains() <em>Holds Over Chains</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHoldsOverChains()
   * @generated
   * @ordered
   */
  protected EList<ListOfProperties> holdsOverChains;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OBOPropertyImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.OBO_PROPERTY;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isCyclic() {
    return cyclic;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCyclic(boolean newCyclic) {
    boolean oldCyclic = cyclic;
    cyclic = newCyclic;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__CYCLIC, oldCyclic, cyclic));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSymmetric() {
    return symmetric;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSymmetric(boolean newSymmetric) {
    boolean oldSymmetric = symmetric;
    symmetric = newSymmetric;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__SYMMETRIC, oldSymmetric, symmetric));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isTransitive() {
    return transitive;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTransitive(boolean newTransitive) {
    boolean oldTransitive = transitive;
    transitive = newTransitive;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__TRANSITIVE, oldTransitive, transitive));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isReflexive() {
    return reflexive;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReflexive(boolean newReflexive) {
    boolean oldReflexive = reflexive;
    reflexive = newReflexive;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__REFLEXIVE, oldReflexive, reflexive));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isAlwaysImpliesInverse() {
    return alwaysImpliesInverse;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAlwaysImpliesInverse(boolean newAlwaysImpliesInverse) {
    boolean oldAlwaysImpliesInverse = alwaysImpliesInverse;
    alwaysImpliesInverse = newAlwaysImpliesInverse;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE, oldAlwaysImpliesInverse, alwaysImpliesInverse));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isDummy() {
    return dummy;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDummy(boolean newDummy) {
    boolean oldDummy = dummy;
    dummy = newDummy;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__DUMMY, oldDummy, dummy));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isMetadataTag() {
    return metadataTag;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMetadataTag(boolean newMetadataTag) {
    boolean oldMetadataTag = metadataTag;
    metadataTag = newMetadataTag;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__METADATA_TAG, oldMetadataTag, metadataTag));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getCyclicExtension() {
    return cyclicExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCyclicExtension(NestedValue newCyclicExtension, NotificationChain msgs) {
    NestedValue oldCyclicExtension = cyclicExtension;
    cyclicExtension = newCyclicExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__CYCLIC_EXTENSION, oldCyclicExtension, newCyclicExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCyclicExtension(NestedValue newCyclicExtension) {
    if (newCyclicExtension != cyclicExtension) {
      NotificationChain msgs = null;
      if (cyclicExtension != null)
        msgs = ((InternalEObject)cyclicExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.OBO_PROPERTY__CYCLIC_EXTENSION, null, msgs);
      if (newCyclicExtension != null)
        msgs = ((InternalEObject)newCyclicExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.OBO_PROPERTY__CYCLIC_EXTENSION, null, msgs);
      msgs = basicSetCyclicExtension(newCyclicExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__CYCLIC_EXTENSION, newCyclicExtension, newCyclicExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getSymmetricExtension() {
    return symmetricExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSymmetricExtension(NestedValue newSymmetricExtension, NotificationChain msgs) {
    NestedValue oldSymmetricExtension = symmetricExtension;
    symmetricExtension = newSymmetricExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__SYMMETRIC_EXTENSION, oldSymmetricExtension, newSymmetricExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSymmetricExtension(NestedValue newSymmetricExtension) {
    if (newSymmetricExtension != symmetricExtension) {
      NotificationChain msgs = null;
      if (symmetricExtension != null)
        msgs = ((InternalEObject)symmetricExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.OBO_PROPERTY__SYMMETRIC_EXTENSION, null, msgs);
      if (newSymmetricExtension != null)
        msgs = ((InternalEObject)newSymmetricExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.OBO_PROPERTY__SYMMETRIC_EXTENSION, null, msgs);
      msgs = basicSetSymmetricExtension(newSymmetricExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__SYMMETRIC_EXTENSION, newSymmetricExtension, newSymmetricExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getTransitiveExtension() {
    return transitiveExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTransitiveExtension(NestedValue newTransitiveExtension, NotificationChain msgs) {
    NestedValue oldTransitiveExtension = transitiveExtension;
    transitiveExtension = newTransitiveExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__TRANSITIVE_EXTENSION, oldTransitiveExtension, newTransitiveExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTransitiveExtension(NestedValue newTransitiveExtension) {
    if (newTransitiveExtension != transitiveExtension) {
      NotificationChain msgs = null;
      if (transitiveExtension != null)
        msgs = ((InternalEObject)transitiveExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.OBO_PROPERTY__TRANSITIVE_EXTENSION, null, msgs);
      if (newTransitiveExtension != null)
        msgs = ((InternalEObject)newTransitiveExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.OBO_PROPERTY__TRANSITIVE_EXTENSION, null, msgs);
      msgs = basicSetTransitiveExtension(newTransitiveExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__TRANSITIVE_EXTENSION, newTransitiveExtension, newTransitiveExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getReflexiveExtension() {
    return reflexiveExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetReflexiveExtension(NestedValue newReflexiveExtension, NotificationChain msgs) {
    NestedValue oldReflexiveExtension = reflexiveExtension;
    reflexiveExtension = newReflexiveExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__REFLEXIVE_EXTENSION, oldReflexiveExtension, newReflexiveExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReflexiveExtension(NestedValue newReflexiveExtension) {
    if (newReflexiveExtension != reflexiveExtension) {
      NotificationChain msgs = null;
      if (reflexiveExtension != null)
        msgs = ((InternalEObject)reflexiveExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.OBO_PROPERTY__REFLEXIVE_EXTENSION, null, msgs);
      if (newReflexiveExtension != null)
        msgs = ((InternalEObject)newReflexiveExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.OBO_PROPERTY__REFLEXIVE_EXTENSION, null, msgs);
      msgs = basicSetReflexiveExtension(newReflexiveExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__REFLEXIVE_EXTENSION, newReflexiveExtension, newReflexiveExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getAlwaysImpliesInverseExtension() {
    return alwaysImpliesInverseExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAlwaysImpliesInverseExtension(NestedValue newAlwaysImpliesInverseExtension, NotificationChain msgs) {
    NestedValue oldAlwaysImpliesInverseExtension = alwaysImpliesInverseExtension;
    alwaysImpliesInverseExtension = newAlwaysImpliesInverseExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION, oldAlwaysImpliesInverseExtension, newAlwaysImpliesInverseExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAlwaysImpliesInverseExtension(NestedValue newAlwaysImpliesInverseExtension) {
    if (newAlwaysImpliesInverseExtension != alwaysImpliesInverseExtension) {
      NotificationChain msgs = null;
      if (alwaysImpliesInverseExtension != null)
        msgs = ((InternalEObject)alwaysImpliesInverseExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION, null, msgs);
      if (newAlwaysImpliesInverseExtension != null)
        msgs = ((InternalEObject)newAlwaysImpliesInverseExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION, null, msgs);
      msgs = basicSetAlwaysImpliesInverseExtension(newAlwaysImpliesInverseExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION, newAlwaysImpliesInverseExtension, newAlwaysImpliesInverseExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getDomainExtension() {
    return domainExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDomainExtension(NestedValue newDomainExtension, NotificationChain msgs) {
    NestedValue oldDomainExtension = domainExtension;
    domainExtension = newDomainExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__DOMAIN_EXTENSION, oldDomainExtension, newDomainExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDomainExtension(NestedValue newDomainExtension) {
    if (newDomainExtension != domainExtension) {
      NotificationChain msgs = null;
      if (domainExtension != null)
        msgs = ((InternalEObject)domainExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.OBO_PROPERTY__DOMAIN_EXTENSION, null, msgs);
      if (newDomainExtension != null)
        msgs = ((InternalEObject)newDomainExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.OBO_PROPERTY__DOMAIN_EXTENSION, null, msgs);
      msgs = basicSetDomainExtension(newDomainExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__DOMAIN_EXTENSION, newDomainExtension, newDomainExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getRangeExtension() {
    return rangeExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRangeExtension(NestedValue newRangeExtension, NotificationChain msgs) {
    NestedValue oldRangeExtension = rangeExtension;
    rangeExtension = newRangeExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__RANGE_EXTENSION, oldRangeExtension, newRangeExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRangeExtension(NestedValue newRangeExtension) {
    if (newRangeExtension != rangeExtension) {
      NotificationChain msgs = null;
      if (rangeExtension != null)
        msgs = ((InternalEObject)rangeExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.OBO_PROPERTY__RANGE_EXTENSION, null, msgs);
      if (newRangeExtension != null)
        msgs = ((InternalEObject)newRangeExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.OBO_PROPERTY__RANGE_EXTENSION, null, msgs);
      msgs = basicSetRangeExtension(newRangeExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__RANGE_EXTENSION, newRangeExtension, newRangeExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type getRange() {
    if (range != null && range.eIsProxy()) {
      InternalEObject oldRange = (InternalEObject)range;
      range = (Type)eResolveProxy(oldRange);
      if (range != oldRange) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, obodatamodelPackage.OBO_PROPERTY__RANGE, oldRange, range));
      }
    }
    return range;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type basicGetRange() {
    return range;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRange(Type newRange) {
    Type oldRange = range;
    range = newRange;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__RANGE, oldRange, range));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type getDomain() {
    if (domain != null && domain.eIsProxy()) {
      InternalEObject oldDomain = (InternalEObject)domain;
      domain = (Type)eResolveProxy(oldDomain);
      if (domain != oldDomain) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, obodatamodelPackage.OBO_PROPERTY__DOMAIN, oldDomain, domain));
      }
    }
    return domain;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type basicGetDomain() {
    return domain;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDomain(Type newDomain) {
    Type oldDomain = domain;
    domain = newDomain;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__DOMAIN, oldDomain, domain));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isNonInheritable() {
    return nonInheritable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNonInheritable(boolean newNonInheritable) {
    boolean oldNonInheritable = nonInheritable;
    nonInheritable = newNonInheritable;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__NON_INHERITABLE, oldNonInheritable, nonInheritable));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBOProperty getDisjointOver() {
    if (disjointOver != null && disjointOver.eIsProxy()) {
      InternalEObject oldDisjointOver = (InternalEObject)disjointOver;
      disjointOver = (OBOProperty)eResolveProxy(oldDisjointOver);
      if (disjointOver != oldDisjointOver) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, obodatamodelPackage.OBO_PROPERTY__DISJOINT_OVER, oldDisjointOver, disjointOver));
      }
    }
    return disjointOver;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBOProperty basicGetDisjointOver() {
    return disjointOver;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDisjointOver(OBOProperty newDisjointOver) {
    OBOProperty oldDisjointOver = disjointOver;
    disjointOver = newDisjointOver;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__DISJOINT_OVER, oldDisjointOver, disjointOver));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBOProperty getTransitiveOver() {
    if (transitiveOver != null && transitiveOver.eIsProxy()) {
      InternalEObject oldTransitiveOver = (InternalEObject)transitiveOver;
      transitiveOver = (OBOProperty)eResolveProxy(oldTransitiveOver);
      if (transitiveOver != oldTransitiveOver) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, obodatamodelPackage.OBO_PROPERTY__TRANSITIVE_OVER, oldTransitiveOver, transitiveOver));
      }
    }
    return transitiveOver;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBOProperty basicGetTransitiveOver() {
    return transitiveOver;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTransitiveOver(OBOProperty newTransitiveOver) {
    OBOProperty oldTransitiveOver = transitiveOver;
    transitiveOver = newTransitiveOver;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_PROPERTY__TRANSITIVE_OVER, oldTransitiveOver, transitiveOver));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ListOfProperties> getHoldsOverChains() {
    if (holdsOverChains == null) {
      holdsOverChains = new EObjectContainmentEList<ListOfProperties>(ListOfProperties.class, this, obodatamodelPackage.OBO_PROPERTY__HOLDS_OVER_CHAINS);
    }
    return holdsOverChains;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.OBO_PROPERTY__CYCLIC_EXTENSION:
        return basicSetCyclicExtension(null, msgs);
      case obodatamodelPackage.OBO_PROPERTY__SYMMETRIC_EXTENSION:
        return basicSetSymmetricExtension(null, msgs);
      case obodatamodelPackage.OBO_PROPERTY__TRANSITIVE_EXTENSION:
        return basicSetTransitiveExtension(null, msgs);
      case obodatamodelPackage.OBO_PROPERTY__REFLEXIVE_EXTENSION:
        return basicSetReflexiveExtension(null, msgs);
      case obodatamodelPackage.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION:
        return basicSetAlwaysImpliesInverseExtension(null, msgs);
      case obodatamodelPackage.OBO_PROPERTY__DOMAIN_EXTENSION:
        return basicSetDomainExtension(null, msgs);
      case obodatamodelPackage.OBO_PROPERTY__RANGE_EXTENSION:
        return basicSetRangeExtension(null, msgs);
      case obodatamodelPackage.OBO_PROPERTY__HOLDS_OVER_CHAINS:
        return ((InternalEList<?>)getHoldsOverChains()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.OBO_PROPERTY__CYCLIC:
        return isCyclic();
      case obodatamodelPackage.OBO_PROPERTY__SYMMETRIC:
        return isSymmetric();
      case obodatamodelPackage.OBO_PROPERTY__TRANSITIVE:
        return isTransitive();
      case obodatamodelPackage.OBO_PROPERTY__REFLEXIVE:
        return isReflexive();
      case obodatamodelPackage.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE:
        return isAlwaysImpliesInverse();
      case obodatamodelPackage.OBO_PROPERTY__DUMMY:
        return isDummy();
      case obodatamodelPackage.OBO_PROPERTY__METADATA_TAG:
        return isMetadataTag();
      case obodatamodelPackage.OBO_PROPERTY__CYCLIC_EXTENSION:
        return getCyclicExtension();
      case obodatamodelPackage.OBO_PROPERTY__SYMMETRIC_EXTENSION:
        return getSymmetricExtension();
      case obodatamodelPackage.OBO_PROPERTY__TRANSITIVE_EXTENSION:
        return getTransitiveExtension();
      case obodatamodelPackage.OBO_PROPERTY__REFLEXIVE_EXTENSION:
        return getReflexiveExtension();
      case obodatamodelPackage.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION:
        return getAlwaysImpliesInverseExtension();
      case obodatamodelPackage.OBO_PROPERTY__DOMAIN_EXTENSION:
        return getDomainExtension();
      case obodatamodelPackage.OBO_PROPERTY__RANGE_EXTENSION:
        return getRangeExtension();
      case obodatamodelPackage.OBO_PROPERTY__RANGE:
        if (resolve) return getRange();
        return basicGetRange();
      case obodatamodelPackage.OBO_PROPERTY__DOMAIN:
        if (resolve) return getDomain();
        return basicGetDomain();
      case obodatamodelPackage.OBO_PROPERTY__NON_INHERITABLE:
        return isNonInheritable();
      case obodatamodelPackage.OBO_PROPERTY__DISJOINT_OVER:
        if (resolve) return getDisjointOver();
        return basicGetDisjointOver();
      case obodatamodelPackage.OBO_PROPERTY__TRANSITIVE_OVER:
        if (resolve) return getTransitiveOver();
        return basicGetTransitiveOver();
      case obodatamodelPackage.OBO_PROPERTY__HOLDS_OVER_CHAINS:
        return getHoldsOverChains();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.OBO_PROPERTY__CYCLIC:
        setCyclic((Boolean)newValue);
        return;
      case obodatamodelPackage.OBO_PROPERTY__SYMMETRIC:
        setSymmetric((Boolean)newValue);
        return;
      case obodatamodelPackage.OBO_PROPERTY__TRANSITIVE:
        setTransitive((Boolean)newValue);
        return;
      case obodatamodelPackage.OBO_PROPERTY__REFLEXIVE:
        setReflexive((Boolean)newValue);
        return;
      case obodatamodelPackage.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE:
        setAlwaysImpliesInverse((Boolean)newValue);
        return;
      case obodatamodelPackage.OBO_PROPERTY__DUMMY:
        setDummy((Boolean)newValue);
        return;
      case obodatamodelPackage.OBO_PROPERTY__METADATA_TAG:
        setMetadataTag((Boolean)newValue);
        return;
      case obodatamodelPackage.OBO_PROPERTY__CYCLIC_EXTENSION:
        setCyclicExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.OBO_PROPERTY__SYMMETRIC_EXTENSION:
        setSymmetricExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.OBO_PROPERTY__TRANSITIVE_EXTENSION:
        setTransitiveExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.OBO_PROPERTY__REFLEXIVE_EXTENSION:
        setReflexiveExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION:
        setAlwaysImpliesInverseExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.OBO_PROPERTY__DOMAIN_EXTENSION:
        setDomainExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.OBO_PROPERTY__RANGE_EXTENSION:
        setRangeExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.OBO_PROPERTY__RANGE:
        setRange((Type)newValue);
        return;
      case obodatamodelPackage.OBO_PROPERTY__DOMAIN:
        setDomain((Type)newValue);
        return;
      case obodatamodelPackage.OBO_PROPERTY__NON_INHERITABLE:
        setNonInheritable((Boolean)newValue);
        return;
      case obodatamodelPackage.OBO_PROPERTY__DISJOINT_OVER:
        setDisjointOver((OBOProperty)newValue);
        return;
      case obodatamodelPackage.OBO_PROPERTY__TRANSITIVE_OVER:
        setTransitiveOver((OBOProperty)newValue);
        return;
      case obodatamodelPackage.OBO_PROPERTY__HOLDS_OVER_CHAINS:
        getHoldsOverChains().clear();
        getHoldsOverChains().addAll((Collection<? extends ListOfProperties>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.OBO_PROPERTY__CYCLIC:
        setCyclic(CYCLIC_EDEFAULT);
        return;
      case obodatamodelPackage.OBO_PROPERTY__SYMMETRIC:
        setSymmetric(SYMMETRIC_EDEFAULT);
        return;
      case obodatamodelPackage.OBO_PROPERTY__TRANSITIVE:
        setTransitive(TRANSITIVE_EDEFAULT);
        return;
      case obodatamodelPackage.OBO_PROPERTY__REFLEXIVE:
        setReflexive(REFLEXIVE_EDEFAULT);
        return;
      case obodatamodelPackage.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE:
        setAlwaysImpliesInverse(ALWAYS_IMPLIES_INVERSE_EDEFAULT);
        return;
      case obodatamodelPackage.OBO_PROPERTY__DUMMY:
        setDummy(DUMMY_EDEFAULT);
        return;
      case obodatamodelPackage.OBO_PROPERTY__METADATA_TAG:
        setMetadataTag(METADATA_TAG_EDEFAULT);
        return;
      case obodatamodelPackage.OBO_PROPERTY__CYCLIC_EXTENSION:
        setCyclicExtension((NestedValue)null);
        return;
      case obodatamodelPackage.OBO_PROPERTY__SYMMETRIC_EXTENSION:
        setSymmetricExtension((NestedValue)null);
        return;
      case obodatamodelPackage.OBO_PROPERTY__TRANSITIVE_EXTENSION:
        setTransitiveExtension((NestedValue)null);
        return;
      case obodatamodelPackage.OBO_PROPERTY__REFLEXIVE_EXTENSION:
        setReflexiveExtension((NestedValue)null);
        return;
      case obodatamodelPackage.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION:
        setAlwaysImpliesInverseExtension((NestedValue)null);
        return;
      case obodatamodelPackage.OBO_PROPERTY__DOMAIN_EXTENSION:
        setDomainExtension((NestedValue)null);
        return;
      case obodatamodelPackage.OBO_PROPERTY__RANGE_EXTENSION:
        setRangeExtension((NestedValue)null);
        return;
      case obodatamodelPackage.OBO_PROPERTY__RANGE:
        setRange((Type)null);
        return;
      case obodatamodelPackage.OBO_PROPERTY__DOMAIN:
        setDomain((Type)null);
        return;
      case obodatamodelPackage.OBO_PROPERTY__NON_INHERITABLE:
        setNonInheritable(NON_INHERITABLE_EDEFAULT);
        return;
      case obodatamodelPackage.OBO_PROPERTY__DISJOINT_OVER:
        setDisjointOver((OBOProperty)null);
        return;
      case obodatamodelPackage.OBO_PROPERTY__TRANSITIVE_OVER:
        setTransitiveOver((OBOProperty)null);
        return;
      case obodatamodelPackage.OBO_PROPERTY__HOLDS_OVER_CHAINS:
        getHoldsOverChains().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.OBO_PROPERTY__CYCLIC:
        return cyclic != CYCLIC_EDEFAULT;
      case obodatamodelPackage.OBO_PROPERTY__SYMMETRIC:
        return symmetric != SYMMETRIC_EDEFAULT;
      case obodatamodelPackage.OBO_PROPERTY__TRANSITIVE:
        return transitive != TRANSITIVE_EDEFAULT;
      case obodatamodelPackage.OBO_PROPERTY__REFLEXIVE:
        return reflexive != REFLEXIVE_EDEFAULT;
      case obodatamodelPackage.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE:
        return alwaysImpliesInverse != ALWAYS_IMPLIES_INVERSE_EDEFAULT;
      case obodatamodelPackage.OBO_PROPERTY__DUMMY:
        return dummy != DUMMY_EDEFAULT;
      case obodatamodelPackage.OBO_PROPERTY__METADATA_TAG:
        return metadataTag != METADATA_TAG_EDEFAULT;
      case obodatamodelPackage.OBO_PROPERTY__CYCLIC_EXTENSION:
        return cyclicExtension != null;
      case obodatamodelPackage.OBO_PROPERTY__SYMMETRIC_EXTENSION:
        return symmetricExtension != null;
      case obodatamodelPackage.OBO_PROPERTY__TRANSITIVE_EXTENSION:
        return transitiveExtension != null;
      case obodatamodelPackage.OBO_PROPERTY__REFLEXIVE_EXTENSION:
        return reflexiveExtension != null;
      case obodatamodelPackage.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION:
        return alwaysImpliesInverseExtension != null;
      case obodatamodelPackage.OBO_PROPERTY__DOMAIN_EXTENSION:
        return domainExtension != null;
      case obodatamodelPackage.OBO_PROPERTY__RANGE_EXTENSION:
        return rangeExtension != null;
      case obodatamodelPackage.OBO_PROPERTY__RANGE:
        return range != null;
      case obodatamodelPackage.OBO_PROPERTY__DOMAIN:
        return domain != null;
      case obodatamodelPackage.OBO_PROPERTY__NON_INHERITABLE:
        return nonInheritable != NON_INHERITABLE_EDEFAULT;
      case obodatamodelPackage.OBO_PROPERTY__DISJOINT_OVER:
        return disjointOver != null;
      case obodatamodelPackage.OBO_PROPERTY__TRANSITIVE_OVER:
        return transitiveOver != null;
      case obodatamodelPackage.OBO_PROPERTY__HOLDS_OVER_CHAINS:
        return holdsOverChains != null && !holdsOverChains.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (cyclic: ");
    result.append(cyclic);
    result.append(", symmetric: ");
    result.append(symmetric);
    result.append(", transitive: ");
    result.append(transitive);
    result.append(", reflexive: ");
    result.append(reflexive);
    result.append(", alwaysImpliesInverse: ");
    result.append(alwaysImpliesInverse);
    result.append(", dummy: ");
    result.append(dummy);
    result.append(", metadataTag: ");
    result.append(metadataTag);
    result.append(", nonInheritable: ");
    result.append(nonInheritable);
    result.append(')');
    return result.toString();
  }

} //OBOPropertyImpl
