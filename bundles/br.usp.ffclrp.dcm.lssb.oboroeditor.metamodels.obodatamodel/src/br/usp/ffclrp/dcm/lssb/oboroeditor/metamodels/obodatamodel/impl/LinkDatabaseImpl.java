/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Link Database</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkDatabaseImpl#getSession <em>Session</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkDatabaseImpl#getObjects <em>Objects</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkDatabaseImpl#getProperties <em>Properties</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LinkDatabaseImpl extends IdentifiedObjectIndexImpl implements LinkDatabase {
  /**
   * The cached value of the '{@link #getObjects() <em>Objects</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getObjects()
   * @generated
   * @ordered
   */
  protected EList<IdentifiedObject> objects;

  /**
   * The cached value of the '{@link #getProperties() <em>Properties</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProperties()
   * @generated
   * @ordered
   */
  protected EList<OBOProperty> properties;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected LinkDatabaseImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.LINK_DATABASE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBOSession getSession() {
    if (eContainerFeatureID() != obodatamodelPackage.LINK_DATABASE__SESSION) return null;
    return (OBOSession)eInternalContainer();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSession(OBOSession newSession, NotificationChain msgs) {
    msgs = eBasicSetContainer((InternalEObject)newSession, obodatamodelPackage.LINK_DATABASE__SESSION, msgs);
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSession(OBOSession newSession) {
    if (newSession != eInternalContainer() || (eContainerFeatureID() != obodatamodelPackage.LINK_DATABASE__SESSION && newSession != null)) {
      if (EcoreUtil.isAncestor(this, newSession))
        throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
      NotificationChain msgs = null;
      if (eInternalContainer() != null)
        msgs = eBasicRemoveFromContainer(msgs);
      if (newSession != null)
        msgs = ((InternalEObject)newSession).eInverseAdd(this, obodatamodelPackage.OBO_SESSION__LINK_DATABASE, OBOSession.class, msgs);
      msgs = basicSetSession(newSession, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.LINK_DATABASE__SESSION, newSession, newSession));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<IdentifiedObject> getObjects() {
    if (objects == null) {
      objects = new EObjectResolvingEList<IdentifiedObject>(IdentifiedObject.class, this, obodatamodelPackage.LINK_DATABASE__OBJECTS);
    }
    return objects;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<OBOProperty> getProperties() {
    if (properties == null) {
      properties = new EObjectResolvingEList<OBOProperty>(OBOProperty.class, this, obodatamodelPackage.LINK_DATABASE__PROPERTIES);
    }
    return properties;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.LINK_DATABASE__SESSION:
        if (eInternalContainer() != null)
          msgs = eBasicRemoveFromContainer(msgs);
        return basicSetSession((OBOSession)otherEnd, msgs);
    }
    return super.eInverseAdd(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.LINK_DATABASE__SESSION:
        return basicSetSession(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
    switch (eContainerFeatureID()) {
      case obodatamodelPackage.LINK_DATABASE__SESSION:
        return eInternalContainer().eInverseRemove(this, obodatamodelPackage.OBO_SESSION__LINK_DATABASE, OBOSession.class, msgs);
    }
    return super.eBasicRemoveFromContainerFeature(msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.LINK_DATABASE__SESSION:
        return getSession();
      case obodatamodelPackage.LINK_DATABASE__OBJECTS:
        return getObjects();
      case obodatamodelPackage.LINK_DATABASE__PROPERTIES:
        return getProperties();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.LINK_DATABASE__SESSION:
        setSession((OBOSession)newValue);
        return;
      case obodatamodelPackage.LINK_DATABASE__OBJECTS:
        getObjects().clear();
        getObjects().addAll((Collection<? extends IdentifiedObject>)newValue);
        return;
      case obodatamodelPackage.LINK_DATABASE__PROPERTIES:
        getProperties().clear();
        getProperties().addAll((Collection<? extends OBOProperty>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.LINK_DATABASE__SESSION:
        setSession((OBOSession)null);
        return;
      case obodatamodelPackage.LINK_DATABASE__OBJECTS:
        getObjects().clear();
        return;
      case obodatamodelPackage.LINK_DATABASE__PROPERTIES:
        getProperties().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.LINK_DATABASE__SESSION:
        return getSession() != null;
      case obodatamodelPackage.LINK_DATABASE__OBJECTS:
        return objects != null && !objects.isEmpty();
      case obodatamodelPackage.LINK_DATABASE__PROPERTIES:
        return properties != null && !properties.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //LinkDatabaseImpl
