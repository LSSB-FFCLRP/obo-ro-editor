/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;

import java.math.BigInteger;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OBO Restriction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#getCardinality <em>Cardinality</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#getMaxCardinality <em>Max Cardinality</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#getMinCardinality <em>Min Cardinality</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#isCompletes <em>Completes</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#isInverseCompletes <em>Inverse Completes</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#isNecessarilyTrue <em>Necessarily True</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#isInverseNecessarilyTrue <em>Inverse Necessarily True</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#getAdditionalArguments <em>Additional Arguments</em>}</li>
 * </ul>
 *
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBORestriction()
 * @model
 * @generated
 */
public interface OBORestriction extends Link, br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable, Serializable {
  /**
   * Returns the value of the '<em><b>Cardinality</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Cardinality</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Cardinality</em>' attribute.
   * @see #setCardinality(BigInteger)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBORestriction_Cardinality()
   * @model
   * @generated
   */
  BigInteger getCardinality();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#getCardinality <em>Cardinality</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Cardinality</em>' attribute.
   * @see #getCardinality()
   * @generated
   */
  void setCardinality(BigInteger value);

  /**
   * Returns the value of the '<em><b>Max Cardinality</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Max Cardinality</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Max Cardinality</em>' attribute.
   * @see #setMaxCardinality(BigInteger)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBORestriction_MaxCardinality()
   * @model
   * @generated
   */
  BigInteger getMaxCardinality();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#getMaxCardinality <em>Max Cardinality</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Max Cardinality</em>' attribute.
   * @see #getMaxCardinality()
   * @generated
   */
  void setMaxCardinality(BigInteger value);

  /**
   * Returns the value of the '<em><b>Min Cardinality</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Min Cardinality</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Min Cardinality</em>' attribute.
   * @see #setMinCardinality(BigInteger)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBORestriction_MinCardinality()
   * @model
   * @generated
   */
  BigInteger getMinCardinality();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#getMinCardinality <em>Min Cardinality</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Min Cardinality</em>' attribute.
   * @see #getMinCardinality()
   * @generated
   */
  void setMinCardinality(BigInteger value);

  /**
   * Returns the value of the '<em><b>Completes</b></em>' attribute.
   * The default value is <code>"false"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Completes</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Completes</em>' attribute.
   * @see #setCompletes(boolean)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBORestriction_Completes()
   * @model default="false" required="true"
   * @generated
   */
  boolean isCompletes();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#isCompletes <em>Completes</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Completes</em>' attribute.
   * @see #isCompletes()
   * @generated
   */
  void setCompletes(boolean value);

  /**
   * Returns the value of the '<em><b>Inverse Completes</b></em>' attribute.
   * The default value is <code>"false"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Inverse Completes</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Inverse Completes</em>' attribute.
   * @see #setInverseCompletes(boolean)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBORestriction_InverseCompletes()
   * @model default="false" required="true"
   * @generated
   */
  boolean isInverseCompletes();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#isInverseCompletes <em>Inverse Completes</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Inverse Completes</em>' attribute.
   * @see #isInverseCompletes()
   * @generated
   */
  void setInverseCompletes(boolean value);

  /**
   * Returns the value of the '<em><b>Necessarily True</b></em>' attribute.
   * The default value is <code>"false"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Necessarily True</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Necessarily True</em>' attribute.
   * @see #setNecessarilyTrue(boolean)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBORestriction_NecessarilyTrue()
   * @model default="false" required="true"
   * @generated
   */
  boolean isNecessarilyTrue();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#isNecessarilyTrue <em>Necessarily True</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Necessarily True</em>' attribute.
   * @see #isNecessarilyTrue()
   * @generated
   */
  void setNecessarilyTrue(boolean value);

  /**
   * Returns the value of the '<em><b>Inverse Necessarily True</b></em>' attribute.
   * The default value is <code>"false"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Inverse Necessarily True</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Inverse Necessarily True</em>' attribute.
   * @see #setInverseNecessarilyTrue(boolean)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBORestriction_InverseNecessarilyTrue()
   * @model default="false" required="true"
   * @generated
   */
  boolean isInverseNecessarilyTrue();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#isInverseNecessarilyTrue <em>Inverse Necessarily True</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Inverse Necessarily True</em>' attribute.
   * @see #isInverseNecessarilyTrue()
   * @generated
   */
  void setInverseNecessarilyTrue(boolean value);

  /**
   * Returns the value of the '<em><b>Additional Arguments</b></em>' reference list.
   * The list contents are of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Additional Arguments</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Additional Arguments</em>' reference list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBORestriction_AdditionalArguments()
   * @model ordered="false"
   * @generated
   */
  EList<LinkedObject> getAdditionalArguments();

} // OBORestriction
