/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Defined Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject#getDefinition <em>Definition</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject#getDefDbxrefs <em>Def Dbxrefs</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject#getDefinitionExtension <em>Definition Extension</em>}</li>
 * </ul>
 *
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getDefinedObject()
 * @model abstract="true"
 * @generated
 */
public interface DefinedObject extends IdentifiedObject {
  /**
   * Returns the value of the '<em><b>Definition</b></em>' attribute.
   * The default value is <code>""</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Definition</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Definition</em>' attribute.
   * @see #setDefinition(String)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getDefinedObject_Definition()
   * @model default="" required="true"
   * @generated
   */
  String getDefinition();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject#getDefinition <em>Definition</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Definition</em>' attribute.
   * @see #getDefinition()
   * @generated
   */
  void setDefinition(String value);

  /**
   * Returns the value of the '<em><b>Def Dbxrefs</b></em>' reference list.
   * The list contents are of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Def Dbxrefs</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Def Dbxrefs</em>' reference list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getDefinedObject_DefDbxrefs()
   * @model ordered="false"
   * @generated
   */
  EList<Dbxref> getDefDbxrefs();

  /**
   * Returns the value of the '<em><b>Definition Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Definition Extension</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Definition Extension</em>' containment reference.
   * @see #setDefinitionExtension(NestedValue)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getDefinedObject_DefinitionExtension()
   * @model containment="true"
   * @generated
   */
  NestedValue getDefinitionExtension();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject#getDefinitionExtension <em>Definition Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Definition Extension</em>' containment reference.
   * @see #getDefinitionExtension()
   * @generated
   */
  void setDefinitionExtension(NestedValue value);

} // DefinedObject
