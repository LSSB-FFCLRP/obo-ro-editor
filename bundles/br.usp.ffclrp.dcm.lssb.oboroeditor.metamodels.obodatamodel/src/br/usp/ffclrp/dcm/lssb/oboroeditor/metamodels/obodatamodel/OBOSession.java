/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OBO Session</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getId <em>Id</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getObjects <em>Objects</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getLinkDatabase <em>Link Database</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getDefaultNamespace <em>Default Namespace</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getNamespaces <em>Namespaces</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getPropertyValues <em>Property Values</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getUnknownStanzas <em>Unknown Stanzas</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getSubsets <em>Subsets</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getCategories <em>Categories</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getSynonymTypes <em>Synonym Types</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getSynonymCategories <em>Synonym Categories</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getCurrentFilenames <em>Current Filenames</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getCurrentUser <em>Current User</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getIdSpaces <em>Id Spaces</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getLoadRemark <em>Load Remark</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getAllDbxRefsContainer <em>All Dbx Refs Container</em>}</li>
 * </ul>
 *
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOSession()
 * @model
 * @generated
 */
public interface OBOSession extends IdentifiedObjectIndex, Serializable {
  /**
   * Returns the value of the '<em><b>Id</b></em>' attribute.
   * The default value is <code>"rootSession"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id</em>' attribute.
   * @see #setId(String)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOSession_Id()
   * @model default="rootSession" id="true" required="true"
   * @generated
   */
  String getId();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getId <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id</em>' attribute.
   * @see #getId()
   * @generated
   */
  void setId(String value);

  /**
   * Returns the value of the '<em><b>Objects</b></em>' containment reference list.
   * The list contents are of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Objects</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Objects</em>' containment reference list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOSession_Objects()
   * @model containment="true" ordered="false"
   * @generated
   */
  EList<IdentifiedObject> getObjects();

  /**
   * Returns the value of the '<em><b>Link Database</b></em>' containment reference.
   * It is bidirectional and its opposite is '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase#getSession <em>Session</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Link Database</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Link Database</em>' containment reference.
   * @see #setLinkDatabase(LinkDatabase)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOSession_LinkDatabase()
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase#getSession
   * @model opposite="session" containment="true"
   * @generated
   */
  LinkDatabase getLinkDatabase();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getLinkDatabase <em>Link Database</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Link Database</em>' containment reference.
   * @see #getLinkDatabase()
   * @generated
   */
  void setLinkDatabase(LinkDatabase value);

  /**
   * Returns the value of the '<em><b>Default Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Default Namespace</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Default Namespace</em>' reference.
   * @see #setDefaultNamespace(Namespace)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOSession_DefaultNamespace()
   * @model
   * @generated
   */
  Namespace getDefaultNamespace();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getDefaultNamespace <em>Default Namespace</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Default Namespace</em>' reference.
   * @see #getDefaultNamespace()
   * @generated
   */
  void setDefaultNamespace(Namespace value);

  /**
   * Returns the value of the '<em><b>Namespaces</b></em>' containment reference list.
   * The list contents are of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Namespaces</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Namespaces</em>' containment reference list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOSession_Namespaces()
   * @model containment="true" ordered="false"
   * @generated
   */
  EList<Namespace> getNamespaces();

  /**
   * Returns the value of the '<em><b>Property Values</b></em>' containment reference list.
   * The list contents are of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Property Values</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Property Values</em>' containment reference list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOSession_PropertyValues()
   * @model containment="true" ordered="false"
   * @generated
   */
  EList<PropertyValue> getPropertyValues();

  /**
   * Returns the value of the '<em><b>Unknown Stanzas</b></em>' containment reference list.
   * The list contents are of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Unknown Stanzas</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Unknown Stanzas</em>' containment reference list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOSession_UnknownStanzas()
   * @model containment="true" ordered="false"
   * @generated
   */
  EList<UnknownStanza> getUnknownStanzas();

  /**
   * Returns the value of the '<em><b>Subsets</b></em>' containment reference list.
   * The list contents are of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Subsets</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Subsets</em>' containment reference list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOSession_Subsets()
   * @model containment="true" ordered="false"
   * @generated
   */
  EList<TermSubset> getSubsets();

  /**
   * Returns the value of the '<em><b>Categories</b></em>' containment reference list.
   * The list contents are of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Categories</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Categories</em>' containment reference list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOSession_Categories()
   * @model containment="true" ordered="false"
   * @generated
   */
  EList<TermSubset> getCategories();

  /**
   * Returns the value of the '<em><b>Synonym Types</b></em>' containment reference list.
   * The list contents are of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Synonym Types</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Synonym Types</em>' containment reference list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOSession_SynonymTypes()
   * @model containment="true" ordered="false"
   * @generated
   */
  EList<SynonymType> getSynonymTypes();

  /**
   * Returns the value of the '<em><b>Synonym Categories</b></em>' containment reference list.
   * The list contents are of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Synonym Categories</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Synonym Categories</em>' containment reference list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOSession_SynonymCategories()
   * @model containment="true" ordered="false"
   * @generated
   */
  EList<SynonymType> getSynonymCategories();

  /**
   * Returns the value of the '<em><b>Current Filenames</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Current Filenames</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Current Filenames</em>' attribute list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOSession_CurrentFilenames()
   * @model ordered="false"
   * @generated
   */
  EList<String> getCurrentFilenames();

  /**
   * Returns the value of the '<em><b>Current User</b></em>' attribute.
   * The default value is <code>""</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Current User</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Current User</em>' attribute.
   * @see #setCurrentUser(String)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOSession_CurrentUser()
   * @model default="" required="true"
   * @generated
   */
  String getCurrentUser();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getCurrentUser <em>Current User</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Current User</em>' attribute.
   * @see #getCurrentUser()
   * @generated
   */
  void setCurrentUser(String value);

  /**
   * Returns the value of the '<em><b>Id Spaces</b></em>' map.
   * The key is of type {@link java.lang.String},
   * and the value is of type {@link java.lang.String},
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id Spaces</em>' map isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id Spaces</em>' map.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOSession_IdSpaces()
   * @model mapType="br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdSpacesMap&lt;org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString&gt;" ordered="false"
   * @generated
   */
  EMap<String, String> getIdSpaces();

  /**
   * Returns the value of the '<em><b>Load Remark</b></em>' attribute.
   * The default value is <code>""</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Load Remark</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Load Remark</em>' attribute.
   * @see #setLoadRemark(String)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOSession_LoadRemark()
   * @model default="" required="true"
   * @generated
   */
  String getLoadRemark();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getLoadRemark <em>Load Remark</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Load Remark</em>' attribute.
   * @see #getLoadRemark()
   * @generated
   */
  void setLoadRemark(String value);

  /**
   * Returns the value of the '<em><b>All Dbx Refs Container</b></em>' containment reference list.
   * The list contents are of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>All Dbx Refs Container</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>All Dbx Refs Container</em>' containment reference list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOSession_AllDbxRefsContainer()
   * @model containment="true" ordered="false"
   * @generated
   */
  EList<Dbxref> getAllDbxRefsContainer();

} // OBOSession
