/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;

import java.math.BigInteger;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Synonym Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType#getId <em>Id</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType#getName <em>Name</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType#getScope <em>Scope</em>}</li>
 * </ul>
 *
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getSynonymType()
 * @model
 * @generated
 */
public interface SynonymType extends br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable {
  /**
   * Returns the value of the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id</em>' attribute.
   * @see #setId(String)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getSynonymType_Id()
   * @model required="true"
   * @generated
   */
  String getId();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType#getId <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id</em>' attribute.
   * @see #getId()
   * @generated
   */
  void setId(String value);

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getSynonymType_Name()
   * @model required="true"
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Scope</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Scope</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Scope</em>' attribute.
   * @see #setScope(BigInteger)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getSynonymType_Scope()
   * @model required="true"
   * @generated
   */
  BigInteger getScope();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType#getScope <em>Scope</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Scope</em>' attribute.
   * @see #getScope()
   * @generated
   */
  void setScope(BigInteger value);

} // SynonymType
