/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Annotated Object</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getAnnotatedObject()
 * @model
 * @generated
 */
public interface AnnotatedObject extends IdentifiedObject, ModificationMetadataObject, MultiIDObject, SynonymedObject, DbxrefedObject, CommentedObject, ObsoletableObject, br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable, Serializable, DefinedObject, SubsetObject {
} // AnnotatedObject
