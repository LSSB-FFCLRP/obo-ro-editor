/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Serializable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.math.BigInteger;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dbxref</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DbxrefImpl#getId <em>Id</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DbxrefImpl#isAnonymous <em>Anonymous</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DbxrefImpl#getNestedValue <em>Nested Value</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DbxrefImpl#isDefRef <em>Def Ref</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DbxrefImpl#getDesc <em>Desc</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DbxrefImpl#getType <em>Type</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DbxrefImpl#getSynonym <em>Synonym</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DbxrefImpl#getDatabaseID <em>Database ID</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DbxrefImpl#getDatabase <em>Database</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DbxrefImpl extends CloneableImpl implements Dbxref {
  /**
   * The default value of the '{@link #getId() <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getId()
   * @generated
   * @ordered
   */
  protected static final String ID_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getId()
   * @generated
   * @ordered
   */
  protected String id = ID_EDEFAULT;

  /**
   * The default value of the '{@link #isAnonymous() <em>Anonymous</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isAnonymous()
   * @generated
   * @ordered
   */
  protected static final boolean ANONYMOUS_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isAnonymous() <em>Anonymous</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isAnonymous()
   * @generated
   * @ordered
   */
  protected boolean anonymous = ANONYMOUS_EDEFAULT;

  /**
   * The cached value of the '{@link #getNestedValue() <em>Nested Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNestedValue()
   * @generated
   * @ordered
   */
  protected NestedValue nestedValue;

  /**
   * The default value of the '{@link #isDefRef() <em>Def Ref</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isDefRef()
   * @generated
   * @ordered
   */
  protected static final boolean DEF_REF_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isDefRef() <em>Def Ref</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isDefRef()
   * @generated
   * @ordered
   */
  protected boolean defRef = DEF_REF_EDEFAULT;

  /**
   * The default value of the '{@link #getDesc() <em>Desc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDesc()
   * @generated
   * @ordered
   */
  protected static final String DESC_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getDesc() <em>Desc</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDesc()
   * @generated
   * @ordered
   */
  protected String desc = DESC_EDEFAULT;

  /**
   * The default value of the '{@link #getType() <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected static final BigInteger TYPE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected BigInteger type = TYPE_EDEFAULT;

  /**
   * The cached value of the '{@link #getSynonym() <em>Synonym</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSynonym()
   * @generated
   * @ordered
   */
  protected Synonym synonym;

  /**
   * The default value of the '{@link #getDatabaseID() <em>Database ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDatabaseID()
   * @generated
   * @ordered
   */
  protected static final String DATABASE_ID_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getDatabaseID() <em>Database ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDatabaseID()
   * @generated
   * @ordered
   */
  protected String databaseID = DATABASE_ID_EDEFAULT;

  /**
   * The default value of the '{@link #getDatabase() <em>Database</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDatabase()
   * @generated
   * @ordered
   */
  protected static final String DATABASE_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getDatabase() <em>Database</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDatabase()
   * @generated
   * @ordered
   */
  protected String database = DATABASE_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DbxrefImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.DBXREF;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getId() {
    return id;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setId(String newId) {
    String oldId = id;
    id = newId;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DBXREF__ID, oldId, id));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isAnonymous() {
    return anonymous;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAnonymous(boolean newAnonymous) {
    boolean oldAnonymous = anonymous;
    anonymous = newAnonymous;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DBXREF__ANONYMOUS, oldAnonymous, anonymous));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getNestedValue() {
    return nestedValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetNestedValue(NestedValue newNestedValue, NotificationChain msgs) {
    NestedValue oldNestedValue = nestedValue;
    nestedValue = newNestedValue;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DBXREF__NESTED_VALUE, oldNestedValue, newNestedValue);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNestedValue(NestedValue newNestedValue) {
    if (newNestedValue != nestedValue) {
      NotificationChain msgs = null;
      if (nestedValue != null)
        msgs = ((InternalEObject)nestedValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DBXREF__NESTED_VALUE, null, msgs);
      if (newNestedValue != null)
        msgs = ((InternalEObject)newNestedValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DBXREF__NESTED_VALUE, null, msgs);
      msgs = basicSetNestedValue(newNestedValue, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DBXREF__NESTED_VALUE, newNestedValue, newNestedValue));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isDefRef() {
    return defRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDefRef(boolean newDefRef) {
    boolean oldDefRef = defRef;
    defRef = newDefRef;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DBXREF__DEF_REF, oldDefRef, defRef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDesc() {
    return desc;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDesc(String newDesc) {
    String oldDesc = desc;
    desc = newDesc;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DBXREF__DESC, oldDesc, desc));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BigInteger getType() {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(BigInteger newType) {
    BigInteger oldType = type;
    type = newType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DBXREF__TYPE, oldType, type));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Synonym getSynonym() {
    return synonym;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSynonym(Synonym newSynonym, NotificationChain msgs) {
    Synonym oldSynonym = synonym;
    synonym = newSynonym;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DBXREF__SYNONYM, oldSynonym, newSynonym);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSynonym(Synonym newSynonym) {
    if (newSynonym != synonym) {
      NotificationChain msgs = null;
      if (synonym != null)
        msgs = ((InternalEObject)synonym).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DBXREF__SYNONYM, null, msgs);
      if (newSynonym != null)
        msgs = ((InternalEObject)newSynonym).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DBXREF__SYNONYM, null, msgs);
      msgs = basicSetSynonym(newSynonym, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DBXREF__SYNONYM, newSynonym, newSynonym));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDatabaseID() {
    return databaseID;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDatabaseID(String newDatabaseID) {
    String oldDatabaseID = databaseID;
    databaseID = newDatabaseID;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DBXREF__DATABASE_ID, oldDatabaseID, databaseID));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDatabase() {
    return database;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDatabase(String newDatabase) {
    String oldDatabase = database;
    database = newDatabase;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DBXREF__DATABASE, oldDatabase, database));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.DBXREF__NESTED_VALUE:
        return basicSetNestedValue(null, msgs);
      case obodatamodelPackage.DBXREF__SYNONYM:
        return basicSetSynonym(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.DBXREF__ID:
        return getId();
      case obodatamodelPackage.DBXREF__ANONYMOUS:
        return isAnonymous();
      case obodatamodelPackage.DBXREF__NESTED_VALUE:
        return getNestedValue();
      case obodatamodelPackage.DBXREF__DEF_REF:
        return isDefRef();
      case obodatamodelPackage.DBXREF__DESC:
        return getDesc();
      case obodatamodelPackage.DBXREF__TYPE:
        return getType();
      case obodatamodelPackage.DBXREF__SYNONYM:
        return getSynonym();
      case obodatamodelPackage.DBXREF__DATABASE_ID:
        return getDatabaseID();
      case obodatamodelPackage.DBXREF__DATABASE:
        return getDatabase();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.DBXREF__ID:
        setId((String)newValue);
        return;
      case obodatamodelPackage.DBXREF__ANONYMOUS:
        setAnonymous((Boolean)newValue);
        return;
      case obodatamodelPackage.DBXREF__NESTED_VALUE:
        setNestedValue((NestedValue)newValue);
        return;
      case obodatamodelPackage.DBXREF__DEF_REF:
        setDefRef((Boolean)newValue);
        return;
      case obodatamodelPackage.DBXREF__DESC:
        setDesc((String)newValue);
        return;
      case obodatamodelPackage.DBXREF__TYPE:
        setType((BigInteger)newValue);
        return;
      case obodatamodelPackage.DBXREF__SYNONYM:
        setSynonym((Synonym)newValue);
        return;
      case obodatamodelPackage.DBXREF__DATABASE_ID:
        setDatabaseID((String)newValue);
        return;
      case obodatamodelPackage.DBXREF__DATABASE:
        setDatabase((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.DBXREF__ID:
        setId(ID_EDEFAULT);
        return;
      case obodatamodelPackage.DBXREF__ANONYMOUS:
        setAnonymous(ANONYMOUS_EDEFAULT);
        return;
      case obodatamodelPackage.DBXREF__NESTED_VALUE:
        setNestedValue((NestedValue)null);
        return;
      case obodatamodelPackage.DBXREF__DEF_REF:
        setDefRef(DEF_REF_EDEFAULT);
        return;
      case obodatamodelPackage.DBXREF__DESC:
        setDesc(DESC_EDEFAULT);
        return;
      case obodatamodelPackage.DBXREF__TYPE:
        setType(TYPE_EDEFAULT);
        return;
      case obodatamodelPackage.DBXREF__SYNONYM:
        setSynonym((Synonym)null);
        return;
      case obodatamodelPackage.DBXREF__DATABASE_ID:
        setDatabaseID(DATABASE_ID_EDEFAULT);
        return;
      case obodatamodelPackage.DBXREF__DATABASE:
        setDatabase(DATABASE_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.DBXREF__ID:
        return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
      case obodatamodelPackage.DBXREF__ANONYMOUS:
        return anonymous != ANONYMOUS_EDEFAULT;
      case obodatamodelPackage.DBXREF__NESTED_VALUE:
        return nestedValue != null;
      case obodatamodelPackage.DBXREF__DEF_REF:
        return defRef != DEF_REF_EDEFAULT;
      case obodatamodelPackage.DBXREF__DESC:
        return DESC_EDEFAULT == null ? desc != null : !DESC_EDEFAULT.equals(desc);
      case obodatamodelPackage.DBXREF__TYPE:
        return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
      case obodatamodelPackage.DBXREF__SYNONYM:
        return synonym != null;
      case obodatamodelPackage.DBXREF__DATABASE_ID:
        return DATABASE_ID_EDEFAULT == null ? databaseID != null : !DATABASE_ID_EDEFAULT.equals(databaseID);
      case obodatamodelPackage.DBXREF__DATABASE:
        return DATABASE_EDEFAULT == null ? database != null : !DATABASE_EDEFAULT.equals(database);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
    if (baseClass == Serializable.class) {
      switch (derivedFeatureID) {
        default: return -1;
      }
    }
    if (baseClass == br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable.class) {
      switch (derivedFeatureID) {
        default: return -1;
      }
    }
    if (baseClass == IdentifiableObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.DBXREF__ID: return obodatamodelPackage.IDENTIFIABLE_OBJECT__ID;
        case obodatamodelPackage.DBXREF__ANONYMOUS: return obodatamodelPackage.IDENTIFIABLE_OBJECT__ANONYMOUS;
        default: return -1;
      }
    }
    return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
    if (baseClass == Serializable.class) {
      switch (baseFeatureID) {
        default: return -1;
      }
    }
    if (baseClass == br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable.class) {
      switch (baseFeatureID) {
        default: return -1;
      }
    }
    if (baseClass == IdentifiableObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.IDENTIFIABLE_OBJECT__ID: return obodatamodelPackage.DBXREF__ID;
        case obodatamodelPackage.IDENTIFIABLE_OBJECT__ANONYMOUS: return obodatamodelPackage.DBXREF__ANONYMOUS;
        default: return -1;
      }
    }
    return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (id: ");
    result.append(id);
    result.append(", anonymous: ");
    result.append(anonymous);
    result.append(", defRef: ");
    result.append(defRef);
    result.append(", desc: ");
    result.append(desc);
    result.append(", type: ");
    result.append(type);
    result.append(", databaseID: ");
    result.append(databaseID);
    result.append(", database: ");
    result.append(database);
    result.append(')');
    return result.toString();
  }

} //DbxrefImpl
