/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dangling Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingObject#isDangling <em>Dangling</em>}</li>
 * </ul>
 *
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getDanglingObject()
 * @model
 * @generated
 */
public interface DanglingObject extends LinkedObject, Type {
  /**
   * Returns the value of the '<em><b>Dangling</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dangling</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dangling</em>' attribute.
   * @see #setDangling(boolean)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getDanglingObject_Dangling()
   * @model required="true"
   * @generated
   */
  boolean isDangling();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingObject#isDangling <em>Dangling</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dangling</em>' attribute.
   * @see #isDangling()
   * @generated
   */
  void setDangling(boolean value);

} // DanglingObject
