/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OBO Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isCyclic <em>Cyclic</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isSymmetric <em>Symmetric</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isTransitive <em>Transitive</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isReflexive <em>Reflexive</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isAlwaysImpliesInverse <em>Always Implies Inverse</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isDummy <em>Dummy</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isMetadataTag <em>Metadata Tag</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getCyclicExtension <em>Cyclic Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getSymmetricExtension <em>Symmetric Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getTransitiveExtension <em>Transitive Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getReflexiveExtension <em>Reflexive Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getAlwaysImpliesInverseExtension <em>Always Implies Inverse Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getDomainExtension <em>Domain Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getRangeExtension <em>Range Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getRange <em>Range</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getDomain <em>Domain</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isNonInheritable <em>Non Inheritable</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getDisjointOver <em>Disjoint Over</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getTransitiveOver <em>Transitive Over</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getHoldsOverChains <em>Holds Over Chains</em>}</li>
 * </ul>
 *
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOProperty()
 * @model
 * @generated
 */
public interface OBOProperty extends OBOObject {
  /**
   * Returns the value of the '<em><b>Cyclic</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Cyclic</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Cyclic</em>' attribute.
   * @see #setCyclic(boolean)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOProperty_Cyclic()
   * @model required="true"
   * @generated
   */
  boolean isCyclic();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isCyclic <em>Cyclic</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Cyclic</em>' attribute.
   * @see #isCyclic()
   * @generated
   */
  void setCyclic(boolean value);

  /**
   * Returns the value of the '<em><b>Symmetric</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Symmetric</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Symmetric</em>' attribute.
   * @see #setSymmetric(boolean)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOProperty_Symmetric()
   * @model required="true"
   * @generated
   */
  boolean isSymmetric();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isSymmetric <em>Symmetric</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Symmetric</em>' attribute.
   * @see #isSymmetric()
   * @generated
   */
  void setSymmetric(boolean value);

  /**
   * Returns the value of the '<em><b>Transitive</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Transitive</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Transitive</em>' attribute.
   * @see #setTransitive(boolean)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOProperty_Transitive()
   * @model required="true"
   * @generated
   */
  boolean isTransitive();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isTransitive <em>Transitive</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Transitive</em>' attribute.
   * @see #isTransitive()
   * @generated
   */
  void setTransitive(boolean value);

  /**
   * Returns the value of the '<em><b>Reflexive</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Reflexive</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Reflexive</em>' attribute.
   * @see #setReflexive(boolean)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOProperty_Reflexive()
   * @model required="true"
   * @generated
   */
  boolean isReflexive();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isReflexive <em>Reflexive</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Reflexive</em>' attribute.
   * @see #isReflexive()
   * @generated
   */
  void setReflexive(boolean value);

  /**
   * Returns the value of the '<em><b>Always Implies Inverse</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Always Implies Inverse</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Always Implies Inverse</em>' attribute.
   * @see #setAlwaysImpliesInverse(boolean)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOProperty_AlwaysImpliesInverse()
   * @model required="true"
   * @generated
   */
  boolean isAlwaysImpliesInverse();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isAlwaysImpliesInverse <em>Always Implies Inverse</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Always Implies Inverse</em>' attribute.
   * @see #isAlwaysImpliesInverse()
   * @generated
   */
  void setAlwaysImpliesInverse(boolean value);

  /**
   * Returns the value of the '<em><b>Dummy</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dummy</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dummy</em>' attribute.
   * @see #setDummy(boolean)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOProperty_Dummy()
   * @model required="true"
   * @generated
   */
  boolean isDummy();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isDummy <em>Dummy</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dummy</em>' attribute.
   * @see #isDummy()
   * @generated
   */
  void setDummy(boolean value);

  /**
   * Returns the value of the '<em><b>Metadata Tag</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Metadata Tag</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Metadata Tag</em>' attribute.
   * @see #setMetadataTag(boolean)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOProperty_MetadataTag()
   * @model required="true"
   * @generated
   */
  boolean isMetadataTag();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isMetadataTag <em>Metadata Tag</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Metadata Tag</em>' attribute.
   * @see #isMetadataTag()
   * @generated
   */
  void setMetadataTag(boolean value);

  /**
   * Returns the value of the '<em><b>Cyclic Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Cyclic Extension</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Cyclic Extension</em>' containment reference.
   * @see #setCyclicExtension(NestedValue)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOProperty_CyclicExtension()
   * @model containment="true"
   * @generated
   */
  NestedValue getCyclicExtension();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getCyclicExtension <em>Cyclic Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Cyclic Extension</em>' containment reference.
   * @see #getCyclicExtension()
   * @generated
   */
  void setCyclicExtension(NestedValue value);

  /**
   * Returns the value of the '<em><b>Symmetric Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Symmetric Extension</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Symmetric Extension</em>' containment reference.
   * @see #setSymmetricExtension(NestedValue)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOProperty_SymmetricExtension()
   * @model containment="true"
   * @generated
   */
  NestedValue getSymmetricExtension();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getSymmetricExtension <em>Symmetric Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Symmetric Extension</em>' containment reference.
   * @see #getSymmetricExtension()
   * @generated
   */
  void setSymmetricExtension(NestedValue value);

  /**
   * Returns the value of the '<em><b>Transitive Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Transitive Extension</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Transitive Extension</em>' containment reference.
   * @see #setTransitiveExtension(NestedValue)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOProperty_TransitiveExtension()
   * @model containment="true"
   * @generated
   */
  NestedValue getTransitiveExtension();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getTransitiveExtension <em>Transitive Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Transitive Extension</em>' containment reference.
   * @see #getTransitiveExtension()
   * @generated
   */
  void setTransitiveExtension(NestedValue value);

  /**
   * Returns the value of the '<em><b>Reflexive Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Reflexive Extension</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Reflexive Extension</em>' containment reference.
   * @see #setReflexiveExtension(NestedValue)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOProperty_ReflexiveExtension()
   * @model containment="true"
   * @generated
   */
  NestedValue getReflexiveExtension();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getReflexiveExtension <em>Reflexive Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Reflexive Extension</em>' containment reference.
   * @see #getReflexiveExtension()
   * @generated
   */
  void setReflexiveExtension(NestedValue value);

  /**
   * Returns the value of the '<em><b>Always Implies Inverse Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Always Implies Inverse Extension</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Always Implies Inverse Extension</em>' containment reference.
   * @see #setAlwaysImpliesInverseExtension(NestedValue)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOProperty_AlwaysImpliesInverseExtension()
   * @model containment="true"
   * @generated
   */
  NestedValue getAlwaysImpliesInverseExtension();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getAlwaysImpliesInverseExtension <em>Always Implies Inverse Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Always Implies Inverse Extension</em>' containment reference.
   * @see #getAlwaysImpliesInverseExtension()
   * @generated
   */
  void setAlwaysImpliesInverseExtension(NestedValue value);

  /**
   * Returns the value of the '<em><b>Domain Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Domain Extension</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Domain Extension</em>' containment reference.
   * @see #setDomainExtension(NestedValue)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOProperty_DomainExtension()
   * @model containment="true"
   * @generated
   */
  NestedValue getDomainExtension();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getDomainExtension <em>Domain Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Domain Extension</em>' containment reference.
   * @see #getDomainExtension()
   * @generated
   */
  void setDomainExtension(NestedValue value);

  /**
   * Returns the value of the '<em><b>Range Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Range Extension</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Range Extension</em>' containment reference.
   * @see #setRangeExtension(NestedValue)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOProperty_RangeExtension()
   * @model containment="true"
   * @generated
   */
  NestedValue getRangeExtension();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getRangeExtension <em>Range Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Range Extension</em>' containment reference.
   * @see #getRangeExtension()
   * @generated
   */
  void setRangeExtension(NestedValue value);

  /**
   * Returns the value of the '<em><b>Range</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Range</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Range</em>' reference.
   * @see #setRange(Type)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOProperty_Range()
   * @model
   * @generated
   */
  Type getRange();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getRange <em>Range</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Range</em>' reference.
   * @see #getRange()
   * @generated
   */
  void setRange(Type value);

  /**
   * Returns the value of the '<em><b>Domain</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Domain</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Domain</em>' reference.
   * @see #setDomain(Type)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOProperty_Domain()
   * @model
   * @generated
   */
  Type getDomain();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getDomain <em>Domain</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Domain</em>' reference.
   * @see #getDomain()
   * @generated
   */
  void setDomain(Type value);

  /**
   * Returns the value of the '<em><b>Non Inheritable</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Non Inheritable</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Non Inheritable</em>' attribute.
   * @see #setNonInheritable(boolean)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOProperty_NonInheritable()
   * @model required="true"
   * @generated
   */
  boolean isNonInheritable();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isNonInheritable <em>Non Inheritable</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Non Inheritable</em>' attribute.
   * @see #isNonInheritable()
   * @generated
   */
  void setNonInheritable(boolean value);

  /**
   * Returns the value of the '<em><b>Disjoint Over</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Disjoint Over</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Disjoint Over</em>' reference.
   * @see #setDisjointOver(OBOProperty)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOProperty_DisjointOver()
   * @model
   * @generated
   */
  OBOProperty getDisjointOver();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getDisjointOver <em>Disjoint Over</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Disjoint Over</em>' reference.
   * @see #getDisjointOver()
   * @generated
   */
  void setDisjointOver(OBOProperty value);

  /**
   * Returns the value of the '<em><b>Transitive Over</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Transitive Over</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Transitive Over</em>' reference.
   * @see #setTransitiveOver(OBOProperty)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOProperty_TransitiveOver()
   * @model
   * @generated
   */
  OBOProperty getTransitiveOver();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getTransitiveOver <em>Transitive Over</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Transitive Over</em>' reference.
   * @see #getTransitiveOver()
   * @generated
   */
  void setTransitiveOver(OBOProperty value);

  /**
   * Returns the value of the '<em><b>Holds Over Chains</b></em>' containment reference list.
   * The list contents are of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ListOfProperties}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Holds Over Chains</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Holds Over Chains</em>' containment reference list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOProperty_HoldsOverChains()
   * @model containment="true" ordered="false"
   * @generated
   */
  EList<ListOfProperties> getHoldsOverChains();

} // OBOProperty
