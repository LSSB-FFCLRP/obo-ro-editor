/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.RootAlgorithm;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Root Algorithm</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RootAlgorithmImpl extends MinimalEObjectImpl.Container implements RootAlgorithm {
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected RootAlgorithmImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.ROOT_ALGORITHM;
  }

} //RootAlgorithmImpl
