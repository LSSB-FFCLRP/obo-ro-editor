/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.AnnotatedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.CommentedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DbxrefedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MultiIDObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SubsetObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Annotated Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getCreatedBy <em>Created By</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getCreatedByExtension <em>Created By Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getModifiedBy <em>Modified By</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getModifiedByExtension <em>Modified By Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getCreationDate <em>Creation Date</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getCreationDateExtension <em>Creation Date Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getModificationDate <em>Modification Date</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getModificationDateExtension <em>Modification Date Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getSecondaryIds <em>Secondary Ids</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getSecondaryIdExtension <em>Secondary Id Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getSynonyms <em>Synonyms</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getDbxrefs <em>Dbxrefs</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getCommentExtension <em>Comment Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#isObsolete <em>Obsolete</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getReplacedBy <em>Replaced By</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getConsiderReplacements <em>Consider Replacements</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getConsiderExtension <em>Consider Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getReplacedByExtension <em>Replaced By Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getObsoleteExtension <em>Obsolete Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getDefinition <em>Definition</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getDefDbxrefs <em>Def Dbxrefs</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getDefinitionExtension <em>Definition Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getSubsets <em>Subsets</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl#getCategoryExtensions <em>Category Extensions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AnnotatedObjectImpl extends IdentifiedObjectImpl implements AnnotatedObject {
  /**
   * The default value of the '{@link #getCreatedBy() <em>Created By</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCreatedBy()
   * @generated
   * @ordered
   */
  protected static final String CREATED_BY_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getCreatedBy() <em>Created By</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCreatedBy()
   * @generated
   * @ordered
   */
  protected String createdBy = CREATED_BY_EDEFAULT;

  /**
   * The cached value of the '{@link #getCreatedByExtension() <em>Created By Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCreatedByExtension()
   * @generated
   * @ordered
   */
  protected NestedValue createdByExtension;

  /**
   * The default value of the '{@link #getModifiedBy() <em>Modified By</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModifiedBy()
   * @generated
   * @ordered
   */
  protected static final String MODIFIED_BY_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getModifiedBy() <em>Modified By</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModifiedBy()
   * @generated
   * @ordered
   */
  protected String modifiedBy = MODIFIED_BY_EDEFAULT;

  /**
   * The cached value of the '{@link #getModifiedByExtension() <em>Modified By Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModifiedByExtension()
   * @generated
   * @ordered
   */
  protected NestedValue modifiedByExtension;

  /**
   * The default value of the '{@link #getCreationDate() <em>Creation Date</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCreationDate()
   * @generated
   * @ordered
   */
  protected static final Date CREATION_DATE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCreationDate() <em>Creation Date</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCreationDate()
   * @generated
   * @ordered
   */
  protected Date creationDate = CREATION_DATE_EDEFAULT;

  /**
   * The cached value of the '{@link #getCreationDateExtension() <em>Creation Date Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCreationDateExtension()
   * @generated
   * @ordered
   */
  protected NestedValue creationDateExtension;

  /**
   * The default value of the '{@link #getModificationDate() <em>Modification Date</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModificationDate()
   * @generated
   * @ordered
   */
  protected static final Date MODIFICATION_DATE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getModificationDate() <em>Modification Date</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModificationDate()
   * @generated
   * @ordered
   */
  protected Date modificationDate = MODIFICATION_DATE_EDEFAULT;

  /**
   * The cached value of the '{@link #getModificationDateExtension() <em>Modification Date Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModificationDateExtension()
   * @generated
   * @ordered
   */
  protected NestedValue modificationDateExtension;

  /**
   * The cached value of the '{@link #getSecondaryIds() <em>Secondary Ids</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSecondaryIds()
   * @generated
   * @ordered
   */
  protected EList<String> secondaryIds;

  /**
   * The cached value of the '{@link #getSecondaryIdExtension() <em>Secondary Id Extension</em>}' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSecondaryIdExtension()
   * @generated
   * @ordered
   */
  protected EMap<String, NestedValue> secondaryIdExtension;

  /**
   * The cached value of the '{@link #getSynonyms() <em>Synonyms</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSynonyms()
   * @generated
   * @ordered
   */
  protected EList<Synonym> synonyms;

  /**
   * The cached value of the '{@link #getDbxrefs() <em>Dbxrefs</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDbxrefs()
   * @generated
   * @ordered
   */
  protected EList<Dbxref> dbxrefs;

  /**
   * The default value of the '{@link #getComment() <em>Comment</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getComment()
   * @generated
   * @ordered
   */
  protected static final String COMMENT_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getComment() <em>Comment</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getComment()
   * @generated
   * @ordered
   */
  protected String comment = COMMENT_EDEFAULT;

  /**
   * The cached value of the '{@link #getCommentExtension() <em>Comment Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCommentExtension()
   * @generated
   * @ordered
   */
  protected NestedValue commentExtension;

  /**
   * The default value of the '{@link #isObsolete() <em>Obsolete</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isObsolete()
   * @generated
   * @ordered
   */
  protected static final boolean OBSOLETE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isObsolete() <em>Obsolete</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isObsolete()
   * @generated
   * @ordered
   */
  protected boolean obsolete = OBSOLETE_EDEFAULT;

  /**
   * The cached value of the '{@link #getReplacedBy() <em>Replaced By</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReplacedBy()
   * @generated
   * @ordered
   */
  protected EList<ObsoletableObject> replacedBy;

  /**
   * The cached value of the '{@link #getConsiderReplacements() <em>Consider Replacements</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConsiderReplacements()
   * @generated
   * @ordered
   */
  protected EList<ObsoletableObject> considerReplacements;

  /**
   * The cached value of the '{@link #getConsiderExtension() <em>Consider Extension</em>}' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConsiderExtension()
   * @generated
   * @ordered
   */
  protected EMap<ObsoletableObject, NestedValue> considerExtension;

  /**
   * The cached value of the '{@link #getReplacedByExtension() <em>Replaced By Extension</em>}' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReplacedByExtension()
   * @generated
   * @ordered
   */
  protected EMap<ObsoletableObject, NestedValue> replacedByExtension;

  /**
   * The cached value of the '{@link #getObsoleteExtension() <em>Obsolete Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getObsoleteExtension()
   * @generated
   * @ordered
   */
  protected NestedValue obsoleteExtension;

  /**
   * The default value of the '{@link #getDefinition() <em>Definition</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDefinition()
   * @generated
   * @ordered
   */
  protected static final String DEFINITION_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getDefinition() <em>Definition</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDefinition()
   * @generated
   * @ordered
   */
  protected String definition = DEFINITION_EDEFAULT;

  /**
   * The cached value of the '{@link #getDefDbxrefs() <em>Def Dbxrefs</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDefDbxrefs()
   * @generated
   * @ordered
   */
  protected EList<Dbxref> defDbxrefs;

  /**
   * The cached value of the '{@link #getDefinitionExtension() <em>Definition Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDefinitionExtension()
   * @generated
   * @ordered
   */
  protected NestedValue definitionExtension;

  /**
   * The cached value of the '{@link #getSubsets() <em>Subsets</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubsets()
   * @generated
   * @ordered
   */
  protected EList<TermSubset> subsets;

  /**
   * The cached value of the '{@link #getCategoryExtensions() <em>Category Extensions</em>}' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCategoryExtensions()
   * @generated
   * @ordered
   */
  protected EMap<TermSubset, NestedValue> categoryExtensions;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AnnotatedObjectImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.ANNOTATED_OBJECT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCreatedBy() {
    return createdBy;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCreatedBy(String newCreatedBy) {
    String oldCreatedBy = createdBy;
    createdBy = newCreatedBy;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.ANNOTATED_OBJECT__CREATED_BY, oldCreatedBy, createdBy));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getCreatedByExtension() {
    return createdByExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCreatedByExtension(NestedValue newCreatedByExtension, NotificationChain msgs) {
    NestedValue oldCreatedByExtension = createdByExtension;
    createdByExtension = newCreatedByExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.ANNOTATED_OBJECT__CREATED_BY_EXTENSION, oldCreatedByExtension, newCreatedByExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCreatedByExtension(NestedValue newCreatedByExtension) {
    if (newCreatedByExtension != createdByExtension) {
      NotificationChain msgs = null;
      if (createdByExtension != null)
        msgs = ((InternalEObject)createdByExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.ANNOTATED_OBJECT__CREATED_BY_EXTENSION, null, msgs);
      if (newCreatedByExtension != null)
        msgs = ((InternalEObject)newCreatedByExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.ANNOTATED_OBJECT__CREATED_BY_EXTENSION, null, msgs);
      msgs = basicSetCreatedByExtension(newCreatedByExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.ANNOTATED_OBJECT__CREATED_BY_EXTENSION, newCreatedByExtension, newCreatedByExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getModifiedBy() {
    return modifiedBy;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setModifiedBy(String newModifiedBy) {
    String oldModifiedBy = modifiedBy;
    modifiedBy = newModifiedBy;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.ANNOTATED_OBJECT__MODIFIED_BY, oldModifiedBy, modifiedBy));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getModifiedByExtension() {
    return modifiedByExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetModifiedByExtension(NestedValue newModifiedByExtension, NotificationChain msgs) {
    NestedValue oldModifiedByExtension = modifiedByExtension;
    modifiedByExtension = newModifiedByExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.ANNOTATED_OBJECT__MODIFIED_BY_EXTENSION, oldModifiedByExtension, newModifiedByExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setModifiedByExtension(NestedValue newModifiedByExtension) {
    if (newModifiedByExtension != modifiedByExtension) {
      NotificationChain msgs = null;
      if (modifiedByExtension != null)
        msgs = ((InternalEObject)modifiedByExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.ANNOTATED_OBJECT__MODIFIED_BY_EXTENSION, null, msgs);
      if (newModifiedByExtension != null)
        msgs = ((InternalEObject)newModifiedByExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.ANNOTATED_OBJECT__MODIFIED_BY_EXTENSION, null, msgs);
      msgs = basicSetModifiedByExtension(newModifiedByExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.ANNOTATED_OBJECT__MODIFIED_BY_EXTENSION, newModifiedByExtension, newModifiedByExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Date getCreationDate() {
    return creationDate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCreationDate(Date newCreationDate) {
    Date oldCreationDate = creationDate;
    creationDate = newCreationDate;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.ANNOTATED_OBJECT__CREATION_DATE, oldCreationDate, creationDate));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getCreationDateExtension() {
    return creationDateExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCreationDateExtension(NestedValue newCreationDateExtension, NotificationChain msgs) {
    NestedValue oldCreationDateExtension = creationDateExtension;
    creationDateExtension = newCreationDateExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.ANNOTATED_OBJECT__CREATION_DATE_EXTENSION, oldCreationDateExtension, newCreationDateExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCreationDateExtension(NestedValue newCreationDateExtension) {
    if (newCreationDateExtension != creationDateExtension) {
      NotificationChain msgs = null;
      if (creationDateExtension != null)
        msgs = ((InternalEObject)creationDateExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.ANNOTATED_OBJECT__CREATION_DATE_EXTENSION, null, msgs);
      if (newCreationDateExtension != null)
        msgs = ((InternalEObject)newCreationDateExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.ANNOTATED_OBJECT__CREATION_DATE_EXTENSION, null, msgs);
      msgs = basicSetCreationDateExtension(newCreationDateExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.ANNOTATED_OBJECT__CREATION_DATE_EXTENSION, newCreationDateExtension, newCreationDateExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Date getModificationDate() {
    return modificationDate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setModificationDate(Date newModificationDate) {
    Date oldModificationDate = modificationDate;
    modificationDate = newModificationDate;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.ANNOTATED_OBJECT__MODIFICATION_DATE, oldModificationDate, modificationDate));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getModificationDateExtension() {
    return modificationDateExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetModificationDateExtension(NestedValue newModificationDateExtension, NotificationChain msgs) {
    NestedValue oldModificationDateExtension = modificationDateExtension;
    modificationDateExtension = newModificationDateExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.ANNOTATED_OBJECT__MODIFICATION_DATE_EXTENSION, oldModificationDateExtension, newModificationDateExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setModificationDateExtension(NestedValue newModificationDateExtension) {
    if (newModificationDateExtension != modificationDateExtension) {
      NotificationChain msgs = null;
      if (modificationDateExtension != null)
        msgs = ((InternalEObject)modificationDateExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.ANNOTATED_OBJECT__MODIFICATION_DATE_EXTENSION, null, msgs);
      if (newModificationDateExtension != null)
        msgs = ((InternalEObject)newModificationDateExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.ANNOTATED_OBJECT__MODIFICATION_DATE_EXTENSION, null, msgs);
      msgs = basicSetModificationDateExtension(newModificationDateExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.ANNOTATED_OBJECT__MODIFICATION_DATE_EXTENSION, newModificationDateExtension, newModificationDateExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getSecondaryIds() {
    if (secondaryIds == null) {
      secondaryIds = new EDataTypeUniqueEList<String>(String.class, this, obodatamodelPackage.ANNOTATED_OBJECT__SECONDARY_IDS);
    }
    return secondaryIds;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EMap<String, NestedValue> getSecondaryIdExtension() {
    if (secondaryIdExtension == null) {
      secondaryIdExtension = new EcoreEMap<String,NestedValue>(obodatamodelPackage.Literals.STRING_TO_NESTED_VALUE_MAP, StringToNestedValueMapImpl.class, this, obodatamodelPackage.ANNOTATED_OBJECT__SECONDARY_ID_EXTENSION);
    }
    return secondaryIdExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Synonym> getSynonyms() {
    if (synonyms == null) {
      synonyms = new EObjectContainmentEList<Synonym>(Synonym.class, this, obodatamodelPackage.ANNOTATED_OBJECT__SYNONYMS);
    }
    return synonyms;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Dbxref> getDbxrefs() {
    if (dbxrefs == null) {
      dbxrefs = new EObjectResolvingEList<Dbxref>(Dbxref.class, this, obodatamodelPackage.ANNOTATED_OBJECT__DBXREFS);
    }
    return dbxrefs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getComment() {
    return comment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setComment(String newComment) {
    String oldComment = comment;
    comment = newComment;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.ANNOTATED_OBJECT__COMMENT, oldComment, comment));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getCommentExtension() {
    return commentExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCommentExtension(NestedValue newCommentExtension, NotificationChain msgs) {
    NestedValue oldCommentExtension = commentExtension;
    commentExtension = newCommentExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.ANNOTATED_OBJECT__COMMENT_EXTENSION, oldCommentExtension, newCommentExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCommentExtension(NestedValue newCommentExtension) {
    if (newCommentExtension != commentExtension) {
      NotificationChain msgs = null;
      if (commentExtension != null)
        msgs = ((InternalEObject)commentExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.ANNOTATED_OBJECT__COMMENT_EXTENSION, null, msgs);
      if (newCommentExtension != null)
        msgs = ((InternalEObject)newCommentExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.ANNOTATED_OBJECT__COMMENT_EXTENSION, null, msgs);
      msgs = basicSetCommentExtension(newCommentExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.ANNOTATED_OBJECT__COMMENT_EXTENSION, newCommentExtension, newCommentExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isObsolete() {
    return obsolete;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setObsolete(boolean newObsolete) {
    boolean oldObsolete = obsolete;
    obsolete = newObsolete;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.ANNOTATED_OBJECT__OBSOLETE, oldObsolete, obsolete));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ObsoletableObject> getReplacedBy() {
    if (replacedBy == null) {
      replacedBy = new EObjectResolvingEList<ObsoletableObject>(ObsoletableObject.class, this, obodatamodelPackage.ANNOTATED_OBJECT__REPLACED_BY);
    }
    return replacedBy;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ObsoletableObject> getConsiderReplacements() {
    if (considerReplacements == null) {
      considerReplacements = new EObjectResolvingEList<ObsoletableObject>(ObsoletableObject.class, this, obodatamodelPackage.ANNOTATED_OBJECT__CONSIDER_REPLACEMENTS);
    }
    return considerReplacements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EMap<ObsoletableObject, NestedValue> getConsiderExtension() {
    if (considerExtension == null) {
      considerExtension = new EcoreEMap<ObsoletableObject,NestedValue>(obodatamodelPackage.Literals.OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP, ObsoletableObjectToNestedValueMapImpl.class, this, obodatamodelPackage.ANNOTATED_OBJECT__CONSIDER_EXTENSION);
    }
    return considerExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EMap<ObsoletableObject, NestedValue> getReplacedByExtension() {
    if (replacedByExtension == null) {
      replacedByExtension = new EcoreEMap<ObsoletableObject,NestedValue>(obodatamodelPackage.Literals.OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP, ObsoletableObjectToNestedValueMapImpl.class, this, obodatamodelPackage.ANNOTATED_OBJECT__REPLACED_BY_EXTENSION);
    }
    return replacedByExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getObsoleteExtension() {
    return obsoleteExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetObsoleteExtension(NestedValue newObsoleteExtension, NotificationChain msgs) {
    NestedValue oldObsoleteExtension = obsoleteExtension;
    obsoleteExtension = newObsoleteExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.ANNOTATED_OBJECT__OBSOLETE_EXTENSION, oldObsoleteExtension, newObsoleteExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setObsoleteExtension(NestedValue newObsoleteExtension) {
    if (newObsoleteExtension != obsoleteExtension) {
      NotificationChain msgs = null;
      if (obsoleteExtension != null)
        msgs = ((InternalEObject)obsoleteExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.ANNOTATED_OBJECT__OBSOLETE_EXTENSION, null, msgs);
      if (newObsoleteExtension != null)
        msgs = ((InternalEObject)newObsoleteExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.ANNOTATED_OBJECT__OBSOLETE_EXTENSION, null, msgs);
      msgs = basicSetObsoleteExtension(newObsoleteExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.ANNOTATED_OBJECT__OBSOLETE_EXTENSION, newObsoleteExtension, newObsoleteExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDefinition() {
    return definition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDefinition(String newDefinition) {
    String oldDefinition = definition;
    definition = newDefinition;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.ANNOTATED_OBJECT__DEFINITION, oldDefinition, definition));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Dbxref> getDefDbxrefs() {
    if (defDbxrefs == null) {
      defDbxrefs = new EObjectResolvingEList<Dbxref>(Dbxref.class, this, obodatamodelPackage.ANNOTATED_OBJECT__DEF_DBXREFS);
    }
    return defDbxrefs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getDefinitionExtension() {
    return definitionExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDefinitionExtension(NestedValue newDefinitionExtension, NotificationChain msgs) {
    NestedValue oldDefinitionExtension = definitionExtension;
    definitionExtension = newDefinitionExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.ANNOTATED_OBJECT__DEFINITION_EXTENSION, oldDefinitionExtension, newDefinitionExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDefinitionExtension(NestedValue newDefinitionExtension) {
    if (newDefinitionExtension != definitionExtension) {
      NotificationChain msgs = null;
      if (definitionExtension != null)
        msgs = ((InternalEObject)definitionExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.ANNOTATED_OBJECT__DEFINITION_EXTENSION, null, msgs);
      if (newDefinitionExtension != null)
        msgs = ((InternalEObject)newDefinitionExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.ANNOTATED_OBJECT__DEFINITION_EXTENSION, null, msgs);
      msgs = basicSetDefinitionExtension(newDefinitionExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.ANNOTATED_OBJECT__DEFINITION_EXTENSION, newDefinitionExtension, newDefinitionExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TermSubset> getSubsets() {
    if (subsets == null) {
      subsets = new EObjectResolvingEList<TermSubset>(TermSubset.class, this, obodatamodelPackage.ANNOTATED_OBJECT__SUBSETS);
    }
    return subsets;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EMap<TermSubset, NestedValue> getCategoryExtensions() {
    if (categoryExtensions == null) {
      categoryExtensions = new EcoreEMap<TermSubset,NestedValue>(obodatamodelPackage.Literals.TERM_SUBSET_TO_NESTED_VALUE_MAP, TermSubsetToNestedValueMapImpl.class, this, obodatamodelPackage.ANNOTATED_OBJECT__CATEGORY_EXTENSIONS);
    }
    return categoryExtensions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATED_BY_EXTENSION:
        return basicSetCreatedByExtension(null, msgs);
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFIED_BY_EXTENSION:
        return basicSetModifiedByExtension(null, msgs);
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATION_DATE_EXTENSION:
        return basicSetCreationDateExtension(null, msgs);
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFICATION_DATE_EXTENSION:
        return basicSetModificationDateExtension(null, msgs);
      case obodatamodelPackage.ANNOTATED_OBJECT__SECONDARY_ID_EXTENSION:
        return ((InternalEList<?>)getSecondaryIdExtension()).basicRemove(otherEnd, msgs);
      case obodatamodelPackage.ANNOTATED_OBJECT__SYNONYMS:
        return ((InternalEList<?>)getSynonyms()).basicRemove(otherEnd, msgs);
      case obodatamodelPackage.ANNOTATED_OBJECT__COMMENT_EXTENSION:
        return basicSetCommentExtension(null, msgs);
      case obodatamodelPackage.ANNOTATED_OBJECT__CONSIDER_EXTENSION:
        return ((InternalEList<?>)getConsiderExtension()).basicRemove(otherEnd, msgs);
      case obodatamodelPackage.ANNOTATED_OBJECT__REPLACED_BY_EXTENSION:
        return ((InternalEList<?>)getReplacedByExtension()).basicRemove(otherEnd, msgs);
      case obodatamodelPackage.ANNOTATED_OBJECT__OBSOLETE_EXTENSION:
        return basicSetObsoleteExtension(null, msgs);
      case obodatamodelPackage.ANNOTATED_OBJECT__DEFINITION_EXTENSION:
        return basicSetDefinitionExtension(null, msgs);
      case obodatamodelPackage.ANNOTATED_OBJECT__CATEGORY_EXTENSIONS:
        return ((InternalEList<?>)getCategoryExtensions()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATED_BY:
        return getCreatedBy();
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATED_BY_EXTENSION:
        return getCreatedByExtension();
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFIED_BY:
        return getModifiedBy();
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFIED_BY_EXTENSION:
        return getModifiedByExtension();
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATION_DATE:
        return getCreationDate();
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATION_DATE_EXTENSION:
        return getCreationDateExtension();
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFICATION_DATE:
        return getModificationDate();
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFICATION_DATE_EXTENSION:
        return getModificationDateExtension();
      case obodatamodelPackage.ANNOTATED_OBJECT__SECONDARY_IDS:
        return getSecondaryIds();
      case obodatamodelPackage.ANNOTATED_OBJECT__SECONDARY_ID_EXTENSION:
        if (coreType) return getSecondaryIdExtension();
        else return getSecondaryIdExtension().map();
      case obodatamodelPackage.ANNOTATED_OBJECT__SYNONYMS:
        return getSynonyms();
      case obodatamodelPackage.ANNOTATED_OBJECT__DBXREFS:
        return getDbxrefs();
      case obodatamodelPackage.ANNOTATED_OBJECT__COMMENT:
        return getComment();
      case obodatamodelPackage.ANNOTATED_OBJECT__COMMENT_EXTENSION:
        return getCommentExtension();
      case obodatamodelPackage.ANNOTATED_OBJECT__OBSOLETE:
        return isObsolete();
      case obodatamodelPackage.ANNOTATED_OBJECT__REPLACED_BY:
        return getReplacedBy();
      case obodatamodelPackage.ANNOTATED_OBJECT__CONSIDER_REPLACEMENTS:
        return getConsiderReplacements();
      case obodatamodelPackage.ANNOTATED_OBJECT__CONSIDER_EXTENSION:
        if (coreType) return getConsiderExtension();
        else return getConsiderExtension().map();
      case obodatamodelPackage.ANNOTATED_OBJECT__REPLACED_BY_EXTENSION:
        if (coreType) return getReplacedByExtension();
        else return getReplacedByExtension().map();
      case obodatamodelPackage.ANNOTATED_OBJECT__OBSOLETE_EXTENSION:
        return getObsoleteExtension();
      case obodatamodelPackage.ANNOTATED_OBJECT__DEFINITION:
        return getDefinition();
      case obodatamodelPackage.ANNOTATED_OBJECT__DEF_DBXREFS:
        return getDefDbxrefs();
      case obodatamodelPackage.ANNOTATED_OBJECT__DEFINITION_EXTENSION:
        return getDefinitionExtension();
      case obodatamodelPackage.ANNOTATED_OBJECT__SUBSETS:
        return getSubsets();
      case obodatamodelPackage.ANNOTATED_OBJECT__CATEGORY_EXTENSIONS:
        if (coreType) return getCategoryExtensions();
        else return getCategoryExtensions().map();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATED_BY:
        setCreatedBy((String)newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATED_BY_EXTENSION:
        setCreatedByExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFIED_BY:
        setModifiedBy((String)newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFIED_BY_EXTENSION:
        setModifiedByExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATION_DATE:
        setCreationDate((Date)newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATION_DATE_EXTENSION:
        setCreationDateExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFICATION_DATE:
        setModificationDate((Date)newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFICATION_DATE_EXTENSION:
        setModificationDateExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__SECONDARY_IDS:
        getSecondaryIds().clear();
        getSecondaryIds().addAll((Collection<? extends String>)newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__SECONDARY_ID_EXTENSION:
        ((EStructuralFeature.Setting)getSecondaryIdExtension()).set(newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__SYNONYMS:
        getSynonyms().clear();
        getSynonyms().addAll((Collection<? extends Synonym>)newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__DBXREFS:
        getDbxrefs().clear();
        getDbxrefs().addAll((Collection<? extends Dbxref>)newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__COMMENT:
        setComment((String)newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__COMMENT_EXTENSION:
        setCommentExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__OBSOLETE:
        setObsolete((Boolean)newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__REPLACED_BY:
        getReplacedBy().clear();
        getReplacedBy().addAll((Collection<? extends ObsoletableObject>)newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__CONSIDER_REPLACEMENTS:
        getConsiderReplacements().clear();
        getConsiderReplacements().addAll((Collection<? extends ObsoletableObject>)newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__CONSIDER_EXTENSION:
        ((EStructuralFeature.Setting)getConsiderExtension()).set(newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__REPLACED_BY_EXTENSION:
        ((EStructuralFeature.Setting)getReplacedByExtension()).set(newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__OBSOLETE_EXTENSION:
        setObsoleteExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__DEFINITION:
        setDefinition((String)newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__DEF_DBXREFS:
        getDefDbxrefs().clear();
        getDefDbxrefs().addAll((Collection<? extends Dbxref>)newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__DEFINITION_EXTENSION:
        setDefinitionExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__SUBSETS:
        getSubsets().clear();
        getSubsets().addAll((Collection<? extends TermSubset>)newValue);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__CATEGORY_EXTENSIONS:
        ((EStructuralFeature.Setting)getCategoryExtensions()).set(newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATED_BY:
        setCreatedBy(CREATED_BY_EDEFAULT);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATED_BY_EXTENSION:
        setCreatedByExtension((NestedValue)null);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFIED_BY:
        setModifiedBy(MODIFIED_BY_EDEFAULT);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFIED_BY_EXTENSION:
        setModifiedByExtension((NestedValue)null);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATION_DATE:
        setCreationDate(CREATION_DATE_EDEFAULT);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATION_DATE_EXTENSION:
        setCreationDateExtension((NestedValue)null);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFICATION_DATE:
        setModificationDate(MODIFICATION_DATE_EDEFAULT);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFICATION_DATE_EXTENSION:
        setModificationDateExtension((NestedValue)null);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__SECONDARY_IDS:
        getSecondaryIds().clear();
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__SECONDARY_ID_EXTENSION:
        getSecondaryIdExtension().clear();
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__SYNONYMS:
        getSynonyms().clear();
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__DBXREFS:
        getDbxrefs().clear();
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__COMMENT:
        setComment(COMMENT_EDEFAULT);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__COMMENT_EXTENSION:
        setCommentExtension((NestedValue)null);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__OBSOLETE:
        setObsolete(OBSOLETE_EDEFAULT);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__REPLACED_BY:
        getReplacedBy().clear();
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__CONSIDER_REPLACEMENTS:
        getConsiderReplacements().clear();
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__CONSIDER_EXTENSION:
        getConsiderExtension().clear();
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__REPLACED_BY_EXTENSION:
        getReplacedByExtension().clear();
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__OBSOLETE_EXTENSION:
        setObsoleteExtension((NestedValue)null);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__DEFINITION:
        setDefinition(DEFINITION_EDEFAULT);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__DEF_DBXREFS:
        getDefDbxrefs().clear();
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__DEFINITION_EXTENSION:
        setDefinitionExtension((NestedValue)null);
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__SUBSETS:
        getSubsets().clear();
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__CATEGORY_EXTENSIONS:
        getCategoryExtensions().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATED_BY:
        return CREATED_BY_EDEFAULT == null ? createdBy != null : !CREATED_BY_EDEFAULT.equals(createdBy);
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATED_BY_EXTENSION:
        return createdByExtension != null;
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFIED_BY:
        return MODIFIED_BY_EDEFAULT == null ? modifiedBy != null : !MODIFIED_BY_EDEFAULT.equals(modifiedBy);
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFIED_BY_EXTENSION:
        return modifiedByExtension != null;
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATION_DATE:
        return CREATION_DATE_EDEFAULT == null ? creationDate != null : !CREATION_DATE_EDEFAULT.equals(creationDate);
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATION_DATE_EXTENSION:
        return creationDateExtension != null;
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFICATION_DATE:
        return MODIFICATION_DATE_EDEFAULT == null ? modificationDate != null : !MODIFICATION_DATE_EDEFAULT.equals(modificationDate);
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFICATION_DATE_EXTENSION:
        return modificationDateExtension != null;
      case obodatamodelPackage.ANNOTATED_OBJECT__SECONDARY_IDS:
        return secondaryIds != null && !secondaryIds.isEmpty();
      case obodatamodelPackage.ANNOTATED_OBJECT__SECONDARY_ID_EXTENSION:
        return secondaryIdExtension != null && !secondaryIdExtension.isEmpty();
      case obodatamodelPackage.ANNOTATED_OBJECT__SYNONYMS:
        return synonyms != null && !synonyms.isEmpty();
      case obodatamodelPackage.ANNOTATED_OBJECT__DBXREFS:
        return dbxrefs != null && !dbxrefs.isEmpty();
      case obodatamodelPackage.ANNOTATED_OBJECT__COMMENT:
        return COMMENT_EDEFAULT == null ? comment != null : !COMMENT_EDEFAULT.equals(comment);
      case obodatamodelPackage.ANNOTATED_OBJECT__COMMENT_EXTENSION:
        return commentExtension != null;
      case obodatamodelPackage.ANNOTATED_OBJECT__OBSOLETE:
        return obsolete != OBSOLETE_EDEFAULT;
      case obodatamodelPackage.ANNOTATED_OBJECT__REPLACED_BY:
        return replacedBy != null && !replacedBy.isEmpty();
      case obodatamodelPackage.ANNOTATED_OBJECT__CONSIDER_REPLACEMENTS:
        return considerReplacements != null && !considerReplacements.isEmpty();
      case obodatamodelPackage.ANNOTATED_OBJECT__CONSIDER_EXTENSION:
        return considerExtension != null && !considerExtension.isEmpty();
      case obodatamodelPackage.ANNOTATED_OBJECT__REPLACED_BY_EXTENSION:
        return replacedByExtension != null && !replacedByExtension.isEmpty();
      case obodatamodelPackage.ANNOTATED_OBJECT__OBSOLETE_EXTENSION:
        return obsoleteExtension != null;
      case obodatamodelPackage.ANNOTATED_OBJECT__DEFINITION:
        return DEFINITION_EDEFAULT == null ? definition != null : !DEFINITION_EDEFAULT.equals(definition);
      case obodatamodelPackage.ANNOTATED_OBJECT__DEF_DBXREFS:
        return defDbxrefs != null && !defDbxrefs.isEmpty();
      case obodatamodelPackage.ANNOTATED_OBJECT__DEFINITION_EXTENSION:
        return definitionExtension != null;
      case obodatamodelPackage.ANNOTATED_OBJECT__SUBSETS:
        return subsets != null && !subsets.isEmpty();
      case obodatamodelPackage.ANNOTATED_OBJECT__CATEGORY_EXTENSIONS:
        return categoryExtensions != null && !categoryExtensions.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
    if (baseClass == ModificationMetadataObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.ANNOTATED_OBJECT__CREATED_BY: return obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY;
        case obodatamodelPackage.ANNOTATED_OBJECT__CREATED_BY_EXTENSION: return obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION;
        case obodatamodelPackage.ANNOTATED_OBJECT__MODIFIED_BY: return obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY;
        case obodatamodelPackage.ANNOTATED_OBJECT__MODIFIED_BY_EXTENSION: return obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION;
        case obodatamodelPackage.ANNOTATED_OBJECT__CREATION_DATE: return obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE;
        case obodatamodelPackage.ANNOTATED_OBJECT__CREATION_DATE_EXTENSION: return obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION;
        case obodatamodelPackage.ANNOTATED_OBJECT__MODIFICATION_DATE: return obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE;
        case obodatamodelPackage.ANNOTATED_OBJECT__MODIFICATION_DATE_EXTENSION: return obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION;
        default: return -1;
      }
    }
    if (baseClass == MultiIDObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.ANNOTATED_OBJECT__SECONDARY_IDS: return obodatamodelPackage.MULTI_ID_OBJECT__SECONDARY_IDS;
        case obodatamodelPackage.ANNOTATED_OBJECT__SECONDARY_ID_EXTENSION: return obodatamodelPackage.MULTI_ID_OBJECT__SECONDARY_ID_EXTENSION;
        default: return -1;
      }
    }
    if (baseClass == SynonymedObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.ANNOTATED_OBJECT__SYNONYMS: return obodatamodelPackage.SYNONYMED_OBJECT__SYNONYMS;
        default: return -1;
      }
    }
    if (baseClass == DbxrefedObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.ANNOTATED_OBJECT__DBXREFS: return obodatamodelPackage.DBXREFED_OBJECT__DBXREFS;
        default: return -1;
      }
    }
    if (baseClass == CommentedObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.ANNOTATED_OBJECT__COMMENT: return obodatamodelPackage.COMMENTED_OBJECT__COMMENT;
        case obodatamodelPackage.ANNOTATED_OBJECT__COMMENT_EXTENSION: return obodatamodelPackage.COMMENTED_OBJECT__COMMENT_EXTENSION;
        default: return -1;
      }
    }
    if (baseClass == ObsoletableObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.ANNOTATED_OBJECT__OBSOLETE: return obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE;
        case obodatamodelPackage.ANNOTATED_OBJECT__REPLACED_BY: return obodatamodelPackage.OBSOLETABLE_OBJECT__REPLACED_BY;
        case obodatamodelPackage.ANNOTATED_OBJECT__CONSIDER_REPLACEMENTS: return obodatamodelPackage.OBSOLETABLE_OBJECT__CONSIDER_REPLACEMENTS;
        case obodatamodelPackage.ANNOTATED_OBJECT__CONSIDER_EXTENSION: return obodatamodelPackage.OBSOLETABLE_OBJECT__CONSIDER_EXTENSION;
        case obodatamodelPackage.ANNOTATED_OBJECT__REPLACED_BY_EXTENSION: return obodatamodelPackage.OBSOLETABLE_OBJECT__REPLACED_BY_EXTENSION;
        case obodatamodelPackage.ANNOTATED_OBJECT__OBSOLETE_EXTENSION: return obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE_EXTENSION;
        default: return -1;
      }
    }
    if (baseClass == DefinedObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.ANNOTATED_OBJECT__DEFINITION: return obodatamodelPackage.DEFINED_OBJECT__DEFINITION;
        case obodatamodelPackage.ANNOTATED_OBJECT__DEF_DBXREFS: return obodatamodelPackage.DEFINED_OBJECT__DEF_DBXREFS;
        case obodatamodelPackage.ANNOTATED_OBJECT__DEFINITION_EXTENSION: return obodatamodelPackage.DEFINED_OBJECT__DEFINITION_EXTENSION;
        default: return -1;
      }
    }
    if (baseClass == SubsetObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.ANNOTATED_OBJECT__SUBSETS: return obodatamodelPackage.SUBSET_OBJECT__SUBSETS;
        case obodatamodelPackage.ANNOTATED_OBJECT__CATEGORY_EXTENSIONS: return obodatamodelPackage.SUBSET_OBJECT__CATEGORY_EXTENSIONS;
        default: return -1;
      }
    }
    return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
    if (baseClass == ModificationMetadataObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY: return obodatamodelPackage.ANNOTATED_OBJECT__CREATED_BY;
        case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION: return obodatamodelPackage.ANNOTATED_OBJECT__CREATED_BY_EXTENSION;
        case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY: return obodatamodelPackage.ANNOTATED_OBJECT__MODIFIED_BY;
        case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION: return obodatamodelPackage.ANNOTATED_OBJECT__MODIFIED_BY_EXTENSION;
        case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE: return obodatamodelPackage.ANNOTATED_OBJECT__CREATION_DATE;
        case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION: return obodatamodelPackage.ANNOTATED_OBJECT__CREATION_DATE_EXTENSION;
        case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE: return obodatamodelPackage.ANNOTATED_OBJECT__MODIFICATION_DATE;
        case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION: return obodatamodelPackage.ANNOTATED_OBJECT__MODIFICATION_DATE_EXTENSION;
        default: return -1;
      }
    }
    if (baseClass == MultiIDObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.MULTI_ID_OBJECT__SECONDARY_IDS: return obodatamodelPackage.ANNOTATED_OBJECT__SECONDARY_IDS;
        case obodatamodelPackage.MULTI_ID_OBJECT__SECONDARY_ID_EXTENSION: return obodatamodelPackage.ANNOTATED_OBJECT__SECONDARY_ID_EXTENSION;
        default: return -1;
      }
    }
    if (baseClass == SynonymedObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.SYNONYMED_OBJECT__SYNONYMS: return obodatamodelPackage.ANNOTATED_OBJECT__SYNONYMS;
        default: return -1;
      }
    }
    if (baseClass == DbxrefedObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.DBXREFED_OBJECT__DBXREFS: return obodatamodelPackage.ANNOTATED_OBJECT__DBXREFS;
        default: return -1;
      }
    }
    if (baseClass == CommentedObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.COMMENTED_OBJECT__COMMENT: return obodatamodelPackage.ANNOTATED_OBJECT__COMMENT;
        case obodatamodelPackage.COMMENTED_OBJECT__COMMENT_EXTENSION: return obodatamodelPackage.ANNOTATED_OBJECT__COMMENT_EXTENSION;
        default: return -1;
      }
    }
    if (baseClass == ObsoletableObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE: return obodatamodelPackage.ANNOTATED_OBJECT__OBSOLETE;
        case obodatamodelPackage.OBSOLETABLE_OBJECT__REPLACED_BY: return obodatamodelPackage.ANNOTATED_OBJECT__REPLACED_BY;
        case obodatamodelPackage.OBSOLETABLE_OBJECT__CONSIDER_REPLACEMENTS: return obodatamodelPackage.ANNOTATED_OBJECT__CONSIDER_REPLACEMENTS;
        case obodatamodelPackage.OBSOLETABLE_OBJECT__CONSIDER_EXTENSION: return obodatamodelPackage.ANNOTATED_OBJECT__CONSIDER_EXTENSION;
        case obodatamodelPackage.OBSOLETABLE_OBJECT__REPLACED_BY_EXTENSION: return obodatamodelPackage.ANNOTATED_OBJECT__REPLACED_BY_EXTENSION;
        case obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE_EXTENSION: return obodatamodelPackage.ANNOTATED_OBJECT__OBSOLETE_EXTENSION;
        default: return -1;
      }
    }
    if (baseClass == DefinedObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.DEFINED_OBJECT__DEFINITION: return obodatamodelPackage.ANNOTATED_OBJECT__DEFINITION;
        case obodatamodelPackage.DEFINED_OBJECT__DEF_DBXREFS: return obodatamodelPackage.ANNOTATED_OBJECT__DEF_DBXREFS;
        case obodatamodelPackage.DEFINED_OBJECT__DEFINITION_EXTENSION: return obodatamodelPackage.ANNOTATED_OBJECT__DEFINITION_EXTENSION;
        default: return -1;
      }
    }
    if (baseClass == SubsetObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.SUBSET_OBJECT__SUBSETS: return obodatamodelPackage.ANNOTATED_OBJECT__SUBSETS;
        case obodatamodelPackage.SUBSET_OBJECT__CATEGORY_EXTENSIONS: return obodatamodelPackage.ANNOTATED_OBJECT__CATEGORY_EXTENSIONS;
        default: return -1;
      }
    }
    return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (createdBy: ");
    result.append(createdBy);
    result.append(", modifiedBy: ");
    result.append(modifiedBy);
    result.append(", creationDate: ");
    result.append(creationDate);
    result.append(", modificationDate: ");
    result.append(modificationDate);
    result.append(", secondaryIds: ");
    result.append(secondaryIds);
    result.append(", comment: ");
    result.append(comment);
    result.append(", obsolete: ");
    result.append(obsolete);
    result.append(", definition: ");
    result.append(definition);
    result.append(')');
    return result.toString();
  }

} //AnnotatedObjectImpl
