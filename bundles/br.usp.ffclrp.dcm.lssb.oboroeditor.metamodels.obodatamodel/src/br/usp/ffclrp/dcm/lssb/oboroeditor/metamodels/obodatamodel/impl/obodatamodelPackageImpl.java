/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.AnnotatedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.CommentedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingProperty;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Datatype;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DatatypeValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DbxrefedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPath;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPathSpec;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObjectIndex;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Impliable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Instance;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Link;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkLinkedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ListOfProperties;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MultiIDObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MutableLinkDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NamespacedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOClass;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectFactory;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectField;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OboPropertyValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PathCapable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.RootAlgorithm;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Serializable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SubsetObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Type;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Value;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueLink;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelFactory;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.util.obodatamodelValidator;

import java.util.Date;
import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class obodatamodelPackageImpl extends EPackageImpl implements obodatamodelPackage {
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass annotatedObjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass commentedObjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass danglingObjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass danglingPropertyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass datatypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass datatypeValueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass dbxrefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass dbxrefedObjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass definedObjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fieldPathEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass fieldPathSpecEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass identifiableObjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass identifiedObjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass identifiedObjectIndexEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass impliableEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass instanceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass linkEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass linkDatabaseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass linkLinkedObjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass linkedObjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass modificationMetadataObjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass multiIDObjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass mutableLinkDatabaseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass namespaceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass namespacedObjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass nestedValueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass oboClassEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass oboObjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass oboPropertyEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass oboRestrictionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass oboSessionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass objectFactoryEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass objectFieldEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass obsoletableObjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass pathCapableEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass propertyValueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass relationshipEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass rootAlgorithmEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass subsetObjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass synonymEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass synonymTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass synonymedObjectEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass termSubsetEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass typeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass unknownStanzaEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass valueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass valueDatabaseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass valueLinkEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass cloneableEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass serializableEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass comparableEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass stringToNestedValueMapEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass obsoletableObjectToNestedValueMapEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass termSubsetToNestedValueMapEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass idSpacesMapEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass listOfPropertiesEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass oboPropertyValueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType dateEDataType = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EDataType ecoreObjectEDataType = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private obodatamodelPackageImpl() {
    super(eNS_URI, obodatamodelFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link obodatamodelPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static obodatamodelPackage init() {
    if (isInited) return (obodatamodelPackage)EPackage.Registry.INSTANCE.getEPackage(obodatamodelPackage.eNS_URI);

    // Obtain or create and register package
    obodatamodelPackageImpl theobodatamodelPackage = (obodatamodelPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof obodatamodelPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new obodatamodelPackageImpl());

    isInited = true;

    // Create package meta-data objects
    theobodatamodelPackage.createPackageContents();

    // Initialize created meta-data
    theobodatamodelPackage.initializePackageContents();

    // Register package validator
    EValidator.Registry.INSTANCE.put
      (theobodatamodelPackage, 
       new EValidator.Descriptor() {
         public EValidator getEValidator() {
           return obodatamodelValidator.INSTANCE;
         }
       });

    // Mark meta-data to indicate it can't be changed
    theobodatamodelPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(obodatamodelPackage.eNS_URI, theobodatamodelPackage);
    return theobodatamodelPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAnnotatedObject() {
    return annotatedObjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCommentedObject() {
    return commentedObjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getCommentedObject_Comment() {
    return (EAttribute)commentedObjectEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCommentedObject_CommentExtension() {
    return (EReference)commentedObjectEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDanglingObject() {
    return danglingObjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDanglingObject_Dangling() {
    return (EAttribute)danglingObjectEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDanglingProperty() {
    return danglingPropertyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDatatype() {
    return datatypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDatatype_Abstract() {
    return (EAttribute)datatypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDatatypeValue() {
    return datatypeValueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDatatypeValue_Value() {
    return (EAttribute)datatypeValueEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDbxref() {
    return dbxrefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDbxref_NestedValue() {
    return (EReference)dbxrefEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDbxref_DefRef() {
    return (EAttribute)dbxrefEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDbxref_Desc() {
    return (EAttribute)dbxrefEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDbxref_Type() {
    return (EAttribute)dbxrefEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDbxref_Synonym() {
    return (EReference)dbxrefEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDbxref_DatabaseID() {
    return (EAttribute)dbxrefEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDbxref_Database() {
    return (EAttribute)dbxrefEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDbxrefedObject() {
    return dbxrefedObjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDbxrefedObject_Dbxrefs() {
    return (EReference)dbxrefedObjectEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDefinedObject() {
    return definedObjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDefinedObject_Definition() {
    return (EAttribute)definedObjectEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDefinedObject_DefDbxrefs() {
    return (EReference)definedObjectEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDefinedObject_DefinitionExtension() {
    return (EReference)definedObjectEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFieldPath() {
    return fieldPathEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFieldPathSpec() {
    return fieldPathSpecEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIdentifiableObject() {
    return identifiableObjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIdentifiableObject_Id() {
    return (EAttribute)identifiableObjectEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIdentifiableObject_Anonymous() {
    return (EAttribute)identifiableObjectEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIdentifiedObject() {
    return identifiedObjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIdentifiedObject_TypeExtension() {
    return (EReference)identifiedObjectEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIdentifiedObject_BuiltIn() {
    return (EAttribute)identifiedObjectEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIdentifiedObject_Name() {
    return (EAttribute)identifiedObjectEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIdentifiedObject_NameExtension() {
    return (EReference)identifiedObjectEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIdentifiedObject_IdExtension() {
    return (EReference)identifiedObjectEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIdentifiedObject_AnonymousExtension() {
    return (EReference)identifiedObjectEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIdentifiedObject_PropertyValues() {
    return (EReference)identifiedObjectEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIdentifiedObjectIndex() {
    return identifiedObjectIndexEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getImpliable() {
    return impliableEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getImpliable_Implied() {
    return (EAttribute)impliableEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInstance() {
    return instanceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getInstance_OboPropertiesValues() {
    return (EReference)instanceEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLink() {
    return linkEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLink_Namespace() {
    return (EReference)linkEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLinkDatabase() {
    return linkDatabaseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLinkDatabase_Session() {
    return (EReference)linkDatabaseEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLinkDatabase_Objects() {
    return (EReference)linkDatabaseEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLinkDatabase_Properties() {
    return (EReference)linkDatabaseEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLinkLinkedObject() {
    return linkLinkedObjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLinkLinkedObject_Link() {
    return (EReference)linkLinkedObjectEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLinkedObject() {
    return linkedObjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLinkedObject_Parents() {
    return (EReference)linkedObjectEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLinkedObject_Children() {
    return (EReference)linkedObjectEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getModificationMetadataObject() {
    return modificationMetadataObjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getModificationMetadataObject_CreatedBy() {
    return (EAttribute)modificationMetadataObjectEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModificationMetadataObject_CreatedByExtension() {
    return (EReference)modificationMetadataObjectEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getModificationMetadataObject_ModifiedBy() {
    return (EAttribute)modificationMetadataObjectEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModificationMetadataObject_ModifiedByExtension() {
    return (EReference)modificationMetadataObjectEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getModificationMetadataObject_CreationDate() {
    return (EAttribute)modificationMetadataObjectEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModificationMetadataObject_CreationDateExtension() {
    return (EReference)modificationMetadataObjectEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getModificationMetadataObject_ModificationDate() {
    return (EAttribute)modificationMetadataObjectEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModificationMetadataObject_ModificationDateExtension() {
    return (EReference)modificationMetadataObjectEClass.getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMultiIDObject() {
    return multiIDObjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMultiIDObject_SecondaryIds() {
    return (EAttribute)multiIDObjectEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMultiIDObject_SecondaryIdExtension() {
    return (EReference)multiIDObjectEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMutableLinkDatabase() {
    return mutableLinkDatabaseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMutableLinkDatabase_IdentifiedObjectIndex() {
    return (EReference)mutableLinkDatabaseEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNamespace() {
    return namespaceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getNamespace_Id() {
    return (EAttribute)namespaceEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getNamespace_Path() {
    return (EAttribute)namespaceEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getNamespace_PrivateId() {
    return (EAttribute)namespaceEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNamespacedObject() {
    return namespacedObjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getNamespacedObject_Namespace() {
    return (EReference)namespacedObjectEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getNamespacedObject_NamespaceExtension() {
    return (EReference)namespacedObjectEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNestedValue() {
    return nestedValueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getNestedValue_Name() {
    return (EAttribute)nestedValueEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getNestedValue_PropertyValues() {
    return (EReference)nestedValueEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getNestedValue_SuggestedComment() {
    return (EAttribute)nestedValueEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getOBOClass() {
    return oboClassEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getOBOObject() {
    return oboObjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getOBOProperty() {
    return oboPropertyEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOBOProperty_Cyclic() {
    return (EAttribute)oboPropertyEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOBOProperty_Symmetric() {
    return (EAttribute)oboPropertyEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOBOProperty_Transitive() {
    return (EAttribute)oboPropertyEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOBOProperty_Reflexive() {
    return (EAttribute)oboPropertyEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOBOProperty_AlwaysImpliesInverse() {
    return (EAttribute)oboPropertyEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOBOProperty_Dummy() {
    return (EAttribute)oboPropertyEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOBOProperty_MetadataTag() {
    return (EAttribute)oboPropertyEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOProperty_CyclicExtension() {
    return (EReference)oboPropertyEClass.getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOProperty_SymmetricExtension() {
    return (EReference)oboPropertyEClass.getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOProperty_TransitiveExtension() {
    return (EReference)oboPropertyEClass.getEStructuralFeatures().get(9);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOProperty_ReflexiveExtension() {
    return (EReference)oboPropertyEClass.getEStructuralFeatures().get(10);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOProperty_AlwaysImpliesInverseExtension() {
    return (EReference)oboPropertyEClass.getEStructuralFeatures().get(11);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOProperty_DomainExtension() {
    return (EReference)oboPropertyEClass.getEStructuralFeatures().get(12);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOProperty_RangeExtension() {
    return (EReference)oboPropertyEClass.getEStructuralFeatures().get(13);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOProperty_Range() {
    return (EReference)oboPropertyEClass.getEStructuralFeatures().get(14);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOProperty_Domain() {
    return (EReference)oboPropertyEClass.getEStructuralFeatures().get(15);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOBOProperty_NonInheritable() {
    return (EAttribute)oboPropertyEClass.getEStructuralFeatures().get(16);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOProperty_DisjointOver() {
    return (EReference)oboPropertyEClass.getEStructuralFeatures().get(17);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOProperty_TransitiveOver() {
    return (EReference)oboPropertyEClass.getEStructuralFeatures().get(18);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOProperty_HoldsOverChains() {
    return (EReference)oboPropertyEClass.getEStructuralFeatures().get(19);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getOBORestriction() {
    return oboRestrictionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOBORestriction_Cardinality() {
    return (EAttribute)oboRestrictionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOBORestriction_MaxCardinality() {
    return (EAttribute)oboRestrictionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOBORestriction_MinCardinality() {
    return (EAttribute)oboRestrictionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOBORestriction_Completes() {
    return (EAttribute)oboRestrictionEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOBORestriction_InverseCompletes() {
    return (EAttribute)oboRestrictionEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOBORestriction_NecessarilyTrue() {
    return (EAttribute)oboRestrictionEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOBORestriction_InverseNecessarilyTrue() {
    return (EAttribute)oboRestrictionEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBORestriction_AdditionalArguments() {
    return (EReference)oboRestrictionEClass.getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getOBOSession() {
    return oboSessionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOBOSession_Id() {
    return (EAttribute)oboSessionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOSession_Objects() {
    return (EReference)oboSessionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOSession_LinkDatabase() {
    return (EReference)oboSessionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOSession_DefaultNamespace() {
    return (EReference)oboSessionEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOSession_Namespaces() {
    return (EReference)oboSessionEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOSession_PropertyValues() {
    return (EReference)oboSessionEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOSession_UnknownStanzas() {
    return (EReference)oboSessionEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOSession_Subsets() {
    return (EReference)oboSessionEClass.getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOSession_Categories() {
    return (EReference)oboSessionEClass.getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOSession_SynonymTypes() {
    return (EReference)oboSessionEClass.getEStructuralFeatures().get(9);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOSession_SynonymCategories() {
    return (EReference)oboSessionEClass.getEStructuralFeatures().get(10);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOBOSession_CurrentFilenames() {
    return (EAttribute)oboSessionEClass.getEStructuralFeatures().get(11);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOBOSession_CurrentUser() {
    return (EAttribute)oboSessionEClass.getEStructuralFeatures().get(12);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOSession_IdSpaces() {
    return (EReference)oboSessionEClass.getEStructuralFeatures().get(13);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOBOSession_LoadRemark() {
    return (EAttribute)oboSessionEClass.getEStructuralFeatures().get(14);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOBOSession_AllDbxRefsContainer() {
    return (EReference)oboSessionEClass.getEStructuralFeatures().get(15);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getObjectFactory() {
    return objectFactoryEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getObjectField() {
    return objectFieldEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getObjectField_Values() {
    return (EAttribute)objectFieldEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getObsoletableObject() {
    return obsoletableObjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getObsoletableObject_Obsolete() {
    return (EAttribute)obsoletableObjectEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getObsoletableObject_ReplacedBy() {
    return (EReference)obsoletableObjectEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getObsoletableObject_ConsiderReplacements() {
    return (EReference)obsoletableObjectEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getObsoletableObject_ConsiderExtension() {
    return (EReference)obsoletableObjectEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getObsoletableObject_ReplacedByExtension() {
    return (EReference)obsoletableObjectEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getObsoletableObject_ObsoleteExtension() {
    return (EReference)obsoletableObjectEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPathCapable() {
    return pathCapableEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPropertyValue() {
    return propertyValueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPropertyValue_Property() {
    return (EAttribute)propertyValueEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPropertyValue_Value() {
    return (EAttribute)propertyValueEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPropertyValue_LineNumber() {
    return (EAttribute)propertyValueEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPropertyValue_Filename() {
    return (EAttribute)propertyValueEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getPropertyValue_Line() {
    return (EAttribute)propertyValueEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRelationship() {
    return relationshipEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRelationship_Child() {
    return (EReference)relationshipEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRelationship_Parent() {
    return (EReference)relationshipEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRelationship_Type() {
    return (EReference)relationshipEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRelationship_NestedValue() {
    return (EReference)relationshipEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRootAlgorithm() {
    return rootAlgorithmEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSubsetObject() {
    return subsetObjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSubsetObject_Subsets() {
    return (EReference)subsetObjectEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSubsetObject_CategoryExtensions() {
    return (EReference)subsetObjectEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSynonym() {
    return synonymEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSynonym_UNKNOWN_SCOPE() {
    return (EAttribute)synonymEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSynonym_RELATED_SYNONYM() {
    return (EAttribute)synonymEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSynonym_EXACT_SYNONYM() {
    return (EAttribute)synonymEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSynonym_NARROW_SYNONYM() {
    return (EAttribute)synonymEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSynonym_BROAD_SYNONYM() {
    return (EAttribute)synonymEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSynonym_SynonymType() {
    return (EReference)synonymEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSynonym_NestedValue() {
    return (EReference)synonymEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSynonym_Scope() {
    return (EAttribute)synonymEClass.getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSynonym_Xrefs() {
    return (EReference)synonymEClass.getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSynonym_Text() {
    return (EAttribute)synonymEClass.getEStructuralFeatures().get(9);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSynonymType() {
    return synonymTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSynonymType_Id() {
    return (EAttribute)synonymTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSynonymType_Name() {
    return (EAttribute)synonymTypeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSynonymType_Scope() {
    return (EAttribute)synonymTypeEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSynonymedObject() {
    return synonymedObjectEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSynonymedObject_Synonyms() {
    return (EReference)synonymedObjectEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTermSubset() {
    return termSubsetEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTermSubset_Name() {
    return (EAttribute)termSubsetEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTermSubset_Desc() {
    return (EAttribute)termSubsetEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getType() {
    return typeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUnknownStanza() {
    return unknownStanzaEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnknownStanza_Namespace() {
    return (EReference)unknownStanzaEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnknownStanza_PropertyValues() {
    return (EReference)unknownStanzaEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUnknownStanza_NestedValues() {
    return (EReference)unknownStanzaEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getUnknownStanza_Stanza() {
    return (EAttribute)unknownStanzaEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getValue() {
    return valueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getValue_Type() {
    return (EReference)valueEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getValueDatabase() {
    return valueDatabaseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getValueDatabase_Objects() {
    return (EReference)valueDatabaseEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getValueLink() {
    return valueLinkEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getValueLink_Value() {
    return (EReference)valueLinkEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCloneable() {
    return cloneableEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSerializable() {
    return serializableEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComparable() {
    return comparableEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStringToNestedValueMap() {
    return stringToNestedValueMapEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getStringToNestedValueMap_Key() {
    return (EAttribute)stringToNestedValueMapEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getStringToNestedValueMap_Value() {
    return (EReference)stringToNestedValueMapEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getObsoletableObjectToNestedValueMap() {
    return obsoletableObjectToNestedValueMapEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getObsoletableObjectToNestedValueMap_Key() {
    return (EReference)obsoletableObjectToNestedValueMapEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getObsoletableObjectToNestedValueMap_Value() {
    return (EReference)obsoletableObjectToNestedValueMapEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTermSubsetToNestedValueMap() {
    return termSubsetToNestedValueMapEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTermSubsetToNestedValueMap_Key() {
    return (EReference)termSubsetToNestedValueMapEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTermSubsetToNestedValueMap_Value() {
    return (EReference)termSubsetToNestedValueMapEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIdSpacesMap() {
    return idSpacesMapEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIdSpacesMap_Key() {
    return (EAttribute)idSpacesMapEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIdSpacesMap_Value() {
    return (EAttribute)idSpacesMapEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getListOfProperties() {
    return listOfPropertiesEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getListOfProperties_Properties() {
    return (EReference)listOfPropertiesEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getOboPropertyValue() {
    return oboPropertyValueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOboPropertyValue_Property() {
    return (EReference)oboPropertyValueEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOboPropertyValue_Value() {
    return (EReference)oboPropertyValueEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getDate() {
    return dateEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EDataType getEcoreObject() {
    return ecoreObjectEDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public obodatamodelFactory getobodatamodelFactory() {
    return (obodatamodelFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents() {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    annotatedObjectEClass = createEClass(ANNOTATED_OBJECT);

    commentedObjectEClass = createEClass(COMMENTED_OBJECT);
    createEAttribute(commentedObjectEClass, COMMENTED_OBJECT__COMMENT);
    createEReference(commentedObjectEClass, COMMENTED_OBJECT__COMMENT_EXTENSION);

    danglingObjectEClass = createEClass(DANGLING_OBJECT);
    createEAttribute(danglingObjectEClass, DANGLING_OBJECT__DANGLING);

    danglingPropertyEClass = createEClass(DANGLING_PROPERTY);

    datatypeEClass = createEClass(DATATYPE);
    createEAttribute(datatypeEClass, DATATYPE__ABSTRACT);

    datatypeValueEClass = createEClass(DATATYPE_VALUE);
    createEAttribute(datatypeValueEClass, DATATYPE_VALUE__VALUE);

    dbxrefEClass = createEClass(DBXREF);
    createEReference(dbxrefEClass, DBXREF__NESTED_VALUE);
    createEAttribute(dbxrefEClass, DBXREF__DEF_REF);
    createEAttribute(dbxrefEClass, DBXREF__DESC);
    createEAttribute(dbxrefEClass, DBXREF__TYPE);
    createEReference(dbxrefEClass, DBXREF__SYNONYM);
    createEAttribute(dbxrefEClass, DBXREF__DATABASE_ID);
    createEAttribute(dbxrefEClass, DBXREF__DATABASE);

    dbxrefedObjectEClass = createEClass(DBXREFED_OBJECT);
    createEReference(dbxrefedObjectEClass, DBXREFED_OBJECT__DBXREFS);

    definedObjectEClass = createEClass(DEFINED_OBJECT);
    createEAttribute(definedObjectEClass, DEFINED_OBJECT__DEFINITION);
    createEReference(definedObjectEClass, DEFINED_OBJECT__DEF_DBXREFS);
    createEReference(definedObjectEClass, DEFINED_OBJECT__DEFINITION_EXTENSION);

    fieldPathEClass = createEClass(FIELD_PATH);

    fieldPathSpecEClass = createEClass(FIELD_PATH_SPEC);

    identifiableObjectEClass = createEClass(IDENTIFIABLE_OBJECT);
    createEAttribute(identifiableObjectEClass, IDENTIFIABLE_OBJECT__ID);
    createEAttribute(identifiableObjectEClass, IDENTIFIABLE_OBJECT__ANONYMOUS);

    identifiedObjectEClass = createEClass(IDENTIFIED_OBJECT);
    createEReference(identifiedObjectEClass, IDENTIFIED_OBJECT__TYPE_EXTENSION);
    createEAttribute(identifiedObjectEClass, IDENTIFIED_OBJECT__BUILT_IN);
    createEAttribute(identifiedObjectEClass, IDENTIFIED_OBJECT__NAME);
    createEReference(identifiedObjectEClass, IDENTIFIED_OBJECT__NAME_EXTENSION);
    createEReference(identifiedObjectEClass, IDENTIFIED_OBJECT__ID_EXTENSION);
    createEReference(identifiedObjectEClass, IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION);
    createEReference(identifiedObjectEClass, IDENTIFIED_OBJECT__PROPERTY_VALUES);

    identifiedObjectIndexEClass = createEClass(IDENTIFIED_OBJECT_INDEX);

    impliableEClass = createEClass(IMPLIABLE);
    createEAttribute(impliableEClass, IMPLIABLE__IMPLIED);

    instanceEClass = createEClass(INSTANCE);
    createEReference(instanceEClass, INSTANCE__OBO_PROPERTIES_VALUES);

    linkEClass = createEClass(LINK);
    createEReference(linkEClass, LINK__NAMESPACE);

    linkDatabaseEClass = createEClass(LINK_DATABASE);
    createEReference(linkDatabaseEClass, LINK_DATABASE__SESSION);
    createEReference(linkDatabaseEClass, LINK_DATABASE__OBJECTS);
    createEReference(linkDatabaseEClass, LINK_DATABASE__PROPERTIES);

    linkLinkedObjectEClass = createEClass(LINK_LINKED_OBJECT);
    createEReference(linkLinkedObjectEClass, LINK_LINKED_OBJECT__LINK);

    linkedObjectEClass = createEClass(LINKED_OBJECT);
    createEReference(linkedObjectEClass, LINKED_OBJECT__PARENTS);
    createEReference(linkedObjectEClass, LINKED_OBJECT__CHILDREN);

    modificationMetadataObjectEClass = createEClass(MODIFICATION_METADATA_OBJECT);
    createEAttribute(modificationMetadataObjectEClass, MODIFICATION_METADATA_OBJECT__CREATED_BY);
    createEReference(modificationMetadataObjectEClass, MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION);
    createEAttribute(modificationMetadataObjectEClass, MODIFICATION_METADATA_OBJECT__MODIFIED_BY);
    createEReference(modificationMetadataObjectEClass, MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION);
    createEAttribute(modificationMetadataObjectEClass, MODIFICATION_METADATA_OBJECT__CREATION_DATE);
    createEReference(modificationMetadataObjectEClass, MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION);
    createEAttribute(modificationMetadataObjectEClass, MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE);
    createEReference(modificationMetadataObjectEClass, MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION);

    multiIDObjectEClass = createEClass(MULTI_ID_OBJECT);
    createEAttribute(multiIDObjectEClass, MULTI_ID_OBJECT__SECONDARY_IDS);
    createEReference(multiIDObjectEClass, MULTI_ID_OBJECT__SECONDARY_ID_EXTENSION);

    mutableLinkDatabaseEClass = createEClass(MUTABLE_LINK_DATABASE);
    createEReference(mutableLinkDatabaseEClass, MUTABLE_LINK_DATABASE__IDENTIFIED_OBJECT_INDEX);

    namespaceEClass = createEClass(NAMESPACE);
    createEAttribute(namespaceEClass, NAMESPACE__ID);
    createEAttribute(namespaceEClass, NAMESPACE__PATH);
    createEAttribute(namespaceEClass, NAMESPACE__PRIVATE_ID);

    namespacedObjectEClass = createEClass(NAMESPACED_OBJECT);
    createEReference(namespacedObjectEClass, NAMESPACED_OBJECT__NAMESPACE);
    createEReference(namespacedObjectEClass, NAMESPACED_OBJECT__NAMESPACE_EXTENSION);

    nestedValueEClass = createEClass(NESTED_VALUE);
    createEAttribute(nestedValueEClass, NESTED_VALUE__NAME);
    createEReference(nestedValueEClass, NESTED_VALUE__PROPERTY_VALUES);
    createEAttribute(nestedValueEClass, NESTED_VALUE__SUGGESTED_COMMENT);

    oboClassEClass = createEClass(OBO_CLASS);

    oboObjectEClass = createEClass(OBO_OBJECT);

    oboPropertyEClass = createEClass(OBO_PROPERTY);
    createEAttribute(oboPropertyEClass, OBO_PROPERTY__CYCLIC);
    createEAttribute(oboPropertyEClass, OBO_PROPERTY__SYMMETRIC);
    createEAttribute(oboPropertyEClass, OBO_PROPERTY__TRANSITIVE);
    createEAttribute(oboPropertyEClass, OBO_PROPERTY__REFLEXIVE);
    createEAttribute(oboPropertyEClass, OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE);
    createEAttribute(oboPropertyEClass, OBO_PROPERTY__DUMMY);
    createEAttribute(oboPropertyEClass, OBO_PROPERTY__METADATA_TAG);
    createEReference(oboPropertyEClass, OBO_PROPERTY__CYCLIC_EXTENSION);
    createEReference(oboPropertyEClass, OBO_PROPERTY__SYMMETRIC_EXTENSION);
    createEReference(oboPropertyEClass, OBO_PROPERTY__TRANSITIVE_EXTENSION);
    createEReference(oboPropertyEClass, OBO_PROPERTY__REFLEXIVE_EXTENSION);
    createEReference(oboPropertyEClass, OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION);
    createEReference(oboPropertyEClass, OBO_PROPERTY__DOMAIN_EXTENSION);
    createEReference(oboPropertyEClass, OBO_PROPERTY__RANGE_EXTENSION);
    createEReference(oboPropertyEClass, OBO_PROPERTY__RANGE);
    createEReference(oboPropertyEClass, OBO_PROPERTY__DOMAIN);
    createEAttribute(oboPropertyEClass, OBO_PROPERTY__NON_INHERITABLE);
    createEReference(oboPropertyEClass, OBO_PROPERTY__DISJOINT_OVER);
    createEReference(oboPropertyEClass, OBO_PROPERTY__TRANSITIVE_OVER);
    createEReference(oboPropertyEClass, OBO_PROPERTY__HOLDS_OVER_CHAINS);

    oboRestrictionEClass = createEClass(OBO_RESTRICTION);
    createEAttribute(oboRestrictionEClass, OBO_RESTRICTION__CARDINALITY);
    createEAttribute(oboRestrictionEClass, OBO_RESTRICTION__MAX_CARDINALITY);
    createEAttribute(oboRestrictionEClass, OBO_RESTRICTION__MIN_CARDINALITY);
    createEAttribute(oboRestrictionEClass, OBO_RESTRICTION__COMPLETES);
    createEAttribute(oboRestrictionEClass, OBO_RESTRICTION__INVERSE_COMPLETES);
    createEAttribute(oboRestrictionEClass, OBO_RESTRICTION__NECESSARILY_TRUE);
    createEAttribute(oboRestrictionEClass, OBO_RESTRICTION__INVERSE_NECESSARILY_TRUE);
    createEReference(oboRestrictionEClass, OBO_RESTRICTION__ADDITIONAL_ARGUMENTS);

    oboSessionEClass = createEClass(OBO_SESSION);
    createEAttribute(oboSessionEClass, OBO_SESSION__ID);
    createEReference(oboSessionEClass, OBO_SESSION__OBJECTS);
    createEReference(oboSessionEClass, OBO_SESSION__LINK_DATABASE);
    createEReference(oboSessionEClass, OBO_SESSION__DEFAULT_NAMESPACE);
    createEReference(oboSessionEClass, OBO_SESSION__NAMESPACES);
    createEReference(oboSessionEClass, OBO_SESSION__PROPERTY_VALUES);
    createEReference(oboSessionEClass, OBO_SESSION__UNKNOWN_STANZAS);
    createEReference(oboSessionEClass, OBO_SESSION__SUBSETS);
    createEReference(oboSessionEClass, OBO_SESSION__CATEGORIES);
    createEReference(oboSessionEClass, OBO_SESSION__SYNONYM_TYPES);
    createEReference(oboSessionEClass, OBO_SESSION__SYNONYM_CATEGORIES);
    createEAttribute(oboSessionEClass, OBO_SESSION__CURRENT_FILENAMES);
    createEAttribute(oboSessionEClass, OBO_SESSION__CURRENT_USER);
    createEReference(oboSessionEClass, OBO_SESSION__ID_SPACES);
    createEAttribute(oboSessionEClass, OBO_SESSION__LOAD_REMARK);
    createEReference(oboSessionEClass, OBO_SESSION__ALL_DBX_REFS_CONTAINER);

    objectFactoryEClass = createEClass(OBJECT_FACTORY);

    objectFieldEClass = createEClass(OBJECT_FIELD);
    createEAttribute(objectFieldEClass, OBJECT_FIELD__VALUES);

    obsoletableObjectEClass = createEClass(OBSOLETABLE_OBJECT);
    createEAttribute(obsoletableObjectEClass, OBSOLETABLE_OBJECT__OBSOLETE);
    createEReference(obsoletableObjectEClass, OBSOLETABLE_OBJECT__REPLACED_BY);
    createEReference(obsoletableObjectEClass, OBSOLETABLE_OBJECT__CONSIDER_REPLACEMENTS);
    createEReference(obsoletableObjectEClass, OBSOLETABLE_OBJECT__CONSIDER_EXTENSION);
    createEReference(obsoletableObjectEClass, OBSOLETABLE_OBJECT__REPLACED_BY_EXTENSION);
    createEReference(obsoletableObjectEClass, OBSOLETABLE_OBJECT__OBSOLETE_EXTENSION);

    pathCapableEClass = createEClass(PATH_CAPABLE);

    propertyValueEClass = createEClass(PROPERTY_VALUE);
    createEAttribute(propertyValueEClass, PROPERTY_VALUE__PROPERTY);
    createEAttribute(propertyValueEClass, PROPERTY_VALUE__VALUE);
    createEAttribute(propertyValueEClass, PROPERTY_VALUE__LINE_NUMBER);
    createEAttribute(propertyValueEClass, PROPERTY_VALUE__FILENAME);
    createEAttribute(propertyValueEClass, PROPERTY_VALUE__LINE);

    relationshipEClass = createEClass(RELATIONSHIP);
    createEReference(relationshipEClass, RELATIONSHIP__CHILD);
    createEReference(relationshipEClass, RELATIONSHIP__PARENT);
    createEReference(relationshipEClass, RELATIONSHIP__TYPE);
    createEReference(relationshipEClass, RELATIONSHIP__NESTED_VALUE);

    rootAlgorithmEClass = createEClass(ROOT_ALGORITHM);

    subsetObjectEClass = createEClass(SUBSET_OBJECT);
    createEReference(subsetObjectEClass, SUBSET_OBJECT__SUBSETS);
    createEReference(subsetObjectEClass, SUBSET_OBJECT__CATEGORY_EXTENSIONS);

    synonymEClass = createEClass(SYNONYM);
    createEAttribute(synonymEClass, SYNONYM__UNKNOWN_SCOPE);
    createEAttribute(synonymEClass, SYNONYM__RELATED_SYNONYM);
    createEAttribute(synonymEClass, SYNONYM__EXACT_SYNONYM);
    createEAttribute(synonymEClass, SYNONYM__NARROW_SYNONYM);
    createEAttribute(synonymEClass, SYNONYM__BROAD_SYNONYM);
    createEReference(synonymEClass, SYNONYM__SYNONYM_TYPE);
    createEReference(synonymEClass, SYNONYM__NESTED_VALUE);
    createEAttribute(synonymEClass, SYNONYM__SCOPE);
    createEReference(synonymEClass, SYNONYM__XREFS);
    createEAttribute(synonymEClass, SYNONYM__TEXT);

    synonymTypeEClass = createEClass(SYNONYM_TYPE);
    createEAttribute(synonymTypeEClass, SYNONYM_TYPE__ID);
    createEAttribute(synonymTypeEClass, SYNONYM_TYPE__NAME);
    createEAttribute(synonymTypeEClass, SYNONYM_TYPE__SCOPE);

    synonymedObjectEClass = createEClass(SYNONYMED_OBJECT);
    createEReference(synonymedObjectEClass, SYNONYMED_OBJECT__SYNONYMS);

    termSubsetEClass = createEClass(TERM_SUBSET);
    createEAttribute(termSubsetEClass, TERM_SUBSET__NAME);
    createEAttribute(termSubsetEClass, TERM_SUBSET__DESC);

    typeEClass = createEClass(TYPE);

    unknownStanzaEClass = createEClass(UNKNOWN_STANZA);
    createEReference(unknownStanzaEClass, UNKNOWN_STANZA__NAMESPACE);
    createEReference(unknownStanzaEClass, UNKNOWN_STANZA__PROPERTY_VALUES);
    createEReference(unknownStanzaEClass, UNKNOWN_STANZA__NESTED_VALUES);
    createEAttribute(unknownStanzaEClass, UNKNOWN_STANZA__STANZA);

    valueEClass = createEClass(VALUE);
    createEReference(valueEClass, VALUE__TYPE);

    valueDatabaseEClass = createEClass(VALUE_DATABASE);
    createEReference(valueDatabaseEClass, VALUE_DATABASE__OBJECTS);

    valueLinkEClass = createEClass(VALUE_LINK);
    createEReference(valueLinkEClass, VALUE_LINK__VALUE);

    cloneableEClass = createEClass(CLONEABLE);

    serializableEClass = createEClass(SERIALIZABLE);

    comparableEClass = createEClass(COMPARABLE);

    stringToNestedValueMapEClass = createEClass(STRING_TO_NESTED_VALUE_MAP);
    createEAttribute(stringToNestedValueMapEClass, STRING_TO_NESTED_VALUE_MAP__KEY);
    createEReference(stringToNestedValueMapEClass, STRING_TO_NESTED_VALUE_MAP__VALUE);

    obsoletableObjectToNestedValueMapEClass = createEClass(OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP);
    createEReference(obsoletableObjectToNestedValueMapEClass, OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP__KEY);
    createEReference(obsoletableObjectToNestedValueMapEClass, OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP__VALUE);

    termSubsetToNestedValueMapEClass = createEClass(TERM_SUBSET_TO_NESTED_VALUE_MAP);
    createEReference(termSubsetToNestedValueMapEClass, TERM_SUBSET_TO_NESTED_VALUE_MAP__KEY);
    createEReference(termSubsetToNestedValueMapEClass, TERM_SUBSET_TO_NESTED_VALUE_MAP__VALUE);

    idSpacesMapEClass = createEClass(ID_SPACES_MAP);
    createEAttribute(idSpacesMapEClass, ID_SPACES_MAP__KEY);
    createEAttribute(idSpacesMapEClass, ID_SPACES_MAP__VALUE);

    listOfPropertiesEClass = createEClass(LIST_OF_PROPERTIES);
    createEReference(listOfPropertiesEClass, LIST_OF_PROPERTIES__PROPERTIES);

    oboPropertyValueEClass = createEClass(OBO_PROPERTY_VALUE);
    createEReference(oboPropertyValueEClass, OBO_PROPERTY_VALUE__PROPERTY);
    createEReference(oboPropertyValueEClass, OBO_PROPERTY_VALUE__VALUE);

    // Create data types
    dateEDataType = createEDataType(DATE);
    ecoreObjectEDataType = createEDataType(ECORE_OBJECT);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents() {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes
    annotatedObjectEClass.getESuperTypes().add(this.getIdentifiedObject());
    annotatedObjectEClass.getESuperTypes().add(this.getModificationMetadataObject());
    annotatedObjectEClass.getESuperTypes().add(this.getMultiIDObject());
    annotatedObjectEClass.getESuperTypes().add(this.getSynonymedObject());
    annotatedObjectEClass.getESuperTypes().add(this.getDbxrefedObject());
    annotatedObjectEClass.getESuperTypes().add(this.getCommentedObject());
    annotatedObjectEClass.getESuperTypes().add(this.getObsoletableObject());
    annotatedObjectEClass.getESuperTypes().add(this.getCloneable());
    annotatedObjectEClass.getESuperTypes().add(this.getSerializable());
    annotatedObjectEClass.getESuperTypes().add(this.getDefinedObject());
    annotatedObjectEClass.getESuperTypes().add(this.getSubsetObject());
    commentedObjectEClass.getESuperTypes().add(this.getIdentifiedObject());
    danglingObjectEClass.getESuperTypes().add(this.getLinkedObject());
    danglingObjectEClass.getESuperTypes().add(this.getType());
    danglingPropertyEClass.getESuperTypes().add(this.getLinkedObject());
    danglingPropertyEClass.getESuperTypes().add(this.getOBOProperty());
    datatypeEClass.getESuperTypes().add(this.getType());
    datatypeValueEClass.getESuperTypes().add(this.getValue());
    dbxrefEClass.getESuperTypes().add(this.getCloneable());
    dbxrefEClass.getESuperTypes().add(this.getSerializable());
    dbxrefEClass.getESuperTypes().add(this.getComparable());
    dbxrefEClass.getESuperTypes().add(this.getIdentifiableObject());
    definedObjectEClass.getESuperTypes().add(this.getIdentifiedObject());
    identifiedObjectEClass.getESuperTypes().add(this.getValue());
    identifiedObjectEClass.getESuperTypes().add(this.getIdentifiableObject());
    identifiedObjectEClass.getESuperTypes().add(this.getNamespacedObject());
    identifiedObjectEClass.getESuperTypes().add(this.getCloneable());
    identifiedObjectEClass.getESuperTypes().add(this.getSerializable());
    instanceEClass.getESuperTypes().add(this.getOBOObject());
    linkEClass.getESuperTypes().add(this.getImpliable());
    linkEClass.getESuperTypes().add(this.getIdentifiableObject());
    linkEClass.getESuperTypes().add(this.getRelationship());
    linkEClass.getESuperTypes().add(this.getPathCapable());
    linkDatabaseEClass.getESuperTypes().add(this.getIdentifiedObjectIndex());
    linkDatabaseEClass.getESuperTypes().add(this.getSerializable());
    linkDatabaseEClass.getESuperTypes().add(this.getCloneable());
    linkLinkedObjectEClass.getESuperTypes().add(this.getLinkedObject());
    linkedObjectEClass.getESuperTypes().add(this.getIdentifiedObject());
    linkedObjectEClass.getESuperTypes().add(this.getPathCapable());
    multiIDObjectEClass.getESuperTypes().add(this.getIdentifiedObject());
    mutableLinkDatabaseEClass.getESuperTypes().add(this.getLinkDatabase());
    nestedValueEClass.getESuperTypes().add(this.getCloneable());
    nestedValueEClass.getESuperTypes().add(this.getSerializable());
    oboClassEClass.getESuperTypes().add(this.getOBOObject());
    oboClassEClass.getESuperTypes().add(this.getType());
    oboClassEClass.getESuperTypes().add(this.getComparable());
    oboObjectEClass.getESuperTypes().add(this.getAnnotatedObject());
    oboObjectEClass.getESuperTypes().add(this.getLinkedObject());
    oboObjectEClass.getESuperTypes().add(this.getComparable());
    oboPropertyEClass.getESuperTypes().add(this.getOBOObject());
    oboRestrictionEClass.getESuperTypes().add(this.getLink());
    oboRestrictionEClass.getESuperTypes().add(this.getCloneable());
    oboRestrictionEClass.getESuperTypes().add(this.getSerializable());
    oboSessionEClass.getESuperTypes().add(this.getIdentifiedObjectIndex());
    oboSessionEClass.getESuperTypes().add(this.getSerializable());
    objectFactoryEClass.getESuperTypes().add(this.getSerializable());
    objectFactoryEClass.getESuperTypes().add(this.getCloneable());
    obsoletableObjectEClass.getESuperTypes().add(this.getIdentifiedObject());
    propertyValueEClass.getESuperTypes().add(this.getCloneable());
    propertyValueEClass.getESuperTypes().add(this.getSerializable());
    relationshipEClass.getESuperTypes().add(this.getSerializable());
    relationshipEClass.getESuperTypes().add(this.getCloneable());
    synonymEClass.getESuperTypes().add(this.getCloneable());
    synonymEClass.getESuperTypes().add(this.getSerializable());
    synonymEClass.getESuperTypes().add(this.getComparable());
    synonymEClass.getESuperTypes().add(this.getIdentifiableObject());
    synonymTypeEClass.getESuperTypes().add(this.getCloneable());
    synonymedObjectEClass.getESuperTypes().add(this.getIdentifiedObject());
    termSubsetEClass.getESuperTypes().add(this.getCloneable());
    termSubsetEClass.getESuperTypes().add(this.getSerializable());
    typeEClass.getESuperTypes().add(this.getIdentifiedObject());
    valueEClass.getESuperTypes().add(this.getCloneable());
    valueEClass.getESuperTypes().add(this.getSerializable());
    valueDatabaseEClass.getESuperTypes().add(this.getIdentifiedObjectIndex());
    valueLinkEClass.getESuperTypes().add(this.getLink());

    // Initialize classes, features, and operations; add parameters
    initEClass(annotatedObjectEClass, AnnotatedObject.class, "AnnotatedObject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(commentedObjectEClass, CommentedObject.class, "CommentedObject", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getCommentedObject_Comment(), ecorePackage.getEString(), "comment", "", 1, 1, CommentedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getCommentedObject_CommentExtension(), this.getNestedValue(), null, "commentExtension", null, 0, 1, CommentedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(danglingObjectEClass, DanglingObject.class, "DanglingObject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getDanglingObject_Dangling(), ecorePackage.getEBoolean(), "dangling", null, 1, 1, DanglingObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(danglingPropertyEClass, DanglingProperty.class, "DanglingProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(datatypeEClass, Datatype.class, "Datatype", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getDatatype_Abstract(), ecorePackage.getEBoolean(), "abstract", null, 1, 1, Datatype.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(datatypeValueEClass, DatatypeValue.class, "DatatypeValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getDatatypeValue_Value(), ecorePackage.getEString(), "value", "", 1, 1, DatatypeValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(dbxrefEClass, Dbxref.class, "Dbxref", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getDbxref_NestedValue(), this.getNestedValue(), null, "nestedValue", null, 0, 1, Dbxref.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getDbxref_DefRef(), ecorePackage.getEBoolean(), "defRef", null, 1, 1, Dbxref.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getDbxref_Desc(), ecorePackage.getEString(), "desc", "", 1, 1, Dbxref.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getDbxref_Type(), ecorePackage.getEBigInteger(), "type", null, 1, 1, Dbxref.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDbxref_Synonym(), this.getSynonym(), null, "synonym", null, 0, 1, Dbxref.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getDbxref_DatabaseID(), ecorePackage.getEString(), "databaseID", "", 1, 1, Dbxref.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getDbxref_Database(), ecorePackage.getEString(), "database", "", 1, 1, Dbxref.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(dbxrefedObjectEClass, DbxrefedObject.class, "DbxrefedObject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getDbxrefedObject_Dbxrefs(), this.getDbxref(), null, "dbxrefs", null, 0, -1, DbxrefedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

    initEClass(definedObjectEClass, DefinedObject.class, "DefinedObject", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getDefinedObject_Definition(), ecorePackage.getEString(), "definition", "", 1, 1, DefinedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDefinedObject_DefDbxrefs(), this.getDbxref(), null, "defDbxrefs", null, 0, -1, DefinedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEReference(getDefinedObject_DefinitionExtension(), this.getNestedValue(), null, "definitionExtension", null, 0, 1, DefinedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(fieldPathEClass, FieldPath.class, "FieldPath", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(fieldPathSpecEClass, FieldPathSpec.class, "FieldPathSpec", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(identifiableObjectEClass, IdentifiableObject.class, "IdentifiableObject", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getIdentifiableObject_Id(), ecorePackage.getEString(), "id", null, 1, 1, IdentifiableObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getIdentifiableObject_Anonymous(), ecorePackage.getEBoolean(), "anonymous", "false", 1, 1, IdentifiableObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(identifiedObjectEClass, IdentifiedObject.class, "IdentifiedObject", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getIdentifiedObject_TypeExtension(), this.getNestedValue(), null, "typeExtension", null, 0, 1, IdentifiedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getIdentifiedObject_BuiltIn(), ecorePackage.getEBoolean(), "builtIn", null, 1, 1, IdentifiedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getIdentifiedObject_Name(), ecorePackage.getEString(), "name", null, 1, 1, IdentifiedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIdentifiedObject_NameExtension(), this.getNestedValue(), null, "nameExtension", null, 0, 1, IdentifiedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIdentifiedObject_IdExtension(), this.getNestedValue(), null, "idExtension", null, 0, 1, IdentifiedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIdentifiedObject_AnonymousExtension(), this.getNestedValue(), null, "anonymousExtension", null, 0, 1, IdentifiedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIdentifiedObject_PropertyValues(), this.getPropertyValue(), null, "propertyValues", null, 0, -1, IdentifiedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

    initEClass(identifiedObjectIndexEClass, IdentifiedObjectIndex.class, "IdentifiedObjectIndex", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(impliableEClass, Impliable.class, "Impliable", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getImpliable_Implied(), ecorePackage.getEBoolean(), "implied", null, 1, 1, Impliable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(instanceEClass, Instance.class, "Instance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getInstance_OboPropertiesValues(), this.getOboPropertyValue(), null, "oboPropertiesValues", null, 0, -1, Instance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

    initEClass(linkEClass, Link.class, "Link", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLink_Namespace(), this.getNamespace(), null, "namespace", null, 0, 1, Link.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(linkDatabaseEClass, LinkDatabase.class, "LinkDatabase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLinkDatabase_Session(), this.getOBOSession(), this.getOBOSession_LinkDatabase(), "session", null, 0, 1, LinkDatabase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLinkDatabase_Objects(), this.getIdentifiedObject(), null, "objects", null, 0, -1, LinkDatabase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEReference(getLinkDatabase_Properties(), this.getOBOProperty(), null, "properties", null, 0, -1, LinkDatabase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

    initEClass(linkLinkedObjectEClass, LinkLinkedObject.class, "LinkLinkedObject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLinkLinkedObject_Link(), this.getLink(), null, "link", null, 0, 1, LinkLinkedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(linkedObjectEClass, LinkedObject.class, "LinkedObject", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLinkedObject_Parents(), this.getRelationship(), this.getRelationship_Child(), "parents", null, 0, -1, LinkedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEReference(getLinkedObject_Children(), this.getRelationship(), this.getRelationship_Parent(), "children", null, 0, -1, LinkedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

    initEClass(modificationMetadataObjectEClass, ModificationMetadataObject.class, "ModificationMetadataObject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getModificationMetadataObject_CreatedBy(), ecorePackage.getEString(), "createdBy", "", 1, 1, ModificationMetadataObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getModificationMetadataObject_CreatedByExtension(), this.getNestedValue(), null, "createdByExtension", null, 0, 1, ModificationMetadataObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getModificationMetadataObject_ModifiedBy(), ecorePackage.getEString(), "modifiedBy", "", 1, 1, ModificationMetadataObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getModificationMetadataObject_ModifiedByExtension(), this.getNestedValue(), null, "modifiedByExtension", null, 0, 1, ModificationMetadataObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getModificationMetadataObject_CreationDate(), this.getDate(), "creationDate", null, 0, 1, ModificationMetadataObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getModificationMetadataObject_CreationDateExtension(), this.getNestedValue(), null, "creationDateExtension", null, 0, 1, ModificationMetadataObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getModificationMetadataObject_ModificationDate(), this.getDate(), "modificationDate", null, 0, 1, ModificationMetadataObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getModificationMetadataObject_ModificationDateExtension(), this.getNestedValue(), null, "modificationDateExtension", null, 0, 1, ModificationMetadataObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(multiIDObjectEClass, MultiIDObject.class, "MultiIDObject", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getMultiIDObject_SecondaryIds(), ecorePackage.getEString(), "secondaryIds", null, 0, -1, MultiIDObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEReference(getMultiIDObject_SecondaryIdExtension(), this.getStringToNestedValueMap(), null, "secondaryIdExtension", null, 0, -1, MultiIDObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

    initEClass(mutableLinkDatabaseEClass, MutableLinkDatabase.class, "MutableLinkDatabase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getMutableLinkDatabase_IdentifiedObjectIndex(), this.getIdentifiedObjectIndex(), null, "identifiedObjectIndex", null, 0, 1, MutableLinkDatabase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(namespaceEClass, Namespace.class, "Namespace", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getNamespace_Id(), ecorePackage.getEString(), "id", "", 1, 1, Namespace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getNamespace_Path(), ecorePackage.getEString(), "path", "", 1, 1, Namespace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getNamespace_PrivateId(), ecorePackage.getEBigInteger(), "privateId", null, 0, 1, Namespace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(namespacedObjectEClass, NamespacedObject.class, "NamespacedObject", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getNamespacedObject_Namespace(), this.getNamespace(), null, "namespace", null, 0, 1, NamespacedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getNamespacedObject_NamespaceExtension(), this.getNestedValue(), null, "namespaceExtension", null, 0, 1, NamespacedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(nestedValueEClass, NestedValue.class, "NestedValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getNestedValue_Name(), ecorePackage.getEString(), "name", "", 1, 1, NestedValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getNestedValue_PropertyValues(), this.getPropertyValue(), null, "propertyValues", null, 0, -1, NestedValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEAttribute(getNestedValue_SuggestedComment(), ecorePackage.getEString(), "suggestedComment", "", 1, 1, NestedValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(oboClassEClass, OBOClass.class, "OBOClass", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(oboObjectEClass, OBOObject.class, "OBOObject", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(oboPropertyEClass, OBOProperty.class, "OBOProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getOBOProperty_Cyclic(), ecorePackage.getEBoolean(), "cyclic", null, 1, 1, OBOProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOBOProperty_Symmetric(), ecorePackage.getEBoolean(), "symmetric", null, 1, 1, OBOProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOBOProperty_Transitive(), ecorePackage.getEBoolean(), "transitive", null, 1, 1, OBOProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOBOProperty_Reflexive(), ecorePackage.getEBoolean(), "reflexive", null, 1, 1, OBOProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOBOProperty_AlwaysImpliesInverse(), ecorePackage.getEBoolean(), "alwaysImpliesInverse", null, 1, 1, OBOProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOBOProperty_Dummy(), ecorePackage.getEBoolean(), "dummy", null, 1, 1, OBOProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOBOProperty_MetadataTag(), ecorePackage.getEBoolean(), "metadataTag", null, 1, 1, OBOProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOBOProperty_CyclicExtension(), this.getNestedValue(), null, "cyclicExtension", null, 0, 1, OBOProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOBOProperty_SymmetricExtension(), this.getNestedValue(), null, "symmetricExtension", null, 0, 1, OBOProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOBOProperty_TransitiveExtension(), this.getNestedValue(), null, "transitiveExtension", null, 0, 1, OBOProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOBOProperty_ReflexiveExtension(), this.getNestedValue(), null, "reflexiveExtension", null, 0, 1, OBOProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOBOProperty_AlwaysImpliesInverseExtension(), this.getNestedValue(), null, "alwaysImpliesInverseExtension", null, 0, 1, OBOProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOBOProperty_DomainExtension(), this.getNestedValue(), null, "domainExtension", null, 0, 1, OBOProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOBOProperty_RangeExtension(), this.getNestedValue(), null, "rangeExtension", null, 0, 1, OBOProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOBOProperty_Range(), this.getType(), null, "range", null, 0, 1, OBOProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOBOProperty_Domain(), this.getType(), null, "domain", null, 0, 1, OBOProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOBOProperty_NonInheritable(), ecorePackage.getEBoolean(), "nonInheritable", null, 1, 1, OBOProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOBOProperty_DisjointOver(), this.getOBOProperty(), null, "disjointOver", null, 0, 1, OBOProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOBOProperty_TransitiveOver(), this.getOBOProperty(), null, "transitiveOver", null, 0, 1, OBOProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOBOProperty_HoldsOverChains(), this.getListOfProperties(), null, "holdsOverChains", null, 0, -1, OBOProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

    initEClass(oboRestrictionEClass, OBORestriction.class, "OBORestriction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getOBORestriction_Cardinality(), ecorePackage.getEBigInteger(), "cardinality", null, 0, 1, OBORestriction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOBORestriction_MaxCardinality(), ecorePackage.getEBigInteger(), "maxCardinality", null, 0, 1, OBORestriction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOBORestriction_MinCardinality(), ecorePackage.getEBigInteger(), "minCardinality", null, 0, 1, OBORestriction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOBORestriction_Completes(), ecorePackage.getEBoolean(), "completes", "false", 1, 1, OBORestriction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOBORestriction_InverseCompletes(), ecorePackage.getEBoolean(), "inverseCompletes", "false", 1, 1, OBORestriction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOBORestriction_NecessarilyTrue(), ecorePackage.getEBoolean(), "necessarilyTrue", "false", 1, 1, OBORestriction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOBORestriction_InverseNecessarilyTrue(), ecorePackage.getEBoolean(), "inverseNecessarilyTrue", "false", 1, 1, OBORestriction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOBORestriction_AdditionalArguments(), this.getLinkedObject(), null, "additionalArguments", null, 0, -1, OBORestriction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

    initEClass(oboSessionEClass, OBOSession.class, "OBOSession", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getOBOSession_Id(), ecorePackage.getEString(), "id", "rootSession", 1, 1, OBOSession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOBOSession_Objects(), this.getIdentifiedObject(), null, "objects", null, 0, -1, OBOSession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEReference(getOBOSession_LinkDatabase(), this.getLinkDatabase(), this.getLinkDatabase_Session(), "linkDatabase", null, 0, 1, OBOSession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOBOSession_DefaultNamespace(), this.getNamespace(), null, "defaultNamespace", null, 0, 1, OBOSession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOBOSession_Namespaces(), this.getNamespace(), null, "namespaces", null, 0, -1, OBOSession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEReference(getOBOSession_PropertyValues(), this.getPropertyValue(), null, "propertyValues", null, 0, -1, OBOSession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEReference(getOBOSession_UnknownStanzas(), this.getUnknownStanza(), null, "unknownStanzas", null, 0, -1, OBOSession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEReference(getOBOSession_Subsets(), this.getTermSubset(), null, "subsets", null, 0, -1, OBOSession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEReference(getOBOSession_Categories(), this.getTermSubset(), null, "categories", null, 0, -1, OBOSession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEReference(getOBOSession_SynonymTypes(), this.getSynonymType(), null, "synonymTypes", null, 0, -1, OBOSession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEReference(getOBOSession_SynonymCategories(), this.getSynonymType(), null, "synonymCategories", null, 0, -1, OBOSession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEAttribute(getOBOSession_CurrentFilenames(), ecorePackage.getEString(), "currentFilenames", null, 0, -1, OBOSession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEAttribute(getOBOSession_CurrentUser(), ecorePackage.getEString(), "currentUser", "", 1, 1, OBOSession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOBOSession_IdSpaces(), this.getIdSpacesMap(), null, "idSpaces", null, 0, -1, OBOSession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEAttribute(getOBOSession_LoadRemark(), ecorePackage.getEString(), "loadRemark", "", 1, 1, OBOSession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOBOSession_AllDbxRefsContainer(), this.getDbxref(), null, "allDbxRefsContainer", null, 0, -1, OBOSession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

    initEClass(objectFactoryEClass, ObjectFactory.class, "ObjectFactory", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(objectFieldEClass, ObjectField.class, "ObjectField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getObjectField_Values(), this.getEcoreObject(), "values", null, 0, -1, ObjectField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

    initEClass(obsoletableObjectEClass, ObsoletableObject.class, "ObsoletableObject", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getObsoletableObject_Obsolete(), ecorePackage.getEBoolean(), "obsolete", null, 1, 1, ObsoletableObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getObsoletableObject_ReplacedBy(), this.getObsoletableObject(), null, "replacedBy", null, 0, -1, ObsoletableObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEReference(getObsoletableObject_ConsiderReplacements(), this.getObsoletableObject(), null, "considerReplacements", null, 0, -1, ObsoletableObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEReference(getObsoletableObject_ConsiderExtension(), this.getObsoletableObjectToNestedValueMap(), null, "considerExtension", null, 0, -1, ObsoletableObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEReference(getObsoletableObject_ReplacedByExtension(), this.getObsoletableObjectToNestedValueMap(), null, "replacedByExtension", null, 0, -1, ObsoletableObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEReference(getObsoletableObject_ObsoleteExtension(), this.getNestedValue(), null, "obsoleteExtension", null, 0, 1, ObsoletableObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(pathCapableEClass, PathCapable.class, "PathCapable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(propertyValueEClass, PropertyValue.class, "PropertyValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getPropertyValue_Property(), ecorePackage.getEString(), "property", "", 1, 1, PropertyValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getPropertyValue_Value(), ecorePackage.getEString(), "value", "", 1, 1, PropertyValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getPropertyValue_LineNumber(), ecorePackage.getEBigInteger(), "lineNumber", null, 1, 1, PropertyValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getPropertyValue_Filename(), ecorePackage.getEString(), "filename", "", 1, 1, PropertyValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getPropertyValue_Line(), ecorePackage.getEString(), "line", "", 1, 1, PropertyValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(relationshipEClass, Relationship.class, "Relationship", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getRelationship_Child(), this.getLinkedObject(), this.getLinkedObject_Parents(), "child", null, 0, 1, Relationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getRelationship_Parent(), this.getLinkedObject(), this.getLinkedObject_Children(), "parent", null, 0, 1, Relationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getRelationship_Type(), this.getOBOProperty(), null, "type", null, 1, 1, Relationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getRelationship_NestedValue(), this.getNestedValue(), null, "nestedValue", null, 0, 1, Relationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(rootAlgorithmEClass, RootAlgorithm.class, "RootAlgorithm", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(subsetObjectEClass, SubsetObject.class, "SubsetObject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSubsetObject_Subsets(), this.getTermSubset(), null, "subsets", null, 0, -1, SubsetObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEReference(getSubsetObject_CategoryExtensions(), this.getTermSubsetToNestedValueMap(), null, "categoryExtensions", null, 0, -1, SubsetObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

    initEClass(synonymEClass, Synonym.class, "Synonym", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getSynonym_UNKNOWN_SCOPE(), ecorePackage.getEBigInteger(), "UNKNOWN_SCOPE", "-1", 1, 1, Synonym.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSynonym_RELATED_SYNONYM(), ecorePackage.getEBigInteger(), "RELATED_SYNONYM", "0", 1, 1, Synonym.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSynonym_EXACT_SYNONYM(), ecorePackage.getEBigInteger(), "EXACT_SYNONYM", "1", 1, 1, Synonym.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSynonym_NARROW_SYNONYM(), ecorePackage.getEBigInteger(), "NARROW_SYNONYM", "2", 1, 1, Synonym.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSynonym_BROAD_SYNONYM(), ecorePackage.getEBigInteger(), "BROAD_SYNONYM", "3", 1, 1, Synonym.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSynonym_SynonymType(), this.getSynonymType(), null, "synonymType", null, 0, 1, Synonym.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSynonym_NestedValue(), this.getNestedValue(), null, "nestedValue", null, 0, 1, Synonym.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSynonym_Scope(), ecorePackage.getEBigInteger(), "scope", null, 1, 1, Synonym.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSynonym_Xrefs(), this.getDbxref(), null, "xrefs", null, 0, -1, Synonym.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEAttribute(getSynonym_Text(), ecorePackage.getEString(), "text", null, 1, 1, Synonym.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(synonymTypeEClass, SynonymType.class, "SynonymType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getSynonymType_Id(), ecorePackage.getEString(), "id", null, 1, 1, SynonymType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSynonymType_Name(), ecorePackage.getEString(), "name", null, 1, 1, SynonymType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSynonymType_Scope(), ecorePackage.getEBigInteger(), "scope", null, 1, 1, SynonymType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(synonymedObjectEClass, SynonymedObject.class, "SynonymedObject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSynonymedObject_Synonyms(), this.getSynonym(), null, "synonyms", null, 0, -1, SynonymedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

    initEClass(termSubsetEClass, TermSubset.class, "TermSubset", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getTermSubset_Name(), ecorePackage.getEString(), "name", null, 1, 1, TermSubset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getTermSubset_Desc(), ecorePackage.getEString(), "desc", null, 1, 1, TermSubset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(typeEClass, Type.class, "Type", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(unknownStanzaEClass, UnknownStanza.class, "UnknownStanza", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getUnknownStanza_Namespace(), this.getNamespace(), null, "namespace", null, 0, 1, UnknownStanza.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getUnknownStanza_PropertyValues(), this.getPropertyValue(), null, "propertyValues", null, 0, -1, UnknownStanza.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEReference(getUnknownStanza_NestedValues(), this.getNestedValue(), null, "nestedValues", null, 0, -1, UnknownStanza.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
    initEAttribute(getUnknownStanza_Stanza(), ecorePackage.getEString(), "stanza", "", 1, 1, UnknownStanza.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(valueEClass, Value.class, "Value", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getValue_Type(), this.getType(), null, "type", null, 0, 1, Value.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(valueDatabaseEClass, ValueDatabase.class, "ValueDatabase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getValueDatabase_Objects(), this.getIdentifiedObject(), null, "objects", null, 0, -1, ValueDatabase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

    initEClass(valueLinkEClass, ValueLink.class, "ValueLink", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getValueLink_Value(), this.getValue(), null, "value", null, 0, 1, ValueLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(cloneableEClass, br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable.class, "Cloneable", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(serializableEClass, Serializable.class, "Serializable", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(comparableEClass, br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable.class, "Comparable", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(stringToNestedValueMapEClass, Map.Entry.class, "StringToNestedValueMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getStringToNestedValueMap_Key(), ecorePackage.getEString(), "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getStringToNestedValueMap_Value(), this.getNestedValue(), null, "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(obsoletableObjectToNestedValueMapEClass, Map.Entry.class, "ObsoletableObjectToNestedValueMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
    initEReference(getObsoletableObjectToNestedValueMap_Key(), this.getObsoletableObject(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getObsoletableObjectToNestedValueMap_Value(), this.getNestedValue(), null, "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(termSubsetToNestedValueMapEClass, Map.Entry.class, "TermSubsetToNestedValueMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
    initEReference(getTermSubsetToNestedValueMap_Key(), this.getTermSubset(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getTermSubsetToNestedValueMap_Value(), this.getNestedValue(), null, "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(idSpacesMapEClass, Map.Entry.class, "IdSpacesMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getIdSpacesMap_Key(), ecorePackage.getEString(), "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getIdSpacesMap_Value(), ecorePackage.getEString(), "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(listOfPropertiesEClass, ListOfProperties.class, "ListOfProperties", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getListOfProperties_Properties(), this.getOBOProperty(), null, "properties", null, 0, -1, ListOfProperties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

    initEClass(oboPropertyValueEClass, OboPropertyValue.class, "OboPropertyValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getOboPropertyValue_Property(), this.getOBOProperty(), null, "property", null, 1, 1, OboPropertyValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOboPropertyValue_Value(), this.getValue(), null, "value", null, 0, 1, OboPropertyValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Initialize data types
    initEDataType(dateEDataType, Date.class, "Date", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
    initEDataType(ecoreObjectEDataType, EObject.class, "EcoreObject", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

    // Create resource
    createResource(eNS_URI);

    // Create annotations
    // http://www.eclipse.org/emf/2002/Ecore
    createEcoreAnnotations();
    // http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot
    createPivotAnnotations();
  }

  /**
   * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createEcoreAnnotations() {
    String source = "http://www.eclipse.org/emf/2002/Ecore";	
    addAnnotation
      (this, 
       source, 
       new String[] {
       "invocationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
       "settingDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
       "validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot"
       });	
    addAnnotation
      (instanceEClass, 
       source, 
       new String[] {
       "constraints", "TypeMustBeClass"
       });
  }

  /**
   * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createPivotAnnotations() {
    String source = "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot";	
    addAnnotation
      (instanceEClass, 
       source, 
       new String[] {
       "TypeMustBeClass", "self.type.oclIsKindOf(OBOClass)"
       });
  }

} //obodatamodelPackageImpl
