/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Relationship</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.RelationshipImpl#getChild <em>Child</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.RelationshipImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.RelationshipImpl#getType <em>Type</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.RelationshipImpl#getNestedValue <em>Nested Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class RelationshipImpl extends SerializableImpl implements Relationship {
  /**
   * The cached value of the '{@link #getParent() <em>Parent</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParent()
   * @generated
   * @ordered
   */
  protected LinkedObject parent;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected OBOProperty type;

  /**
   * The cached value of the '{@link #getNestedValue() <em>Nested Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNestedValue()
   * @generated
   * @ordered
   */
  protected NestedValue nestedValue;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected RelationshipImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.RELATIONSHIP;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LinkedObject getChild() {
    if (eContainerFeatureID() != obodatamodelPackage.RELATIONSHIP__CHILD) return null;
    return (LinkedObject)eInternalContainer();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetChild(LinkedObject newChild, NotificationChain msgs) {
    msgs = eBasicSetContainer((InternalEObject)newChild, obodatamodelPackage.RELATIONSHIP__CHILD, msgs);
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setChild(LinkedObject newChild) {
    if (newChild != eInternalContainer() || (eContainerFeatureID() != obodatamodelPackage.RELATIONSHIP__CHILD && newChild != null)) {
      if (EcoreUtil.isAncestor(this, newChild))
        throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
      NotificationChain msgs = null;
      if (eInternalContainer() != null)
        msgs = eBasicRemoveFromContainer(msgs);
      if (newChild != null)
        msgs = ((InternalEObject)newChild).eInverseAdd(this, obodatamodelPackage.LINKED_OBJECT__PARENTS, LinkedObject.class, msgs);
      msgs = basicSetChild(newChild, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.RELATIONSHIP__CHILD, newChild, newChild));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LinkedObject getParent() {
    if (parent != null && parent.eIsProxy()) {
      InternalEObject oldParent = (InternalEObject)parent;
      parent = (LinkedObject)eResolveProxy(oldParent);
      if (parent != oldParent) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, obodatamodelPackage.RELATIONSHIP__PARENT, oldParent, parent));
      }
    }
    return parent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LinkedObject basicGetParent() {
    return parent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetParent(LinkedObject newParent, NotificationChain msgs) {
    LinkedObject oldParent = parent;
    parent = newParent;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.RELATIONSHIP__PARENT, oldParent, newParent);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParent(LinkedObject newParent) {
    if (newParent != parent) {
      NotificationChain msgs = null;
      if (parent != null)
        msgs = ((InternalEObject)parent).eInverseRemove(this, obodatamodelPackage.LINKED_OBJECT__CHILDREN, LinkedObject.class, msgs);
      if (newParent != null)
        msgs = ((InternalEObject)newParent).eInverseAdd(this, obodatamodelPackage.LINKED_OBJECT__CHILDREN, LinkedObject.class, msgs);
      msgs = basicSetParent(newParent, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.RELATIONSHIP__PARENT, newParent, newParent));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBOProperty getType() {
    if (type != null && type.eIsProxy()) {
      InternalEObject oldType = (InternalEObject)type;
      type = (OBOProperty)eResolveProxy(oldType);
      if (type != oldType) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, obodatamodelPackage.RELATIONSHIP__TYPE, oldType, type));
      }
    }
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBOProperty basicGetType() {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(OBOProperty newType) {
    OBOProperty oldType = type;
    type = newType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.RELATIONSHIP__TYPE, oldType, type));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getNestedValue() {
    return nestedValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetNestedValue(NestedValue newNestedValue, NotificationChain msgs) {
    NestedValue oldNestedValue = nestedValue;
    nestedValue = newNestedValue;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.RELATIONSHIP__NESTED_VALUE, oldNestedValue, newNestedValue);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNestedValue(NestedValue newNestedValue) {
    if (newNestedValue != nestedValue) {
      NotificationChain msgs = null;
      if (nestedValue != null)
        msgs = ((InternalEObject)nestedValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.RELATIONSHIP__NESTED_VALUE, null, msgs);
      if (newNestedValue != null)
        msgs = ((InternalEObject)newNestedValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.RELATIONSHIP__NESTED_VALUE, null, msgs);
      msgs = basicSetNestedValue(newNestedValue, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.RELATIONSHIP__NESTED_VALUE, newNestedValue, newNestedValue));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.RELATIONSHIP__CHILD:
        if (eInternalContainer() != null)
          msgs = eBasicRemoveFromContainer(msgs);
        return basicSetChild((LinkedObject)otherEnd, msgs);
      case obodatamodelPackage.RELATIONSHIP__PARENT:
        if (parent != null)
          msgs = ((InternalEObject)parent).eInverseRemove(this, obodatamodelPackage.LINKED_OBJECT__CHILDREN, LinkedObject.class, msgs);
        return basicSetParent((LinkedObject)otherEnd, msgs);
    }
    return super.eInverseAdd(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.RELATIONSHIP__CHILD:
        return basicSetChild(null, msgs);
      case obodatamodelPackage.RELATIONSHIP__PARENT:
        return basicSetParent(null, msgs);
      case obodatamodelPackage.RELATIONSHIP__NESTED_VALUE:
        return basicSetNestedValue(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
    switch (eContainerFeatureID()) {
      case obodatamodelPackage.RELATIONSHIP__CHILD:
        return eInternalContainer().eInverseRemove(this, obodatamodelPackage.LINKED_OBJECT__PARENTS, LinkedObject.class, msgs);
    }
    return super.eBasicRemoveFromContainerFeature(msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.RELATIONSHIP__CHILD:
        return getChild();
      case obodatamodelPackage.RELATIONSHIP__PARENT:
        if (resolve) return getParent();
        return basicGetParent();
      case obodatamodelPackage.RELATIONSHIP__TYPE:
        if (resolve) return getType();
        return basicGetType();
      case obodatamodelPackage.RELATIONSHIP__NESTED_VALUE:
        return getNestedValue();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.RELATIONSHIP__CHILD:
        setChild((LinkedObject)newValue);
        return;
      case obodatamodelPackage.RELATIONSHIP__PARENT:
        setParent((LinkedObject)newValue);
        return;
      case obodatamodelPackage.RELATIONSHIP__TYPE:
        setType((OBOProperty)newValue);
        return;
      case obodatamodelPackage.RELATIONSHIP__NESTED_VALUE:
        setNestedValue((NestedValue)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.RELATIONSHIP__CHILD:
        setChild((LinkedObject)null);
        return;
      case obodatamodelPackage.RELATIONSHIP__PARENT:
        setParent((LinkedObject)null);
        return;
      case obodatamodelPackage.RELATIONSHIP__TYPE:
        setType((OBOProperty)null);
        return;
      case obodatamodelPackage.RELATIONSHIP__NESTED_VALUE:
        setNestedValue((NestedValue)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.RELATIONSHIP__CHILD:
        return getChild() != null;
      case obodatamodelPackage.RELATIONSHIP__PARENT:
        return parent != null;
      case obodatamodelPackage.RELATIONSHIP__TYPE:
        return type != null;
      case obodatamodelPackage.RELATIONSHIP__NESTED_VALUE:
        return nestedValue != null;
    }
    return super.eIsSet(featureID);
  }

} //RelationshipImpl
