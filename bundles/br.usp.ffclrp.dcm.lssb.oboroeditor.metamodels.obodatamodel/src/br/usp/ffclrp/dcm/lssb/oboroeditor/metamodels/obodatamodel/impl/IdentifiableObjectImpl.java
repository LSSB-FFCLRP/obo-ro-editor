/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Identifiable Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiableObjectImpl#getId <em>Id</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiableObjectImpl#isAnonymous <em>Anonymous</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class IdentifiableObjectImpl extends MinimalEObjectImpl.Container implements IdentifiableObject {
  /**
   * The default value of the '{@link #getId() <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getId()
   * @generated
   * @ordered
   */
  protected static final String ID_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getId()
   * @generated
   * @ordered
   */
  protected String id = ID_EDEFAULT;

  /**
   * The default value of the '{@link #isAnonymous() <em>Anonymous</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isAnonymous()
   * @generated
   * @ordered
   */
  protected static final boolean ANONYMOUS_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isAnonymous() <em>Anonymous</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isAnonymous()
   * @generated
   * @ordered
   */
  protected boolean anonymous = ANONYMOUS_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected IdentifiableObjectImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.IDENTIFIABLE_OBJECT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getId() {
    return id;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setId(String newId) {
    String oldId = id;
    id = newId;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.IDENTIFIABLE_OBJECT__ID, oldId, id));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isAnonymous() {
    return anonymous;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAnonymous(boolean newAnonymous) {
    boolean oldAnonymous = anonymous;
    anonymous = newAnonymous;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.IDENTIFIABLE_OBJECT__ANONYMOUS, oldAnonymous, anonymous));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.IDENTIFIABLE_OBJECT__ID:
        return getId();
      case obodatamodelPackage.IDENTIFIABLE_OBJECT__ANONYMOUS:
        return isAnonymous();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.IDENTIFIABLE_OBJECT__ID:
        setId((String)newValue);
        return;
      case obodatamodelPackage.IDENTIFIABLE_OBJECT__ANONYMOUS:
        setAnonymous((Boolean)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.IDENTIFIABLE_OBJECT__ID:
        setId(ID_EDEFAULT);
        return;
      case obodatamodelPackage.IDENTIFIABLE_OBJECT__ANONYMOUS:
        setAnonymous(ANONYMOUS_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.IDENTIFIABLE_OBJECT__ID:
        return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
      case obodatamodelPackage.IDENTIFIABLE_OBJECT__ANONYMOUS:
        return anonymous != ANONYMOUS_EDEFAULT;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (id: ");
    result.append(id);
    result.append(", anonymous: ");
    result.append(anonymous);
    result.append(')');
    return result.toString();
  }

} //IdentifiableObjectImpl
