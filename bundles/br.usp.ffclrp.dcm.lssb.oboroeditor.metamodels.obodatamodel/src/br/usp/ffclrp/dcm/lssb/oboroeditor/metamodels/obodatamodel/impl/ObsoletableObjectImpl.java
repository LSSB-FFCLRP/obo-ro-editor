/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Obsoletable Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObsoletableObjectImpl#isObsolete <em>Obsolete</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObsoletableObjectImpl#getReplacedBy <em>Replaced By</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObsoletableObjectImpl#getConsiderReplacements <em>Consider Replacements</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObsoletableObjectImpl#getConsiderExtension <em>Consider Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObsoletableObjectImpl#getReplacedByExtension <em>Replaced By Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObsoletableObjectImpl#getObsoleteExtension <em>Obsolete Extension</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ObsoletableObjectImpl extends IdentifiedObjectImpl implements ObsoletableObject {
  /**
   * The default value of the '{@link #isObsolete() <em>Obsolete</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isObsolete()
   * @generated
   * @ordered
   */
  protected static final boolean OBSOLETE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isObsolete() <em>Obsolete</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isObsolete()
   * @generated
   * @ordered
   */
  protected boolean obsolete = OBSOLETE_EDEFAULT;

  /**
   * The cached value of the '{@link #getReplacedBy() <em>Replaced By</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReplacedBy()
   * @generated
   * @ordered
   */
  protected EList<ObsoletableObject> replacedBy;

  /**
   * The cached value of the '{@link #getConsiderReplacements() <em>Consider Replacements</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConsiderReplacements()
   * @generated
   * @ordered
   */
  protected EList<ObsoletableObject> considerReplacements;

  /**
   * The cached value of the '{@link #getConsiderExtension() <em>Consider Extension</em>}' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConsiderExtension()
   * @generated
   * @ordered
   */
  protected EMap<ObsoletableObject, NestedValue> considerExtension;

  /**
   * The cached value of the '{@link #getReplacedByExtension() <em>Replaced By Extension</em>}' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReplacedByExtension()
   * @generated
   * @ordered
   */
  protected EMap<ObsoletableObject, NestedValue> replacedByExtension;

  /**
   * The cached value of the '{@link #getObsoleteExtension() <em>Obsolete Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getObsoleteExtension()
   * @generated
   * @ordered
   */
  protected NestedValue obsoleteExtension;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ObsoletableObjectImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.OBSOLETABLE_OBJECT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isObsolete() {
    return obsolete;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setObsolete(boolean newObsolete) {
    boolean oldObsolete = obsolete;
    obsolete = newObsolete;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE, oldObsolete, obsolete));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ObsoletableObject> getReplacedBy() {
    if (replacedBy == null) {
      replacedBy = new EObjectResolvingEList<ObsoletableObject>(ObsoletableObject.class, this, obodatamodelPackage.OBSOLETABLE_OBJECT__REPLACED_BY);
    }
    return replacedBy;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ObsoletableObject> getConsiderReplacements() {
    if (considerReplacements == null) {
      considerReplacements = new EObjectResolvingEList<ObsoletableObject>(ObsoletableObject.class, this, obodatamodelPackage.OBSOLETABLE_OBJECT__CONSIDER_REPLACEMENTS);
    }
    return considerReplacements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EMap<ObsoletableObject, NestedValue> getConsiderExtension() {
    if (considerExtension == null) {
      considerExtension = new EcoreEMap<ObsoletableObject,NestedValue>(obodatamodelPackage.Literals.OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP, ObsoletableObjectToNestedValueMapImpl.class, this, obodatamodelPackage.OBSOLETABLE_OBJECT__CONSIDER_EXTENSION);
    }
    return considerExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EMap<ObsoletableObject, NestedValue> getReplacedByExtension() {
    if (replacedByExtension == null) {
      replacedByExtension = new EcoreEMap<ObsoletableObject,NestedValue>(obodatamodelPackage.Literals.OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP, ObsoletableObjectToNestedValueMapImpl.class, this, obodatamodelPackage.OBSOLETABLE_OBJECT__REPLACED_BY_EXTENSION);
    }
    return replacedByExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getObsoleteExtension() {
    return obsoleteExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetObsoleteExtension(NestedValue newObsoleteExtension, NotificationChain msgs) {
    NestedValue oldObsoleteExtension = obsoleteExtension;
    obsoleteExtension = newObsoleteExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE_EXTENSION, oldObsoleteExtension, newObsoleteExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setObsoleteExtension(NestedValue newObsoleteExtension) {
    if (newObsoleteExtension != obsoleteExtension) {
      NotificationChain msgs = null;
      if (obsoleteExtension != null)
        msgs = ((InternalEObject)obsoleteExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE_EXTENSION, null, msgs);
      if (newObsoleteExtension != null)
        msgs = ((InternalEObject)newObsoleteExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE_EXTENSION, null, msgs);
      msgs = basicSetObsoleteExtension(newObsoleteExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE_EXTENSION, newObsoleteExtension, newObsoleteExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.OBSOLETABLE_OBJECT__CONSIDER_EXTENSION:
        return ((InternalEList<?>)getConsiderExtension()).basicRemove(otherEnd, msgs);
      case obodatamodelPackage.OBSOLETABLE_OBJECT__REPLACED_BY_EXTENSION:
        return ((InternalEList<?>)getReplacedByExtension()).basicRemove(otherEnd, msgs);
      case obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE_EXTENSION:
        return basicSetObsoleteExtension(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE:
        return isObsolete();
      case obodatamodelPackage.OBSOLETABLE_OBJECT__REPLACED_BY:
        return getReplacedBy();
      case obodatamodelPackage.OBSOLETABLE_OBJECT__CONSIDER_REPLACEMENTS:
        return getConsiderReplacements();
      case obodatamodelPackage.OBSOLETABLE_OBJECT__CONSIDER_EXTENSION:
        if (coreType) return getConsiderExtension();
        else return getConsiderExtension().map();
      case obodatamodelPackage.OBSOLETABLE_OBJECT__REPLACED_BY_EXTENSION:
        if (coreType) return getReplacedByExtension();
        else return getReplacedByExtension().map();
      case obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE_EXTENSION:
        return getObsoleteExtension();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE:
        setObsolete((Boolean)newValue);
        return;
      case obodatamodelPackage.OBSOLETABLE_OBJECT__REPLACED_BY:
        getReplacedBy().clear();
        getReplacedBy().addAll((Collection<? extends ObsoletableObject>)newValue);
        return;
      case obodatamodelPackage.OBSOLETABLE_OBJECT__CONSIDER_REPLACEMENTS:
        getConsiderReplacements().clear();
        getConsiderReplacements().addAll((Collection<? extends ObsoletableObject>)newValue);
        return;
      case obodatamodelPackage.OBSOLETABLE_OBJECT__CONSIDER_EXTENSION:
        ((EStructuralFeature.Setting)getConsiderExtension()).set(newValue);
        return;
      case obodatamodelPackage.OBSOLETABLE_OBJECT__REPLACED_BY_EXTENSION:
        ((EStructuralFeature.Setting)getReplacedByExtension()).set(newValue);
        return;
      case obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE_EXTENSION:
        setObsoleteExtension((NestedValue)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE:
        setObsolete(OBSOLETE_EDEFAULT);
        return;
      case obodatamodelPackage.OBSOLETABLE_OBJECT__REPLACED_BY:
        getReplacedBy().clear();
        return;
      case obodatamodelPackage.OBSOLETABLE_OBJECT__CONSIDER_REPLACEMENTS:
        getConsiderReplacements().clear();
        return;
      case obodatamodelPackage.OBSOLETABLE_OBJECT__CONSIDER_EXTENSION:
        getConsiderExtension().clear();
        return;
      case obodatamodelPackage.OBSOLETABLE_OBJECT__REPLACED_BY_EXTENSION:
        getReplacedByExtension().clear();
        return;
      case obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE_EXTENSION:
        setObsoleteExtension((NestedValue)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE:
        return obsolete != OBSOLETE_EDEFAULT;
      case obodatamodelPackage.OBSOLETABLE_OBJECT__REPLACED_BY:
        return replacedBy != null && !replacedBy.isEmpty();
      case obodatamodelPackage.OBSOLETABLE_OBJECT__CONSIDER_REPLACEMENTS:
        return considerReplacements != null && !considerReplacements.isEmpty();
      case obodatamodelPackage.OBSOLETABLE_OBJECT__CONSIDER_EXTENSION:
        return considerExtension != null && !considerExtension.isEmpty();
      case obodatamodelPackage.OBSOLETABLE_OBJECT__REPLACED_BY_EXTENSION:
        return replacedByExtension != null && !replacedByExtension.isEmpty();
      case obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE_EXTENSION:
        return obsoleteExtension != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (obsolete: ");
    result.append(obsolete);
    result.append(')');
    return result.toString();
  }

} //ObsoletableObjectImpl
