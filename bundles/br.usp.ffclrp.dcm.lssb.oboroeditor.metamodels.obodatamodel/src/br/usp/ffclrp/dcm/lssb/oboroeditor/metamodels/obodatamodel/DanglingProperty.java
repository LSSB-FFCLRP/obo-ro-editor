/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dangling Property</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getDanglingProperty()
 * @model
 * @generated
 */
public interface DanglingProperty extends LinkedObject, OBOProperty {
} // DanglingProperty
