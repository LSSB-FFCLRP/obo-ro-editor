/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PathCapable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>OBO Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOObjectImpl#getParents <em>Parents</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOObjectImpl#getChildren <em>Children</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class OBOObjectImpl extends AnnotatedObjectImpl implements OBOObject {
  /**
   * The cached value of the '{@link #getParents() <em>Parents</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParents()
   * @generated
   * @ordered
   */
  protected EList<Relationship> parents;

  /**
   * The cached value of the '{@link #getChildren() <em>Children</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getChildren()
   * @generated
   * @ordered
   */
  protected EList<Relationship> children;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OBOObjectImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.OBO_OBJECT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Relationship> getParents() {
    if (parents == null) {
      parents = new EObjectContainmentWithInverseEList<Relationship>(Relationship.class, this, obodatamodelPackage.OBO_OBJECT__PARENTS, obodatamodelPackage.RELATIONSHIP__CHILD);
    }
    return parents;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Relationship> getChildren() {
    if (children == null) {
      children = new EObjectWithInverseResolvingEList<Relationship>(Relationship.class, this, obodatamodelPackage.OBO_OBJECT__CHILDREN, obodatamodelPackage.RELATIONSHIP__PARENT);
    }
    return children;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.OBO_OBJECT__PARENTS:
        return ((InternalEList<InternalEObject>)(InternalEList<?>)getParents()).basicAdd(otherEnd, msgs);
      case obodatamodelPackage.OBO_OBJECT__CHILDREN:
        return ((InternalEList<InternalEObject>)(InternalEList<?>)getChildren()).basicAdd(otherEnd, msgs);
    }
    return super.eInverseAdd(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.OBO_OBJECT__PARENTS:
        return ((InternalEList<?>)getParents()).basicRemove(otherEnd, msgs);
      case obodatamodelPackage.OBO_OBJECT__CHILDREN:
        return ((InternalEList<?>)getChildren()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.OBO_OBJECT__PARENTS:
        return getParents();
      case obodatamodelPackage.OBO_OBJECT__CHILDREN:
        return getChildren();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.OBO_OBJECT__PARENTS:
        getParents().clear();
        getParents().addAll((Collection<? extends Relationship>)newValue);
        return;
      case obodatamodelPackage.OBO_OBJECT__CHILDREN:
        getChildren().clear();
        getChildren().addAll((Collection<? extends Relationship>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.OBO_OBJECT__PARENTS:
        getParents().clear();
        return;
      case obodatamodelPackage.OBO_OBJECT__CHILDREN:
        getChildren().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.OBO_OBJECT__PARENTS:
        return parents != null && !parents.isEmpty();
      case obodatamodelPackage.OBO_OBJECT__CHILDREN:
        return children != null && !children.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
    if (baseClass == PathCapable.class) {
      switch (derivedFeatureID) {
        default: return -1;
      }
    }
    if (baseClass == LinkedObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.OBO_OBJECT__PARENTS: return obodatamodelPackage.LINKED_OBJECT__PARENTS;
        case obodatamodelPackage.OBO_OBJECT__CHILDREN: return obodatamodelPackage.LINKED_OBJECT__CHILDREN;
        default: return -1;
      }
    }
    if (baseClass == br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable.class) {
      switch (derivedFeatureID) {
        default: return -1;
      }
    }
    return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
    if (baseClass == PathCapable.class) {
      switch (baseFeatureID) {
        default: return -1;
      }
    }
    if (baseClass == LinkedObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.LINKED_OBJECT__PARENTS: return obodatamodelPackage.OBO_OBJECT__PARENTS;
        case obodatamodelPackage.LINKED_OBJECT__CHILDREN: return obodatamodelPackage.OBO_OBJECT__CHILDREN;
        default: return -1;
      }
    }
    if (baseClass == br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable.class) {
      switch (baseFeatureID) {
        default: return -1;
      }
    }
    return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
  }

} //OBOObjectImpl
