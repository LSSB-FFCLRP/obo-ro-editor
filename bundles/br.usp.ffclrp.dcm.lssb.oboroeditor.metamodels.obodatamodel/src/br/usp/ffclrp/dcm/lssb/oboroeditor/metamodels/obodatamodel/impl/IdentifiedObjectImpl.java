/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NamespacedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Identified Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiedObjectImpl#getId <em>Id</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiedObjectImpl#isAnonymous <em>Anonymous</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiedObjectImpl#getNamespace <em>Namespace</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiedObjectImpl#getNamespaceExtension <em>Namespace Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiedObjectImpl#getTypeExtension <em>Type Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiedObjectImpl#isBuiltIn <em>Built In</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiedObjectImpl#getName <em>Name</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiedObjectImpl#getNameExtension <em>Name Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiedObjectImpl#getIdExtension <em>Id Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiedObjectImpl#getAnonymousExtension <em>Anonymous Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiedObjectImpl#getPropertyValues <em>Property Values</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class IdentifiedObjectImpl extends ValueImpl implements IdentifiedObject {
  /**
   * The default value of the '{@link #getId() <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getId()
   * @generated
   * @ordered
   */
  protected static final String ID_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getId()
   * @generated
   * @ordered
   */
  protected String id = ID_EDEFAULT;

  /**
   * The default value of the '{@link #isAnonymous() <em>Anonymous</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isAnonymous()
   * @generated
   * @ordered
   */
  protected static final boolean ANONYMOUS_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isAnonymous() <em>Anonymous</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isAnonymous()
   * @generated
   * @ordered
   */
  protected boolean anonymous = ANONYMOUS_EDEFAULT;

  /**
   * The cached value of the '{@link #getNamespace() <em>Namespace</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNamespace()
   * @generated
   * @ordered
   */
  protected Namespace namespace;

  /**
   * The cached value of the '{@link #getNamespaceExtension() <em>Namespace Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNamespaceExtension()
   * @generated
   * @ordered
   */
  protected NestedValue namespaceExtension;

  /**
   * The cached value of the '{@link #getTypeExtension() <em>Type Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTypeExtension()
   * @generated
   * @ordered
   */
  protected NestedValue typeExtension;

  /**
   * The default value of the '{@link #isBuiltIn() <em>Built In</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isBuiltIn()
   * @generated
   * @ordered
   */
  protected static final boolean BUILT_IN_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isBuiltIn() <em>Built In</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isBuiltIn()
   * @generated
   * @ordered
   */
  protected boolean builtIn = BUILT_IN_EDEFAULT;

  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getNameExtension() <em>Name Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNameExtension()
   * @generated
   * @ordered
   */
  protected NestedValue nameExtension;

  /**
   * The cached value of the '{@link #getIdExtension() <em>Id Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdExtension()
   * @generated
   * @ordered
   */
  protected NestedValue idExtension;

  /**
   * The cached value of the '{@link #getAnonymousExtension() <em>Anonymous Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAnonymousExtension()
   * @generated
   * @ordered
   */
  protected NestedValue anonymousExtension;

  /**
   * The cached value of the '{@link #getPropertyValues() <em>Property Values</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPropertyValues()
   * @generated
   * @ordered
   */
  protected EList<PropertyValue> propertyValues;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected IdentifiedObjectImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.IDENTIFIED_OBJECT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getId() {
    return id;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setId(String newId) {
    String oldId = id;
    id = newId;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.IDENTIFIED_OBJECT__ID, oldId, id));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isAnonymous() {
    return anonymous;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAnonymous(boolean newAnonymous) {
    boolean oldAnonymous = anonymous;
    anonymous = newAnonymous;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.IDENTIFIED_OBJECT__ANONYMOUS, oldAnonymous, anonymous));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Namespace getNamespace() {
    if (namespace != null && namespace.eIsProxy()) {
      InternalEObject oldNamespace = (InternalEObject)namespace;
      namespace = (Namespace)eResolveProxy(oldNamespace);
      if (namespace != oldNamespace) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, obodatamodelPackage.IDENTIFIED_OBJECT__NAMESPACE, oldNamespace, namespace));
      }
    }
    return namespace;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Namespace basicGetNamespace() {
    return namespace;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNamespace(Namespace newNamespace) {
    Namespace oldNamespace = namespace;
    namespace = newNamespace;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.IDENTIFIED_OBJECT__NAMESPACE, oldNamespace, namespace));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getNamespaceExtension() {
    return namespaceExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetNamespaceExtension(NestedValue newNamespaceExtension, NotificationChain msgs) {
    NestedValue oldNamespaceExtension = namespaceExtension;
    namespaceExtension = newNamespaceExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.IDENTIFIED_OBJECT__NAMESPACE_EXTENSION, oldNamespaceExtension, newNamespaceExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNamespaceExtension(NestedValue newNamespaceExtension) {
    if (newNamespaceExtension != namespaceExtension) {
      NotificationChain msgs = null;
      if (namespaceExtension != null)
        msgs = ((InternalEObject)namespaceExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.IDENTIFIED_OBJECT__NAMESPACE_EXTENSION, null, msgs);
      if (newNamespaceExtension != null)
        msgs = ((InternalEObject)newNamespaceExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.IDENTIFIED_OBJECT__NAMESPACE_EXTENSION, null, msgs);
      msgs = basicSetNamespaceExtension(newNamespaceExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.IDENTIFIED_OBJECT__NAMESPACE_EXTENSION, newNamespaceExtension, newNamespaceExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getTypeExtension() {
    return typeExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTypeExtension(NestedValue newTypeExtension, NotificationChain msgs) {
    NestedValue oldTypeExtension = typeExtension;
    typeExtension = newTypeExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.IDENTIFIED_OBJECT__TYPE_EXTENSION, oldTypeExtension, newTypeExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTypeExtension(NestedValue newTypeExtension) {
    if (newTypeExtension != typeExtension) {
      NotificationChain msgs = null;
      if (typeExtension != null)
        msgs = ((InternalEObject)typeExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.IDENTIFIED_OBJECT__TYPE_EXTENSION, null, msgs);
      if (newTypeExtension != null)
        msgs = ((InternalEObject)newTypeExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.IDENTIFIED_OBJECT__TYPE_EXTENSION, null, msgs);
      msgs = basicSetTypeExtension(newTypeExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.IDENTIFIED_OBJECT__TYPE_EXTENSION, newTypeExtension, newTypeExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isBuiltIn() {
    return builtIn;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBuiltIn(boolean newBuiltIn) {
    boolean oldBuiltIn = builtIn;
    builtIn = newBuiltIn;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.IDENTIFIED_OBJECT__BUILT_IN, oldBuiltIn, builtIn));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName() {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName) {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.IDENTIFIED_OBJECT__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getNameExtension() {
    return nameExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetNameExtension(NestedValue newNameExtension, NotificationChain msgs) {
    NestedValue oldNameExtension = nameExtension;
    nameExtension = newNameExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.IDENTIFIED_OBJECT__NAME_EXTENSION, oldNameExtension, newNameExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNameExtension(NestedValue newNameExtension) {
    if (newNameExtension != nameExtension) {
      NotificationChain msgs = null;
      if (nameExtension != null)
        msgs = ((InternalEObject)nameExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.IDENTIFIED_OBJECT__NAME_EXTENSION, null, msgs);
      if (newNameExtension != null)
        msgs = ((InternalEObject)newNameExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.IDENTIFIED_OBJECT__NAME_EXTENSION, null, msgs);
      msgs = basicSetNameExtension(newNameExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.IDENTIFIED_OBJECT__NAME_EXTENSION, newNameExtension, newNameExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getIdExtension() {
    return idExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetIdExtension(NestedValue newIdExtension, NotificationChain msgs) {
    NestedValue oldIdExtension = idExtension;
    idExtension = newIdExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.IDENTIFIED_OBJECT__ID_EXTENSION, oldIdExtension, newIdExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIdExtension(NestedValue newIdExtension) {
    if (newIdExtension != idExtension) {
      NotificationChain msgs = null;
      if (idExtension != null)
        msgs = ((InternalEObject)idExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.IDENTIFIED_OBJECT__ID_EXTENSION, null, msgs);
      if (newIdExtension != null)
        msgs = ((InternalEObject)newIdExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.IDENTIFIED_OBJECT__ID_EXTENSION, null, msgs);
      msgs = basicSetIdExtension(newIdExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.IDENTIFIED_OBJECT__ID_EXTENSION, newIdExtension, newIdExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getAnonymousExtension() {
    return anonymousExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAnonymousExtension(NestedValue newAnonymousExtension, NotificationChain msgs) {
    NestedValue oldAnonymousExtension = anonymousExtension;
    anonymousExtension = newAnonymousExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION, oldAnonymousExtension, newAnonymousExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAnonymousExtension(NestedValue newAnonymousExtension) {
    if (newAnonymousExtension != anonymousExtension) {
      NotificationChain msgs = null;
      if (anonymousExtension != null)
        msgs = ((InternalEObject)anonymousExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION, null, msgs);
      if (newAnonymousExtension != null)
        msgs = ((InternalEObject)newAnonymousExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION, null, msgs);
      msgs = basicSetAnonymousExtension(newAnonymousExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION, newAnonymousExtension, newAnonymousExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<PropertyValue> getPropertyValues() {
    if (propertyValues == null) {
      propertyValues = new EObjectContainmentEList<PropertyValue>(PropertyValue.class, this, obodatamodelPackage.IDENTIFIED_OBJECT__PROPERTY_VALUES);
    }
    return propertyValues;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.IDENTIFIED_OBJECT__NAMESPACE_EXTENSION:
        return basicSetNamespaceExtension(null, msgs);
      case obodatamodelPackage.IDENTIFIED_OBJECT__TYPE_EXTENSION:
        return basicSetTypeExtension(null, msgs);
      case obodatamodelPackage.IDENTIFIED_OBJECT__NAME_EXTENSION:
        return basicSetNameExtension(null, msgs);
      case obodatamodelPackage.IDENTIFIED_OBJECT__ID_EXTENSION:
        return basicSetIdExtension(null, msgs);
      case obodatamodelPackage.IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION:
        return basicSetAnonymousExtension(null, msgs);
      case obodatamodelPackage.IDENTIFIED_OBJECT__PROPERTY_VALUES:
        return ((InternalEList<?>)getPropertyValues()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.IDENTIFIED_OBJECT__ID:
        return getId();
      case obodatamodelPackage.IDENTIFIED_OBJECT__ANONYMOUS:
        return isAnonymous();
      case obodatamodelPackage.IDENTIFIED_OBJECT__NAMESPACE:
        if (resolve) return getNamespace();
        return basicGetNamespace();
      case obodatamodelPackage.IDENTIFIED_OBJECT__NAMESPACE_EXTENSION:
        return getNamespaceExtension();
      case obodatamodelPackage.IDENTIFIED_OBJECT__TYPE_EXTENSION:
        return getTypeExtension();
      case obodatamodelPackage.IDENTIFIED_OBJECT__BUILT_IN:
        return isBuiltIn();
      case obodatamodelPackage.IDENTIFIED_OBJECT__NAME:
        return getName();
      case obodatamodelPackage.IDENTIFIED_OBJECT__NAME_EXTENSION:
        return getNameExtension();
      case obodatamodelPackage.IDENTIFIED_OBJECT__ID_EXTENSION:
        return getIdExtension();
      case obodatamodelPackage.IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION:
        return getAnonymousExtension();
      case obodatamodelPackage.IDENTIFIED_OBJECT__PROPERTY_VALUES:
        return getPropertyValues();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.IDENTIFIED_OBJECT__ID:
        setId((String)newValue);
        return;
      case obodatamodelPackage.IDENTIFIED_OBJECT__ANONYMOUS:
        setAnonymous((Boolean)newValue);
        return;
      case obodatamodelPackage.IDENTIFIED_OBJECT__NAMESPACE:
        setNamespace((Namespace)newValue);
        return;
      case obodatamodelPackage.IDENTIFIED_OBJECT__NAMESPACE_EXTENSION:
        setNamespaceExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.IDENTIFIED_OBJECT__TYPE_EXTENSION:
        setTypeExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.IDENTIFIED_OBJECT__BUILT_IN:
        setBuiltIn((Boolean)newValue);
        return;
      case obodatamodelPackage.IDENTIFIED_OBJECT__NAME:
        setName((String)newValue);
        return;
      case obodatamodelPackage.IDENTIFIED_OBJECT__NAME_EXTENSION:
        setNameExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.IDENTIFIED_OBJECT__ID_EXTENSION:
        setIdExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION:
        setAnonymousExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.IDENTIFIED_OBJECT__PROPERTY_VALUES:
        getPropertyValues().clear();
        getPropertyValues().addAll((Collection<? extends PropertyValue>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.IDENTIFIED_OBJECT__ID:
        setId(ID_EDEFAULT);
        return;
      case obodatamodelPackage.IDENTIFIED_OBJECT__ANONYMOUS:
        setAnonymous(ANONYMOUS_EDEFAULT);
        return;
      case obodatamodelPackage.IDENTIFIED_OBJECT__NAMESPACE:
        setNamespace((Namespace)null);
        return;
      case obodatamodelPackage.IDENTIFIED_OBJECT__NAMESPACE_EXTENSION:
        setNamespaceExtension((NestedValue)null);
        return;
      case obodatamodelPackage.IDENTIFIED_OBJECT__TYPE_EXTENSION:
        setTypeExtension((NestedValue)null);
        return;
      case obodatamodelPackage.IDENTIFIED_OBJECT__BUILT_IN:
        setBuiltIn(BUILT_IN_EDEFAULT);
        return;
      case obodatamodelPackage.IDENTIFIED_OBJECT__NAME:
        setName(NAME_EDEFAULT);
        return;
      case obodatamodelPackage.IDENTIFIED_OBJECT__NAME_EXTENSION:
        setNameExtension((NestedValue)null);
        return;
      case obodatamodelPackage.IDENTIFIED_OBJECT__ID_EXTENSION:
        setIdExtension((NestedValue)null);
        return;
      case obodatamodelPackage.IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION:
        setAnonymousExtension((NestedValue)null);
        return;
      case obodatamodelPackage.IDENTIFIED_OBJECT__PROPERTY_VALUES:
        getPropertyValues().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.IDENTIFIED_OBJECT__ID:
        return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
      case obodatamodelPackage.IDENTIFIED_OBJECT__ANONYMOUS:
        return anonymous != ANONYMOUS_EDEFAULT;
      case obodatamodelPackage.IDENTIFIED_OBJECT__NAMESPACE:
        return namespace != null;
      case obodatamodelPackage.IDENTIFIED_OBJECT__NAMESPACE_EXTENSION:
        return namespaceExtension != null;
      case obodatamodelPackage.IDENTIFIED_OBJECT__TYPE_EXTENSION:
        return typeExtension != null;
      case obodatamodelPackage.IDENTIFIED_OBJECT__BUILT_IN:
        return builtIn != BUILT_IN_EDEFAULT;
      case obodatamodelPackage.IDENTIFIED_OBJECT__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case obodatamodelPackage.IDENTIFIED_OBJECT__NAME_EXTENSION:
        return nameExtension != null;
      case obodatamodelPackage.IDENTIFIED_OBJECT__ID_EXTENSION:
        return idExtension != null;
      case obodatamodelPackage.IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION:
        return anonymousExtension != null;
      case obodatamodelPackage.IDENTIFIED_OBJECT__PROPERTY_VALUES:
        return propertyValues != null && !propertyValues.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
    if (baseClass == IdentifiableObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.IDENTIFIED_OBJECT__ID: return obodatamodelPackage.IDENTIFIABLE_OBJECT__ID;
        case obodatamodelPackage.IDENTIFIED_OBJECT__ANONYMOUS: return obodatamodelPackage.IDENTIFIABLE_OBJECT__ANONYMOUS;
        default: return -1;
      }
    }
    if (baseClass == NamespacedObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.IDENTIFIED_OBJECT__NAMESPACE: return obodatamodelPackage.NAMESPACED_OBJECT__NAMESPACE;
        case obodatamodelPackage.IDENTIFIED_OBJECT__NAMESPACE_EXTENSION: return obodatamodelPackage.NAMESPACED_OBJECT__NAMESPACE_EXTENSION;
        default: return -1;
      }
    }
    return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
    if (baseClass == IdentifiableObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.IDENTIFIABLE_OBJECT__ID: return obodatamodelPackage.IDENTIFIED_OBJECT__ID;
        case obodatamodelPackage.IDENTIFIABLE_OBJECT__ANONYMOUS: return obodatamodelPackage.IDENTIFIED_OBJECT__ANONYMOUS;
        default: return -1;
      }
    }
    if (baseClass == NamespacedObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.NAMESPACED_OBJECT__NAMESPACE: return obodatamodelPackage.IDENTIFIED_OBJECT__NAMESPACE;
        case obodatamodelPackage.NAMESPACED_OBJECT__NAMESPACE_EXTENSION: return obodatamodelPackage.IDENTIFIED_OBJECT__NAMESPACE_EXTENSION;
        default: return -1;
      }
    }
    return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (id: ");
    result.append(id);
    result.append(", anonymous: ");
    result.append(anonymous);
    result.append(", builtIn: ");
    result.append(builtIn);
    result.append(", name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //IdentifiedObjectImpl
