/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.AnnotatedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingProperty;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Datatype;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DatatypeValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DbxrefedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPath;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPathSpec;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObjectIndex;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Instance;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkLinkedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ListOfProperties;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MutableLinkDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOClass;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectFactory;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectField;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OboPropertyValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PathCapable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.RootAlgorithm;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SubsetObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Value;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueLink;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelFactory;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Date;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class obodatamodelFactoryImpl extends EFactoryImpl implements obodatamodelFactory {
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static obodatamodelFactory init() {
    try {
      obodatamodelFactory theobodatamodelFactory = (obodatamodelFactory)EPackage.Registry.INSTANCE.getEFactory(obodatamodelPackage.eNS_URI);
      if (theobodatamodelFactory != null) {
        return theobodatamodelFactory;
      }
    }
    catch (Exception exception) {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new obodatamodelFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public obodatamodelFactoryImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass) {
    switch (eClass.getClassifierID()) {
      case obodatamodelPackage.ANNOTATED_OBJECT: return createAnnotatedObject();
      case obodatamodelPackage.DANGLING_OBJECT: return createDanglingObject();
      case obodatamodelPackage.DANGLING_PROPERTY: return createDanglingProperty();
      case obodatamodelPackage.DATATYPE: return createDatatype();
      case obodatamodelPackage.DATATYPE_VALUE: return createDatatypeValue();
      case obodatamodelPackage.DBXREF: return createDbxref();
      case obodatamodelPackage.DBXREFED_OBJECT: return createDbxrefedObject();
      case obodatamodelPackage.FIELD_PATH: return createFieldPath();
      case obodatamodelPackage.FIELD_PATH_SPEC: return createFieldPathSpec();
      case obodatamodelPackage.IDENTIFIED_OBJECT_INDEX: return createIdentifiedObjectIndex();
      case obodatamodelPackage.INSTANCE: return createInstance();
      case obodatamodelPackage.LINK_DATABASE: return createLinkDatabase();
      case obodatamodelPackage.LINK_LINKED_OBJECT: return createLinkLinkedObject();
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT: return createModificationMetadataObject();
      case obodatamodelPackage.MUTABLE_LINK_DATABASE: return createMutableLinkDatabase();
      case obodatamodelPackage.NAMESPACE: return createNamespace();
      case obodatamodelPackage.NESTED_VALUE: return createNestedValue();
      case obodatamodelPackage.OBO_CLASS: return createOBOClass();
      case obodatamodelPackage.OBO_PROPERTY: return createOBOProperty();
      case obodatamodelPackage.OBO_RESTRICTION: return createOBORestriction();
      case obodatamodelPackage.OBO_SESSION: return createOBOSession();
      case obodatamodelPackage.OBJECT_FACTORY: return createObjectFactory();
      case obodatamodelPackage.OBJECT_FIELD: return createObjectField();
      case obodatamodelPackage.PATH_CAPABLE: return createPathCapable();
      case obodatamodelPackage.PROPERTY_VALUE: return createPropertyValue();
      case obodatamodelPackage.ROOT_ALGORITHM: return createRootAlgorithm();
      case obodatamodelPackage.SUBSET_OBJECT: return createSubsetObject();
      case obodatamodelPackage.SYNONYM: return createSynonym();
      case obodatamodelPackage.SYNONYM_TYPE: return createSynonymType();
      case obodatamodelPackage.SYNONYMED_OBJECT: return createSynonymedObject();
      case obodatamodelPackage.TERM_SUBSET: return createTermSubset();
      case obodatamodelPackage.UNKNOWN_STANZA: return createUnknownStanza();
      case obodatamodelPackage.VALUE: return createValue();
      case obodatamodelPackage.VALUE_DATABASE: return createValueDatabase();
      case obodatamodelPackage.VALUE_LINK: return createValueLink();
      case obodatamodelPackage.STRING_TO_NESTED_VALUE_MAP: return (EObject)createStringToNestedValueMap();
      case obodatamodelPackage.OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP: return (EObject)createObsoletableObjectToNestedValueMap();
      case obodatamodelPackage.TERM_SUBSET_TO_NESTED_VALUE_MAP: return (EObject)createTermSubsetToNestedValueMap();
      case obodatamodelPackage.ID_SPACES_MAP: return (EObject)createIdSpacesMap();
      case obodatamodelPackage.LIST_OF_PROPERTIES: return createListOfProperties();
      case obodatamodelPackage.OBO_PROPERTY_VALUE: return createOboPropertyValue();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object createFromString(EDataType eDataType, String initialValue) {
    switch (eDataType.getClassifierID()) {
      case obodatamodelPackage.DATE:
        return createDateFromString(eDataType, initialValue);
      case obodatamodelPackage.ECORE_OBJECT:
        return createEcoreObjectFromString(eDataType, initialValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String convertToString(EDataType eDataType, Object instanceValue) {
    switch (eDataType.getClassifierID()) {
      case obodatamodelPackage.DATE:
        return convertDateToString(eDataType, instanceValue);
      case obodatamodelPackage.ECORE_OBJECT:
        return convertEcoreObjectToString(eDataType, instanceValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AnnotatedObject createAnnotatedObject() {
    AnnotatedObjectImpl annotatedObject = new AnnotatedObjectImpl();
    return annotatedObject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DanglingObject createDanglingObject() {
    DanglingObjectImpl danglingObject = new DanglingObjectImpl();
    return danglingObject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DanglingProperty createDanglingProperty() {
    DanglingPropertyImpl danglingProperty = new DanglingPropertyImpl();
    return danglingProperty;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Datatype createDatatype() {
    DatatypeImpl datatype = new DatatypeImpl();
    return datatype;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DatatypeValue createDatatypeValue() {
    DatatypeValueImpl datatypeValue = new DatatypeValueImpl();
    return datatypeValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Dbxref createDbxref() {
    DbxrefImpl dbxref = new DbxrefImpl();
    return dbxref;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DbxrefedObject createDbxrefedObject() {
    DbxrefedObjectImpl dbxrefedObject = new DbxrefedObjectImpl();
    return dbxrefedObject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FieldPath createFieldPath() {
    FieldPathImpl fieldPath = new FieldPathImpl();
    return fieldPath;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FieldPathSpec createFieldPathSpec() {
    FieldPathSpecImpl fieldPathSpec = new FieldPathSpecImpl();
    return fieldPathSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IdentifiedObjectIndex createIdentifiedObjectIndex() {
    IdentifiedObjectIndexImpl identifiedObjectIndex = new IdentifiedObjectIndexImpl();
    return identifiedObjectIndex;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Instance createInstance() {
    InstanceImpl instance = new InstanceImpl();
    return instance;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LinkDatabase createLinkDatabase() {
    LinkDatabaseImpl linkDatabase = new LinkDatabaseImpl();
    return linkDatabase;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LinkLinkedObject createLinkLinkedObject() {
    LinkLinkedObjectImpl linkLinkedObject = new LinkLinkedObjectImpl();
    return linkLinkedObject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ModificationMetadataObject createModificationMetadataObject() {
    ModificationMetadataObjectImpl modificationMetadataObject = new ModificationMetadataObjectImpl();
    return modificationMetadataObject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MutableLinkDatabase createMutableLinkDatabase() {
    MutableLinkDatabaseImpl mutableLinkDatabase = new MutableLinkDatabaseImpl();
    return mutableLinkDatabase;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Namespace createNamespace() {
    NamespaceImpl namespace = new NamespaceImpl();
    return namespace;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue createNestedValue() {
    NestedValueImpl nestedValue = new NestedValueImpl();
    return nestedValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBOClass createOBOClass() {
    OBOClassImpl oboClass = new OBOClassImpl();
    return oboClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBOProperty createOBOProperty() {
    OBOPropertyImpl oboProperty = new OBOPropertyImpl();
    return oboProperty;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBORestriction createOBORestriction() {
    OBORestrictionImpl oboRestriction = new OBORestrictionImpl();
    return oboRestriction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBOSession createOBOSession() {
    OBOSessionImpl oboSession = new OBOSessionImpl();
    return oboSession;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ObjectFactory createObjectFactory() {
    ObjectFactoryImpl objectFactory = new ObjectFactoryImpl();
    return objectFactory;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ObjectField createObjectField() {
    ObjectFieldImpl objectField = new ObjectFieldImpl();
    return objectField;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PathCapable createPathCapable() {
    PathCapableImpl pathCapable = new PathCapableImpl();
    return pathCapable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PropertyValue createPropertyValue() {
    PropertyValueImpl propertyValue = new PropertyValueImpl();
    return propertyValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RootAlgorithm createRootAlgorithm() {
    RootAlgorithmImpl rootAlgorithm = new RootAlgorithmImpl();
    return rootAlgorithm;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SubsetObject createSubsetObject() {
    SubsetObjectImpl subsetObject = new SubsetObjectImpl();
    return subsetObject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Synonym createSynonym() {
    SynonymImpl synonym = new SynonymImpl();
    return synonym;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SynonymType createSynonymType() {
    SynonymTypeImpl synonymType = new SynonymTypeImpl();
    return synonymType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SynonymedObject createSynonymedObject() {
    SynonymedObjectImpl synonymedObject = new SynonymedObjectImpl();
    return synonymedObject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TermSubset createTermSubset() {
    TermSubsetImpl termSubset = new TermSubsetImpl();
    return termSubset;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UnknownStanza createUnknownStanza() {
    UnknownStanzaImpl unknownStanza = new UnknownStanzaImpl();
    return unknownStanza;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Value createValue() {
    ValueImpl value = new ValueImpl();
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ValueDatabase createValueDatabase() {
    ValueDatabaseImpl valueDatabase = new ValueDatabaseImpl();
    return valueDatabase;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ValueLink createValueLink() {
    ValueLinkImpl valueLink = new ValueLinkImpl();
    return valueLink;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Map.Entry<String, NestedValue> createStringToNestedValueMap() {
    StringToNestedValueMapImpl stringToNestedValueMap = new StringToNestedValueMapImpl();
    return stringToNestedValueMap;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Map.Entry<ObsoletableObject, NestedValue> createObsoletableObjectToNestedValueMap() {
    ObsoletableObjectToNestedValueMapImpl obsoletableObjectToNestedValueMap = new ObsoletableObjectToNestedValueMapImpl();
    return obsoletableObjectToNestedValueMap;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Map.Entry<TermSubset, NestedValue> createTermSubsetToNestedValueMap() {
    TermSubsetToNestedValueMapImpl termSubsetToNestedValueMap = new TermSubsetToNestedValueMapImpl();
    return termSubsetToNestedValueMap;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Map.Entry<String, String> createIdSpacesMap() {
    IdSpacesMapImpl idSpacesMap = new IdSpacesMapImpl();
    return idSpacesMap;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ListOfProperties createListOfProperties() {
    ListOfPropertiesImpl listOfProperties = new ListOfPropertiesImpl();
    return listOfProperties;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OboPropertyValue createOboPropertyValue() {
    OboPropertyValueImpl oboPropertyValue = new OboPropertyValueImpl();
    return oboPropertyValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Date createDateFromString(EDataType eDataType, String initialValue) {
    return (Date)super.createFromString(eDataType, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertDateToString(EDataType eDataType, Object instanceValue) {
    return super.convertToString(eDataType, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EObject createEcoreObjectFromString(EDataType eDataType, String initialValue) {
    return (EObject)super.createFromString(eDataType, initialValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertEcoreObjectToString(EDataType eDataType, Object instanceValue) {
    return super.convertToString(eDataType, instanceValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public obodatamodelPackage getobodatamodelPackage() {
    return (obodatamodelPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static obodatamodelPackage getPackage() {
    return obodatamodelPackage.eINSTANCE;
  }

} //obodatamodelFactoryImpl
