/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DbxrefedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dbxrefed Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DbxrefedObjectImpl#getDbxrefs <em>Dbxrefs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DbxrefedObjectImpl extends MinimalEObjectImpl.Container implements DbxrefedObject {
  /**
   * The cached value of the '{@link #getDbxrefs() <em>Dbxrefs</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDbxrefs()
   * @generated
   * @ordered
   */
  protected EList<Dbxref> dbxrefs;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DbxrefedObjectImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.DBXREFED_OBJECT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Dbxref> getDbxrefs() {
    if (dbxrefs == null) {
      dbxrefs = new EObjectResolvingEList<Dbxref>(Dbxref.class, this, obodatamodelPackage.DBXREFED_OBJECT__DBXREFS);
    }
    return dbxrefs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.DBXREFED_OBJECT__DBXREFS:
        return getDbxrefs();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.DBXREFED_OBJECT__DBXREFS:
        getDbxrefs().clear();
        getDbxrefs().addAll((Collection<? extends Dbxref>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.DBXREFED_OBJECT__DBXREFS:
        getDbxrefs().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.DBXREFED_OBJECT__DBXREFS:
        return dbxrefs != null && !dbxrefs.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //DbxrefedObjectImpl
