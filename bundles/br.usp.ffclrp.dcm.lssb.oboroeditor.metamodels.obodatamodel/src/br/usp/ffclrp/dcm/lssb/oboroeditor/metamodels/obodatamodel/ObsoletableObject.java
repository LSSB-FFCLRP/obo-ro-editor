/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Obsoletable Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject#isObsolete <em>Obsolete</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject#getReplacedBy <em>Replaced By</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject#getConsiderReplacements <em>Consider Replacements</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject#getConsiderExtension <em>Consider Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject#getReplacedByExtension <em>Replaced By Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject#getObsoleteExtension <em>Obsolete Extension</em>}</li>
 * </ul>
 *
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getObsoletableObject()
 * @model abstract="true"
 * @generated
 */
public interface ObsoletableObject extends IdentifiedObject {
  /**
   * Returns the value of the '<em><b>Obsolete</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Obsolete</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Obsolete</em>' attribute.
   * @see #setObsolete(boolean)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getObsoletableObject_Obsolete()
   * @model required="true"
   * @generated
   */
  boolean isObsolete();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject#isObsolete <em>Obsolete</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Obsolete</em>' attribute.
   * @see #isObsolete()
   * @generated
   */
  void setObsolete(boolean value);

  /**
   * Returns the value of the '<em><b>Replaced By</b></em>' reference list.
   * The list contents are of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Replaced By</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Replaced By</em>' reference list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getObsoletableObject_ReplacedBy()
   * @model ordered="false"
   * @generated
   */
  EList<ObsoletableObject> getReplacedBy();

  /**
   * Returns the value of the '<em><b>Consider Replacements</b></em>' reference list.
   * The list contents are of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Consider Replacements</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Consider Replacements</em>' reference list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getObsoletableObject_ConsiderReplacements()
   * @model ordered="false"
   * @generated
   */
  EList<ObsoletableObject> getConsiderReplacements();

  /**
   * Returns the value of the '<em><b>Consider Extension</b></em>' map.
   * The key is of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject},
   * and the value is of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue},
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Consider Extension</em>' map isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Consider Extension</em>' map.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getObsoletableObject_ConsiderExtension()
   * @model mapType="br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObjectToNestedValueMap&lt;br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject, br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue&gt;" ordered="false"
   * @generated
   */
  EMap<ObsoletableObject, NestedValue> getConsiderExtension();

  /**
   * Returns the value of the '<em><b>Replaced By Extension</b></em>' map.
   * The key is of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject},
   * and the value is of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue},
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Replaced By Extension</em>' map isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Replaced By Extension</em>' map.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getObsoletableObject_ReplacedByExtension()
   * @model mapType="br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObjectToNestedValueMap&lt;br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject, br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue&gt;" ordered="false"
   * @generated
   */
  EMap<ObsoletableObject, NestedValue> getReplacedByExtension();

  /**
   * Returns the value of the '<em><b>Obsolete Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Obsolete Extension</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Obsolete Extension</em>' containment reference.
   * @see #setObsoleteExtension(NestedValue)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getObsoletableObject_ObsoleteExtension()
   * @model containment="true"
   * @generated
   */
  NestedValue getObsoleteExtension();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject#getObsoleteExtension <em>Obsolete Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Obsolete Extension</em>' containment reference.
   * @see #getObsoleteExtension()
   * @generated
   */
  void setObsoleteExtension(NestedValue value);

} // ObsoletableObject
