/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OBO Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOClass()
 * @model
 * @generated
 */
public interface OBOClass extends OBOObject, Type, br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable {
} // OBOClass
