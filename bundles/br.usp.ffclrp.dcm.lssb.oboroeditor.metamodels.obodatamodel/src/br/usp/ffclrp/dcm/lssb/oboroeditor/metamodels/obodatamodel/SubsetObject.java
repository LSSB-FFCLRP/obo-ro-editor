/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Subset Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SubsetObject#getSubsets <em>Subsets</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SubsetObject#getCategoryExtensions <em>Category Extensions</em>}</li>
 * </ul>
 *
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getSubsetObject()
 * @model
 * @generated
 */
public interface SubsetObject extends EObject {
  /**
   * Returns the value of the '<em><b>Subsets</b></em>' reference list.
   * The list contents are of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Subsets</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Subsets</em>' reference list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getSubsetObject_Subsets()
   * @model ordered="false"
   * @generated
   */
  EList<TermSubset> getSubsets();

  /**
   * Returns the value of the '<em><b>Category Extensions</b></em>' map.
   * The key is of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset},
   * and the value is of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue},
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Category Extensions</em>' map isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Category Extensions</em>' map.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getSubsetObject_CategoryExtensions()
   * @model mapType="br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubsetToNestedValueMap&lt;br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset, br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue&gt;" ordered="false"
   * @generated
   */
  EMap<TermSubset, NestedValue> getCategoryExtensions();

} // SubsetObject
