/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OBO Object</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getOBOObject()
 * @model abstract="true"
 * @generated
 */
public interface OBOObject extends AnnotatedObject, LinkedObject, br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable {
} // OBOObject
