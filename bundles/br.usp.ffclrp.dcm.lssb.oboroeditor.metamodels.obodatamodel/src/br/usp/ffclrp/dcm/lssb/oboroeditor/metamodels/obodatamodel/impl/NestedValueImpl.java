/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Nested Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.NestedValueImpl#getName <em>Name</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.NestedValueImpl#getPropertyValues <em>Property Values</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.NestedValueImpl#getSuggestedComment <em>Suggested Comment</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NestedValueImpl extends CloneableImpl implements NestedValue {
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getPropertyValues() <em>Property Values</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPropertyValues()
   * @generated
   * @ordered
   */
  protected EList<PropertyValue> propertyValues;

  /**
   * The default value of the '{@link #getSuggestedComment() <em>Suggested Comment</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSuggestedComment()
   * @generated
   * @ordered
   */
  protected static final String SUGGESTED_COMMENT_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getSuggestedComment() <em>Suggested Comment</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSuggestedComment()
   * @generated
   * @ordered
   */
  protected String suggestedComment = SUGGESTED_COMMENT_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected NestedValueImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.NESTED_VALUE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName() {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName) {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.NESTED_VALUE__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<PropertyValue> getPropertyValues() {
    if (propertyValues == null) {
      propertyValues = new EObjectContainmentEList<PropertyValue>(PropertyValue.class, this, obodatamodelPackage.NESTED_VALUE__PROPERTY_VALUES);
    }
    return propertyValues;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSuggestedComment() {
    return suggestedComment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSuggestedComment(String newSuggestedComment) {
    String oldSuggestedComment = suggestedComment;
    suggestedComment = newSuggestedComment;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.NESTED_VALUE__SUGGESTED_COMMENT, oldSuggestedComment, suggestedComment));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.NESTED_VALUE__PROPERTY_VALUES:
        return ((InternalEList<?>)getPropertyValues()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.NESTED_VALUE__NAME:
        return getName();
      case obodatamodelPackage.NESTED_VALUE__PROPERTY_VALUES:
        return getPropertyValues();
      case obodatamodelPackage.NESTED_VALUE__SUGGESTED_COMMENT:
        return getSuggestedComment();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.NESTED_VALUE__NAME:
        setName((String)newValue);
        return;
      case obodatamodelPackage.NESTED_VALUE__PROPERTY_VALUES:
        getPropertyValues().clear();
        getPropertyValues().addAll((Collection<? extends PropertyValue>)newValue);
        return;
      case obodatamodelPackage.NESTED_VALUE__SUGGESTED_COMMENT:
        setSuggestedComment((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.NESTED_VALUE__NAME:
        setName(NAME_EDEFAULT);
        return;
      case obodatamodelPackage.NESTED_VALUE__PROPERTY_VALUES:
        getPropertyValues().clear();
        return;
      case obodatamodelPackage.NESTED_VALUE__SUGGESTED_COMMENT:
        setSuggestedComment(SUGGESTED_COMMENT_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.NESTED_VALUE__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case obodatamodelPackage.NESTED_VALUE__PROPERTY_VALUES:
        return propertyValues != null && !propertyValues.isEmpty();
      case obodatamodelPackage.NESTED_VALUE__SUGGESTED_COMMENT:
        return SUGGESTED_COMMENT_EDEFAULT == null ? suggestedComment != null : !SUGGESTED_COMMENT_EDEFAULT.equals(suggestedComment);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(", suggestedComment: ");
    result.append(suggestedComment);
    result.append(')');
    return result.toString();
  }

} //NestedValueImpl
