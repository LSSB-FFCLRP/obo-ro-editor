/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Linked Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject#getParents <em>Parents</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject#getChildren <em>Children</em>}</li>
 * </ul>
 *
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getLinkedObject()
 * @model abstract="true"
 * @generated
 */
public interface LinkedObject extends IdentifiedObject, PathCapable {
  /**
   * Returns the value of the '<em><b>Parents</b></em>' containment reference list.
   * The list contents are of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship}.
   * It is bidirectional and its opposite is '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship#getChild <em>Child</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parents</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parents</em>' containment reference list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getLinkedObject_Parents()
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship#getChild
   * @model opposite="child" containment="true" ordered="false"
   * @generated
   */
  EList<Relationship> getParents();

  /**
   * Returns the value of the '<em><b>Children</b></em>' reference list.
   * The list contents are of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship}.
   * It is bidirectional and its opposite is '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship#getParent <em>Parent</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Children</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Children</em>' reference list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getLinkedObject_Children()
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship#getParent
   * @model opposite="parent" ordered="false"
   * @generated
   */
  EList<Relationship> getChildren();

} // LinkedObject
