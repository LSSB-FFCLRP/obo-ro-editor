/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;

import java.util.Date;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Modification Metadata Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getCreatedBy <em>Created By</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getCreatedByExtension <em>Created By Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getModifiedBy <em>Modified By</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getModifiedByExtension <em>Modified By Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getCreationDate <em>Creation Date</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getCreationDateExtension <em>Creation Date Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getModificationDate <em>Modification Date</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getModificationDateExtension <em>Modification Date Extension</em>}</li>
 * </ul>
 *
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getModificationMetadataObject()
 * @model
 * @generated
 */
public interface ModificationMetadataObject extends EObject {
  /**
   * Returns the value of the '<em><b>Created By</b></em>' attribute.
   * The default value is <code>""</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Created By</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Created By</em>' attribute.
   * @see #setCreatedBy(String)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getModificationMetadataObject_CreatedBy()
   * @model default="" required="true"
   * @generated
   */
  String getCreatedBy();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getCreatedBy <em>Created By</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Created By</em>' attribute.
   * @see #getCreatedBy()
   * @generated
   */
  void setCreatedBy(String value);

  /**
   * Returns the value of the '<em><b>Created By Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Created By Extension</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Created By Extension</em>' containment reference.
   * @see #setCreatedByExtension(NestedValue)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getModificationMetadataObject_CreatedByExtension()
   * @model containment="true"
   * @generated
   */
  NestedValue getCreatedByExtension();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getCreatedByExtension <em>Created By Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Created By Extension</em>' containment reference.
   * @see #getCreatedByExtension()
   * @generated
   */
  void setCreatedByExtension(NestedValue value);

  /**
   * Returns the value of the '<em><b>Modified By</b></em>' attribute.
   * The default value is <code>""</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Modified By</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Modified By</em>' attribute.
   * @see #setModifiedBy(String)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getModificationMetadataObject_ModifiedBy()
   * @model default="" required="true"
   * @generated
   */
  String getModifiedBy();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getModifiedBy <em>Modified By</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Modified By</em>' attribute.
   * @see #getModifiedBy()
   * @generated
   */
  void setModifiedBy(String value);

  /**
   * Returns the value of the '<em><b>Modified By Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Modified By Extension</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Modified By Extension</em>' containment reference.
   * @see #setModifiedByExtension(NestedValue)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getModificationMetadataObject_ModifiedByExtension()
   * @model containment="true"
   * @generated
   */
  NestedValue getModifiedByExtension();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getModifiedByExtension <em>Modified By Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Modified By Extension</em>' containment reference.
   * @see #getModifiedByExtension()
   * @generated
   */
  void setModifiedByExtension(NestedValue value);

  /**
   * Returns the value of the '<em><b>Creation Date</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Creation Date</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Creation Date</em>' attribute.
   * @see #setCreationDate(Date)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getModificationMetadataObject_CreationDate()
   * @model dataType="br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Date"
   * @generated
   */
  Date getCreationDate();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getCreationDate <em>Creation Date</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Creation Date</em>' attribute.
   * @see #getCreationDate()
   * @generated
   */
  void setCreationDate(Date value);

  /**
   * Returns the value of the '<em><b>Creation Date Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Creation Date Extension</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Creation Date Extension</em>' containment reference.
   * @see #setCreationDateExtension(NestedValue)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getModificationMetadataObject_CreationDateExtension()
   * @model containment="true"
   * @generated
   */
  NestedValue getCreationDateExtension();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getCreationDateExtension <em>Creation Date Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Creation Date Extension</em>' containment reference.
   * @see #getCreationDateExtension()
   * @generated
   */
  void setCreationDateExtension(NestedValue value);

  /**
   * Returns the value of the '<em><b>Modification Date</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Modification Date</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Modification Date</em>' attribute.
   * @see #setModificationDate(Date)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getModificationMetadataObject_ModificationDate()
   * @model dataType="br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Date"
   * @generated
   */
  Date getModificationDate();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getModificationDate <em>Modification Date</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Modification Date</em>' attribute.
   * @see #getModificationDate()
   * @generated
   */
  void setModificationDate(Date value);

  /**
   * Returns the value of the '<em><b>Modification Date Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Modification Date Extension</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Modification Date Extension</em>' containment reference.
   * @see #setModificationDateExtension(NestedValue)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getModificationMetadataObject_ModificationDateExtension()
   * @model containment="true"
   * @generated
   */
  NestedValue getModificationDateExtension();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getModificationDateExtension <em>Modification Date Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Modification Date Extension</em>' containment reference.
   * @see #getModificationDateExtension()
   * @generated
   */
  void setModificationDateExtension(NestedValue value);

} // ModificationMetadataObject
