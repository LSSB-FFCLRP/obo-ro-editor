/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.math.BigInteger;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>OBO Restriction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBORestrictionImpl#getCardinality <em>Cardinality</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBORestrictionImpl#getMaxCardinality <em>Max Cardinality</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBORestrictionImpl#getMinCardinality <em>Min Cardinality</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBORestrictionImpl#isCompletes <em>Completes</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBORestrictionImpl#isInverseCompletes <em>Inverse Completes</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBORestrictionImpl#isNecessarilyTrue <em>Necessarily True</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBORestrictionImpl#isInverseNecessarilyTrue <em>Inverse Necessarily True</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBORestrictionImpl#getAdditionalArguments <em>Additional Arguments</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OBORestrictionImpl extends LinkImpl implements OBORestriction {
  /**
   * The default value of the '{@link #getCardinality() <em>Cardinality</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCardinality()
   * @generated
   * @ordered
   */
  protected static final BigInteger CARDINALITY_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCardinality() <em>Cardinality</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCardinality()
   * @generated
   * @ordered
   */
  protected BigInteger cardinality = CARDINALITY_EDEFAULT;

  /**
   * The default value of the '{@link #getMaxCardinality() <em>Max Cardinality</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMaxCardinality()
   * @generated
   * @ordered
   */
  protected static final BigInteger MAX_CARDINALITY_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getMaxCardinality() <em>Max Cardinality</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMaxCardinality()
   * @generated
   * @ordered
   */
  protected BigInteger maxCardinality = MAX_CARDINALITY_EDEFAULT;

  /**
   * The default value of the '{@link #getMinCardinality() <em>Min Cardinality</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMinCardinality()
   * @generated
   * @ordered
   */
  protected static final BigInteger MIN_CARDINALITY_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getMinCardinality() <em>Min Cardinality</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMinCardinality()
   * @generated
   * @ordered
   */
  protected BigInteger minCardinality = MIN_CARDINALITY_EDEFAULT;

  /**
   * The default value of the '{@link #isCompletes() <em>Completes</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isCompletes()
   * @generated
   * @ordered
   */
  protected static final boolean COMPLETES_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isCompletes() <em>Completes</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isCompletes()
   * @generated
   * @ordered
   */
  protected boolean completes = COMPLETES_EDEFAULT;

  /**
   * The default value of the '{@link #isInverseCompletes() <em>Inverse Completes</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isInverseCompletes()
   * @generated
   * @ordered
   */
  protected static final boolean INVERSE_COMPLETES_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isInverseCompletes() <em>Inverse Completes</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isInverseCompletes()
   * @generated
   * @ordered
   */
  protected boolean inverseCompletes = INVERSE_COMPLETES_EDEFAULT;

  /**
   * The default value of the '{@link #isNecessarilyTrue() <em>Necessarily True</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isNecessarilyTrue()
   * @generated
   * @ordered
   */
  protected static final boolean NECESSARILY_TRUE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isNecessarilyTrue() <em>Necessarily True</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isNecessarilyTrue()
   * @generated
   * @ordered
   */
  protected boolean necessarilyTrue = NECESSARILY_TRUE_EDEFAULT;

  /**
   * The default value of the '{@link #isInverseNecessarilyTrue() <em>Inverse Necessarily True</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isInverseNecessarilyTrue()
   * @generated
   * @ordered
   */
  protected static final boolean INVERSE_NECESSARILY_TRUE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isInverseNecessarilyTrue() <em>Inverse Necessarily True</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isInverseNecessarilyTrue()
   * @generated
   * @ordered
   */
  protected boolean inverseNecessarilyTrue = INVERSE_NECESSARILY_TRUE_EDEFAULT;

  /**
   * The cached value of the '{@link #getAdditionalArguments() <em>Additional Arguments</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAdditionalArguments()
   * @generated
   * @ordered
   */
  protected EList<LinkedObject> additionalArguments;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OBORestrictionImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.OBO_RESTRICTION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BigInteger getCardinality() {
    return cardinality;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCardinality(BigInteger newCardinality) {
    BigInteger oldCardinality = cardinality;
    cardinality = newCardinality;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_RESTRICTION__CARDINALITY, oldCardinality, cardinality));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BigInteger getMaxCardinality() {
    return maxCardinality;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMaxCardinality(BigInteger newMaxCardinality) {
    BigInteger oldMaxCardinality = maxCardinality;
    maxCardinality = newMaxCardinality;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_RESTRICTION__MAX_CARDINALITY, oldMaxCardinality, maxCardinality));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BigInteger getMinCardinality() {
    return minCardinality;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMinCardinality(BigInteger newMinCardinality) {
    BigInteger oldMinCardinality = minCardinality;
    minCardinality = newMinCardinality;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_RESTRICTION__MIN_CARDINALITY, oldMinCardinality, minCardinality));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isCompletes() {
    return completes;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCompletes(boolean newCompletes) {
    boolean oldCompletes = completes;
    completes = newCompletes;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_RESTRICTION__COMPLETES, oldCompletes, completes));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isInverseCompletes() {
    return inverseCompletes;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInverseCompletes(boolean newInverseCompletes) {
    boolean oldInverseCompletes = inverseCompletes;
    inverseCompletes = newInverseCompletes;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_RESTRICTION__INVERSE_COMPLETES, oldInverseCompletes, inverseCompletes));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isNecessarilyTrue() {
    return necessarilyTrue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNecessarilyTrue(boolean newNecessarilyTrue) {
    boolean oldNecessarilyTrue = necessarilyTrue;
    necessarilyTrue = newNecessarilyTrue;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_RESTRICTION__NECESSARILY_TRUE, oldNecessarilyTrue, necessarilyTrue));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isInverseNecessarilyTrue() {
    return inverseNecessarilyTrue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInverseNecessarilyTrue(boolean newInverseNecessarilyTrue) {
    boolean oldInverseNecessarilyTrue = inverseNecessarilyTrue;
    inverseNecessarilyTrue = newInverseNecessarilyTrue;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_RESTRICTION__INVERSE_NECESSARILY_TRUE, oldInverseNecessarilyTrue, inverseNecessarilyTrue));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<LinkedObject> getAdditionalArguments() {
    if (additionalArguments == null) {
      additionalArguments = new EObjectResolvingEList<LinkedObject>(LinkedObject.class, this, obodatamodelPackage.OBO_RESTRICTION__ADDITIONAL_ARGUMENTS);
    }
    return additionalArguments;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.OBO_RESTRICTION__CARDINALITY:
        return getCardinality();
      case obodatamodelPackage.OBO_RESTRICTION__MAX_CARDINALITY:
        return getMaxCardinality();
      case obodatamodelPackage.OBO_RESTRICTION__MIN_CARDINALITY:
        return getMinCardinality();
      case obodatamodelPackage.OBO_RESTRICTION__COMPLETES:
        return isCompletes();
      case obodatamodelPackage.OBO_RESTRICTION__INVERSE_COMPLETES:
        return isInverseCompletes();
      case obodatamodelPackage.OBO_RESTRICTION__NECESSARILY_TRUE:
        return isNecessarilyTrue();
      case obodatamodelPackage.OBO_RESTRICTION__INVERSE_NECESSARILY_TRUE:
        return isInverseNecessarilyTrue();
      case obodatamodelPackage.OBO_RESTRICTION__ADDITIONAL_ARGUMENTS:
        return getAdditionalArguments();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.OBO_RESTRICTION__CARDINALITY:
        setCardinality((BigInteger)newValue);
        return;
      case obodatamodelPackage.OBO_RESTRICTION__MAX_CARDINALITY:
        setMaxCardinality((BigInteger)newValue);
        return;
      case obodatamodelPackage.OBO_RESTRICTION__MIN_CARDINALITY:
        setMinCardinality((BigInteger)newValue);
        return;
      case obodatamodelPackage.OBO_RESTRICTION__COMPLETES:
        setCompletes((Boolean)newValue);
        return;
      case obodatamodelPackage.OBO_RESTRICTION__INVERSE_COMPLETES:
        setInverseCompletes((Boolean)newValue);
        return;
      case obodatamodelPackage.OBO_RESTRICTION__NECESSARILY_TRUE:
        setNecessarilyTrue((Boolean)newValue);
        return;
      case obodatamodelPackage.OBO_RESTRICTION__INVERSE_NECESSARILY_TRUE:
        setInverseNecessarilyTrue((Boolean)newValue);
        return;
      case obodatamodelPackage.OBO_RESTRICTION__ADDITIONAL_ARGUMENTS:
        getAdditionalArguments().clear();
        getAdditionalArguments().addAll((Collection<? extends LinkedObject>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.OBO_RESTRICTION__CARDINALITY:
        setCardinality(CARDINALITY_EDEFAULT);
        return;
      case obodatamodelPackage.OBO_RESTRICTION__MAX_CARDINALITY:
        setMaxCardinality(MAX_CARDINALITY_EDEFAULT);
        return;
      case obodatamodelPackage.OBO_RESTRICTION__MIN_CARDINALITY:
        setMinCardinality(MIN_CARDINALITY_EDEFAULT);
        return;
      case obodatamodelPackage.OBO_RESTRICTION__COMPLETES:
        setCompletes(COMPLETES_EDEFAULT);
        return;
      case obodatamodelPackage.OBO_RESTRICTION__INVERSE_COMPLETES:
        setInverseCompletes(INVERSE_COMPLETES_EDEFAULT);
        return;
      case obodatamodelPackage.OBO_RESTRICTION__NECESSARILY_TRUE:
        setNecessarilyTrue(NECESSARILY_TRUE_EDEFAULT);
        return;
      case obodatamodelPackage.OBO_RESTRICTION__INVERSE_NECESSARILY_TRUE:
        setInverseNecessarilyTrue(INVERSE_NECESSARILY_TRUE_EDEFAULT);
        return;
      case obodatamodelPackage.OBO_RESTRICTION__ADDITIONAL_ARGUMENTS:
        getAdditionalArguments().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.OBO_RESTRICTION__CARDINALITY:
        return CARDINALITY_EDEFAULT == null ? cardinality != null : !CARDINALITY_EDEFAULT.equals(cardinality);
      case obodatamodelPackage.OBO_RESTRICTION__MAX_CARDINALITY:
        return MAX_CARDINALITY_EDEFAULT == null ? maxCardinality != null : !MAX_CARDINALITY_EDEFAULT.equals(maxCardinality);
      case obodatamodelPackage.OBO_RESTRICTION__MIN_CARDINALITY:
        return MIN_CARDINALITY_EDEFAULT == null ? minCardinality != null : !MIN_CARDINALITY_EDEFAULT.equals(minCardinality);
      case obodatamodelPackage.OBO_RESTRICTION__COMPLETES:
        return completes != COMPLETES_EDEFAULT;
      case obodatamodelPackage.OBO_RESTRICTION__INVERSE_COMPLETES:
        return inverseCompletes != INVERSE_COMPLETES_EDEFAULT;
      case obodatamodelPackage.OBO_RESTRICTION__NECESSARILY_TRUE:
        return necessarilyTrue != NECESSARILY_TRUE_EDEFAULT;
      case obodatamodelPackage.OBO_RESTRICTION__INVERSE_NECESSARILY_TRUE:
        return inverseNecessarilyTrue != INVERSE_NECESSARILY_TRUE_EDEFAULT;
      case obodatamodelPackage.OBO_RESTRICTION__ADDITIONAL_ARGUMENTS:
        return additionalArguments != null && !additionalArguments.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (cardinality: ");
    result.append(cardinality);
    result.append(", maxCardinality: ");
    result.append(maxCardinality);
    result.append(", minCardinality: ");
    result.append(minCardinality);
    result.append(", completes: ");
    result.append(completes);
    result.append(", inverseCompletes: ");
    result.append(inverseCompletes);
    result.append(", necessarilyTrue: ");
    result.append(necessarilyTrue);
    result.append(", inverseNecessarilyTrue: ");
    result.append(inverseNecessarilyTrue);
    result.append(')');
    return result.toString();
  }

} //OBORestrictionImpl
