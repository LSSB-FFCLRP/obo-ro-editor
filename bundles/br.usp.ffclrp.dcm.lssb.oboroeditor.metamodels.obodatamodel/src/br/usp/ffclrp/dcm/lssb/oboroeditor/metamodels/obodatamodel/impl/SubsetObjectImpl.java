/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SubsetObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Subset Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SubsetObjectImpl#getSubsets <em>Subsets</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SubsetObjectImpl#getCategoryExtensions <em>Category Extensions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubsetObjectImpl extends MinimalEObjectImpl.Container implements SubsetObject {
  /**
   * The cached value of the '{@link #getSubsets() <em>Subsets</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubsets()
   * @generated
   * @ordered
   */
  protected EList<TermSubset> subsets;

  /**
   * The cached value of the '{@link #getCategoryExtensions() <em>Category Extensions</em>}' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCategoryExtensions()
   * @generated
   * @ordered
   */
  protected EMap<TermSubset, NestedValue> categoryExtensions;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SubsetObjectImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.SUBSET_OBJECT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TermSubset> getSubsets() {
    if (subsets == null) {
      subsets = new EObjectResolvingEList<TermSubset>(TermSubset.class, this, obodatamodelPackage.SUBSET_OBJECT__SUBSETS);
    }
    return subsets;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EMap<TermSubset, NestedValue> getCategoryExtensions() {
    if (categoryExtensions == null) {
      categoryExtensions = new EcoreEMap<TermSubset,NestedValue>(obodatamodelPackage.Literals.TERM_SUBSET_TO_NESTED_VALUE_MAP, TermSubsetToNestedValueMapImpl.class, this, obodatamodelPackage.SUBSET_OBJECT__CATEGORY_EXTENSIONS);
    }
    return categoryExtensions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.SUBSET_OBJECT__CATEGORY_EXTENSIONS:
        return ((InternalEList<?>)getCategoryExtensions()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.SUBSET_OBJECT__SUBSETS:
        return getSubsets();
      case obodatamodelPackage.SUBSET_OBJECT__CATEGORY_EXTENSIONS:
        if (coreType) return getCategoryExtensions();
        else return getCategoryExtensions().map();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.SUBSET_OBJECT__SUBSETS:
        getSubsets().clear();
        getSubsets().addAll((Collection<? extends TermSubset>)newValue);
        return;
      case obodatamodelPackage.SUBSET_OBJECT__CATEGORY_EXTENSIONS:
        ((EStructuralFeature.Setting)getCategoryExtensions()).set(newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.SUBSET_OBJECT__SUBSETS:
        getSubsets().clear();
        return;
      case obodatamodelPackage.SUBSET_OBJECT__CATEGORY_EXTENSIONS:
        getCategoryExtensions().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.SUBSET_OBJECT__SUBSETS:
        return subsets != null && !subsets.isEmpty();
      case obodatamodelPackage.SUBSET_OBJECT__CATEGORY_EXTENSIONS:
        return categoryExtensions != null && !categoryExtensions.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //SubsetObjectImpl
