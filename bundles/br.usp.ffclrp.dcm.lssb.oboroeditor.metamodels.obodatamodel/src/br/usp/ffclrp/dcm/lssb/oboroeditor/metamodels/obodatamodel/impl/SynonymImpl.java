/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Serializable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.math.BigInteger;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Synonym</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymImpl#getId <em>Id</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymImpl#isAnonymous <em>Anonymous</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymImpl#getUNKNOWN_SCOPE <em>UNKNOWN SCOPE</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymImpl#getRELATED_SYNONYM <em>RELATED SYNONYM</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymImpl#getEXACT_SYNONYM <em>EXACT SYNONYM</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymImpl#getNARROW_SYNONYM <em>NARROW SYNONYM</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymImpl#getBROAD_SYNONYM <em>BROAD SYNONYM</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymImpl#getSynonymType <em>Synonym Type</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymImpl#getNestedValue <em>Nested Value</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymImpl#getScope <em>Scope</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymImpl#getXrefs <em>Xrefs</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymImpl#getText <em>Text</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SynonymImpl extends CloneableImpl implements Synonym {
  /**
   * The default value of the '{@link #getId() <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getId()
   * @generated
   * @ordered
   */
  protected static final String ID_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getId()
   * @generated
   * @ordered
   */
  protected String id = ID_EDEFAULT;

  /**
   * The default value of the '{@link #isAnonymous() <em>Anonymous</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isAnonymous()
   * @generated
   * @ordered
   */
  protected static final boolean ANONYMOUS_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isAnonymous() <em>Anonymous</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isAnonymous()
   * @generated
   * @ordered
   */
  protected boolean anonymous = ANONYMOUS_EDEFAULT;

  /**
   * The default value of the '{@link #getUNKNOWN_SCOPE() <em>UNKNOWN SCOPE</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUNKNOWN_SCOPE()
   * @generated
   * @ordered
   */
  protected static final BigInteger UNKNOWN_SCOPE_EDEFAULT = new BigInteger("-1");

  /**
   * The cached value of the '{@link #getUNKNOWN_SCOPE() <em>UNKNOWN SCOPE</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUNKNOWN_SCOPE()
   * @generated
   * @ordered
   */
  protected BigInteger unknowN_SCOPE = UNKNOWN_SCOPE_EDEFAULT;

  /**
   * The default value of the '{@link #getRELATED_SYNONYM() <em>RELATED SYNONYM</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRELATED_SYNONYM()
   * @generated
   * @ordered
   */
  protected static final BigInteger RELATED_SYNONYM_EDEFAULT = new BigInteger("0");

  /**
   * The cached value of the '{@link #getRELATED_SYNONYM() <em>RELATED SYNONYM</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRELATED_SYNONYM()
   * @generated
   * @ordered
   */
  protected BigInteger relateD_SYNONYM = RELATED_SYNONYM_EDEFAULT;

  /**
   * The default value of the '{@link #getEXACT_SYNONYM() <em>EXACT SYNONYM</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEXACT_SYNONYM()
   * @generated
   * @ordered
   */
  protected static final BigInteger EXACT_SYNONYM_EDEFAULT = new BigInteger("1");

  /**
   * The cached value of the '{@link #getEXACT_SYNONYM() <em>EXACT SYNONYM</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEXACT_SYNONYM()
   * @generated
   * @ordered
   */
  protected BigInteger exacT_SYNONYM = EXACT_SYNONYM_EDEFAULT;

  /**
   * The default value of the '{@link #getNARROW_SYNONYM() <em>NARROW SYNONYM</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNARROW_SYNONYM()
   * @generated
   * @ordered
   */
  protected static final BigInteger NARROW_SYNONYM_EDEFAULT = new BigInteger("2");

  /**
   * The cached value of the '{@link #getNARROW_SYNONYM() <em>NARROW SYNONYM</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNARROW_SYNONYM()
   * @generated
   * @ordered
   */
  protected BigInteger narroW_SYNONYM = NARROW_SYNONYM_EDEFAULT;

  /**
   * The default value of the '{@link #getBROAD_SYNONYM() <em>BROAD SYNONYM</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBROAD_SYNONYM()
   * @generated
   * @ordered
   */
  protected static final BigInteger BROAD_SYNONYM_EDEFAULT = new BigInteger("3");

  /**
   * The cached value of the '{@link #getBROAD_SYNONYM() <em>BROAD SYNONYM</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBROAD_SYNONYM()
   * @generated
   * @ordered
   */
  protected BigInteger broaD_SYNONYM = BROAD_SYNONYM_EDEFAULT;

  /**
   * The cached value of the '{@link #getSynonymType() <em>Synonym Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSynonymType()
   * @generated
   * @ordered
   */
  protected SynonymType synonymType;

  /**
   * The cached value of the '{@link #getNestedValue() <em>Nested Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNestedValue()
   * @generated
   * @ordered
   */
  protected NestedValue nestedValue;

  /**
   * The default value of the '{@link #getScope() <em>Scope</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getScope()
   * @generated
   * @ordered
   */
  protected static final BigInteger SCOPE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getScope() <em>Scope</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getScope()
   * @generated
   * @ordered
   */
  protected BigInteger scope = SCOPE_EDEFAULT;

  /**
   * The cached value of the '{@link #getXrefs() <em>Xrefs</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getXrefs()
   * @generated
   * @ordered
   */
  protected EList<Dbxref> xrefs;

  /**
   * The default value of the '{@link #getText() <em>Text</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getText()
   * @generated
   * @ordered
   */
  protected static final String TEXT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getText() <em>Text</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getText()
   * @generated
   * @ordered
   */
  protected String text = TEXT_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SynonymImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.SYNONYM;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getId() {
    return id;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setId(String newId) {
    String oldId = id;
    id = newId;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.SYNONYM__ID, oldId, id));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isAnonymous() {
    return anonymous;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAnonymous(boolean newAnonymous) {
    boolean oldAnonymous = anonymous;
    anonymous = newAnonymous;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.SYNONYM__ANONYMOUS, oldAnonymous, anonymous));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BigInteger getUNKNOWN_SCOPE() {
    return unknowN_SCOPE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BigInteger getRELATED_SYNONYM() {
    return relateD_SYNONYM;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BigInteger getEXACT_SYNONYM() {
    return exacT_SYNONYM;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BigInteger getNARROW_SYNONYM() {
    return narroW_SYNONYM;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BigInteger getBROAD_SYNONYM() {
    return broaD_SYNONYM;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SynonymType getSynonymType() {
    return synonymType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSynonymType(SynonymType newSynonymType, NotificationChain msgs) {
    SynonymType oldSynonymType = synonymType;
    synonymType = newSynonymType;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.SYNONYM__SYNONYM_TYPE, oldSynonymType, newSynonymType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSynonymType(SynonymType newSynonymType) {
    if (newSynonymType != synonymType) {
      NotificationChain msgs = null;
      if (synonymType != null)
        msgs = ((InternalEObject)synonymType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.SYNONYM__SYNONYM_TYPE, null, msgs);
      if (newSynonymType != null)
        msgs = ((InternalEObject)newSynonymType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.SYNONYM__SYNONYM_TYPE, null, msgs);
      msgs = basicSetSynonymType(newSynonymType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.SYNONYM__SYNONYM_TYPE, newSynonymType, newSynonymType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getNestedValue() {
    return nestedValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetNestedValue(NestedValue newNestedValue, NotificationChain msgs) {
    NestedValue oldNestedValue = nestedValue;
    nestedValue = newNestedValue;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.SYNONYM__NESTED_VALUE, oldNestedValue, newNestedValue);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNestedValue(NestedValue newNestedValue) {
    if (newNestedValue != nestedValue) {
      NotificationChain msgs = null;
      if (nestedValue != null)
        msgs = ((InternalEObject)nestedValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.SYNONYM__NESTED_VALUE, null, msgs);
      if (newNestedValue != null)
        msgs = ((InternalEObject)newNestedValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.SYNONYM__NESTED_VALUE, null, msgs);
      msgs = basicSetNestedValue(newNestedValue, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.SYNONYM__NESTED_VALUE, newNestedValue, newNestedValue));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BigInteger getScope() {
    return scope;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setScope(BigInteger newScope) {
    BigInteger oldScope = scope;
    scope = newScope;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.SYNONYM__SCOPE, oldScope, scope));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Dbxref> getXrefs() {
    if (xrefs == null) {
      xrefs = new EObjectResolvingEList<Dbxref>(Dbxref.class, this, obodatamodelPackage.SYNONYM__XREFS);
    }
    return xrefs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getText() {
    return text;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setText(String newText) {
    String oldText = text;
    text = newText;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.SYNONYM__TEXT, oldText, text));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.SYNONYM__SYNONYM_TYPE:
        return basicSetSynonymType(null, msgs);
      case obodatamodelPackage.SYNONYM__NESTED_VALUE:
        return basicSetNestedValue(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.SYNONYM__ID:
        return getId();
      case obodatamodelPackage.SYNONYM__ANONYMOUS:
        return isAnonymous();
      case obodatamodelPackage.SYNONYM__UNKNOWN_SCOPE:
        return getUNKNOWN_SCOPE();
      case obodatamodelPackage.SYNONYM__RELATED_SYNONYM:
        return getRELATED_SYNONYM();
      case obodatamodelPackage.SYNONYM__EXACT_SYNONYM:
        return getEXACT_SYNONYM();
      case obodatamodelPackage.SYNONYM__NARROW_SYNONYM:
        return getNARROW_SYNONYM();
      case obodatamodelPackage.SYNONYM__BROAD_SYNONYM:
        return getBROAD_SYNONYM();
      case obodatamodelPackage.SYNONYM__SYNONYM_TYPE:
        return getSynonymType();
      case obodatamodelPackage.SYNONYM__NESTED_VALUE:
        return getNestedValue();
      case obodatamodelPackage.SYNONYM__SCOPE:
        return getScope();
      case obodatamodelPackage.SYNONYM__XREFS:
        return getXrefs();
      case obodatamodelPackage.SYNONYM__TEXT:
        return getText();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.SYNONYM__ID:
        setId((String)newValue);
        return;
      case obodatamodelPackage.SYNONYM__ANONYMOUS:
        setAnonymous((Boolean)newValue);
        return;
      case obodatamodelPackage.SYNONYM__SYNONYM_TYPE:
        setSynonymType((SynonymType)newValue);
        return;
      case obodatamodelPackage.SYNONYM__NESTED_VALUE:
        setNestedValue((NestedValue)newValue);
        return;
      case obodatamodelPackage.SYNONYM__SCOPE:
        setScope((BigInteger)newValue);
        return;
      case obodatamodelPackage.SYNONYM__XREFS:
        getXrefs().clear();
        getXrefs().addAll((Collection<? extends Dbxref>)newValue);
        return;
      case obodatamodelPackage.SYNONYM__TEXT:
        setText((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.SYNONYM__ID:
        setId(ID_EDEFAULT);
        return;
      case obodatamodelPackage.SYNONYM__ANONYMOUS:
        setAnonymous(ANONYMOUS_EDEFAULT);
        return;
      case obodatamodelPackage.SYNONYM__SYNONYM_TYPE:
        setSynonymType((SynonymType)null);
        return;
      case obodatamodelPackage.SYNONYM__NESTED_VALUE:
        setNestedValue((NestedValue)null);
        return;
      case obodatamodelPackage.SYNONYM__SCOPE:
        setScope(SCOPE_EDEFAULT);
        return;
      case obodatamodelPackage.SYNONYM__XREFS:
        getXrefs().clear();
        return;
      case obodatamodelPackage.SYNONYM__TEXT:
        setText(TEXT_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.SYNONYM__ID:
        return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
      case obodatamodelPackage.SYNONYM__ANONYMOUS:
        return anonymous != ANONYMOUS_EDEFAULT;
      case obodatamodelPackage.SYNONYM__UNKNOWN_SCOPE:
        return UNKNOWN_SCOPE_EDEFAULT == null ? unknowN_SCOPE != null : !UNKNOWN_SCOPE_EDEFAULT.equals(unknowN_SCOPE);
      case obodatamodelPackage.SYNONYM__RELATED_SYNONYM:
        return RELATED_SYNONYM_EDEFAULT == null ? relateD_SYNONYM != null : !RELATED_SYNONYM_EDEFAULT.equals(relateD_SYNONYM);
      case obodatamodelPackage.SYNONYM__EXACT_SYNONYM:
        return EXACT_SYNONYM_EDEFAULT == null ? exacT_SYNONYM != null : !EXACT_SYNONYM_EDEFAULT.equals(exacT_SYNONYM);
      case obodatamodelPackage.SYNONYM__NARROW_SYNONYM:
        return NARROW_SYNONYM_EDEFAULT == null ? narroW_SYNONYM != null : !NARROW_SYNONYM_EDEFAULT.equals(narroW_SYNONYM);
      case obodatamodelPackage.SYNONYM__BROAD_SYNONYM:
        return BROAD_SYNONYM_EDEFAULT == null ? broaD_SYNONYM != null : !BROAD_SYNONYM_EDEFAULT.equals(broaD_SYNONYM);
      case obodatamodelPackage.SYNONYM__SYNONYM_TYPE:
        return synonymType != null;
      case obodatamodelPackage.SYNONYM__NESTED_VALUE:
        return nestedValue != null;
      case obodatamodelPackage.SYNONYM__SCOPE:
        return SCOPE_EDEFAULT == null ? scope != null : !SCOPE_EDEFAULT.equals(scope);
      case obodatamodelPackage.SYNONYM__XREFS:
        return xrefs != null && !xrefs.isEmpty();
      case obodatamodelPackage.SYNONYM__TEXT:
        return TEXT_EDEFAULT == null ? text != null : !TEXT_EDEFAULT.equals(text);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
    if (baseClass == Serializable.class) {
      switch (derivedFeatureID) {
        default: return -1;
      }
    }
    if (baseClass == br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable.class) {
      switch (derivedFeatureID) {
        default: return -1;
      }
    }
    if (baseClass == IdentifiableObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.SYNONYM__ID: return obodatamodelPackage.IDENTIFIABLE_OBJECT__ID;
        case obodatamodelPackage.SYNONYM__ANONYMOUS: return obodatamodelPackage.IDENTIFIABLE_OBJECT__ANONYMOUS;
        default: return -1;
      }
    }
    return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
    if (baseClass == Serializable.class) {
      switch (baseFeatureID) {
        default: return -1;
      }
    }
    if (baseClass == br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable.class) {
      switch (baseFeatureID) {
        default: return -1;
      }
    }
    if (baseClass == IdentifiableObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.IDENTIFIABLE_OBJECT__ID: return obodatamodelPackage.SYNONYM__ID;
        case obodatamodelPackage.IDENTIFIABLE_OBJECT__ANONYMOUS: return obodatamodelPackage.SYNONYM__ANONYMOUS;
        default: return -1;
      }
    }
    return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (id: ");
    result.append(id);
    result.append(", anonymous: ");
    result.append(anonymous);
    result.append(", UNKNOWN_SCOPE: ");
    result.append(unknowN_SCOPE);
    result.append(", RELATED_SYNONYM: ");
    result.append(relateD_SYNONYM);
    result.append(", EXACT_SYNONYM: ");
    result.append(exacT_SYNONYM);
    result.append(", NARROW_SYNONYM: ");
    result.append(narroW_SYNONYM);
    result.append(", BROAD_SYNONYM: ");
    result.append(broaD_SYNONYM);
    result.append(", scope: ");
    result.append(scope);
    result.append(", text: ");
    result.append(text);
    result.append(')');
    return result.toString();
  }

} //SynonymImpl
