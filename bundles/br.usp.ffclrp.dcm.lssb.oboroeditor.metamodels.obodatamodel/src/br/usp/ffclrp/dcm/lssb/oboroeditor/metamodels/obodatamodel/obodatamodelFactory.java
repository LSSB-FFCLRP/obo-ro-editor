/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage
 * @generated
 */
public interface obodatamodelFactory extends EFactory {
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  obodatamodelFactory eINSTANCE = br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Annotated Object</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Annotated Object</em>'.
   * @generated
   */
  AnnotatedObject createAnnotatedObject();

  /**
   * Returns a new object of class '<em>Dangling Object</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Dangling Object</em>'.
   * @generated
   */
  DanglingObject createDanglingObject();

  /**
   * Returns a new object of class '<em>Dangling Property</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Dangling Property</em>'.
   * @generated
   */
  DanglingProperty createDanglingProperty();

  /**
   * Returns a new object of class '<em>Datatype</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Datatype</em>'.
   * @generated
   */
  Datatype createDatatype();

  /**
   * Returns a new object of class '<em>Datatype Value</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Datatype Value</em>'.
   * @generated
   */
  DatatypeValue createDatatypeValue();

  /**
   * Returns a new object of class '<em>Dbxref</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Dbxref</em>'.
   * @generated
   */
  Dbxref createDbxref();

  /**
   * Returns a new object of class '<em>Dbxrefed Object</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Dbxrefed Object</em>'.
   * @generated
   */
  DbxrefedObject createDbxrefedObject();

  /**
   * Returns a new object of class '<em>Field Path</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Field Path</em>'.
   * @generated
   */
  FieldPath createFieldPath();

  /**
   * Returns a new object of class '<em>Field Path Spec</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Field Path Spec</em>'.
   * @generated
   */
  FieldPathSpec createFieldPathSpec();

  /**
   * Returns a new object of class '<em>Identified Object Index</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Identified Object Index</em>'.
   * @generated
   */
  IdentifiedObjectIndex createIdentifiedObjectIndex();

  /**
   * Returns a new object of class '<em>Instance</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Instance</em>'.
   * @generated
   */
  Instance createInstance();

  /**
   * Returns a new object of class '<em>Link Database</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Link Database</em>'.
   * @generated
   */
  LinkDatabase createLinkDatabase();

  /**
   * Returns a new object of class '<em>Link Linked Object</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Link Linked Object</em>'.
   * @generated
   */
  LinkLinkedObject createLinkLinkedObject();

  /**
   * Returns a new object of class '<em>Modification Metadata Object</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Modification Metadata Object</em>'.
   * @generated
   */
  ModificationMetadataObject createModificationMetadataObject();

  /**
   * Returns a new object of class '<em>Mutable Link Database</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Mutable Link Database</em>'.
   * @generated
   */
  MutableLinkDatabase createMutableLinkDatabase();

  /**
   * Returns a new object of class '<em>Namespace</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Namespace</em>'.
   * @generated
   */
  Namespace createNamespace();

  /**
   * Returns a new object of class '<em>Nested Value</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Nested Value</em>'.
   * @generated
   */
  NestedValue createNestedValue();

  /**
   * Returns a new object of class '<em>OBO Class</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>OBO Class</em>'.
   * @generated
   */
  OBOClass createOBOClass();

  /**
   * Returns a new object of class '<em>OBO Property</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>OBO Property</em>'.
   * @generated
   */
  OBOProperty createOBOProperty();

  /**
   * Returns a new object of class '<em>OBO Restriction</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>OBO Restriction</em>'.
   * @generated
   */
  OBORestriction createOBORestriction();

  /**
   * Returns a new object of class '<em>OBO Session</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>OBO Session</em>'.
   * @generated
   */
  OBOSession createOBOSession();

  /**
   * Returns a new object of class '<em>Object Factory</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Object Factory</em>'.
   * @generated
   */
  ObjectFactory createObjectFactory();

  /**
   * Returns a new object of class '<em>Object Field</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Object Field</em>'.
   * @generated
   */
  ObjectField createObjectField();

  /**
   * Returns a new object of class '<em>Path Capable</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Path Capable</em>'.
   * @generated
   */
  PathCapable createPathCapable();

  /**
   * Returns a new object of class '<em>Property Value</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Property Value</em>'.
   * @generated
   */
  PropertyValue createPropertyValue();

  /**
   * Returns a new object of class '<em>Root Algorithm</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Root Algorithm</em>'.
   * @generated
   */
  RootAlgorithm createRootAlgorithm();

  /**
   * Returns a new object of class '<em>Subset Object</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Subset Object</em>'.
   * @generated
   */
  SubsetObject createSubsetObject();

  /**
   * Returns a new object of class '<em>Synonym</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Synonym</em>'.
   * @generated
   */
  Synonym createSynonym();

  /**
   * Returns a new object of class '<em>Synonym Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Synonym Type</em>'.
   * @generated
   */
  SynonymType createSynonymType();

  /**
   * Returns a new object of class '<em>Synonymed Object</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Synonymed Object</em>'.
   * @generated
   */
  SynonymedObject createSynonymedObject();

  /**
   * Returns a new object of class '<em>Term Subset</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Term Subset</em>'.
   * @generated
   */
  TermSubset createTermSubset();

  /**
   * Returns a new object of class '<em>Unknown Stanza</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Unknown Stanza</em>'.
   * @generated
   */
  UnknownStanza createUnknownStanza();

  /**
   * Returns a new object of class '<em>Value</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Value</em>'.
   * @generated
   */
  Value createValue();

  /**
   * Returns a new object of class '<em>Value Database</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Value Database</em>'.
   * @generated
   */
  ValueDatabase createValueDatabase();

  /**
   * Returns a new object of class '<em>Value Link</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Value Link</em>'.
   * @generated
   */
  ValueLink createValueLink();

  /**
   * Returns a new object of class '<em>List Of Properties</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>List Of Properties</em>'.
   * @generated
   */
  ListOfProperties createListOfProperties();

  /**
   * Returns a new object of class '<em>Obo Property Value</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Obo Property Value</em>'.
   * @generated
   */
  OboPropertyValue createOboPropertyValue();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  obodatamodelPackage getobodatamodelPackage();

} //obodatamodelFactory
