/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.util;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.AnnotatedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.CommentedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingProperty;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Datatype;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DatatypeValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DbxrefedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPath;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPathSpec;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObjectIndex;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Impliable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Instance;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Link;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkLinkedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ListOfProperties;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MultiIDObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MutableLinkDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NamespacedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOClass;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectFactory;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectField;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OboPropertyValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PathCapable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.RootAlgorithm;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Serializable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SubsetObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Type;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Value;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueLink;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Date;
import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage
 * @generated
 */
public class obodatamodelValidator extends EObjectValidator {
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static final obodatamodelValidator INSTANCE = new obodatamodelValidator();

  /**
   * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.common.util.Diagnostic#getSource()
   * @see org.eclipse.emf.common.util.Diagnostic#getCode()
   * @generated
   */
  public static final String DIAGNOSTIC_SOURCE = "br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel";

  /**
   * A constant with a fixed name that can be used as the base value for additional hand written constants.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

  /**
   * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public obodatamodelValidator() {
    super();
  }

  /**
   * Returns the package of this validator switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EPackage getEPackage() {
    return obodatamodelPackage.eINSTANCE;
  }

  /**
   * Calls <code>validateXXX</code> for the corresponding classifier of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
    switch (classifierID) {
      case obodatamodelPackage.ANNOTATED_OBJECT:
        return validateAnnotatedObject((AnnotatedObject)value, diagnostics, context);
      case obodatamodelPackage.COMMENTED_OBJECT:
        return validateCommentedObject((CommentedObject)value, diagnostics, context);
      case obodatamodelPackage.DANGLING_OBJECT:
        return validateDanglingObject((DanglingObject)value, diagnostics, context);
      case obodatamodelPackage.DANGLING_PROPERTY:
        return validateDanglingProperty((DanglingProperty)value, diagnostics, context);
      case obodatamodelPackage.DATATYPE:
        return validateDatatype((Datatype)value, diagnostics, context);
      case obodatamodelPackage.DATATYPE_VALUE:
        return validateDatatypeValue((DatatypeValue)value, diagnostics, context);
      case obodatamodelPackage.DBXREF:
        return validateDbxref((Dbxref)value, diagnostics, context);
      case obodatamodelPackage.DBXREFED_OBJECT:
        return validateDbxrefedObject((DbxrefedObject)value, diagnostics, context);
      case obodatamodelPackage.DEFINED_OBJECT:
        return validateDefinedObject((DefinedObject)value, diagnostics, context);
      case obodatamodelPackage.FIELD_PATH:
        return validateFieldPath((FieldPath)value, diagnostics, context);
      case obodatamodelPackage.FIELD_PATH_SPEC:
        return validateFieldPathSpec((FieldPathSpec)value, diagnostics, context);
      case obodatamodelPackage.IDENTIFIABLE_OBJECT:
        return validateIdentifiableObject((IdentifiableObject)value, diagnostics, context);
      case obodatamodelPackage.IDENTIFIED_OBJECT:
        return validateIdentifiedObject((IdentifiedObject)value, diagnostics, context);
      case obodatamodelPackage.IDENTIFIED_OBJECT_INDEX:
        return validateIdentifiedObjectIndex((IdentifiedObjectIndex)value, diagnostics, context);
      case obodatamodelPackage.IMPLIABLE:
        return validateImpliable((Impliable)value, diagnostics, context);
      case obodatamodelPackage.INSTANCE:
        return validateInstance((Instance)value, diagnostics, context);
      case obodatamodelPackage.LINK:
        return validateLink((Link)value, diagnostics, context);
      case obodatamodelPackage.LINK_DATABASE:
        return validateLinkDatabase((LinkDatabase)value, diagnostics, context);
      case obodatamodelPackage.LINK_LINKED_OBJECT:
        return validateLinkLinkedObject((LinkLinkedObject)value, diagnostics, context);
      case obodatamodelPackage.LINKED_OBJECT:
        return validateLinkedObject((LinkedObject)value, diagnostics, context);
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT:
        return validateModificationMetadataObject((ModificationMetadataObject)value, diagnostics, context);
      case obodatamodelPackage.MULTI_ID_OBJECT:
        return validateMultiIDObject((MultiIDObject)value, diagnostics, context);
      case obodatamodelPackage.MUTABLE_LINK_DATABASE:
        return validateMutableLinkDatabase((MutableLinkDatabase)value, diagnostics, context);
      case obodatamodelPackage.NAMESPACE:
        return validateNamespace((Namespace)value, diagnostics, context);
      case obodatamodelPackage.NAMESPACED_OBJECT:
        return validateNamespacedObject((NamespacedObject)value, diagnostics, context);
      case obodatamodelPackage.NESTED_VALUE:
        return validateNestedValue((NestedValue)value, diagnostics, context);
      case obodatamodelPackage.OBO_CLASS:
        return validateOBOClass((OBOClass)value, diagnostics, context);
      case obodatamodelPackage.OBO_OBJECT:
        return validateOBOObject((OBOObject)value, diagnostics, context);
      case obodatamodelPackage.OBO_PROPERTY:
        return validateOBOProperty((OBOProperty)value, diagnostics, context);
      case obodatamodelPackage.OBO_RESTRICTION:
        return validateOBORestriction((OBORestriction)value, diagnostics, context);
      case obodatamodelPackage.OBO_SESSION:
        return validateOBOSession((OBOSession)value, diagnostics, context);
      case obodatamodelPackage.OBJECT_FACTORY:
        return validateObjectFactory((ObjectFactory)value, diagnostics, context);
      case obodatamodelPackage.OBJECT_FIELD:
        return validateObjectField((ObjectField)value, diagnostics, context);
      case obodatamodelPackage.OBSOLETABLE_OBJECT:
        return validateObsoletableObject((ObsoletableObject)value, diagnostics, context);
      case obodatamodelPackage.PATH_CAPABLE:
        return validatePathCapable((PathCapable)value, diagnostics, context);
      case obodatamodelPackage.PROPERTY_VALUE:
        return validatePropertyValue((PropertyValue)value, diagnostics, context);
      case obodatamodelPackage.RELATIONSHIP:
        return validateRelationship((Relationship)value, diagnostics, context);
      case obodatamodelPackage.ROOT_ALGORITHM:
        return validateRootAlgorithm((RootAlgorithm)value, diagnostics, context);
      case obodatamodelPackage.SUBSET_OBJECT:
        return validateSubsetObject((SubsetObject)value, diagnostics, context);
      case obodatamodelPackage.SYNONYM:
        return validateSynonym((Synonym)value, diagnostics, context);
      case obodatamodelPackage.SYNONYM_TYPE:
        return validateSynonymType((SynonymType)value, diagnostics, context);
      case obodatamodelPackage.SYNONYMED_OBJECT:
        return validateSynonymedObject((SynonymedObject)value, diagnostics, context);
      case obodatamodelPackage.TERM_SUBSET:
        return validateTermSubset((TermSubset)value, diagnostics, context);
      case obodatamodelPackage.TYPE:
        return validateType((Type)value, diagnostics, context);
      case obodatamodelPackage.UNKNOWN_STANZA:
        return validateUnknownStanza((UnknownStanza)value, diagnostics, context);
      case obodatamodelPackage.VALUE:
        return validateValue((Value)value, diagnostics, context);
      case obodatamodelPackage.VALUE_DATABASE:
        return validateValueDatabase((ValueDatabase)value, diagnostics, context);
      case obodatamodelPackage.VALUE_LINK:
        return validateValueLink((ValueLink)value, diagnostics, context);
      case obodatamodelPackage.CLONEABLE:
        return validateCloneable((br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable)value, diagnostics, context);
      case obodatamodelPackage.SERIALIZABLE:
        return validateSerializable((Serializable)value, diagnostics, context);
      case obodatamodelPackage.COMPARABLE:
        return validateComparable((br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable)value, diagnostics, context);
      case obodatamodelPackage.STRING_TO_NESTED_VALUE_MAP:
        return validateStringToNestedValueMap((Map.Entry<?, ?>)value, diagnostics, context);
      case obodatamodelPackage.OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP:
        return validateObsoletableObjectToNestedValueMap((Map.Entry<?, ?>)value, diagnostics, context);
      case obodatamodelPackage.TERM_SUBSET_TO_NESTED_VALUE_MAP:
        return validateTermSubsetToNestedValueMap((Map.Entry<?, ?>)value, diagnostics, context);
      case obodatamodelPackage.ID_SPACES_MAP:
        return validateIdSpacesMap((Map.Entry<?, ?>)value, diagnostics, context);
      case obodatamodelPackage.LIST_OF_PROPERTIES:
        return validateListOfProperties((ListOfProperties)value, diagnostics, context);
      case obodatamodelPackage.OBO_PROPERTY_VALUE:
        return validateOboPropertyValue((OboPropertyValue)value, diagnostics, context);
      case obodatamodelPackage.DATE:
        return validateDate((Date)value, diagnostics, context);
      case obodatamodelPackage.ECORE_OBJECT:
        return validateEcoreObject((EObject)value, diagnostics, context);
      default:
        return true;
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateAnnotatedObject(AnnotatedObject annotatedObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(annotatedObject, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateCommentedObject(CommentedObject commentedObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(commentedObject, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateDanglingObject(DanglingObject danglingObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(danglingObject, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateDanglingProperty(DanglingProperty danglingProperty, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(danglingProperty, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateDatatype(Datatype datatype, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(datatype, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateDatatypeValue(DatatypeValue datatypeValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(datatypeValue, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateDbxref(Dbxref dbxref, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(dbxref, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateDbxrefedObject(DbxrefedObject dbxrefedObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(dbxrefedObject, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateDefinedObject(DefinedObject definedObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(definedObject, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateFieldPath(FieldPath fieldPath, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(fieldPath, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateFieldPathSpec(FieldPathSpec fieldPathSpec, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(fieldPathSpec, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateIdentifiableObject(IdentifiableObject identifiableObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(identifiableObject, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateIdentifiedObject(IdentifiedObject identifiedObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(identifiedObject, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateIdentifiedObjectIndex(IdentifiedObjectIndex identifiedObjectIndex, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(identifiedObjectIndex, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateImpliable(Impliable impliable, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(impliable, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateInstance(Instance instance, DiagnosticChain diagnostics, Map<Object, Object> context) {
    if (!validate_NoCircularContainment(instance, diagnostics, context)) return false;
    boolean result = validate_EveryMultiplicityConforms(instance, diagnostics, context);
    if (result || diagnostics != null) result &= validate_EveryDataValueConforms(instance, diagnostics, context);
    if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(instance, diagnostics, context);
    if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(instance, diagnostics, context);
    if (result || diagnostics != null) result &= validate_EveryProxyResolves(instance, diagnostics, context);
    if (result || diagnostics != null) result &= validate_UniqueID(instance, diagnostics, context);
    if (result || diagnostics != null) result &= validate_EveryKeyUnique(instance, diagnostics, context);
    if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(instance, diagnostics, context);
    if (result || diagnostics != null) result &= validateInstance_TypeMustBeClass(instance, diagnostics, context);
    return result;
  }

  /**
   * The cached validation expression for the TypeMustBeClass constraint of '<em>Instance</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static final String INSTANCE__TYPE_MUST_BE_CLASS__EEXPRESSION = "self.type.oclIsKindOf(OBOClass)";

  /**
   * Validates the TypeMustBeClass constraint of '<em>Instance</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateInstance_TypeMustBeClass(Instance instance, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return
      validate
        (obodatamodelPackage.Literals.INSTANCE,
         instance,
         diagnostics,
         context,
         "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
         "TypeMustBeClass",
         INSTANCE__TYPE_MUST_BE_CLASS__EEXPRESSION,
         Diagnostic.ERROR,
         DIAGNOSTIC_SOURCE,
         0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateLink(Link link, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(link, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateLinkDatabase(LinkDatabase linkDatabase, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(linkDatabase, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateLinkLinkedObject(LinkLinkedObject linkLinkedObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(linkLinkedObject, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateLinkedObject(LinkedObject linkedObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(linkedObject, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateModificationMetadataObject(ModificationMetadataObject modificationMetadataObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(modificationMetadataObject, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateMultiIDObject(MultiIDObject multiIDObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(multiIDObject, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateMutableLinkDatabase(MutableLinkDatabase mutableLinkDatabase, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(mutableLinkDatabase, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateNamespace(Namespace namespace, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(namespace, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateNamespacedObject(NamespacedObject namespacedObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(namespacedObject, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateNestedValue(NestedValue nestedValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(nestedValue, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateOBOClass(OBOClass oboClass, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(oboClass, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateOBOObject(OBOObject oboObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(oboObject, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateOBOProperty(OBOProperty oboProperty, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(oboProperty, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateOBORestriction(OBORestriction oboRestriction, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(oboRestriction, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateOBOSession(OBOSession oboSession, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(oboSession, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateObjectFactory(ObjectFactory objectFactory, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(objectFactory, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateObjectField(ObjectField objectField, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(objectField, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateObsoletableObject(ObsoletableObject obsoletableObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(obsoletableObject, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validatePathCapable(PathCapable pathCapable, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(pathCapable, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validatePropertyValue(PropertyValue propertyValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(propertyValue, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateRelationship(Relationship relationship, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(relationship, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateRootAlgorithm(RootAlgorithm rootAlgorithm, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(rootAlgorithm, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateSubsetObject(SubsetObject subsetObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(subsetObject, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateSynonym(Synonym synonym, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(synonym, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateSynonymType(SynonymType synonymType, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(synonymType, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateSynonymedObject(SynonymedObject synonymedObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(synonymedObject, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateTermSubset(TermSubset termSubset, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(termSubset, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateType(Type type, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(type, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateUnknownStanza(UnknownStanza unknownStanza, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(unknownStanza, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateValue(Value value, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(value, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateValueDatabase(ValueDatabase valueDatabase, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(valueDatabase, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateValueLink(ValueLink valueLink, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(valueLink, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateCloneable(br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable cloneable, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(cloneable, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateSerializable(Serializable serializable, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(serializable, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateComparable(br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable comparable, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(comparable, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateStringToNestedValueMap(Map.Entry<?, ?> stringToNestedValueMap, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint((EObject)stringToNestedValueMap, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateObsoletableObjectToNestedValueMap(Map.Entry<?, ?> obsoletableObjectToNestedValueMap, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint((EObject)obsoletableObjectToNestedValueMap, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateTermSubsetToNestedValueMap(Map.Entry<?, ?> termSubsetToNestedValueMap, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint((EObject)termSubsetToNestedValueMap, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateIdSpacesMap(Map.Entry<?, ?> idSpacesMap, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint((EObject)idSpacesMap, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateListOfProperties(ListOfProperties listOfProperties, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(listOfProperties, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateOboPropertyValue(OboPropertyValue oboPropertyValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return validate_EveryDefaultConstraint(oboPropertyValue, diagnostics, context);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateDate(Date date, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return true;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean validateEcoreObject(EObject ecoreObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
    return true;
  }

  /**
   * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ResourceLocator getResourceLocator() {
    // TODO
    // Specialize this to return a resource locator for messages specific to this validator.
    // Ensure that you remove @generated or mark it @generated NOT
    return super.getResourceLocator();
  }

} //obodatamodelValidator
