/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObjectIndex;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MutableLinkDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mutable Link Database</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.MutableLinkDatabaseImpl#getIdentifiedObjectIndex <em>Identified Object Index</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MutableLinkDatabaseImpl extends LinkDatabaseImpl implements MutableLinkDatabase {
  /**
   * The cached value of the '{@link #getIdentifiedObjectIndex() <em>Identified Object Index</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdentifiedObjectIndex()
   * @generated
   * @ordered
   */
  protected IdentifiedObjectIndex identifiedObjectIndex;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MutableLinkDatabaseImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.MUTABLE_LINK_DATABASE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IdentifiedObjectIndex getIdentifiedObjectIndex() {
    return identifiedObjectIndex;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetIdentifiedObjectIndex(IdentifiedObjectIndex newIdentifiedObjectIndex, NotificationChain msgs) {
    IdentifiedObjectIndex oldIdentifiedObjectIndex = identifiedObjectIndex;
    identifiedObjectIndex = newIdentifiedObjectIndex;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.MUTABLE_LINK_DATABASE__IDENTIFIED_OBJECT_INDEX, oldIdentifiedObjectIndex, newIdentifiedObjectIndex);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIdentifiedObjectIndex(IdentifiedObjectIndex newIdentifiedObjectIndex) {
    if (newIdentifiedObjectIndex != identifiedObjectIndex) {
      NotificationChain msgs = null;
      if (identifiedObjectIndex != null)
        msgs = ((InternalEObject)identifiedObjectIndex).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.MUTABLE_LINK_DATABASE__IDENTIFIED_OBJECT_INDEX, null, msgs);
      if (newIdentifiedObjectIndex != null)
        msgs = ((InternalEObject)newIdentifiedObjectIndex).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.MUTABLE_LINK_DATABASE__IDENTIFIED_OBJECT_INDEX, null, msgs);
      msgs = basicSetIdentifiedObjectIndex(newIdentifiedObjectIndex, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.MUTABLE_LINK_DATABASE__IDENTIFIED_OBJECT_INDEX, newIdentifiedObjectIndex, newIdentifiedObjectIndex));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.MUTABLE_LINK_DATABASE__IDENTIFIED_OBJECT_INDEX:
        return basicSetIdentifiedObjectIndex(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.MUTABLE_LINK_DATABASE__IDENTIFIED_OBJECT_INDEX:
        return getIdentifiedObjectIndex();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.MUTABLE_LINK_DATABASE__IDENTIFIED_OBJECT_INDEX:
        setIdentifiedObjectIndex((IdentifiedObjectIndex)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.MUTABLE_LINK_DATABASE__IDENTIFIED_OBJECT_INDEX:
        setIdentifiedObjectIndex((IdentifiedObjectIndex)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.MUTABLE_LINK_DATABASE__IDENTIFIED_OBJECT_INDEX:
        return identifiedObjectIndex != null;
    }
    return super.eIsSet(featureID);
  }

} //MutableLinkDatabaseImpl
