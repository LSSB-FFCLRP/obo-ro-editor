/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mutable Link Database</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MutableLinkDatabase#getIdentifiedObjectIndex <em>Identified Object Index</em>}</li>
 * </ul>
 *
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getMutableLinkDatabase()
 * @model
 * @generated
 */
public interface MutableLinkDatabase extends LinkDatabase {
  /**
   * Returns the value of the '<em><b>Identified Object Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Identified Object Index</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Identified Object Index</em>' containment reference.
   * @see #setIdentifiedObjectIndex(IdentifiedObjectIndex)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getMutableLinkDatabase_IdentifiedObjectIndex()
   * @model containment="true"
   * @generated
   */
  IdentifiedObjectIndex getIdentifiedObjectIndex();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MutableLinkDatabase#getIdentifiedObjectIndex <em>Identified Object Index</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Identified Object Index</em>' containment reference.
   * @see #getIdentifiedObjectIndex()
   * @generated
   */
  void setIdentifiedObjectIndex(IdentifiedObjectIndex value);

} // MutableLinkDatabase
