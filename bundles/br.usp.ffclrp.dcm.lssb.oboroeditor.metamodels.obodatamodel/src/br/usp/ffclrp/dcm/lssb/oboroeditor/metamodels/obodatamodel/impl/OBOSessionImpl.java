/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>OBO Session</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOSessionImpl#getId <em>Id</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOSessionImpl#getObjects <em>Objects</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOSessionImpl#getLinkDatabase <em>Link Database</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOSessionImpl#getDefaultNamespace <em>Default Namespace</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOSessionImpl#getNamespaces <em>Namespaces</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOSessionImpl#getPropertyValues <em>Property Values</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOSessionImpl#getUnknownStanzas <em>Unknown Stanzas</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOSessionImpl#getSubsets <em>Subsets</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOSessionImpl#getCategories <em>Categories</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOSessionImpl#getSynonymTypes <em>Synonym Types</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOSessionImpl#getSynonymCategories <em>Synonym Categories</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOSessionImpl#getCurrentFilenames <em>Current Filenames</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOSessionImpl#getCurrentUser <em>Current User</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOSessionImpl#getIdSpaces <em>Id Spaces</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOSessionImpl#getLoadRemark <em>Load Remark</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOSessionImpl#getAllDbxRefsContainer <em>All Dbx Refs Container</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OBOSessionImpl extends IdentifiedObjectIndexImpl implements OBOSession {
  /**
   * The default value of the '{@link #getId() <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getId()
   * @generated
   * @ordered
   */
  protected static final String ID_EDEFAULT = "rootSession";

  /**
   * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getId()
   * @generated
   * @ordered
   */
  protected String id = ID_EDEFAULT;

  /**
   * The cached value of the '{@link #getObjects() <em>Objects</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getObjects()
   * @generated
   * @ordered
   */
  protected EList<IdentifiedObject> objects;

  /**
   * The cached value of the '{@link #getLinkDatabase() <em>Link Database</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLinkDatabase()
   * @generated
   * @ordered
   */
  protected LinkDatabase linkDatabase;

  /**
   * The cached value of the '{@link #getDefaultNamespace() <em>Default Namespace</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDefaultNamespace()
   * @generated
   * @ordered
   */
  protected Namespace defaultNamespace;

  /**
   * The cached value of the '{@link #getNamespaces() <em>Namespaces</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNamespaces()
   * @generated
   * @ordered
   */
  protected EList<Namespace> namespaces;

  /**
   * The cached value of the '{@link #getPropertyValues() <em>Property Values</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPropertyValues()
   * @generated
   * @ordered
   */
  protected EList<PropertyValue> propertyValues;

  /**
   * The cached value of the '{@link #getUnknownStanzas() <em>Unknown Stanzas</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUnknownStanzas()
   * @generated
   * @ordered
   */
  protected EList<UnknownStanza> unknownStanzas;

  /**
   * The cached value of the '{@link #getSubsets() <em>Subsets</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubsets()
   * @generated
   * @ordered
   */
  protected EList<TermSubset> subsets;

  /**
   * The cached value of the '{@link #getCategories() <em>Categories</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCategories()
   * @generated
   * @ordered
   */
  protected EList<TermSubset> categories;

  /**
   * The cached value of the '{@link #getSynonymTypes() <em>Synonym Types</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSynonymTypes()
   * @generated
   * @ordered
   */
  protected EList<SynonymType> synonymTypes;

  /**
   * The cached value of the '{@link #getSynonymCategories() <em>Synonym Categories</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSynonymCategories()
   * @generated
   * @ordered
   */
  protected EList<SynonymType> synonymCategories;

  /**
   * The cached value of the '{@link #getCurrentFilenames() <em>Current Filenames</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCurrentFilenames()
   * @generated
   * @ordered
   */
  protected EList<String> currentFilenames;

  /**
   * The default value of the '{@link #getCurrentUser() <em>Current User</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCurrentUser()
   * @generated
   * @ordered
   */
  protected static final String CURRENT_USER_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getCurrentUser() <em>Current User</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCurrentUser()
   * @generated
   * @ordered
   */
  protected String currentUser = CURRENT_USER_EDEFAULT;

  /**
   * The cached value of the '{@link #getIdSpaces() <em>Id Spaces</em>}' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdSpaces()
   * @generated
   * @ordered
   */
  protected EMap<String, String> idSpaces;

  /**
   * The default value of the '{@link #getLoadRemark() <em>Load Remark</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLoadRemark()
   * @generated
   * @ordered
   */
  protected static final String LOAD_REMARK_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getLoadRemark() <em>Load Remark</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLoadRemark()
   * @generated
   * @ordered
   */
  protected String loadRemark = LOAD_REMARK_EDEFAULT;

  /**
   * The cached value of the '{@link #getAllDbxRefsContainer() <em>All Dbx Refs Container</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAllDbxRefsContainer()
   * @generated
   * @ordered
   */
  protected EList<Dbxref> allDbxRefsContainer;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OBOSessionImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.OBO_SESSION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getId() {
    return id;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setId(String newId) {
    String oldId = id;
    id = newId;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_SESSION__ID, oldId, id));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<IdentifiedObject> getObjects() {
    if (objects == null) {
      objects = new EObjectContainmentEList<IdentifiedObject>(IdentifiedObject.class, this, obodatamodelPackage.OBO_SESSION__OBJECTS);
    }
    return objects;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LinkDatabase getLinkDatabase() {
    return linkDatabase;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLinkDatabase(LinkDatabase newLinkDatabase, NotificationChain msgs) {
    LinkDatabase oldLinkDatabase = linkDatabase;
    linkDatabase = newLinkDatabase;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_SESSION__LINK_DATABASE, oldLinkDatabase, newLinkDatabase);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLinkDatabase(LinkDatabase newLinkDatabase) {
    if (newLinkDatabase != linkDatabase) {
      NotificationChain msgs = null;
      if (linkDatabase != null)
        msgs = ((InternalEObject)linkDatabase).eInverseRemove(this, obodatamodelPackage.LINK_DATABASE__SESSION, LinkDatabase.class, msgs);
      if (newLinkDatabase != null)
        msgs = ((InternalEObject)newLinkDatabase).eInverseAdd(this, obodatamodelPackage.LINK_DATABASE__SESSION, LinkDatabase.class, msgs);
      msgs = basicSetLinkDatabase(newLinkDatabase, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_SESSION__LINK_DATABASE, newLinkDatabase, newLinkDatabase));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Namespace getDefaultNamespace() {
    if (defaultNamespace != null && defaultNamespace.eIsProxy()) {
      InternalEObject oldDefaultNamespace = (InternalEObject)defaultNamespace;
      defaultNamespace = (Namespace)eResolveProxy(oldDefaultNamespace);
      if (defaultNamespace != oldDefaultNamespace) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, obodatamodelPackage.OBO_SESSION__DEFAULT_NAMESPACE, oldDefaultNamespace, defaultNamespace));
      }
    }
    return defaultNamespace;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Namespace basicGetDefaultNamespace() {
    return defaultNamespace;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDefaultNamespace(Namespace newDefaultNamespace) {
    Namespace oldDefaultNamespace = defaultNamespace;
    defaultNamespace = newDefaultNamespace;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_SESSION__DEFAULT_NAMESPACE, oldDefaultNamespace, defaultNamespace));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Namespace> getNamespaces() {
    if (namespaces == null) {
      namespaces = new EObjectContainmentEList<Namespace>(Namespace.class, this, obodatamodelPackage.OBO_SESSION__NAMESPACES);
    }
    return namespaces;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<PropertyValue> getPropertyValues() {
    if (propertyValues == null) {
      propertyValues = new EObjectContainmentEList<PropertyValue>(PropertyValue.class, this, obodatamodelPackage.OBO_SESSION__PROPERTY_VALUES);
    }
    return propertyValues;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<UnknownStanza> getUnknownStanzas() {
    if (unknownStanzas == null) {
      unknownStanzas = new EObjectContainmentEList<UnknownStanza>(UnknownStanza.class, this, obodatamodelPackage.OBO_SESSION__UNKNOWN_STANZAS);
    }
    return unknownStanzas;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TermSubset> getSubsets() {
    if (subsets == null) {
      subsets = new EObjectContainmentEList<TermSubset>(TermSubset.class, this, obodatamodelPackage.OBO_SESSION__SUBSETS);
    }
    return subsets;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TermSubset> getCategories() {
    if (categories == null) {
      categories = new EObjectContainmentEList<TermSubset>(TermSubset.class, this, obodatamodelPackage.OBO_SESSION__CATEGORIES);
    }
    return categories;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SynonymType> getSynonymTypes() {
    if (synonymTypes == null) {
      synonymTypes = new EObjectContainmentEList<SynonymType>(SynonymType.class, this, obodatamodelPackage.OBO_SESSION__SYNONYM_TYPES);
    }
    return synonymTypes;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SynonymType> getSynonymCategories() {
    if (synonymCategories == null) {
      synonymCategories = new EObjectContainmentEList<SynonymType>(SynonymType.class, this, obodatamodelPackage.OBO_SESSION__SYNONYM_CATEGORIES);
    }
    return synonymCategories;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getCurrentFilenames() {
    if (currentFilenames == null) {
      currentFilenames = new EDataTypeUniqueEList<String>(String.class, this, obodatamodelPackage.OBO_SESSION__CURRENT_FILENAMES);
    }
    return currentFilenames;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCurrentUser() {
    return currentUser;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCurrentUser(String newCurrentUser) {
    String oldCurrentUser = currentUser;
    currentUser = newCurrentUser;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_SESSION__CURRENT_USER, oldCurrentUser, currentUser));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EMap<String, String> getIdSpaces() {
    if (idSpaces == null) {
      idSpaces = new EcoreEMap<String,String>(obodatamodelPackage.Literals.ID_SPACES_MAP, IdSpacesMapImpl.class, this, obodatamodelPackage.OBO_SESSION__ID_SPACES);
    }
    return idSpaces;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLoadRemark() {
    return loadRemark;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLoadRemark(String newLoadRemark) {
    String oldLoadRemark = loadRemark;
    loadRemark = newLoadRemark;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.OBO_SESSION__LOAD_REMARK, oldLoadRemark, loadRemark));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Dbxref> getAllDbxRefsContainer() {
    if (allDbxRefsContainer == null) {
      allDbxRefsContainer = new EObjectContainmentEList<Dbxref>(Dbxref.class, this, obodatamodelPackage.OBO_SESSION__ALL_DBX_REFS_CONTAINER);
    }
    return allDbxRefsContainer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.OBO_SESSION__LINK_DATABASE:
        if (linkDatabase != null)
          msgs = ((InternalEObject)linkDatabase).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.OBO_SESSION__LINK_DATABASE, null, msgs);
        return basicSetLinkDatabase((LinkDatabase)otherEnd, msgs);
    }
    return super.eInverseAdd(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.OBO_SESSION__OBJECTS:
        return ((InternalEList<?>)getObjects()).basicRemove(otherEnd, msgs);
      case obodatamodelPackage.OBO_SESSION__LINK_DATABASE:
        return basicSetLinkDatabase(null, msgs);
      case obodatamodelPackage.OBO_SESSION__NAMESPACES:
        return ((InternalEList<?>)getNamespaces()).basicRemove(otherEnd, msgs);
      case obodatamodelPackage.OBO_SESSION__PROPERTY_VALUES:
        return ((InternalEList<?>)getPropertyValues()).basicRemove(otherEnd, msgs);
      case obodatamodelPackage.OBO_SESSION__UNKNOWN_STANZAS:
        return ((InternalEList<?>)getUnknownStanzas()).basicRemove(otherEnd, msgs);
      case obodatamodelPackage.OBO_SESSION__SUBSETS:
        return ((InternalEList<?>)getSubsets()).basicRemove(otherEnd, msgs);
      case obodatamodelPackage.OBO_SESSION__CATEGORIES:
        return ((InternalEList<?>)getCategories()).basicRemove(otherEnd, msgs);
      case obodatamodelPackage.OBO_SESSION__SYNONYM_TYPES:
        return ((InternalEList<?>)getSynonymTypes()).basicRemove(otherEnd, msgs);
      case obodatamodelPackage.OBO_SESSION__SYNONYM_CATEGORIES:
        return ((InternalEList<?>)getSynonymCategories()).basicRemove(otherEnd, msgs);
      case obodatamodelPackage.OBO_SESSION__ID_SPACES:
        return ((InternalEList<?>)getIdSpaces()).basicRemove(otherEnd, msgs);
      case obodatamodelPackage.OBO_SESSION__ALL_DBX_REFS_CONTAINER:
        return ((InternalEList<?>)getAllDbxRefsContainer()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.OBO_SESSION__ID:
        return getId();
      case obodatamodelPackage.OBO_SESSION__OBJECTS:
        return getObjects();
      case obodatamodelPackage.OBO_SESSION__LINK_DATABASE:
        return getLinkDatabase();
      case obodatamodelPackage.OBO_SESSION__DEFAULT_NAMESPACE:
        if (resolve) return getDefaultNamespace();
        return basicGetDefaultNamespace();
      case obodatamodelPackage.OBO_SESSION__NAMESPACES:
        return getNamespaces();
      case obodatamodelPackage.OBO_SESSION__PROPERTY_VALUES:
        return getPropertyValues();
      case obodatamodelPackage.OBO_SESSION__UNKNOWN_STANZAS:
        return getUnknownStanzas();
      case obodatamodelPackage.OBO_SESSION__SUBSETS:
        return getSubsets();
      case obodatamodelPackage.OBO_SESSION__CATEGORIES:
        return getCategories();
      case obodatamodelPackage.OBO_SESSION__SYNONYM_TYPES:
        return getSynonymTypes();
      case obodatamodelPackage.OBO_SESSION__SYNONYM_CATEGORIES:
        return getSynonymCategories();
      case obodatamodelPackage.OBO_SESSION__CURRENT_FILENAMES:
        return getCurrentFilenames();
      case obodatamodelPackage.OBO_SESSION__CURRENT_USER:
        return getCurrentUser();
      case obodatamodelPackage.OBO_SESSION__ID_SPACES:
        if (coreType) return getIdSpaces();
        else return getIdSpaces().map();
      case obodatamodelPackage.OBO_SESSION__LOAD_REMARK:
        return getLoadRemark();
      case obodatamodelPackage.OBO_SESSION__ALL_DBX_REFS_CONTAINER:
        return getAllDbxRefsContainer();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.OBO_SESSION__ID:
        setId((String)newValue);
        return;
      case obodatamodelPackage.OBO_SESSION__OBJECTS:
        getObjects().clear();
        getObjects().addAll((Collection<? extends IdentifiedObject>)newValue);
        return;
      case obodatamodelPackage.OBO_SESSION__LINK_DATABASE:
        setLinkDatabase((LinkDatabase)newValue);
        return;
      case obodatamodelPackage.OBO_SESSION__DEFAULT_NAMESPACE:
        setDefaultNamespace((Namespace)newValue);
        return;
      case obodatamodelPackage.OBO_SESSION__NAMESPACES:
        getNamespaces().clear();
        getNamespaces().addAll((Collection<? extends Namespace>)newValue);
        return;
      case obodatamodelPackage.OBO_SESSION__PROPERTY_VALUES:
        getPropertyValues().clear();
        getPropertyValues().addAll((Collection<? extends PropertyValue>)newValue);
        return;
      case obodatamodelPackage.OBO_SESSION__UNKNOWN_STANZAS:
        getUnknownStanzas().clear();
        getUnknownStanzas().addAll((Collection<? extends UnknownStanza>)newValue);
        return;
      case obodatamodelPackage.OBO_SESSION__SUBSETS:
        getSubsets().clear();
        getSubsets().addAll((Collection<? extends TermSubset>)newValue);
        return;
      case obodatamodelPackage.OBO_SESSION__CATEGORIES:
        getCategories().clear();
        getCategories().addAll((Collection<? extends TermSubset>)newValue);
        return;
      case obodatamodelPackage.OBO_SESSION__SYNONYM_TYPES:
        getSynonymTypes().clear();
        getSynonymTypes().addAll((Collection<? extends SynonymType>)newValue);
        return;
      case obodatamodelPackage.OBO_SESSION__SYNONYM_CATEGORIES:
        getSynonymCategories().clear();
        getSynonymCategories().addAll((Collection<? extends SynonymType>)newValue);
        return;
      case obodatamodelPackage.OBO_SESSION__CURRENT_FILENAMES:
        getCurrentFilenames().clear();
        getCurrentFilenames().addAll((Collection<? extends String>)newValue);
        return;
      case obodatamodelPackage.OBO_SESSION__CURRENT_USER:
        setCurrentUser((String)newValue);
        return;
      case obodatamodelPackage.OBO_SESSION__ID_SPACES:
        ((EStructuralFeature.Setting)getIdSpaces()).set(newValue);
        return;
      case obodatamodelPackage.OBO_SESSION__LOAD_REMARK:
        setLoadRemark((String)newValue);
        return;
      case obodatamodelPackage.OBO_SESSION__ALL_DBX_REFS_CONTAINER:
        getAllDbxRefsContainer().clear();
        getAllDbxRefsContainer().addAll((Collection<? extends Dbxref>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.OBO_SESSION__ID:
        setId(ID_EDEFAULT);
        return;
      case obodatamodelPackage.OBO_SESSION__OBJECTS:
        getObjects().clear();
        return;
      case obodatamodelPackage.OBO_SESSION__LINK_DATABASE:
        setLinkDatabase((LinkDatabase)null);
        return;
      case obodatamodelPackage.OBO_SESSION__DEFAULT_NAMESPACE:
        setDefaultNamespace((Namespace)null);
        return;
      case obodatamodelPackage.OBO_SESSION__NAMESPACES:
        getNamespaces().clear();
        return;
      case obodatamodelPackage.OBO_SESSION__PROPERTY_VALUES:
        getPropertyValues().clear();
        return;
      case obodatamodelPackage.OBO_SESSION__UNKNOWN_STANZAS:
        getUnknownStanzas().clear();
        return;
      case obodatamodelPackage.OBO_SESSION__SUBSETS:
        getSubsets().clear();
        return;
      case obodatamodelPackage.OBO_SESSION__CATEGORIES:
        getCategories().clear();
        return;
      case obodatamodelPackage.OBO_SESSION__SYNONYM_TYPES:
        getSynonymTypes().clear();
        return;
      case obodatamodelPackage.OBO_SESSION__SYNONYM_CATEGORIES:
        getSynonymCategories().clear();
        return;
      case obodatamodelPackage.OBO_SESSION__CURRENT_FILENAMES:
        getCurrentFilenames().clear();
        return;
      case obodatamodelPackage.OBO_SESSION__CURRENT_USER:
        setCurrentUser(CURRENT_USER_EDEFAULT);
        return;
      case obodatamodelPackage.OBO_SESSION__ID_SPACES:
        getIdSpaces().clear();
        return;
      case obodatamodelPackage.OBO_SESSION__LOAD_REMARK:
        setLoadRemark(LOAD_REMARK_EDEFAULT);
        return;
      case obodatamodelPackage.OBO_SESSION__ALL_DBX_REFS_CONTAINER:
        getAllDbxRefsContainer().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.OBO_SESSION__ID:
        return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
      case obodatamodelPackage.OBO_SESSION__OBJECTS:
        return objects != null && !objects.isEmpty();
      case obodatamodelPackage.OBO_SESSION__LINK_DATABASE:
        return linkDatabase != null;
      case obodatamodelPackage.OBO_SESSION__DEFAULT_NAMESPACE:
        return defaultNamespace != null;
      case obodatamodelPackage.OBO_SESSION__NAMESPACES:
        return namespaces != null && !namespaces.isEmpty();
      case obodatamodelPackage.OBO_SESSION__PROPERTY_VALUES:
        return propertyValues != null && !propertyValues.isEmpty();
      case obodatamodelPackage.OBO_SESSION__UNKNOWN_STANZAS:
        return unknownStanzas != null && !unknownStanzas.isEmpty();
      case obodatamodelPackage.OBO_SESSION__SUBSETS:
        return subsets != null && !subsets.isEmpty();
      case obodatamodelPackage.OBO_SESSION__CATEGORIES:
        return categories != null && !categories.isEmpty();
      case obodatamodelPackage.OBO_SESSION__SYNONYM_TYPES:
        return synonymTypes != null && !synonymTypes.isEmpty();
      case obodatamodelPackage.OBO_SESSION__SYNONYM_CATEGORIES:
        return synonymCategories != null && !synonymCategories.isEmpty();
      case obodatamodelPackage.OBO_SESSION__CURRENT_FILENAMES:
        return currentFilenames != null && !currentFilenames.isEmpty();
      case obodatamodelPackage.OBO_SESSION__CURRENT_USER:
        return CURRENT_USER_EDEFAULT == null ? currentUser != null : !CURRENT_USER_EDEFAULT.equals(currentUser);
      case obodatamodelPackage.OBO_SESSION__ID_SPACES:
        return idSpaces != null && !idSpaces.isEmpty();
      case obodatamodelPackage.OBO_SESSION__LOAD_REMARK:
        return LOAD_REMARK_EDEFAULT == null ? loadRemark != null : !LOAD_REMARK_EDEFAULT.equals(loadRemark);
      case obodatamodelPackage.OBO_SESSION__ALL_DBX_REFS_CONTAINER:
        return allDbxRefsContainer != null && !allDbxRefsContainer.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (id: ");
    result.append(id);
    result.append(", currentFilenames: ");
    result.append(currentFilenames);
    result.append(", currentUser: ");
    result.append(currentUser);
    result.append(", loadRemark: ");
    result.append(loadRemark);
    result.append(')');
    return result.toString();
  }

} //OBOSessionImpl
