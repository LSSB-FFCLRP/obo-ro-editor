/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.util;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.AnnotatedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.CommentedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingProperty;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Datatype;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DatatypeValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DbxrefedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPath;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPathSpec;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObjectIndex;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Impliable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Instance;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Link;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkLinkedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ListOfProperties;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MultiIDObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MutableLinkDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NamespacedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOClass;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectFactory;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectField;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OboPropertyValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PathCapable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.RootAlgorithm;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Serializable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SubsetObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Type;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Value;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueLink;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Map;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage
 * @generated
 */
public class obodatamodelAdapterFactory extends AdapterFactoryImpl {
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static obodatamodelPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public obodatamodelAdapterFactory() {
    if (modelPackage == null) {
      modelPackage = obodatamodelPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object) {
    if (object == modelPackage) {
      return true;
    }
    if (object instanceof EObject) {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected obodatamodelSwitch<Adapter> modelSwitch =
    new obodatamodelSwitch<Adapter>() {
      @Override
      public Adapter caseAnnotatedObject(AnnotatedObject object) {
        return createAnnotatedObjectAdapter();
      }
      @Override
      public Adapter caseCommentedObject(CommentedObject object) {
        return createCommentedObjectAdapter();
      }
      @Override
      public Adapter caseDanglingObject(DanglingObject object) {
        return createDanglingObjectAdapter();
      }
      @Override
      public Adapter caseDanglingProperty(DanglingProperty object) {
        return createDanglingPropertyAdapter();
      }
      @Override
      public Adapter caseDatatype(Datatype object) {
        return createDatatypeAdapter();
      }
      @Override
      public Adapter caseDatatypeValue(DatatypeValue object) {
        return createDatatypeValueAdapter();
      }
      @Override
      public Adapter caseDbxref(Dbxref object) {
        return createDbxrefAdapter();
      }
      @Override
      public Adapter caseDbxrefedObject(DbxrefedObject object) {
        return createDbxrefedObjectAdapter();
      }
      @Override
      public Adapter caseDefinedObject(DefinedObject object) {
        return createDefinedObjectAdapter();
      }
      @Override
      public Adapter caseFieldPath(FieldPath object) {
        return createFieldPathAdapter();
      }
      @Override
      public Adapter caseFieldPathSpec(FieldPathSpec object) {
        return createFieldPathSpecAdapter();
      }
      @Override
      public Adapter caseIdentifiableObject(IdentifiableObject object) {
        return createIdentifiableObjectAdapter();
      }
      @Override
      public Adapter caseIdentifiedObject(IdentifiedObject object) {
        return createIdentifiedObjectAdapter();
      }
      @Override
      public Adapter caseIdentifiedObjectIndex(IdentifiedObjectIndex object) {
        return createIdentifiedObjectIndexAdapter();
      }
      @Override
      public Adapter caseImpliable(Impliable object) {
        return createImpliableAdapter();
      }
      @Override
      public Adapter caseInstance(Instance object) {
        return createInstanceAdapter();
      }
      @Override
      public Adapter caseLink(Link object) {
        return createLinkAdapter();
      }
      @Override
      public Adapter caseLinkDatabase(LinkDatabase object) {
        return createLinkDatabaseAdapter();
      }
      @Override
      public Adapter caseLinkLinkedObject(LinkLinkedObject object) {
        return createLinkLinkedObjectAdapter();
      }
      @Override
      public Adapter caseLinkedObject(LinkedObject object) {
        return createLinkedObjectAdapter();
      }
      @Override
      public Adapter caseModificationMetadataObject(ModificationMetadataObject object) {
        return createModificationMetadataObjectAdapter();
      }
      @Override
      public Adapter caseMultiIDObject(MultiIDObject object) {
        return createMultiIDObjectAdapter();
      }
      @Override
      public Adapter caseMutableLinkDatabase(MutableLinkDatabase object) {
        return createMutableLinkDatabaseAdapter();
      }
      @Override
      public Adapter caseNamespace(Namespace object) {
        return createNamespaceAdapter();
      }
      @Override
      public Adapter caseNamespacedObject(NamespacedObject object) {
        return createNamespacedObjectAdapter();
      }
      @Override
      public Adapter caseNestedValue(NestedValue object) {
        return createNestedValueAdapter();
      }
      @Override
      public Adapter caseOBOClass(OBOClass object) {
        return createOBOClassAdapter();
      }
      @Override
      public Adapter caseOBOObject(OBOObject object) {
        return createOBOObjectAdapter();
      }
      @Override
      public Adapter caseOBOProperty(OBOProperty object) {
        return createOBOPropertyAdapter();
      }
      @Override
      public Adapter caseOBORestriction(OBORestriction object) {
        return createOBORestrictionAdapter();
      }
      @Override
      public Adapter caseOBOSession(OBOSession object) {
        return createOBOSessionAdapter();
      }
      @Override
      public Adapter caseObjectFactory(ObjectFactory object) {
        return createObjectFactoryAdapter();
      }
      @Override
      public Adapter caseObjectField(ObjectField object) {
        return createObjectFieldAdapter();
      }
      @Override
      public Adapter caseObsoletableObject(ObsoletableObject object) {
        return createObsoletableObjectAdapter();
      }
      @Override
      public Adapter casePathCapable(PathCapable object) {
        return createPathCapableAdapter();
      }
      @Override
      public Adapter casePropertyValue(PropertyValue object) {
        return createPropertyValueAdapter();
      }
      @Override
      public Adapter caseRelationship(Relationship object) {
        return createRelationshipAdapter();
      }
      @Override
      public Adapter caseRootAlgorithm(RootAlgorithm object) {
        return createRootAlgorithmAdapter();
      }
      @Override
      public Adapter caseSubsetObject(SubsetObject object) {
        return createSubsetObjectAdapter();
      }
      @Override
      public Adapter caseSynonym(Synonym object) {
        return createSynonymAdapter();
      }
      @Override
      public Adapter caseSynonymType(SynonymType object) {
        return createSynonymTypeAdapter();
      }
      @Override
      public Adapter caseSynonymedObject(SynonymedObject object) {
        return createSynonymedObjectAdapter();
      }
      @Override
      public Adapter caseTermSubset(TermSubset object) {
        return createTermSubsetAdapter();
      }
      @Override
      public Adapter caseType(Type object) {
        return createTypeAdapter();
      }
      @Override
      public Adapter caseUnknownStanza(UnknownStanza object) {
        return createUnknownStanzaAdapter();
      }
      @Override
      public Adapter caseValue(Value object) {
        return createValueAdapter();
      }
      @Override
      public Adapter caseValueDatabase(ValueDatabase object) {
        return createValueDatabaseAdapter();
      }
      @Override
      public Adapter caseValueLink(ValueLink object) {
        return createValueLinkAdapter();
      }
      @Override
      public Adapter caseCloneable(br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable object) {
        return createCloneableAdapter();
      }
      @Override
      public Adapter caseSerializable(Serializable object) {
        return createSerializableAdapter();
      }
      @Override
      public Adapter caseComparable(br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable object) {
        return createComparableAdapter();
      }
      @Override
      public Adapter caseStringToNestedValueMap(Map.Entry<String, NestedValue> object) {
        return createStringToNestedValueMapAdapter();
      }
      @Override
      public Adapter caseObsoletableObjectToNestedValueMap(Map.Entry<ObsoletableObject, NestedValue> object) {
        return createObsoletableObjectToNestedValueMapAdapter();
      }
      @Override
      public Adapter caseTermSubsetToNestedValueMap(Map.Entry<TermSubset, NestedValue> object) {
        return createTermSubsetToNestedValueMapAdapter();
      }
      @Override
      public Adapter caseIdSpacesMap(Map.Entry<String, String> object) {
        return createIdSpacesMapAdapter();
      }
      @Override
      public Adapter caseListOfProperties(ListOfProperties object) {
        return createListOfPropertiesAdapter();
      }
      @Override
      public Adapter caseOboPropertyValue(OboPropertyValue object) {
        return createOboPropertyValueAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object) {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target) {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.AnnotatedObject <em>Annotated Object</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.AnnotatedObject
   * @generated
   */
  public Adapter createAnnotatedObjectAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.CommentedObject <em>Commented Object</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.CommentedObject
   * @generated
   */
  public Adapter createCommentedObjectAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingObject <em>Dangling Object</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingObject
   * @generated
   */
  public Adapter createDanglingObjectAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingProperty <em>Dangling Property</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingProperty
   * @generated
   */
  public Adapter createDanglingPropertyAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Datatype <em>Datatype</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Datatype
   * @generated
   */
  public Adapter createDatatypeAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DatatypeValue <em>Datatype Value</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DatatypeValue
   * @generated
   */
  public Adapter createDatatypeValueAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref <em>Dbxref</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref
   * @generated
   */
  public Adapter createDbxrefAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DbxrefedObject <em>Dbxrefed Object</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DbxrefedObject
   * @generated
   */
  public Adapter createDbxrefedObjectAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject <em>Defined Object</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject
   * @generated
   */
  public Adapter createDefinedObjectAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPath <em>Field Path</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPath
   * @generated
   */
  public Adapter createFieldPathAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPathSpec <em>Field Path Spec</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPathSpec
   * @generated
   */
  public Adapter createFieldPathSpecAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiableObject <em>Identifiable Object</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiableObject
   * @generated
   */
  public Adapter createIdentifiableObjectAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject <em>Identified Object</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject
   * @generated
   */
  public Adapter createIdentifiedObjectAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObjectIndex <em>Identified Object Index</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObjectIndex
   * @generated
   */
  public Adapter createIdentifiedObjectIndexAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Impliable <em>Impliable</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Impliable
   * @generated
   */
  public Adapter createImpliableAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Instance <em>Instance</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Instance
   * @generated
   */
  public Adapter createInstanceAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Link <em>Link</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Link
   * @generated
   */
  public Adapter createLinkAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase <em>Link Database</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase
   * @generated
   */
  public Adapter createLinkDatabaseAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkLinkedObject <em>Link Linked Object</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkLinkedObject
   * @generated
   */
  public Adapter createLinkLinkedObjectAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject <em>Linked Object</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject
   * @generated
   */
  public Adapter createLinkedObjectAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject <em>Modification Metadata Object</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject
   * @generated
   */
  public Adapter createModificationMetadataObjectAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MultiIDObject <em>Multi ID Object</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MultiIDObject
   * @generated
   */
  public Adapter createMultiIDObjectAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MutableLinkDatabase <em>Mutable Link Database</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MutableLinkDatabase
   * @generated
   */
  public Adapter createMutableLinkDatabaseAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace <em>Namespace</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace
   * @generated
   */
  public Adapter createNamespaceAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NamespacedObject <em>Namespaced Object</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NamespacedObject
   * @generated
   */
  public Adapter createNamespacedObjectAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue <em>Nested Value</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue
   * @generated
   */
  public Adapter createNestedValueAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOClass <em>OBO Class</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOClass
   * @generated
   */
  public Adapter createOBOClassAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOObject <em>OBO Object</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOObject
   * @generated
   */
  public Adapter createOBOObjectAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty <em>OBO Property</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty
   * @generated
   */
  public Adapter createOBOPropertyAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction <em>OBO Restriction</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction
   * @generated
   */
  public Adapter createOBORestrictionAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession <em>OBO Session</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession
   * @generated
   */
  public Adapter createOBOSessionAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectFactory <em>Object Factory</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectFactory
   * @generated
   */
  public Adapter createObjectFactoryAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectField <em>Object Field</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectField
   * @generated
   */
  public Adapter createObjectFieldAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject <em>Obsoletable Object</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject
   * @generated
   */
  public Adapter createObsoletableObjectAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PathCapable <em>Path Capable</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PathCapable
   * @generated
   */
  public Adapter createPathCapableAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue <em>Property Value</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue
   * @generated
   */
  public Adapter createPropertyValueAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship <em>Relationship</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship
   * @generated
   */
  public Adapter createRelationshipAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.RootAlgorithm <em>Root Algorithm</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.RootAlgorithm
   * @generated
   */
  public Adapter createRootAlgorithmAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SubsetObject <em>Subset Object</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SubsetObject
   * @generated
   */
  public Adapter createSubsetObjectAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym <em>Synonym</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym
   * @generated
   */
  public Adapter createSynonymAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType <em>Synonym Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType
   * @generated
   */
  public Adapter createSynonymTypeAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymedObject <em>Synonymed Object</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymedObject
   * @generated
   */
  public Adapter createSynonymedObjectAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset <em>Term Subset</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset
   * @generated
   */
  public Adapter createTermSubsetAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Type <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Type
   * @generated
   */
  public Adapter createTypeAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza <em>Unknown Stanza</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza
   * @generated
   */
  public Adapter createUnknownStanzaAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Value <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Value
   * @generated
   */
  public Adapter createValueAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueDatabase <em>Value Database</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueDatabase
   * @generated
   */
  public Adapter createValueDatabaseAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueLink <em>Value Link</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueLink
   * @generated
   */
  public Adapter createValueLinkAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable <em>Cloneable</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable
   * @generated
   */
  public Adapter createCloneableAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Serializable <em>Serializable</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Serializable
   * @generated
   */
  public Adapter createSerializableAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable <em>Comparable</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable
   * @generated
   */
  public Adapter createComparableAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>String To Nested Value Map</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see java.util.Map.Entry
   * @generated
   */
  public Adapter createStringToNestedValueMapAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Obsoletable Object To Nested Value Map</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see java.util.Map.Entry
   * @generated
   */
  public Adapter createObsoletableObjectToNestedValueMapAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Term Subset To Nested Value Map</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see java.util.Map.Entry
   * @generated
   */
  public Adapter createTermSubsetToNestedValueMapAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>Id Spaces Map</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see java.util.Map.Entry
   * @generated
   */
  public Adapter createIdSpacesMapAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ListOfProperties <em>List Of Properties</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ListOfProperties
   * @generated
   */
  public Adapter createListOfPropertiesAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OboPropertyValue <em>Obo Property Value</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OboPropertyValue
   * @generated
   */
  public Adapter createOboPropertyValueAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter() {
    return null;
  }

} //obodatamodelAdapterFactory
