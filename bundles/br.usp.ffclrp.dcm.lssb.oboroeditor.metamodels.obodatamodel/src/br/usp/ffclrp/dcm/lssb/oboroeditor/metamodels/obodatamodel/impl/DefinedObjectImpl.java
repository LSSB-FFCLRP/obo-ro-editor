/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Defined Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DefinedObjectImpl#getDefinition <em>Definition</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DefinedObjectImpl#getDefDbxrefs <em>Def Dbxrefs</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DefinedObjectImpl#getDefinitionExtension <em>Definition Extension</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class DefinedObjectImpl extends IdentifiedObjectImpl implements DefinedObject {
  /**
   * The default value of the '{@link #getDefinition() <em>Definition</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDefinition()
   * @generated
   * @ordered
   */
  protected static final String DEFINITION_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getDefinition() <em>Definition</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDefinition()
   * @generated
   * @ordered
   */
  protected String definition = DEFINITION_EDEFAULT;

  /**
   * The cached value of the '{@link #getDefDbxrefs() <em>Def Dbxrefs</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDefDbxrefs()
   * @generated
   * @ordered
   */
  protected EList<Dbxref> defDbxrefs;

  /**
   * The cached value of the '{@link #getDefinitionExtension() <em>Definition Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDefinitionExtension()
   * @generated
   * @ordered
   */
  protected NestedValue definitionExtension;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DefinedObjectImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.DEFINED_OBJECT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDefinition() {
    return definition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDefinition(String newDefinition) {
    String oldDefinition = definition;
    definition = newDefinition;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DEFINED_OBJECT__DEFINITION, oldDefinition, definition));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Dbxref> getDefDbxrefs() {
    if (defDbxrefs == null) {
      defDbxrefs = new EObjectResolvingEList<Dbxref>(Dbxref.class, this, obodatamodelPackage.DEFINED_OBJECT__DEF_DBXREFS);
    }
    return defDbxrefs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getDefinitionExtension() {
    return definitionExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDefinitionExtension(NestedValue newDefinitionExtension, NotificationChain msgs) {
    NestedValue oldDefinitionExtension = definitionExtension;
    definitionExtension = newDefinitionExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DEFINED_OBJECT__DEFINITION_EXTENSION, oldDefinitionExtension, newDefinitionExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDefinitionExtension(NestedValue newDefinitionExtension) {
    if (newDefinitionExtension != definitionExtension) {
      NotificationChain msgs = null;
      if (definitionExtension != null)
        msgs = ((InternalEObject)definitionExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DEFINED_OBJECT__DEFINITION_EXTENSION, null, msgs);
      if (newDefinitionExtension != null)
        msgs = ((InternalEObject)newDefinitionExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DEFINED_OBJECT__DEFINITION_EXTENSION, null, msgs);
      msgs = basicSetDefinitionExtension(newDefinitionExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DEFINED_OBJECT__DEFINITION_EXTENSION, newDefinitionExtension, newDefinitionExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.DEFINED_OBJECT__DEFINITION_EXTENSION:
        return basicSetDefinitionExtension(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.DEFINED_OBJECT__DEFINITION:
        return getDefinition();
      case obodatamodelPackage.DEFINED_OBJECT__DEF_DBXREFS:
        return getDefDbxrefs();
      case obodatamodelPackage.DEFINED_OBJECT__DEFINITION_EXTENSION:
        return getDefinitionExtension();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.DEFINED_OBJECT__DEFINITION:
        setDefinition((String)newValue);
        return;
      case obodatamodelPackage.DEFINED_OBJECT__DEF_DBXREFS:
        getDefDbxrefs().clear();
        getDefDbxrefs().addAll((Collection<? extends Dbxref>)newValue);
        return;
      case obodatamodelPackage.DEFINED_OBJECT__DEFINITION_EXTENSION:
        setDefinitionExtension((NestedValue)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.DEFINED_OBJECT__DEFINITION:
        setDefinition(DEFINITION_EDEFAULT);
        return;
      case obodatamodelPackage.DEFINED_OBJECT__DEF_DBXREFS:
        getDefDbxrefs().clear();
        return;
      case obodatamodelPackage.DEFINED_OBJECT__DEFINITION_EXTENSION:
        setDefinitionExtension((NestedValue)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.DEFINED_OBJECT__DEFINITION:
        return DEFINITION_EDEFAULT == null ? definition != null : !DEFINITION_EDEFAULT.equals(definition);
      case obodatamodelPackage.DEFINED_OBJECT__DEF_DBXREFS:
        return defDbxrefs != null && !defDbxrefs.isEmpty();
      case obodatamodelPackage.DEFINED_OBJECT__DEFINITION_EXTENSION:
        return definitionExtension != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (definition: ");
    result.append(definition);
    result.append(')');
    return result.toString();
  }

} //DefinedObjectImpl
