/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Unknown Stanza</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.UnknownStanzaImpl#getNamespace <em>Namespace</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.UnknownStanzaImpl#getPropertyValues <em>Property Values</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.UnknownStanzaImpl#getNestedValues <em>Nested Values</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.UnknownStanzaImpl#getStanza <em>Stanza</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UnknownStanzaImpl extends MinimalEObjectImpl.Container implements UnknownStanza {
  /**
   * The cached value of the '{@link #getNamespace() <em>Namespace</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNamespace()
   * @generated
   * @ordered
   */
  protected Namespace namespace;

  /**
   * The cached value of the '{@link #getPropertyValues() <em>Property Values</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPropertyValues()
   * @generated
   * @ordered
   */
  protected EList<PropertyValue> propertyValues;

  /**
   * The cached value of the '{@link #getNestedValues() <em>Nested Values</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNestedValues()
   * @generated
   * @ordered
   */
  protected EList<NestedValue> nestedValues;

  /**
   * The default value of the '{@link #getStanza() <em>Stanza</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStanza()
   * @generated
   * @ordered
   */
  protected static final String STANZA_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getStanza() <em>Stanza</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStanza()
   * @generated
   * @ordered
   */
  protected String stanza = STANZA_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected UnknownStanzaImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.UNKNOWN_STANZA;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Namespace getNamespace() {
    if (namespace != null && namespace.eIsProxy()) {
      InternalEObject oldNamespace = (InternalEObject)namespace;
      namespace = (Namespace)eResolveProxy(oldNamespace);
      if (namespace != oldNamespace) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, obodatamodelPackage.UNKNOWN_STANZA__NAMESPACE, oldNamespace, namespace));
      }
    }
    return namespace;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Namespace basicGetNamespace() {
    return namespace;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNamespace(Namespace newNamespace) {
    Namespace oldNamespace = namespace;
    namespace = newNamespace;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.UNKNOWN_STANZA__NAMESPACE, oldNamespace, namespace));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<PropertyValue> getPropertyValues() {
    if (propertyValues == null) {
      propertyValues = new EObjectResolvingEList<PropertyValue>(PropertyValue.class, this, obodatamodelPackage.UNKNOWN_STANZA__PROPERTY_VALUES);
    }
    return propertyValues;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<NestedValue> getNestedValues() {
    if (nestedValues == null) {
      nestedValues = new EObjectResolvingEList<NestedValue>(NestedValue.class, this, obodatamodelPackage.UNKNOWN_STANZA__NESTED_VALUES);
    }
    return nestedValues;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getStanza() {
    return stanza;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setStanza(String newStanza) {
    String oldStanza = stanza;
    stanza = newStanza;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.UNKNOWN_STANZA__STANZA, oldStanza, stanza));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.UNKNOWN_STANZA__NAMESPACE:
        if (resolve) return getNamespace();
        return basicGetNamespace();
      case obodatamodelPackage.UNKNOWN_STANZA__PROPERTY_VALUES:
        return getPropertyValues();
      case obodatamodelPackage.UNKNOWN_STANZA__NESTED_VALUES:
        return getNestedValues();
      case obodatamodelPackage.UNKNOWN_STANZA__STANZA:
        return getStanza();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.UNKNOWN_STANZA__NAMESPACE:
        setNamespace((Namespace)newValue);
        return;
      case obodatamodelPackage.UNKNOWN_STANZA__PROPERTY_VALUES:
        getPropertyValues().clear();
        getPropertyValues().addAll((Collection<? extends PropertyValue>)newValue);
        return;
      case obodatamodelPackage.UNKNOWN_STANZA__NESTED_VALUES:
        getNestedValues().clear();
        getNestedValues().addAll((Collection<? extends NestedValue>)newValue);
        return;
      case obodatamodelPackage.UNKNOWN_STANZA__STANZA:
        setStanza((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.UNKNOWN_STANZA__NAMESPACE:
        setNamespace((Namespace)null);
        return;
      case obodatamodelPackage.UNKNOWN_STANZA__PROPERTY_VALUES:
        getPropertyValues().clear();
        return;
      case obodatamodelPackage.UNKNOWN_STANZA__NESTED_VALUES:
        getNestedValues().clear();
        return;
      case obodatamodelPackage.UNKNOWN_STANZA__STANZA:
        setStanza(STANZA_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.UNKNOWN_STANZA__NAMESPACE:
        return namespace != null;
      case obodatamodelPackage.UNKNOWN_STANZA__PROPERTY_VALUES:
        return propertyValues != null && !propertyValues.isEmpty();
      case obodatamodelPackage.UNKNOWN_STANZA__NESTED_VALUES:
        return nestedValues != null && !nestedValues.isEmpty();
      case obodatamodelPackage.UNKNOWN_STANZA__STANZA:
        return STANZA_EDEFAULT == null ? stanza != null : !STANZA_EDEFAULT.equals(stanza);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (stanza: ");
    result.append(stanza);
    result.append(')');
    return result.toString();
  }

} //UnknownStanzaImpl
