/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.util;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.AnnotatedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.CommentedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingProperty;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Datatype;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DatatypeValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DbxrefedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPath;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPathSpec;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObjectIndex;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Impliable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Instance;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Link;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkLinkedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ListOfProperties;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MultiIDObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MutableLinkDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NamespacedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOClass;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectFactory;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectField;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OboPropertyValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PathCapable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.RootAlgorithm;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Serializable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SubsetObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Type;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Value;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueLink;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage
 * @generated
 */
public class obodatamodelSwitch<T> extends Switch<T> {
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static obodatamodelPackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public obodatamodelSwitch() {
    if (modelPackage == null) {
      modelPackage = obodatamodelPackage.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage) {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject) {
    switch (classifierID) {
      case obodatamodelPackage.ANNOTATED_OBJECT: {
        AnnotatedObject annotatedObject = (AnnotatedObject)theEObject;
        T result = caseAnnotatedObject(annotatedObject);
        if (result == null) result = caseModificationMetadataObject(annotatedObject);
        if (result == null) result = caseMultiIDObject(annotatedObject);
        if (result == null) result = caseSynonymedObject(annotatedObject);
        if (result == null) result = caseDbxrefedObject(annotatedObject);
        if (result == null) result = caseCommentedObject(annotatedObject);
        if (result == null) result = caseObsoletableObject(annotatedObject);
        if (result == null) result = caseDefinedObject(annotatedObject);
        if (result == null) result = caseSubsetObject(annotatedObject);
        if (result == null) result = caseIdentifiedObject(annotatedObject);
        if (result == null) result = caseValue(annotatedObject);
        if (result == null) result = caseIdentifiableObject(annotatedObject);
        if (result == null) result = caseNamespacedObject(annotatedObject);
        if (result == null) result = caseCloneable(annotatedObject);
        if (result == null) result = caseSerializable(annotatedObject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.COMMENTED_OBJECT: {
        CommentedObject commentedObject = (CommentedObject)theEObject;
        T result = caseCommentedObject(commentedObject);
        if (result == null) result = caseIdentifiedObject(commentedObject);
        if (result == null) result = caseValue(commentedObject);
        if (result == null) result = caseIdentifiableObject(commentedObject);
        if (result == null) result = caseNamespacedObject(commentedObject);
        if (result == null) result = caseCloneable(commentedObject);
        if (result == null) result = caseSerializable(commentedObject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.DANGLING_OBJECT: {
        DanglingObject danglingObject = (DanglingObject)theEObject;
        T result = caseDanglingObject(danglingObject);
        if (result == null) result = caseLinkedObject(danglingObject);
        if (result == null) result = caseType(danglingObject);
        if (result == null) result = caseIdentifiedObject(danglingObject);
        if (result == null) result = casePathCapable(danglingObject);
        if (result == null) result = caseValue(danglingObject);
        if (result == null) result = caseIdentifiableObject(danglingObject);
        if (result == null) result = caseNamespacedObject(danglingObject);
        if (result == null) result = caseCloneable(danglingObject);
        if (result == null) result = caseSerializable(danglingObject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.DANGLING_PROPERTY: {
        DanglingProperty danglingProperty = (DanglingProperty)theEObject;
        T result = caseDanglingProperty(danglingProperty);
        if (result == null) result = caseOBOProperty(danglingProperty);
        if (result == null) result = casePathCapable(danglingProperty);
        if (result == null) result = caseOBOObject(danglingProperty);
        if (result == null) result = caseLinkedObject(danglingProperty);
        if (result == null) result = caseValue(danglingProperty);
        if (result == null) result = caseIdentifiableObject(danglingProperty);
        if (result == null) result = caseNamespacedObject(danglingProperty);
        if (result == null) result = caseAnnotatedObject(danglingProperty);
        if (result == null) result = caseComparable(danglingProperty);
        if (result == null) result = caseCloneable(danglingProperty);
        if (result == null) result = caseSerializable(danglingProperty);
        if (result == null) result = caseModificationMetadataObject(danglingProperty);
        if (result == null) result = caseMultiIDObject(danglingProperty);
        if (result == null) result = caseSynonymedObject(danglingProperty);
        if (result == null) result = caseDbxrefedObject(danglingProperty);
        if (result == null) result = caseCommentedObject(danglingProperty);
        if (result == null) result = caseObsoletableObject(danglingProperty);
        if (result == null) result = caseDefinedObject(danglingProperty);
        if (result == null) result = caseSubsetObject(danglingProperty);
        if (result == null) result = caseIdentifiedObject(danglingProperty);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.DATATYPE: {
        Datatype datatype = (Datatype)theEObject;
        T result = caseDatatype(datatype);
        if (result == null) result = caseType(datatype);
        if (result == null) result = caseIdentifiedObject(datatype);
        if (result == null) result = caseValue(datatype);
        if (result == null) result = caseIdentifiableObject(datatype);
        if (result == null) result = caseNamespacedObject(datatype);
        if (result == null) result = caseCloneable(datatype);
        if (result == null) result = caseSerializable(datatype);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.DATATYPE_VALUE: {
        DatatypeValue datatypeValue = (DatatypeValue)theEObject;
        T result = caseDatatypeValue(datatypeValue);
        if (result == null) result = caseValue(datatypeValue);
        if (result == null) result = caseCloneable(datatypeValue);
        if (result == null) result = caseSerializable(datatypeValue);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.DBXREF: {
        Dbxref dbxref = (Dbxref)theEObject;
        T result = caseDbxref(dbxref);
        if (result == null) result = caseCloneable(dbxref);
        if (result == null) result = caseSerializable(dbxref);
        if (result == null) result = caseComparable(dbxref);
        if (result == null) result = caseIdentifiableObject(dbxref);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.DBXREFED_OBJECT: {
        DbxrefedObject dbxrefedObject = (DbxrefedObject)theEObject;
        T result = caseDbxrefedObject(dbxrefedObject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.DEFINED_OBJECT: {
        DefinedObject definedObject = (DefinedObject)theEObject;
        T result = caseDefinedObject(definedObject);
        if (result == null) result = caseIdentifiedObject(definedObject);
        if (result == null) result = caseValue(definedObject);
        if (result == null) result = caseIdentifiableObject(definedObject);
        if (result == null) result = caseNamespacedObject(definedObject);
        if (result == null) result = caseCloneable(definedObject);
        if (result == null) result = caseSerializable(definedObject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.FIELD_PATH: {
        FieldPath fieldPath = (FieldPath)theEObject;
        T result = caseFieldPath(fieldPath);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.FIELD_PATH_SPEC: {
        FieldPathSpec fieldPathSpec = (FieldPathSpec)theEObject;
        T result = caseFieldPathSpec(fieldPathSpec);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.IDENTIFIABLE_OBJECT: {
        IdentifiableObject identifiableObject = (IdentifiableObject)theEObject;
        T result = caseIdentifiableObject(identifiableObject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.IDENTIFIED_OBJECT: {
        IdentifiedObject identifiedObject = (IdentifiedObject)theEObject;
        T result = caseIdentifiedObject(identifiedObject);
        if (result == null) result = caseValue(identifiedObject);
        if (result == null) result = caseIdentifiableObject(identifiedObject);
        if (result == null) result = caseNamespacedObject(identifiedObject);
        if (result == null) result = caseCloneable(identifiedObject);
        if (result == null) result = caseSerializable(identifiedObject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.IDENTIFIED_OBJECT_INDEX: {
        IdentifiedObjectIndex identifiedObjectIndex = (IdentifiedObjectIndex)theEObject;
        T result = caseIdentifiedObjectIndex(identifiedObjectIndex);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.IMPLIABLE: {
        Impliable impliable = (Impliable)theEObject;
        T result = caseImpliable(impliable);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.INSTANCE: {
        Instance instance = (Instance)theEObject;
        T result = caseInstance(instance);
        if (result == null) result = caseOBOObject(instance);
        if (result == null) result = caseAnnotatedObject(instance);
        if (result == null) result = caseLinkedObject(instance);
        if (result == null) result = caseComparable(instance);
        if (result == null) result = caseModificationMetadataObject(instance);
        if (result == null) result = caseMultiIDObject(instance);
        if (result == null) result = caseSynonymedObject(instance);
        if (result == null) result = caseDbxrefedObject(instance);
        if (result == null) result = caseCommentedObject(instance);
        if (result == null) result = caseObsoletableObject(instance);
        if (result == null) result = caseDefinedObject(instance);
        if (result == null) result = caseSubsetObject(instance);
        if (result == null) result = casePathCapable(instance);
        if (result == null) result = caseIdentifiedObject(instance);
        if (result == null) result = caseValue(instance);
        if (result == null) result = caseIdentifiableObject(instance);
        if (result == null) result = caseNamespacedObject(instance);
        if (result == null) result = caseCloneable(instance);
        if (result == null) result = caseSerializable(instance);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.LINK: {
        Link link = (Link)theEObject;
        T result = caseLink(link);
        if (result == null) result = caseImpliable(link);
        if (result == null) result = caseIdentifiableObject(link);
        if (result == null) result = caseRelationship(link);
        if (result == null) result = casePathCapable(link);
        if (result == null) result = caseSerializable(link);
        if (result == null) result = caseCloneable(link);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.LINK_DATABASE: {
        LinkDatabase linkDatabase = (LinkDatabase)theEObject;
        T result = caseLinkDatabase(linkDatabase);
        if (result == null) result = caseIdentifiedObjectIndex(linkDatabase);
        if (result == null) result = caseSerializable(linkDatabase);
        if (result == null) result = caseCloneable(linkDatabase);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.LINK_LINKED_OBJECT: {
        LinkLinkedObject linkLinkedObject = (LinkLinkedObject)theEObject;
        T result = caseLinkLinkedObject(linkLinkedObject);
        if (result == null) result = caseLinkedObject(linkLinkedObject);
        if (result == null) result = caseIdentifiedObject(linkLinkedObject);
        if (result == null) result = casePathCapable(linkLinkedObject);
        if (result == null) result = caseValue(linkLinkedObject);
        if (result == null) result = caseIdentifiableObject(linkLinkedObject);
        if (result == null) result = caseNamespacedObject(linkLinkedObject);
        if (result == null) result = caseCloneable(linkLinkedObject);
        if (result == null) result = caseSerializable(linkLinkedObject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.LINKED_OBJECT: {
        LinkedObject linkedObject = (LinkedObject)theEObject;
        T result = caseLinkedObject(linkedObject);
        if (result == null) result = caseIdentifiedObject(linkedObject);
        if (result == null) result = casePathCapable(linkedObject);
        if (result == null) result = caseValue(linkedObject);
        if (result == null) result = caseIdentifiableObject(linkedObject);
        if (result == null) result = caseNamespacedObject(linkedObject);
        if (result == null) result = caseCloneable(linkedObject);
        if (result == null) result = caseSerializable(linkedObject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT: {
        ModificationMetadataObject modificationMetadataObject = (ModificationMetadataObject)theEObject;
        T result = caseModificationMetadataObject(modificationMetadataObject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.MULTI_ID_OBJECT: {
        MultiIDObject multiIDObject = (MultiIDObject)theEObject;
        T result = caseMultiIDObject(multiIDObject);
        if (result == null) result = caseIdentifiedObject(multiIDObject);
        if (result == null) result = caseValue(multiIDObject);
        if (result == null) result = caseIdentifiableObject(multiIDObject);
        if (result == null) result = caseNamespacedObject(multiIDObject);
        if (result == null) result = caseCloneable(multiIDObject);
        if (result == null) result = caseSerializable(multiIDObject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.MUTABLE_LINK_DATABASE: {
        MutableLinkDatabase mutableLinkDatabase = (MutableLinkDatabase)theEObject;
        T result = caseMutableLinkDatabase(mutableLinkDatabase);
        if (result == null) result = caseLinkDatabase(mutableLinkDatabase);
        if (result == null) result = caseIdentifiedObjectIndex(mutableLinkDatabase);
        if (result == null) result = caseSerializable(mutableLinkDatabase);
        if (result == null) result = caseCloneable(mutableLinkDatabase);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.NAMESPACE: {
        Namespace namespace = (Namespace)theEObject;
        T result = caseNamespace(namespace);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.NAMESPACED_OBJECT: {
        NamespacedObject namespacedObject = (NamespacedObject)theEObject;
        T result = caseNamespacedObject(namespacedObject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.NESTED_VALUE: {
        NestedValue nestedValue = (NestedValue)theEObject;
        T result = caseNestedValue(nestedValue);
        if (result == null) result = caseCloneable(nestedValue);
        if (result == null) result = caseSerializable(nestedValue);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.OBO_CLASS: {
        OBOClass oboClass = (OBOClass)theEObject;
        T result = caseOBOClass(oboClass);
        if (result == null) result = caseOBOObject(oboClass);
        if (result == null) result = caseType(oboClass);
        if (result == null) result = caseAnnotatedObject(oboClass);
        if (result == null) result = caseLinkedObject(oboClass);
        if (result == null) result = caseComparable(oboClass);
        if (result == null) result = caseModificationMetadataObject(oboClass);
        if (result == null) result = caseMultiIDObject(oboClass);
        if (result == null) result = caseSynonymedObject(oboClass);
        if (result == null) result = caseDbxrefedObject(oboClass);
        if (result == null) result = caseCommentedObject(oboClass);
        if (result == null) result = caseObsoletableObject(oboClass);
        if (result == null) result = caseDefinedObject(oboClass);
        if (result == null) result = caseSubsetObject(oboClass);
        if (result == null) result = casePathCapable(oboClass);
        if (result == null) result = caseIdentifiedObject(oboClass);
        if (result == null) result = caseValue(oboClass);
        if (result == null) result = caseIdentifiableObject(oboClass);
        if (result == null) result = caseNamespacedObject(oboClass);
        if (result == null) result = caseCloneable(oboClass);
        if (result == null) result = caseSerializable(oboClass);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.OBO_OBJECT: {
        OBOObject oboObject = (OBOObject)theEObject;
        T result = caseOBOObject(oboObject);
        if (result == null) result = caseAnnotatedObject(oboObject);
        if (result == null) result = caseLinkedObject(oboObject);
        if (result == null) result = caseComparable(oboObject);
        if (result == null) result = caseModificationMetadataObject(oboObject);
        if (result == null) result = caseMultiIDObject(oboObject);
        if (result == null) result = caseSynonymedObject(oboObject);
        if (result == null) result = caseDbxrefedObject(oboObject);
        if (result == null) result = caseCommentedObject(oboObject);
        if (result == null) result = caseObsoletableObject(oboObject);
        if (result == null) result = caseDefinedObject(oboObject);
        if (result == null) result = caseSubsetObject(oboObject);
        if (result == null) result = casePathCapable(oboObject);
        if (result == null) result = caseIdentifiedObject(oboObject);
        if (result == null) result = caseValue(oboObject);
        if (result == null) result = caseIdentifiableObject(oboObject);
        if (result == null) result = caseNamespacedObject(oboObject);
        if (result == null) result = caseCloneable(oboObject);
        if (result == null) result = caseSerializable(oboObject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.OBO_PROPERTY: {
        OBOProperty oboProperty = (OBOProperty)theEObject;
        T result = caseOBOProperty(oboProperty);
        if (result == null) result = caseOBOObject(oboProperty);
        if (result == null) result = caseAnnotatedObject(oboProperty);
        if (result == null) result = caseLinkedObject(oboProperty);
        if (result == null) result = caseComparable(oboProperty);
        if (result == null) result = caseModificationMetadataObject(oboProperty);
        if (result == null) result = caseMultiIDObject(oboProperty);
        if (result == null) result = caseSynonymedObject(oboProperty);
        if (result == null) result = caseDbxrefedObject(oboProperty);
        if (result == null) result = caseCommentedObject(oboProperty);
        if (result == null) result = caseObsoletableObject(oboProperty);
        if (result == null) result = caseDefinedObject(oboProperty);
        if (result == null) result = caseSubsetObject(oboProperty);
        if (result == null) result = casePathCapable(oboProperty);
        if (result == null) result = caseIdentifiedObject(oboProperty);
        if (result == null) result = caseValue(oboProperty);
        if (result == null) result = caseIdentifiableObject(oboProperty);
        if (result == null) result = caseNamespacedObject(oboProperty);
        if (result == null) result = caseCloneable(oboProperty);
        if (result == null) result = caseSerializable(oboProperty);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.OBO_RESTRICTION: {
        OBORestriction oboRestriction = (OBORestriction)theEObject;
        T result = caseOBORestriction(oboRestriction);
        if (result == null) result = caseLink(oboRestriction);
        if (result == null) result = caseImpliable(oboRestriction);
        if (result == null) result = caseIdentifiableObject(oboRestriction);
        if (result == null) result = caseRelationship(oboRestriction);
        if (result == null) result = casePathCapable(oboRestriction);
        if (result == null) result = caseSerializable(oboRestriction);
        if (result == null) result = caseCloneable(oboRestriction);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.OBO_SESSION: {
        OBOSession oboSession = (OBOSession)theEObject;
        T result = caseOBOSession(oboSession);
        if (result == null) result = caseIdentifiedObjectIndex(oboSession);
        if (result == null) result = caseSerializable(oboSession);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.OBJECT_FACTORY: {
        ObjectFactory objectFactory = (ObjectFactory)theEObject;
        T result = caseObjectFactory(objectFactory);
        if (result == null) result = caseSerializable(objectFactory);
        if (result == null) result = caseCloneable(objectFactory);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.OBJECT_FIELD: {
        ObjectField objectField = (ObjectField)theEObject;
        T result = caseObjectField(objectField);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.OBSOLETABLE_OBJECT: {
        ObsoletableObject obsoletableObject = (ObsoletableObject)theEObject;
        T result = caseObsoletableObject(obsoletableObject);
        if (result == null) result = caseIdentifiedObject(obsoletableObject);
        if (result == null) result = caseValue(obsoletableObject);
        if (result == null) result = caseIdentifiableObject(obsoletableObject);
        if (result == null) result = caseNamespacedObject(obsoletableObject);
        if (result == null) result = caseCloneable(obsoletableObject);
        if (result == null) result = caseSerializable(obsoletableObject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.PATH_CAPABLE: {
        PathCapable pathCapable = (PathCapable)theEObject;
        T result = casePathCapable(pathCapable);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.PROPERTY_VALUE: {
        PropertyValue propertyValue = (PropertyValue)theEObject;
        T result = casePropertyValue(propertyValue);
        if (result == null) result = caseCloneable(propertyValue);
        if (result == null) result = caseSerializable(propertyValue);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.RELATIONSHIP: {
        Relationship relationship = (Relationship)theEObject;
        T result = caseRelationship(relationship);
        if (result == null) result = caseSerializable(relationship);
        if (result == null) result = caseCloneable(relationship);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.ROOT_ALGORITHM: {
        RootAlgorithm rootAlgorithm = (RootAlgorithm)theEObject;
        T result = caseRootAlgorithm(rootAlgorithm);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.SUBSET_OBJECT: {
        SubsetObject subsetObject = (SubsetObject)theEObject;
        T result = caseSubsetObject(subsetObject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.SYNONYM: {
        Synonym synonym = (Synonym)theEObject;
        T result = caseSynonym(synonym);
        if (result == null) result = caseCloneable(synonym);
        if (result == null) result = caseSerializable(synonym);
        if (result == null) result = caseComparable(synonym);
        if (result == null) result = caseIdentifiableObject(synonym);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.SYNONYM_TYPE: {
        SynonymType synonymType = (SynonymType)theEObject;
        T result = caseSynonymType(synonymType);
        if (result == null) result = caseCloneable(synonymType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.SYNONYMED_OBJECT: {
        SynonymedObject synonymedObject = (SynonymedObject)theEObject;
        T result = caseSynonymedObject(synonymedObject);
        if (result == null) result = caseIdentifiedObject(synonymedObject);
        if (result == null) result = caseValue(synonymedObject);
        if (result == null) result = caseIdentifiableObject(synonymedObject);
        if (result == null) result = caseNamespacedObject(synonymedObject);
        if (result == null) result = caseCloneable(synonymedObject);
        if (result == null) result = caseSerializable(synonymedObject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.TERM_SUBSET: {
        TermSubset termSubset = (TermSubset)theEObject;
        T result = caseTermSubset(termSubset);
        if (result == null) result = caseCloneable(termSubset);
        if (result == null) result = caseSerializable(termSubset);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.TYPE: {
        Type type = (Type)theEObject;
        T result = caseType(type);
        if (result == null) result = caseIdentifiedObject(type);
        if (result == null) result = caseValue(type);
        if (result == null) result = caseIdentifiableObject(type);
        if (result == null) result = caseNamespacedObject(type);
        if (result == null) result = caseCloneable(type);
        if (result == null) result = caseSerializable(type);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.UNKNOWN_STANZA: {
        UnknownStanza unknownStanza = (UnknownStanza)theEObject;
        T result = caseUnknownStanza(unknownStanza);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.VALUE: {
        Value value = (Value)theEObject;
        T result = caseValue(value);
        if (result == null) result = caseCloneable(value);
        if (result == null) result = caseSerializable(value);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.VALUE_DATABASE: {
        ValueDatabase valueDatabase = (ValueDatabase)theEObject;
        T result = caseValueDatabase(valueDatabase);
        if (result == null) result = caseIdentifiedObjectIndex(valueDatabase);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.VALUE_LINK: {
        ValueLink valueLink = (ValueLink)theEObject;
        T result = caseValueLink(valueLink);
        if (result == null) result = caseLink(valueLink);
        if (result == null) result = caseImpliable(valueLink);
        if (result == null) result = caseIdentifiableObject(valueLink);
        if (result == null) result = caseRelationship(valueLink);
        if (result == null) result = casePathCapable(valueLink);
        if (result == null) result = caseSerializable(valueLink);
        if (result == null) result = caseCloneable(valueLink);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.CLONEABLE: {
        br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable cloneable = (br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable)theEObject;
        T result = caseCloneable(cloneable);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.SERIALIZABLE: {
        Serializable serializable = (Serializable)theEObject;
        T result = caseSerializable(serializable);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.COMPARABLE: {
        br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable comparable = (br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable)theEObject;
        T result = caseComparable(comparable);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.STRING_TO_NESTED_VALUE_MAP: {
        @SuppressWarnings("unchecked") Map.Entry<String, NestedValue> stringToNestedValueMap = (Map.Entry<String, NestedValue>)theEObject;
        T result = caseStringToNestedValueMap(stringToNestedValueMap);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP: {
        @SuppressWarnings("unchecked") Map.Entry<ObsoletableObject, NestedValue> obsoletableObjectToNestedValueMap = (Map.Entry<ObsoletableObject, NestedValue>)theEObject;
        T result = caseObsoletableObjectToNestedValueMap(obsoletableObjectToNestedValueMap);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.TERM_SUBSET_TO_NESTED_VALUE_MAP: {
        @SuppressWarnings("unchecked") Map.Entry<TermSubset, NestedValue> termSubsetToNestedValueMap = (Map.Entry<TermSubset, NestedValue>)theEObject;
        T result = caseTermSubsetToNestedValueMap(termSubsetToNestedValueMap);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.ID_SPACES_MAP: {
        @SuppressWarnings("unchecked") Map.Entry<String, String> idSpacesMap = (Map.Entry<String, String>)theEObject;
        T result = caseIdSpacesMap(idSpacesMap);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.LIST_OF_PROPERTIES: {
        ListOfProperties listOfProperties = (ListOfProperties)theEObject;
        T result = caseListOfProperties(listOfProperties);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case obodatamodelPackage.OBO_PROPERTY_VALUE: {
        OboPropertyValue oboPropertyValue = (OboPropertyValue)theEObject;
        T result = caseOboPropertyValue(oboPropertyValue);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Annotated Object</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Annotated Object</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAnnotatedObject(AnnotatedObject object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Commented Object</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Commented Object</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCommentedObject(CommentedObject object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Dangling Object</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Dangling Object</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDanglingObject(DanglingObject object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Dangling Property</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Dangling Property</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDanglingProperty(DanglingProperty object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Datatype</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Datatype</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDatatype(Datatype object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Datatype Value</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Datatype Value</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDatatypeValue(DatatypeValue object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Dbxref</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Dbxref</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDbxref(Dbxref object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Dbxrefed Object</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Dbxrefed Object</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDbxrefedObject(DbxrefedObject object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Defined Object</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Defined Object</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDefinedObject(DefinedObject object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Field Path</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Field Path</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFieldPath(FieldPath object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Field Path Spec</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Field Path Spec</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFieldPathSpec(FieldPathSpec object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Identifiable Object</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Identifiable Object</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIdentifiableObject(IdentifiableObject object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Identified Object</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Identified Object</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIdentifiedObject(IdentifiedObject object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Identified Object Index</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Identified Object Index</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIdentifiedObjectIndex(IdentifiedObjectIndex object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Impliable</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Impliable</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseImpliable(Impliable object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Instance</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Instance</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseInstance(Instance object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Link</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Link</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLink(Link object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Link Database</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Link Database</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLinkDatabase(LinkDatabase object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Link Linked Object</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Link Linked Object</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLinkLinkedObject(LinkLinkedObject object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Linked Object</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Linked Object</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLinkedObject(LinkedObject object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Modification Metadata Object</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Modification Metadata Object</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseModificationMetadataObject(ModificationMetadataObject object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Multi ID Object</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Multi ID Object</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMultiIDObject(MultiIDObject object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Mutable Link Database</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Mutable Link Database</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMutableLinkDatabase(MutableLinkDatabase object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Namespace</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Namespace</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNamespace(Namespace object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Namespaced Object</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Namespaced Object</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNamespacedObject(NamespacedObject object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Nested Value</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Nested Value</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNestedValue(NestedValue object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>OBO Class</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>OBO Class</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOBOClass(OBOClass object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>OBO Object</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>OBO Object</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOBOObject(OBOObject object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>OBO Property</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>OBO Property</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOBOProperty(OBOProperty object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>OBO Restriction</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>OBO Restriction</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOBORestriction(OBORestriction object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>OBO Session</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>OBO Session</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOBOSession(OBOSession object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Object Factory</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Object Factory</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseObjectFactory(ObjectFactory object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Object Field</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Object Field</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseObjectField(ObjectField object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Obsoletable Object</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Obsoletable Object</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseObsoletableObject(ObsoletableObject object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Path Capable</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Path Capable</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePathCapable(PathCapable object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Property Value</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Property Value</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePropertyValue(PropertyValue object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Relationship</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Relationship</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRelationship(Relationship object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Root Algorithm</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Root Algorithm</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRootAlgorithm(RootAlgorithm object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Subset Object</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Subset Object</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSubsetObject(SubsetObject object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Synonym</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Synonym</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSynonym(Synonym object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Synonym Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Synonym Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSynonymType(SynonymType object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Synonymed Object</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Synonymed Object</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSynonymedObject(SynonymedObject object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Term Subset</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Term Subset</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTermSubset(TermSubset object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseType(Type object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Unknown Stanza</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Unknown Stanza</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseUnknownStanza(UnknownStanza object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Value</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Value</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseValue(Value object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Value Database</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Value Database</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseValueDatabase(ValueDatabase object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Value Link</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Value Link</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseValueLink(ValueLink object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Cloneable</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Cloneable</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCloneable(br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Serializable</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Serializable</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSerializable(Serializable object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Comparable</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Comparable</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComparable(br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>String To Nested Value Map</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>String To Nested Value Map</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseStringToNestedValueMap(Map.Entry<String, NestedValue> object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Obsoletable Object To Nested Value Map</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Obsoletable Object To Nested Value Map</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseObsoletableObjectToNestedValueMap(Map.Entry<ObsoletableObject, NestedValue> object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Term Subset To Nested Value Map</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Term Subset To Nested Value Map</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTermSubsetToNestedValueMap(Map.Entry<TermSubset, NestedValue> object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Id Spaces Map</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Id Spaces Map</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIdSpacesMap(Map.Entry<String, String> object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>List Of Properties</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>List Of Properties</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseListOfProperties(ListOfProperties object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Obo Property Value</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Obo Property Value</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOboPropertyValue(OboPropertyValue object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object) {
    return null;
  }

} //obodatamodelSwitch
