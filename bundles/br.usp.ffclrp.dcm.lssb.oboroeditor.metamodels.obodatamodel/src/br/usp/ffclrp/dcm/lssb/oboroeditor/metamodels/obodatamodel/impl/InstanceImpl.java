/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Instance;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OboPropertyValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.InstanceImpl#getOboPropertiesValues <em>Obo Properties Values</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InstanceImpl extends OBOObjectImpl implements Instance {
  /**
   * The cached value of the '{@link #getOboPropertiesValues() <em>Obo Properties Values</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOboPropertiesValues()
   * @generated
   * @ordered
   */
  protected EList<OboPropertyValue> oboPropertiesValues;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected InstanceImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.INSTANCE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<OboPropertyValue> getOboPropertiesValues() {
    if (oboPropertiesValues == null) {
      oboPropertiesValues = new EObjectContainmentEList<OboPropertyValue>(OboPropertyValue.class, this, obodatamodelPackage.INSTANCE__OBO_PROPERTIES_VALUES);
    }
    return oboPropertiesValues;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.INSTANCE__OBO_PROPERTIES_VALUES:
        return ((InternalEList<?>)getOboPropertiesValues()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.INSTANCE__OBO_PROPERTIES_VALUES:
        return getOboPropertiesValues();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.INSTANCE__OBO_PROPERTIES_VALUES:
        getOboPropertiesValues().clear();
        getOboPropertiesValues().addAll((Collection<? extends OboPropertyValue>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.INSTANCE__OBO_PROPERTIES_VALUES:
        getOboPropertiesValues().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.INSTANCE__OBO_PROPERTIES_VALUES:
        return oboPropertiesValues != null && !oboPropertiesValues.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //InstanceImpl
