/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multi ID Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MultiIDObject#getSecondaryIds <em>Secondary Ids</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MultiIDObject#getSecondaryIdExtension <em>Secondary Id Extension</em>}</li>
 * </ul>
 *
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getMultiIDObject()
 * @model abstract="true"
 * @generated
 */
public interface MultiIDObject extends IdentifiedObject {
  /**
   * Returns the value of the '<em><b>Secondary Ids</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Secondary Ids</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Secondary Ids</em>' attribute list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getMultiIDObject_SecondaryIds()
   * @model ordered="false"
   * @generated
   */
  EList<String> getSecondaryIds();

  /**
   * Returns the value of the '<em><b>Secondary Id Extension</b></em>' map.
   * The key is of type {@link java.lang.String},
   * and the value is of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue},
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Secondary Id Extension</em>' map isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Secondary Id Extension</em>' map.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getMultiIDObject_SecondaryIdExtension()
   * @model mapType="br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.StringToNestedValueMap&lt;org.eclipse.emf.ecore.EString, br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue&gt;" ordered="false"
   * @generated
   */
  EMap<String, NestedValue> getSecondaryIdExtension();

} // MultiIDObject
