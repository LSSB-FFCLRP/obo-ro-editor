/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NamespacedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Namespaced Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.NamespacedObjectImpl#getNamespace <em>Namespace</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.NamespacedObjectImpl#getNamespaceExtension <em>Namespace Extension</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class NamespacedObjectImpl extends MinimalEObjectImpl.Container implements NamespacedObject {
  /**
   * The cached value of the '{@link #getNamespace() <em>Namespace</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNamespace()
   * @generated
   * @ordered
   */
  protected Namespace namespace;

  /**
   * The cached value of the '{@link #getNamespaceExtension() <em>Namespace Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNamespaceExtension()
   * @generated
   * @ordered
   */
  protected NestedValue namespaceExtension;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected NamespacedObjectImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.NAMESPACED_OBJECT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Namespace getNamespace() {
    if (namespace != null && namespace.eIsProxy()) {
      InternalEObject oldNamespace = (InternalEObject)namespace;
      namespace = (Namespace)eResolveProxy(oldNamespace);
      if (namespace != oldNamespace) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, obodatamodelPackage.NAMESPACED_OBJECT__NAMESPACE, oldNamespace, namespace));
      }
    }
    return namespace;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Namespace basicGetNamespace() {
    return namespace;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNamespace(Namespace newNamespace) {
    Namespace oldNamespace = namespace;
    namespace = newNamespace;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.NAMESPACED_OBJECT__NAMESPACE, oldNamespace, namespace));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getNamespaceExtension() {
    return namespaceExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetNamespaceExtension(NestedValue newNamespaceExtension, NotificationChain msgs) {
    NestedValue oldNamespaceExtension = namespaceExtension;
    namespaceExtension = newNamespaceExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.NAMESPACED_OBJECT__NAMESPACE_EXTENSION, oldNamespaceExtension, newNamespaceExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNamespaceExtension(NestedValue newNamespaceExtension) {
    if (newNamespaceExtension != namespaceExtension) {
      NotificationChain msgs = null;
      if (namespaceExtension != null)
        msgs = ((InternalEObject)namespaceExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.NAMESPACED_OBJECT__NAMESPACE_EXTENSION, null, msgs);
      if (newNamespaceExtension != null)
        msgs = ((InternalEObject)newNamespaceExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.NAMESPACED_OBJECT__NAMESPACE_EXTENSION, null, msgs);
      msgs = basicSetNamespaceExtension(newNamespaceExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.NAMESPACED_OBJECT__NAMESPACE_EXTENSION, newNamespaceExtension, newNamespaceExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.NAMESPACED_OBJECT__NAMESPACE_EXTENSION:
        return basicSetNamespaceExtension(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.NAMESPACED_OBJECT__NAMESPACE:
        if (resolve) return getNamespace();
        return basicGetNamespace();
      case obodatamodelPackage.NAMESPACED_OBJECT__NAMESPACE_EXTENSION:
        return getNamespaceExtension();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.NAMESPACED_OBJECT__NAMESPACE:
        setNamespace((Namespace)newValue);
        return;
      case obodatamodelPackage.NAMESPACED_OBJECT__NAMESPACE_EXTENSION:
        setNamespaceExtension((NestedValue)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.NAMESPACED_OBJECT__NAMESPACE:
        setNamespace((Namespace)null);
        return;
      case obodatamodelPackage.NAMESPACED_OBJECT__NAMESPACE_EXTENSION:
        setNamespaceExtension((NestedValue)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.NAMESPACED_OBJECT__NAMESPACE:
        return namespace != null;
      case obodatamodelPackage.NAMESPACED_OBJECT__NAMESPACE_EXTENSION:
        return namespaceExtension != null;
    }
    return super.eIsSet(featureID);
  }

} //NamespacedObjectImpl
