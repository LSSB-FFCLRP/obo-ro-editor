/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.AnnotatedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.CommentedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingProperty;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DbxrefedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ListOfProperties;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MultiIDObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SubsetObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Type;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dangling Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getCreatedBy <em>Created By</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getCreatedByExtension <em>Created By Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getModifiedBy <em>Modified By</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getModifiedByExtension <em>Modified By Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getCreationDate <em>Creation Date</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getCreationDateExtension <em>Creation Date Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getModificationDate <em>Modification Date</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getModificationDateExtension <em>Modification Date Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getSecondaryIds <em>Secondary Ids</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getSecondaryIdExtension <em>Secondary Id Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getSynonyms <em>Synonyms</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getDbxrefs <em>Dbxrefs</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getCommentExtension <em>Comment Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#isObsolete <em>Obsolete</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getReplacedBy <em>Replaced By</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getConsiderReplacements <em>Consider Replacements</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getConsiderExtension <em>Consider Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getReplacedByExtension <em>Replaced By Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getObsoleteExtension <em>Obsolete Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getDefinition <em>Definition</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getDefDbxrefs <em>Def Dbxrefs</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getDefinitionExtension <em>Definition Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getSubsets <em>Subsets</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getCategoryExtensions <em>Category Extensions</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#isCyclic <em>Cyclic</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#isSymmetric <em>Symmetric</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#isTransitive <em>Transitive</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#isReflexive <em>Reflexive</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#isAlwaysImpliesInverse <em>Always Implies Inverse</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#isDummy <em>Dummy</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#isMetadataTag <em>Metadata Tag</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getCyclicExtension <em>Cyclic Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getSymmetricExtension <em>Symmetric Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getTransitiveExtension <em>Transitive Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getReflexiveExtension <em>Reflexive Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getAlwaysImpliesInverseExtension <em>Always Implies Inverse Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getDomainExtension <em>Domain Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getRangeExtension <em>Range Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getRange <em>Range</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getDomain <em>Domain</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#isNonInheritable <em>Non Inheritable</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getDisjointOver <em>Disjoint Over</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getTransitiveOver <em>Transitive Over</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl#getHoldsOverChains <em>Holds Over Chains</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DanglingPropertyImpl extends LinkedObjectImpl implements DanglingProperty {
  /**
   * The default value of the '{@link #getCreatedBy() <em>Created By</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCreatedBy()
   * @generated
   * @ordered
   */
  protected static final String CREATED_BY_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getCreatedBy() <em>Created By</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCreatedBy()
   * @generated
   * @ordered
   */
  protected String createdBy = CREATED_BY_EDEFAULT;

  /**
   * The cached value of the '{@link #getCreatedByExtension() <em>Created By Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCreatedByExtension()
   * @generated
   * @ordered
   */
  protected NestedValue createdByExtension;

  /**
   * The default value of the '{@link #getModifiedBy() <em>Modified By</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModifiedBy()
   * @generated
   * @ordered
   */
  protected static final String MODIFIED_BY_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getModifiedBy() <em>Modified By</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModifiedBy()
   * @generated
   * @ordered
   */
  protected String modifiedBy = MODIFIED_BY_EDEFAULT;

  /**
   * The cached value of the '{@link #getModifiedByExtension() <em>Modified By Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModifiedByExtension()
   * @generated
   * @ordered
   */
  protected NestedValue modifiedByExtension;

  /**
   * The default value of the '{@link #getCreationDate() <em>Creation Date</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCreationDate()
   * @generated
   * @ordered
   */
  protected static final Date CREATION_DATE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCreationDate() <em>Creation Date</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCreationDate()
   * @generated
   * @ordered
   */
  protected Date creationDate = CREATION_DATE_EDEFAULT;

  /**
   * The cached value of the '{@link #getCreationDateExtension() <em>Creation Date Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCreationDateExtension()
   * @generated
   * @ordered
   */
  protected NestedValue creationDateExtension;

  /**
   * The default value of the '{@link #getModificationDate() <em>Modification Date</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModificationDate()
   * @generated
   * @ordered
   */
  protected static final Date MODIFICATION_DATE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getModificationDate() <em>Modification Date</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModificationDate()
   * @generated
   * @ordered
   */
  protected Date modificationDate = MODIFICATION_DATE_EDEFAULT;

  /**
   * The cached value of the '{@link #getModificationDateExtension() <em>Modification Date Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModificationDateExtension()
   * @generated
   * @ordered
   */
  protected NestedValue modificationDateExtension;

  /**
   * The cached value of the '{@link #getSecondaryIds() <em>Secondary Ids</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSecondaryIds()
   * @generated
   * @ordered
   */
  protected EList<String> secondaryIds;

  /**
   * The cached value of the '{@link #getSecondaryIdExtension() <em>Secondary Id Extension</em>}' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSecondaryIdExtension()
   * @generated
   * @ordered
   */
  protected EMap<String, NestedValue> secondaryIdExtension;

  /**
   * The cached value of the '{@link #getSynonyms() <em>Synonyms</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSynonyms()
   * @generated
   * @ordered
   */
  protected EList<Synonym> synonyms;

  /**
   * The cached value of the '{@link #getDbxrefs() <em>Dbxrefs</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDbxrefs()
   * @generated
   * @ordered
   */
  protected EList<Dbxref> dbxrefs;

  /**
   * The default value of the '{@link #getComment() <em>Comment</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getComment()
   * @generated
   * @ordered
   */
  protected static final String COMMENT_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getComment() <em>Comment</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getComment()
   * @generated
   * @ordered
   */
  protected String comment = COMMENT_EDEFAULT;

  /**
   * The cached value of the '{@link #getCommentExtension() <em>Comment Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCommentExtension()
   * @generated
   * @ordered
   */
  protected NestedValue commentExtension;

  /**
   * The default value of the '{@link #isObsolete() <em>Obsolete</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isObsolete()
   * @generated
   * @ordered
   */
  protected static final boolean OBSOLETE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isObsolete() <em>Obsolete</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isObsolete()
   * @generated
   * @ordered
   */
  protected boolean obsolete = OBSOLETE_EDEFAULT;

  /**
   * The cached value of the '{@link #getReplacedBy() <em>Replaced By</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReplacedBy()
   * @generated
   * @ordered
   */
  protected EList<ObsoletableObject> replacedBy;

  /**
   * The cached value of the '{@link #getConsiderReplacements() <em>Consider Replacements</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConsiderReplacements()
   * @generated
   * @ordered
   */
  protected EList<ObsoletableObject> considerReplacements;

  /**
   * The cached value of the '{@link #getConsiderExtension() <em>Consider Extension</em>}' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConsiderExtension()
   * @generated
   * @ordered
   */
  protected EMap<ObsoletableObject, NestedValue> considerExtension;

  /**
   * The cached value of the '{@link #getReplacedByExtension() <em>Replaced By Extension</em>}' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReplacedByExtension()
   * @generated
   * @ordered
   */
  protected EMap<ObsoletableObject, NestedValue> replacedByExtension;

  /**
   * The cached value of the '{@link #getObsoleteExtension() <em>Obsolete Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getObsoleteExtension()
   * @generated
   * @ordered
   */
  protected NestedValue obsoleteExtension;

  /**
   * The default value of the '{@link #getDefinition() <em>Definition</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDefinition()
   * @generated
   * @ordered
   */
  protected static final String DEFINITION_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getDefinition() <em>Definition</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDefinition()
   * @generated
   * @ordered
   */
  protected String definition = DEFINITION_EDEFAULT;

  /**
   * The cached value of the '{@link #getDefDbxrefs() <em>Def Dbxrefs</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDefDbxrefs()
   * @generated
   * @ordered
   */
  protected EList<Dbxref> defDbxrefs;

  /**
   * The cached value of the '{@link #getDefinitionExtension() <em>Definition Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDefinitionExtension()
   * @generated
   * @ordered
   */
  protected NestedValue definitionExtension;

  /**
   * The cached value of the '{@link #getSubsets() <em>Subsets</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubsets()
   * @generated
   * @ordered
   */
  protected EList<TermSubset> subsets;

  /**
   * The cached value of the '{@link #getCategoryExtensions() <em>Category Extensions</em>}' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCategoryExtensions()
   * @generated
   * @ordered
   */
  protected EMap<TermSubset, NestedValue> categoryExtensions;

  /**
   * The default value of the '{@link #isCyclic() <em>Cyclic</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isCyclic()
   * @generated
   * @ordered
   */
  protected static final boolean CYCLIC_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isCyclic() <em>Cyclic</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isCyclic()
   * @generated
   * @ordered
   */
  protected boolean cyclic = CYCLIC_EDEFAULT;

  /**
   * The default value of the '{@link #isSymmetric() <em>Symmetric</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSymmetric()
   * @generated
   * @ordered
   */
  protected static final boolean SYMMETRIC_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isSymmetric() <em>Symmetric</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSymmetric()
   * @generated
   * @ordered
   */
  protected boolean symmetric = SYMMETRIC_EDEFAULT;

  /**
   * The default value of the '{@link #isTransitive() <em>Transitive</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isTransitive()
   * @generated
   * @ordered
   */
  protected static final boolean TRANSITIVE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isTransitive() <em>Transitive</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isTransitive()
   * @generated
   * @ordered
   */
  protected boolean transitive = TRANSITIVE_EDEFAULT;

  /**
   * The default value of the '{@link #isReflexive() <em>Reflexive</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isReflexive()
   * @generated
   * @ordered
   */
  protected static final boolean REFLEXIVE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isReflexive() <em>Reflexive</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isReflexive()
   * @generated
   * @ordered
   */
  protected boolean reflexive = REFLEXIVE_EDEFAULT;

  /**
   * The default value of the '{@link #isAlwaysImpliesInverse() <em>Always Implies Inverse</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isAlwaysImpliesInverse()
   * @generated
   * @ordered
   */
  protected static final boolean ALWAYS_IMPLIES_INVERSE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isAlwaysImpliesInverse() <em>Always Implies Inverse</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isAlwaysImpliesInverse()
   * @generated
   * @ordered
   */
  protected boolean alwaysImpliesInverse = ALWAYS_IMPLIES_INVERSE_EDEFAULT;

  /**
   * The default value of the '{@link #isDummy() <em>Dummy</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isDummy()
   * @generated
   * @ordered
   */
  protected static final boolean DUMMY_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isDummy() <em>Dummy</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isDummy()
   * @generated
   * @ordered
   */
  protected boolean dummy = DUMMY_EDEFAULT;

  /**
   * The default value of the '{@link #isMetadataTag() <em>Metadata Tag</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isMetadataTag()
   * @generated
   * @ordered
   */
  protected static final boolean METADATA_TAG_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isMetadataTag() <em>Metadata Tag</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isMetadataTag()
   * @generated
   * @ordered
   */
  protected boolean metadataTag = METADATA_TAG_EDEFAULT;

  /**
   * The cached value of the '{@link #getCyclicExtension() <em>Cyclic Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCyclicExtension()
   * @generated
   * @ordered
   */
  protected NestedValue cyclicExtension;

  /**
   * The cached value of the '{@link #getSymmetricExtension() <em>Symmetric Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSymmetricExtension()
   * @generated
   * @ordered
   */
  protected NestedValue symmetricExtension;

  /**
   * The cached value of the '{@link #getTransitiveExtension() <em>Transitive Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTransitiveExtension()
   * @generated
   * @ordered
   */
  protected NestedValue transitiveExtension;

  /**
   * The cached value of the '{@link #getReflexiveExtension() <em>Reflexive Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReflexiveExtension()
   * @generated
   * @ordered
   */
  protected NestedValue reflexiveExtension;

  /**
   * The cached value of the '{@link #getAlwaysImpliesInverseExtension() <em>Always Implies Inverse Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAlwaysImpliesInverseExtension()
   * @generated
   * @ordered
   */
  protected NestedValue alwaysImpliesInverseExtension;

  /**
   * The cached value of the '{@link #getDomainExtension() <em>Domain Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDomainExtension()
   * @generated
   * @ordered
   */
  protected NestedValue domainExtension;

  /**
   * The cached value of the '{@link #getRangeExtension() <em>Range Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRangeExtension()
   * @generated
   * @ordered
   */
  protected NestedValue rangeExtension;

  /**
   * The cached value of the '{@link #getRange() <em>Range</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRange()
   * @generated
   * @ordered
   */
  protected Type range;

  /**
   * The cached value of the '{@link #getDomain() <em>Domain</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDomain()
   * @generated
   * @ordered
   */
  protected Type domain;

  /**
   * The default value of the '{@link #isNonInheritable() <em>Non Inheritable</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isNonInheritable()
   * @generated
   * @ordered
   */
  protected static final boolean NON_INHERITABLE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isNonInheritable() <em>Non Inheritable</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isNonInheritable()
   * @generated
   * @ordered
   */
  protected boolean nonInheritable = NON_INHERITABLE_EDEFAULT;

  /**
   * The cached value of the '{@link #getDisjointOver() <em>Disjoint Over</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDisjointOver()
   * @generated
   * @ordered
   */
  protected OBOProperty disjointOver;

  /**
   * The cached value of the '{@link #getTransitiveOver() <em>Transitive Over</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTransitiveOver()
   * @generated
   * @ordered
   */
  protected OBOProperty transitiveOver;

  /**
   * The cached value of the '{@link #getHoldsOverChains() <em>Holds Over Chains</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHoldsOverChains()
   * @generated
   * @ordered
   */
  protected EList<ListOfProperties> holdsOverChains;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DanglingPropertyImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.DANGLING_PROPERTY;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCreatedBy() {
    return createdBy;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCreatedBy(String newCreatedBy) {
    String oldCreatedBy = createdBy;
    createdBy = newCreatedBy;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__CREATED_BY, oldCreatedBy, createdBy));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getCreatedByExtension() {
    return createdByExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCreatedByExtension(NestedValue newCreatedByExtension, NotificationChain msgs) {
    NestedValue oldCreatedByExtension = createdByExtension;
    createdByExtension = newCreatedByExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__CREATED_BY_EXTENSION, oldCreatedByExtension, newCreatedByExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCreatedByExtension(NestedValue newCreatedByExtension) {
    if (newCreatedByExtension != createdByExtension) {
      NotificationChain msgs = null;
      if (createdByExtension != null)
        msgs = ((InternalEObject)createdByExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__CREATED_BY_EXTENSION, null, msgs);
      if (newCreatedByExtension != null)
        msgs = ((InternalEObject)newCreatedByExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__CREATED_BY_EXTENSION, null, msgs);
      msgs = basicSetCreatedByExtension(newCreatedByExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__CREATED_BY_EXTENSION, newCreatedByExtension, newCreatedByExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getModifiedBy() {
    return modifiedBy;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setModifiedBy(String newModifiedBy) {
    String oldModifiedBy = modifiedBy;
    modifiedBy = newModifiedBy;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__MODIFIED_BY, oldModifiedBy, modifiedBy));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getModifiedByExtension() {
    return modifiedByExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetModifiedByExtension(NestedValue newModifiedByExtension, NotificationChain msgs) {
    NestedValue oldModifiedByExtension = modifiedByExtension;
    modifiedByExtension = newModifiedByExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__MODIFIED_BY_EXTENSION, oldModifiedByExtension, newModifiedByExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setModifiedByExtension(NestedValue newModifiedByExtension) {
    if (newModifiedByExtension != modifiedByExtension) {
      NotificationChain msgs = null;
      if (modifiedByExtension != null)
        msgs = ((InternalEObject)modifiedByExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__MODIFIED_BY_EXTENSION, null, msgs);
      if (newModifiedByExtension != null)
        msgs = ((InternalEObject)newModifiedByExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__MODIFIED_BY_EXTENSION, null, msgs);
      msgs = basicSetModifiedByExtension(newModifiedByExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__MODIFIED_BY_EXTENSION, newModifiedByExtension, newModifiedByExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Date getCreationDate() {
    return creationDate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCreationDate(Date newCreationDate) {
    Date oldCreationDate = creationDate;
    creationDate = newCreationDate;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__CREATION_DATE, oldCreationDate, creationDate));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getCreationDateExtension() {
    return creationDateExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCreationDateExtension(NestedValue newCreationDateExtension, NotificationChain msgs) {
    NestedValue oldCreationDateExtension = creationDateExtension;
    creationDateExtension = newCreationDateExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__CREATION_DATE_EXTENSION, oldCreationDateExtension, newCreationDateExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCreationDateExtension(NestedValue newCreationDateExtension) {
    if (newCreationDateExtension != creationDateExtension) {
      NotificationChain msgs = null;
      if (creationDateExtension != null)
        msgs = ((InternalEObject)creationDateExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__CREATION_DATE_EXTENSION, null, msgs);
      if (newCreationDateExtension != null)
        msgs = ((InternalEObject)newCreationDateExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__CREATION_DATE_EXTENSION, null, msgs);
      msgs = basicSetCreationDateExtension(newCreationDateExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__CREATION_DATE_EXTENSION, newCreationDateExtension, newCreationDateExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Date getModificationDate() {
    return modificationDate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setModificationDate(Date newModificationDate) {
    Date oldModificationDate = modificationDate;
    modificationDate = newModificationDate;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__MODIFICATION_DATE, oldModificationDate, modificationDate));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getModificationDateExtension() {
    return modificationDateExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetModificationDateExtension(NestedValue newModificationDateExtension, NotificationChain msgs) {
    NestedValue oldModificationDateExtension = modificationDateExtension;
    modificationDateExtension = newModificationDateExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__MODIFICATION_DATE_EXTENSION, oldModificationDateExtension, newModificationDateExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setModificationDateExtension(NestedValue newModificationDateExtension) {
    if (newModificationDateExtension != modificationDateExtension) {
      NotificationChain msgs = null;
      if (modificationDateExtension != null)
        msgs = ((InternalEObject)modificationDateExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__MODIFICATION_DATE_EXTENSION, null, msgs);
      if (newModificationDateExtension != null)
        msgs = ((InternalEObject)newModificationDateExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__MODIFICATION_DATE_EXTENSION, null, msgs);
      msgs = basicSetModificationDateExtension(newModificationDateExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__MODIFICATION_DATE_EXTENSION, newModificationDateExtension, newModificationDateExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getSecondaryIds() {
    if (secondaryIds == null) {
      secondaryIds = new EDataTypeUniqueEList<String>(String.class, this, obodatamodelPackage.DANGLING_PROPERTY__SECONDARY_IDS);
    }
    return secondaryIds;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EMap<String, NestedValue> getSecondaryIdExtension() {
    if (secondaryIdExtension == null) {
      secondaryIdExtension = new EcoreEMap<String,NestedValue>(obodatamodelPackage.Literals.STRING_TO_NESTED_VALUE_MAP, StringToNestedValueMapImpl.class, this, obodatamodelPackage.DANGLING_PROPERTY__SECONDARY_ID_EXTENSION);
    }
    return secondaryIdExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Synonym> getSynonyms() {
    if (synonyms == null) {
      synonyms = new EObjectContainmentEList<Synonym>(Synonym.class, this, obodatamodelPackage.DANGLING_PROPERTY__SYNONYMS);
    }
    return synonyms;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Dbxref> getDbxrefs() {
    if (dbxrefs == null) {
      dbxrefs = new EObjectResolvingEList<Dbxref>(Dbxref.class, this, obodatamodelPackage.DANGLING_PROPERTY__DBXREFS);
    }
    return dbxrefs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getComment() {
    return comment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setComment(String newComment) {
    String oldComment = comment;
    comment = newComment;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__COMMENT, oldComment, comment));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getCommentExtension() {
    return commentExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCommentExtension(NestedValue newCommentExtension, NotificationChain msgs) {
    NestedValue oldCommentExtension = commentExtension;
    commentExtension = newCommentExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__COMMENT_EXTENSION, oldCommentExtension, newCommentExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCommentExtension(NestedValue newCommentExtension) {
    if (newCommentExtension != commentExtension) {
      NotificationChain msgs = null;
      if (commentExtension != null)
        msgs = ((InternalEObject)commentExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__COMMENT_EXTENSION, null, msgs);
      if (newCommentExtension != null)
        msgs = ((InternalEObject)newCommentExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__COMMENT_EXTENSION, null, msgs);
      msgs = basicSetCommentExtension(newCommentExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__COMMENT_EXTENSION, newCommentExtension, newCommentExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isObsolete() {
    return obsolete;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setObsolete(boolean newObsolete) {
    boolean oldObsolete = obsolete;
    obsolete = newObsolete;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__OBSOLETE, oldObsolete, obsolete));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ObsoletableObject> getReplacedBy() {
    if (replacedBy == null) {
      replacedBy = new EObjectResolvingEList<ObsoletableObject>(ObsoletableObject.class, this, obodatamodelPackage.DANGLING_PROPERTY__REPLACED_BY);
    }
    return replacedBy;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ObsoletableObject> getConsiderReplacements() {
    if (considerReplacements == null) {
      considerReplacements = new EObjectResolvingEList<ObsoletableObject>(ObsoletableObject.class, this, obodatamodelPackage.DANGLING_PROPERTY__CONSIDER_REPLACEMENTS);
    }
    return considerReplacements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EMap<ObsoletableObject, NestedValue> getConsiderExtension() {
    if (considerExtension == null) {
      considerExtension = new EcoreEMap<ObsoletableObject,NestedValue>(obodatamodelPackage.Literals.OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP, ObsoletableObjectToNestedValueMapImpl.class, this, obodatamodelPackage.DANGLING_PROPERTY__CONSIDER_EXTENSION);
    }
    return considerExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EMap<ObsoletableObject, NestedValue> getReplacedByExtension() {
    if (replacedByExtension == null) {
      replacedByExtension = new EcoreEMap<ObsoletableObject,NestedValue>(obodatamodelPackage.Literals.OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP, ObsoletableObjectToNestedValueMapImpl.class, this, obodatamodelPackage.DANGLING_PROPERTY__REPLACED_BY_EXTENSION);
    }
    return replacedByExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getObsoleteExtension() {
    return obsoleteExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetObsoleteExtension(NestedValue newObsoleteExtension, NotificationChain msgs) {
    NestedValue oldObsoleteExtension = obsoleteExtension;
    obsoleteExtension = newObsoleteExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__OBSOLETE_EXTENSION, oldObsoleteExtension, newObsoleteExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setObsoleteExtension(NestedValue newObsoleteExtension) {
    if (newObsoleteExtension != obsoleteExtension) {
      NotificationChain msgs = null;
      if (obsoleteExtension != null)
        msgs = ((InternalEObject)obsoleteExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__OBSOLETE_EXTENSION, null, msgs);
      if (newObsoleteExtension != null)
        msgs = ((InternalEObject)newObsoleteExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__OBSOLETE_EXTENSION, null, msgs);
      msgs = basicSetObsoleteExtension(newObsoleteExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__OBSOLETE_EXTENSION, newObsoleteExtension, newObsoleteExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDefinition() {
    return definition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDefinition(String newDefinition) {
    String oldDefinition = definition;
    definition = newDefinition;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__DEFINITION, oldDefinition, definition));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Dbxref> getDefDbxrefs() {
    if (defDbxrefs == null) {
      defDbxrefs = new EObjectResolvingEList<Dbxref>(Dbxref.class, this, obodatamodelPackage.DANGLING_PROPERTY__DEF_DBXREFS);
    }
    return defDbxrefs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getDefinitionExtension() {
    return definitionExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDefinitionExtension(NestedValue newDefinitionExtension, NotificationChain msgs) {
    NestedValue oldDefinitionExtension = definitionExtension;
    definitionExtension = newDefinitionExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__DEFINITION_EXTENSION, oldDefinitionExtension, newDefinitionExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDefinitionExtension(NestedValue newDefinitionExtension) {
    if (newDefinitionExtension != definitionExtension) {
      NotificationChain msgs = null;
      if (definitionExtension != null)
        msgs = ((InternalEObject)definitionExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__DEFINITION_EXTENSION, null, msgs);
      if (newDefinitionExtension != null)
        msgs = ((InternalEObject)newDefinitionExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__DEFINITION_EXTENSION, null, msgs);
      msgs = basicSetDefinitionExtension(newDefinitionExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__DEFINITION_EXTENSION, newDefinitionExtension, newDefinitionExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TermSubset> getSubsets() {
    if (subsets == null) {
      subsets = new EObjectResolvingEList<TermSubset>(TermSubset.class, this, obodatamodelPackage.DANGLING_PROPERTY__SUBSETS);
    }
    return subsets;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EMap<TermSubset, NestedValue> getCategoryExtensions() {
    if (categoryExtensions == null) {
      categoryExtensions = new EcoreEMap<TermSubset,NestedValue>(obodatamodelPackage.Literals.TERM_SUBSET_TO_NESTED_VALUE_MAP, TermSubsetToNestedValueMapImpl.class, this, obodatamodelPackage.DANGLING_PROPERTY__CATEGORY_EXTENSIONS);
    }
    return categoryExtensions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isCyclic() {
    return cyclic;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCyclic(boolean newCyclic) {
    boolean oldCyclic = cyclic;
    cyclic = newCyclic;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__CYCLIC, oldCyclic, cyclic));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSymmetric() {
    return symmetric;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSymmetric(boolean newSymmetric) {
    boolean oldSymmetric = symmetric;
    symmetric = newSymmetric;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__SYMMETRIC, oldSymmetric, symmetric));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isTransitive() {
    return transitive;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTransitive(boolean newTransitive) {
    boolean oldTransitive = transitive;
    transitive = newTransitive;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE, oldTransitive, transitive));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isReflexive() {
    return reflexive;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReflexive(boolean newReflexive) {
    boolean oldReflexive = reflexive;
    reflexive = newReflexive;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__REFLEXIVE, oldReflexive, reflexive));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isAlwaysImpliesInverse() {
    return alwaysImpliesInverse;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAlwaysImpliesInverse(boolean newAlwaysImpliesInverse) {
    boolean oldAlwaysImpliesInverse = alwaysImpliesInverse;
    alwaysImpliesInverse = newAlwaysImpliesInverse;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__ALWAYS_IMPLIES_INVERSE, oldAlwaysImpliesInverse, alwaysImpliesInverse));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isDummy() {
    return dummy;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDummy(boolean newDummy) {
    boolean oldDummy = dummy;
    dummy = newDummy;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__DUMMY, oldDummy, dummy));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isMetadataTag() {
    return metadataTag;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMetadataTag(boolean newMetadataTag) {
    boolean oldMetadataTag = metadataTag;
    metadataTag = newMetadataTag;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__METADATA_TAG, oldMetadataTag, metadataTag));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getCyclicExtension() {
    return cyclicExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCyclicExtension(NestedValue newCyclicExtension, NotificationChain msgs) {
    NestedValue oldCyclicExtension = cyclicExtension;
    cyclicExtension = newCyclicExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__CYCLIC_EXTENSION, oldCyclicExtension, newCyclicExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCyclicExtension(NestedValue newCyclicExtension) {
    if (newCyclicExtension != cyclicExtension) {
      NotificationChain msgs = null;
      if (cyclicExtension != null)
        msgs = ((InternalEObject)cyclicExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__CYCLIC_EXTENSION, null, msgs);
      if (newCyclicExtension != null)
        msgs = ((InternalEObject)newCyclicExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__CYCLIC_EXTENSION, null, msgs);
      msgs = basicSetCyclicExtension(newCyclicExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__CYCLIC_EXTENSION, newCyclicExtension, newCyclicExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getSymmetricExtension() {
    return symmetricExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSymmetricExtension(NestedValue newSymmetricExtension, NotificationChain msgs) {
    NestedValue oldSymmetricExtension = symmetricExtension;
    symmetricExtension = newSymmetricExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__SYMMETRIC_EXTENSION, oldSymmetricExtension, newSymmetricExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSymmetricExtension(NestedValue newSymmetricExtension) {
    if (newSymmetricExtension != symmetricExtension) {
      NotificationChain msgs = null;
      if (symmetricExtension != null)
        msgs = ((InternalEObject)symmetricExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__SYMMETRIC_EXTENSION, null, msgs);
      if (newSymmetricExtension != null)
        msgs = ((InternalEObject)newSymmetricExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__SYMMETRIC_EXTENSION, null, msgs);
      msgs = basicSetSymmetricExtension(newSymmetricExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__SYMMETRIC_EXTENSION, newSymmetricExtension, newSymmetricExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getTransitiveExtension() {
    return transitiveExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTransitiveExtension(NestedValue newTransitiveExtension, NotificationChain msgs) {
    NestedValue oldTransitiveExtension = transitiveExtension;
    transitiveExtension = newTransitiveExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE_EXTENSION, oldTransitiveExtension, newTransitiveExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTransitiveExtension(NestedValue newTransitiveExtension) {
    if (newTransitiveExtension != transitiveExtension) {
      NotificationChain msgs = null;
      if (transitiveExtension != null)
        msgs = ((InternalEObject)transitiveExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE_EXTENSION, null, msgs);
      if (newTransitiveExtension != null)
        msgs = ((InternalEObject)newTransitiveExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE_EXTENSION, null, msgs);
      msgs = basicSetTransitiveExtension(newTransitiveExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE_EXTENSION, newTransitiveExtension, newTransitiveExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getReflexiveExtension() {
    return reflexiveExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetReflexiveExtension(NestedValue newReflexiveExtension, NotificationChain msgs) {
    NestedValue oldReflexiveExtension = reflexiveExtension;
    reflexiveExtension = newReflexiveExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__REFLEXIVE_EXTENSION, oldReflexiveExtension, newReflexiveExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReflexiveExtension(NestedValue newReflexiveExtension) {
    if (newReflexiveExtension != reflexiveExtension) {
      NotificationChain msgs = null;
      if (reflexiveExtension != null)
        msgs = ((InternalEObject)reflexiveExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__REFLEXIVE_EXTENSION, null, msgs);
      if (newReflexiveExtension != null)
        msgs = ((InternalEObject)newReflexiveExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__REFLEXIVE_EXTENSION, null, msgs);
      msgs = basicSetReflexiveExtension(newReflexiveExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__REFLEXIVE_EXTENSION, newReflexiveExtension, newReflexiveExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getAlwaysImpliesInverseExtension() {
    return alwaysImpliesInverseExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAlwaysImpliesInverseExtension(NestedValue newAlwaysImpliesInverseExtension, NotificationChain msgs) {
    NestedValue oldAlwaysImpliesInverseExtension = alwaysImpliesInverseExtension;
    alwaysImpliesInverseExtension = newAlwaysImpliesInverseExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION, oldAlwaysImpliesInverseExtension, newAlwaysImpliesInverseExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAlwaysImpliesInverseExtension(NestedValue newAlwaysImpliesInverseExtension) {
    if (newAlwaysImpliesInverseExtension != alwaysImpliesInverseExtension) {
      NotificationChain msgs = null;
      if (alwaysImpliesInverseExtension != null)
        msgs = ((InternalEObject)alwaysImpliesInverseExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION, null, msgs);
      if (newAlwaysImpliesInverseExtension != null)
        msgs = ((InternalEObject)newAlwaysImpliesInverseExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION, null, msgs);
      msgs = basicSetAlwaysImpliesInverseExtension(newAlwaysImpliesInverseExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION, newAlwaysImpliesInverseExtension, newAlwaysImpliesInverseExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getDomainExtension() {
    return domainExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDomainExtension(NestedValue newDomainExtension, NotificationChain msgs) {
    NestedValue oldDomainExtension = domainExtension;
    domainExtension = newDomainExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__DOMAIN_EXTENSION, oldDomainExtension, newDomainExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDomainExtension(NestedValue newDomainExtension) {
    if (newDomainExtension != domainExtension) {
      NotificationChain msgs = null;
      if (domainExtension != null)
        msgs = ((InternalEObject)domainExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__DOMAIN_EXTENSION, null, msgs);
      if (newDomainExtension != null)
        msgs = ((InternalEObject)newDomainExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__DOMAIN_EXTENSION, null, msgs);
      msgs = basicSetDomainExtension(newDomainExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__DOMAIN_EXTENSION, newDomainExtension, newDomainExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getRangeExtension() {
    return rangeExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRangeExtension(NestedValue newRangeExtension, NotificationChain msgs) {
    NestedValue oldRangeExtension = rangeExtension;
    rangeExtension = newRangeExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__RANGE_EXTENSION, oldRangeExtension, newRangeExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRangeExtension(NestedValue newRangeExtension) {
    if (newRangeExtension != rangeExtension) {
      NotificationChain msgs = null;
      if (rangeExtension != null)
        msgs = ((InternalEObject)rangeExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__RANGE_EXTENSION, null, msgs);
      if (newRangeExtension != null)
        msgs = ((InternalEObject)newRangeExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.DANGLING_PROPERTY__RANGE_EXTENSION, null, msgs);
      msgs = basicSetRangeExtension(newRangeExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__RANGE_EXTENSION, newRangeExtension, newRangeExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type getRange() {
    if (range != null && range.eIsProxy()) {
      InternalEObject oldRange = (InternalEObject)range;
      range = (Type)eResolveProxy(oldRange);
      if (range != oldRange) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, obodatamodelPackage.DANGLING_PROPERTY__RANGE, oldRange, range));
      }
    }
    return range;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type basicGetRange() {
    return range;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRange(Type newRange) {
    Type oldRange = range;
    range = newRange;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__RANGE, oldRange, range));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type getDomain() {
    if (domain != null && domain.eIsProxy()) {
      InternalEObject oldDomain = (InternalEObject)domain;
      domain = (Type)eResolveProxy(oldDomain);
      if (domain != oldDomain) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, obodatamodelPackage.DANGLING_PROPERTY__DOMAIN, oldDomain, domain));
      }
    }
    return domain;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type basicGetDomain() {
    return domain;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDomain(Type newDomain) {
    Type oldDomain = domain;
    domain = newDomain;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__DOMAIN, oldDomain, domain));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isNonInheritable() {
    return nonInheritable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNonInheritable(boolean newNonInheritable) {
    boolean oldNonInheritable = nonInheritable;
    nonInheritable = newNonInheritable;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__NON_INHERITABLE, oldNonInheritable, nonInheritable));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBOProperty getDisjointOver() {
    if (disjointOver != null && disjointOver.eIsProxy()) {
      InternalEObject oldDisjointOver = (InternalEObject)disjointOver;
      disjointOver = (OBOProperty)eResolveProxy(oldDisjointOver);
      if (disjointOver != oldDisjointOver) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, obodatamodelPackage.DANGLING_PROPERTY__DISJOINT_OVER, oldDisjointOver, disjointOver));
      }
    }
    return disjointOver;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBOProperty basicGetDisjointOver() {
    return disjointOver;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDisjointOver(OBOProperty newDisjointOver) {
    OBOProperty oldDisjointOver = disjointOver;
    disjointOver = newDisjointOver;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__DISJOINT_OVER, oldDisjointOver, disjointOver));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBOProperty getTransitiveOver() {
    if (transitiveOver != null && transitiveOver.eIsProxy()) {
      InternalEObject oldTransitiveOver = (InternalEObject)transitiveOver;
      transitiveOver = (OBOProperty)eResolveProxy(oldTransitiveOver);
      if (transitiveOver != oldTransitiveOver) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE_OVER, oldTransitiveOver, transitiveOver));
      }
    }
    return transitiveOver;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBOProperty basicGetTransitiveOver() {
    return transitiveOver;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTransitiveOver(OBOProperty newTransitiveOver) {
    OBOProperty oldTransitiveOver = transitiveOver;
    transitiveOver = newTransitiveOver;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE_OVER, oldTransitiveOver, transitiveOver));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ListOfProperties> getHoldsOverChains() {
    if (holdsOverChains == null) {
      holdsOverChains = new EObjectContainmentEList<ListOfProperties>(ListOfProperties.class, this, obodatamodelPackage.DANGLING_PROPERTY__HOLDS_OVER_CHAINS);
    }
    return holdsOverChains;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.DANGLING_PROPERTY__CREATED_BY_EXTENSION:
        return basicSetCreatedByExtension(null, msgs);
      case obodatamodelPackage.DANGLING_PROPERTY__MODIFIED_BY_EXTENSION:
        return basicSetModifiedByExtension(null, msgs);
      case obodatamodelPackage.DANGLING_PROPERTY__CREATION_DATE_EXTENSION:
        return basicSetCreationDateExtension(null, msgs);
      case obodatamodelPackage.DANGLING_PROPERTY__MODIFICATION_DATE_EXTENSION:
        return basicSetModificationDateExtension(null, msgs);
      case obodatamodelPackage.DANGLING_PROPERTY__SECONDARY_ID_EXTENSION:
        return ((InternalEList<?>)getSecondaryIdExtension()).basicRemove(otherEnd, msgs);
      case obodatamodelPackage.DANGLING_PROPERTY__SYNONYMS:
        return ((InternalEList<?>)getSynonyms()).basicRemove(otherEnd, msgs);
      case obodatamodelPackage.DANGLING_PROPERTY__COMMENT_EXTENSION:
        return basicSetCommentExtension(null, msgs);
      case obodatamodelPackage.DANGLING_PROPERTY__CONSIDER_EXTENSION:
        return ((InternalEList<?>)getConsiderExtension()).basicRemove(otherEnd, msgs);
      case obodatamodelPackage.DANGLING_PROPERTY__REPLACED_BY_EXTENSION:
        return ((InternalEList<?>)getReplacedByExtension()).basicRemove(otherEnd, msgs);
      case obodatamodelPackage.DANGLING_PROPERTY__OBSOLETE_EXTENSION:
        return basicSetObsoleteExtension(null, msgs);
      case obodatamodelPackage.DANGLING_PROPERTY__DEFINITION_EXTENSION:
        return basicSetDefinitionExtension(null, msgs);
      case obodatamodelPackage.DANGLING_PROPERTY__CATEGORY_EXTENSIONS:
        return ((InternalEList<?>)getCategoryExtensions()).basicRemove(otherEnd, msgs);
      case obodatamodelPackage.DANGLING_PROPERTY__CYCLIC_EXTENSION:
        return basicSetCyclicExtension(null, msgs);
      case obodatamodelPackage.DANGLING_PROPERTY__SYMMETRIC_EXTENSION:
        return basicSetSymmetricExtension(null, msgs);
      case obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE_EXTENSION:
        return basicSetTransitiveExtension(null, msgs);
      case obodatamodelPackage.DANGLING_PROPERTY__REFLEXIVE_EXTENSION:
        return basicSetReflexiveExtension(null, msgs);
      case obodatamodelPackage.DANGLING_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION:
        return basicSetAlwaysImpliesInverseExtension(null, msgs);
      case obodatamodelPackage.DANGLING_PROPERTY__DOMAIN_EXTENSION:
        return basicSetDomainExtension(null, msgs);
      case obodatamodelPackage.DANGLING_PROPERTY__RANGE_EXTENSION:
        return basicSetRangeExtension(null, msgs);
      case obodatamodelPackage.DANGLING_PROPERTY__HOLDS_OVER_CHAINS:
        return ((InternalEList<?>)getHoldsOverChains()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.DANGLING_PROPERTY__CREATED_BY:
        return getCreatedBy();
      case obodatamodelPackage.DANGLING_PROPERTY__CREATED_BY_EXTENSION:
        return getCreatedByExtension();
      case obodatamodelPackage.DANGLING_PROPERTY__MODIFIED_BY:
        return getModifiedBy();
      case obodatamodelPackage.DANGLING_PROPERTY__MODIFIED_BY_EXTENSION:
        return getModifiedByExtension();
      case obodatamodelPackage.DANGLING_PROPERTY__CREATION_DATE:
        return getCreationDate();
      case obodatamodelPackage.DANGLING_PROPERTY__CREATION_DATE_EXTENSION:
        return getCreationDateExtension();
      case obodatamodelPackage.DANGLING_PROPERTY__MODIFICATION_DATE:
        return getModificationDate();
      case obodatamodelPackage.DANGLING_PROPERTY__MODIFICATION_DATE_EXTENSION:
        return getModificationDateExtension();
      case obodatamodelPackage.DANGLING_PROPERTY__SECONDARY_IDS:
        return getSecondaryIds();
      case obodatamodelPackage.DANGLING_PROPERTY__SECONDARY_ID_EXTENSION:
        if (coreType) return getSecondaryIdExtension();
        else return getSecondaryIdExtension().map();
      case obodatamodelPackage.DANGLING_PROPERTY__SYNONYMS:
        return getSynonyms();
      case obodatamodelPackage.DANGLING_PROPERTY__DBXREFS:
        return getDbxrefs();
      case obodatamodelPackage.DANGLING_PROPERTY__COMMENT:
        return getComment();
      case obodatamodelPackage.DANGLING_PROPERTY__COMMENT_EXTENSION:
        return getCommentExtension();
      case obodatamodelPackage.DANGLING_PROPERTY__OBSOLETE:
        return isObsolete();
      case obodatamodelPackage.DANGLING_PROPERTY__REPLACED_BY:
        return getReplacedBy();
      case obodatamodelPackage.DANGLING_PROPERTY__CONSIDER_REPLACEMENTS:
        return getConsiderReplacements();
      case obodatamodelPackage.DANGLING_PROPERTY__CONSIDER_EXTENSION:
        if (coreType) return getConsiderExtension();
        else return getConsiderExtension().map();
      case obodatamodelPackage.DANGLING_PROPERTY__REPLACED_BY_EXTENSION:
        if (coreType) return getReplacedByExtension();
        else return getReplacedByExtension().map();
      case obodatamodelPackage.DANGLING_PROPERTY__OBSOLETE_EXTENSION:
        return getObsoleteExtension();
      case obodatamodelPackage.DANGLING_PROPERTY__DEFINITION:
        return getDefinition();
      case obodatamodelPackage.DANGLING_PROPERTY__DEF_DBXREFS:
        return getDefDbxrefs();
      case obodatamodelPackage.DANGLING_PROPERTY__DEFINITION_EXTENSION:
        return getDefinitionExtension();
      case obodatamodelPackage.DANGLING_PROPERTY__SUBSETS:
        return getSubsets();
      case obodatamodelPackage.DANGLING_PROPERTY__CATEGORY_EXTENSIONS:
        if (coreType) return getCategoryExtensions();
        else return getCategoryExtensions().map();
      case obodatamodelPackage.DANGLING_PROPERTY__CYCLIC:
        return isCyclic();
      case obodatamodelPackage.DANGLING_PROPERTY__SYMMETRIC:
        return isSymmetric();
      case obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE:
        return isTransitive();
      case obodatamodelPackage.DANGLING_PROPERTY__REFLEXIVE:
        return isReflexive();
      case obodatamodelPackage.DANGLING_PROPERTY__ALWAYS_IMPLIES_INVERSE:
        return isAlwaysImpliesInverse();
      case obodatamodelPackage.DANGLING_PROPERTY__DUMMY:
        return isDummy();
      case obodatamodelPackage.DANGLING_PROPERTY__METADATA_TAG:
        return isMetadataTag();
      case obodatamodelPackage.DANGLING_PROPERTY__CYCLIC_EXTENSION:
        return getCyclicExtension();
      case obodatamodelPackage.DANGLING_PROPERTY__SYMMETRIC_EXTENSION:
        return getSymmetricExtension();
      case obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE_EXTENSION:
        return getTransitiveExtension();
      case obodatamodelPackage.DANGLING_PROPERTY__REFLEXIVE_EXTENSION:
        return getReflexiveExtension();
      case obodatamodelPackage.DANGLING_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION:
        return getAlwaysImpliesInverseExtension();
      case obodatamodelPackage.DANGLING_PROPERTY__DOMAIN_EXTENSION:
        return getDomainExtension();
      case obodatamodelPackage.DANGLING_PROPERTY__RANGE_EXTENSION:
        return getRangeExtension();
      case obodatamodelPackage.DANGLING_PROPERTY__RANGE:
        if (resolve) return getRange();
        return basicGetRange();
      case obodatamodelPackage.DANGLING_PROPERTY__DOMAIN:
        if (resolve) return getDomain();
        return basicGetDomain();
      case obodatamodelPackage.DANGLING_PROPERTY__NON_INHERITABLE:
        return isNonInheritable();
      case obodatamodelPackage.DANGLING_PROPERTY__DISJOINT_OVER:
        if (resolve) return getDisjointOver();
        return basicGetDisjointOver();
      case obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE_OVER:
        if (resolve) return getTransitiveOver();
        return basicGetTransitiveOver();
      case obodatamodelPackage.DANGLING_PROPERTY__HOLDS_OVER_CHAINS:
        return getHoldsOverChains();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.DANGLING_PROPERTY__CREATED_BY:
        setCreatedBy((String)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__CREATED_BY_EXTENSION:
        setCreatedByExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__MODIFIED_BY:
        setModifiedBy((String)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__MODIFIED_BY_EXTENSION:
        setModifiedByExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__CREATION_DATE:
        setCreationDate((Date)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__CREATION_DATE_EXTENSION:
        setCreationDateExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__MODIFICATION_DATE:
        setModificationDate((Date)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__MODIFICATION_DATE_EXTENSION:
        setModificationDateExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__SECONDARY_IDS:
        getSecondaryIds().clear();
        getSecondaryIds().addAll((Collection<? extends String>)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__SECONDARY_ID_EXTENSION:
        ((EStructuralFeature.Setting)getSecondaryIdExtension()).set(newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__SYNONYMS:
        getSynonyms().clear();
        getSynonyms().addAll((Collection<? extends Synonym>)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__DBXREFS:
        getDbxrefs().clear();
        getDbxrefs().addAll((Collection<? extends Dbxref>)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__COMMENT:
        setComment((String)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__COMMENT_EXTENSION:
        setCommentExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__OBSOLETE:
        setObsolete((Boolean)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__REPLACED_BY:
        getReplacedBy().clear();
        getReplacedBy().addAll((Collection<? extends ObsoletableObject>)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__CONSIDER_REPLACEMENTS:
        getConsiderReplacements().clear();
        getConsiderReplacements().addAll((Collection<? extends ObsoletableObject>)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__CONSIDER_EXTENSION:
        ((EStructuralFeature.Setting)getConsiderExtension()).set(newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__REPLACED_BY_EXTENSION:
        ((EStructuralFeature.Setting)getReplacedByExtension()).set(newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__OBSOLETE_EXTENSION:
        setObsoleteExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__DEFINITION:
        setDefinition((String)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__DEF_DBXREFS:
        getDefDbxrefs().clear();
        getDefDbxrefs().addAll((Collection<? extends Dbxref>)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__DEFINITION_EXTENSION:
        setDefinitionExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__SUBSETS:
        getSubsets().clear();
        getSubsets().addAll((Collection<? extends TermSubset>)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__CATEGORY_EXTENSIONS:
        ((EStructuralFeature.Setting)getCategoryExtensions()).set(newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__CYCLIC:
        setCyclic((Boolean)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__SYMMETRIC:
        setSymmetric((Boolean)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE:
        setTransitive((Boolean)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__REFLEXIVE:
        setReflexive((Boolean)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__ALWAYS_IMPLIES_INVERSE:
        setAlwaysImpliesInverse((Boolean)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__DUMMY:
        setDummy((Boolean)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__METADATA_TAG:
        setMetadataTag((Boolean)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__CYCLIC_EXTENSION:
        setCyclicExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__SYMMETRIC_EXTENSION:
        setSymmetricExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE_EXTENSION:
        setTransitiveExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__REFLEXIVE_EXTENSION:
        setReflexiveExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION:
        setAlwaysImpliesInverseExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__DOMAIN_EXTENSION:
        setDomainExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__RANGE_EXTENSION:
        setRangeExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__RANGE:
        setRange((Type)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__DOMAIN:
        setDomain((Type)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__NON_INHERITABLE:
        setNonInheritable((Boolean)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__DISJOINT_OVER:
        setDisjointOver((OBOProperty)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE_OVER:
        setTransitiveOver((OBOProperty)newValue);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__HOLDS_OVER_CHAINS:
        getHoldsOverChains().clear();
        getHoldsOverChains().addAll((Collection<? extends ListOfProperties>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.DANGLING_PROPERTY__CREATED_BY:
        setCreatedBy(CREATED_BY_EDEFAULT);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__CREATED_BY_EXTENSION:
        setCreatedByExtension((NestedValue)null);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__MODIFIED_BY:
        setModifiedBy(MODIFIED_BY_EDEFAULT);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__MODIFIED_BY_EXTENSION:
        setModifiedByExtension((NestedValue)null);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__CREATION_DATE:
        setCreationDate(CREATION_DATE_EDEFAULT);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__CREATION_DATE_EXTENSION:
        setCreationDateExtension((NestedValue)null);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__MODIFICATION_DATE:
        setModificationDate(MODIFICATION_DATE_EDEFAULT);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__MODIFICATION_DATE_EXTENSION:
        setModificationDateExtension((NestedValue)null);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__SECONDARY_IDS:
        getSecondaryIds().clear();
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__SECONDARY_ID_EXTENSION:
        getSecondaryIdExtension().clear();
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__SYNONYMS:
        getSynonyms().clear();
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__DBXREFS:
        getDbxrefs().clear();
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__COMMENT:
        setComment(COMMENT_EDEFAULT);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__COMMENT_EXTENSION:
        setCommentExtension((NestedValue)null);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__OBSOLETE:
        setObsolete(OBSOLETE_EDEFAULT);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__REPLACED_BY:
        getReplacedBy().clear();
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__CONSIDER_REPLACEMENTS:
        getConsiderReplacements().clear();
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__CONSIDER_EXTENSION:
        getConsiderExtension().clear();
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__REPLACED_BY_EXTENSION:
        getReplacedByExtension().clear();
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__OBSOLETE_EXTENSION:
        setObsoleteExtension((NestedValue)null);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__DEFINITION:
        setDefinition(DEFINITION_EDEFAULT);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__DEF_DBXREFS:
        getDefDbxrefs().clear();
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__DEFINITION_EXTENSION:
        setDefinitionExtension((NestedValue)null);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__SUBSETS:
        getSubsets().clear();
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__CATEGORY_EXTENSIONS:
        getCategoryExtensions().clear();
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__CYCLIC:
        setCyclic(CYCLIC_EDEFAULT);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__SYMMETRIC:
        setSymmetric(SYMMETRIC_EDEFAULT);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE:
        setTransitive(TRANSITIVE_EDEFAULT);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__REFLEXIVE:
        setReflexive(REFLEXIVE_EDEFAULT);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__ALWAYS_IMPLIES_INVERSE:
        setAlwaysImpliesInverse(ALWAYS_IMPLIES_INVERSE_EDEFAULT);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__DUMMY:
        setDummy(DUMMY_EDEFAULT);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__METADATA_TAG:
        setMetadataTag(METADATA_TAG_EDEFAULT);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__CYCLIC_EXTENSION:
        setCyclicExtension((NestedValue)null);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__SYMMETRIC_EXTENSION:
        setSymmetricExtension((NestedValue)null);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE_EXTENSION:
        setTransitiveExtension((NestedValue)null);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__REFLEXIVE_EXTENSION:
        setReflexiveExtension((NestedValue)null);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION:
        setAlwaysImpliesInverseExtension((NestedValue)null);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__DOMAIN_EXTENSION:
        setDomainExtension((NestedValue)null);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__RANGE_EXTENSION:
        setRangeExtension((NestedValue)null);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__RANGE:
        setRange((Type)null);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__DOMAIN:
        setDomain((Type)null);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__NON_INHERITABLE:
        setNonInheritable(NON_INHERITABLE_EDEFAULT);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__DISJOINT_OVER:
        setDisjointOver((OBOProperty)null);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE_OVER:
        setTransitiveOver((OBOProperty)null);
        return;
      case obodatamodelPackage.DANGLING_PROPERTY__HOLDS_OVER_CHAINS:
        getHoldsOverChains().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.DANGLING_PROPERTY__CREATED_BY:
        return CREATED_BY_EDEFAULT == null ? createdBy != null : !CREATED_BY_EDEFAULT.equals(createdBy);
      case obodatamodelPackage.DANGLING_PROPERTY__CREATED_BY_EXTENSION:
        return createdByExtension != null;
      case obodatamodelPackage.DANGLING_PROPERTY__MODIFIED_BY:
        return MODIFIED_BY_EDEFAULT == null ? modifiedBy != null : !MODIFIED_BY_EDEFAULT.equals(modifiedBy);
      case obodatamodelPackage.DANGLING_PROPERTY__MODIFIED_BY_EXTENSION:
        return modifiedByExtension != null;
      case obodatamodelPackage.DANGLING_PROPERTY__CREATION_DATE:
        return CREATION_DATE_EDEFAULT == null ? creationDate != null : !CREATION_DATE_EDEFAULT.equals(creationDate);
      case obodatamodelPackage.DANGLING_PROPERTY__CREATION_DATE_EXTENSION:
        return creationDateExtension != null;
      case obodatamodelPackage.DANGLING_PROPERTY__MODIFICATION_DATE:
        return MODIFICATION_DATE_EDEFAULT == null ? modificationDate != null : !MODIFICATION_DATE_EDEFAULT.equals(modificationDate);
      case obodatamodelPackage.DANGLING_PROPERTY__MODIFICATION_DATE_EXTENSION:
        return modificationDateExtension != null;
      case obodatamodelPackage.DANGLING_PROPERTY__SECONDARY_IDS:
        return secondaryIds != null && !secondaryIds.isEmpty();
      case obodatamodelPackage.DANGLING_PROPERTY__SECONDARY_ID_EXTENSION:
        return secondaryIdExtension != null && !secondaryIdExtension.isEmpty();
      case obodatamodelPackage.DANGLING_PROPERTY__SYNONYMS:
        return synonyms != null && !synonyms.isEmpty();
      case obodatamodelPackage.DANGLING_PROPERTY__DBXREFS:
        return dbxrefs != null && !dbxrefs.isEmpty();
      case obodatamodelPackage.DANGLING_PROPERTY__COMMENT:
        return COMMENT_EDEFAULT == null ? comment != null : !COMMENT_EDEFAULT.equals(comment);
      case obodatamodelPackage.DANGLING_PROPERTY__COMMENT_EXTENSION:
        return commentExtension != null;
      case obodatamodelPackage.DANGLING_PROPERTY__OBSOLETE:
        return obsolete != OBSOLETE_EDEFAULT;
      case obodatamodelPackage.DANGLING_PROPERTY__REPLACED_BY:
        return replacedBy != null && !replacedBy.isEmpty();
      case obodatamodelPackage.DANGLING_PROPERTY__CONSIDER_REPLACEMENTS:
        return considerReplacements != null && !considerReplacements.isEmpty();
      case obodatamodelPackage.DANGLING_PROPERTY__CONSIDER_EXTENSION:
        return considerExtension != null && !considerExtension.isEmpty();
      case obodatamodelPackage.DANGLING_PROPERTY__REPLACED_BY_EXTENSION:
        return replacedByExtension != null && !replacedByExtension.isEmpty();
      case obodatamodelPackage.DANGLING_PROPERTY__OBSOLETE_EXTENSION:
        return obsoleteExtension != null;
      case obodatamodelPackage.DANGLING_PROPERTY__DEFINITION:
        return DEFINITION_EDEFAULT == null ? definition != null : !DEFINITION_EDEFAULT.equals(definition);
      case obodatamodelPackage.DANGLING_PROPERTY__DEF_DBXREFS:
        return defDbxrefs != null && !defDbxrefs.isEmpty();
      case obodatamodelPackage.DANGLING_PROPERTY__DEFINITION_EXTENSION:
        return definitionExtension != null;
      case obodatamodelPackage.DANGLING_PROPERTY__SUBSETS:
        return subsets != null && !subsets.isEmpty();
      case obodatamodelPackage.DANGLING_PROPERTY__CATEGORY_EXTENSIONS:
        return categoryExtensions != null && !categoryExtensions.isEmpty();
      case obodatamodelPackage.DANGLING_PROPERTY__CYCLIC:
        return cyclic != CYCLIC_EDEFAULT;
      case obodatamodelPackage.DANGLING_PROPERTY__SYMMETRIC:
        return symmetric != SYMMETRIC_EDEFAULT;
      case obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE:
        return transitive != TRANSITIVE_EDEFAULT;
      case obodatamodelPackage.DANGLING_PROPERTY__REFLEXIVE:
        return reflexive != REFLEXIVE_EDEFAULT;
      case obodatamodelPackage.DANGLING_PROPERTY__ALWAYS_IMPLIES_INVERSE:
        return alwaysImpliesInverse != ALWAYS_IMPLIES_INVERSE_EDEFAULT;
      case obodatamodelPackage.DANGLING_PROPERTY__DUMMY:
        return dummy != DUMMY_EDEFAULT;
      case obodatamodelPackage.DANGLING_PROPERTY__METADATA_TAG:
        return metadataTag != METADATA_TAG_EDEFAULT;
      case obodatamodelPackage.DANGLING_PROPERTY__CYCLIC_EXTENSION:
        return cyclicExtension != null;
      case obodatamodelPackage.DANGLING_PROPERTY__SYMMETRIC_EXTENSION:
        return symmetricExtension != null;
      case obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE_EXTENSION:
        return transitiveExtension != null;
      case obodatamodelPackage.DANGLING_PROPERTY__REFLEXIVE_EXTENSION:
        return reflexiveExtension != null;
      case obodatamodelPackage.DANGLING_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION:
        return alwaysImpliesInverseExtension != null;
      case obodatamodelPackage.DANGLING_PROPERTY__DOMAIN_EXTENSION:
        return domainExtension != null;
      case obodatamodelPackage.DANGLING_PROPERTY__RANGE_EXTENSION:
        return rangeExtension != null;
      case obodatamodelPackage.DANGLING_PROPERTY__RANGE:
        return range != null;
      case obodatamodelPackage.DANGLING_PROPERTY__DOMAIN:
        return domain != null;
      case obodatamodelPackage.DANGLING_PROPERTY__NON_INHERITABLE:
        return nonInheritable != NON_INHERITABLE_EDEFAULT;
      case obodatamodelPackage.DANGLING_PROPERTY__DISJOINT_OVER:
        return disjointOver != null;
      case obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE_OVER:
        return transitiveOver != null;
      case obodatamodelPackage.DANGLING_PROPERTY__HOLDS_OVER_CHAINS:
        return holdsOverChains != null && !holdsOverChains.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
    if (baseClass == ModificationMetadataObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.DANGLING_PROPERTY__CREATED_BY: return obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY;
        case obodatamodelPackage.DANGLING_PROPERTY__CREATED_BY_EXTENSION: return obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION;
        case obodatamodelPackage.DANGLING_PROPERTY__MODIFIED_BY: return obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY;
        case obodatamodelPackage.DANGLING_PROPERTY__MODIFIED_BY_EXTENSION: return obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION;
        case obodatamodelPackage.DANGLING_PROPERTY__CREATION_DATE: return obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE;
        case obodatamodelPackage.DANGLING_PROPERTY__CREATION_DATE_EXTENSION: return obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION;
        case obodatamodelPackage.DANGLING_PROPERTY__MODIFICATION_DATE: return obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE;
        case obodatamodelPackage.DANGLING_PROPERTY__MODIFICATION_DATE_EXTENSION: return obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION;
        default: return -1;
      }
    }
    if (baseClass == MultiIDObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.DANGLING_PROPERTY__SECONDARY_IDS: return obodatamodelPackage.MULTI_ID_OBJECT__SECONDARY_IDS;
        case obodatamodelPackage.DANGLING_PROPERTY__SECONDARY_ID_EXTENSION: return obodatamodelPackage.MULTI_ID_OBJECT__SECONDARY_ID_EXTENSION;
        default: return -1;
      }
    }
    if (baseClass == SynonymedObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.DANGLING_PROPERTY__SYNONYMS: return obodatamodelPackage.SYNONYMED_OBJECT__SYNONYMS;
        default: return -1;
      }
    }
    if (baseClass == DbxrefedObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.DANGLING_PROPERTY__DBXREFS: return obodatamodelPackage.DBXREFED_OBJECT__DBXREFS;
        default: return -1;
      }
    }
    if (baseClass == CommentedObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.DANGLING_PROPERTY__COMMENT: return obodatamodelPackage.COMMENTED_OBJECT__COMMENT;
        case obodatamodelPackage.DANGLING_PROPERTY__COMMENT_EXTENSION: return obodatamodelPackage.COMMENTED_OBJECT__COMMENT_EXTENSION;
        default: return -1;
      }
    }
    if (baseClass == ObsoletableObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.DANGLING_PROPERTY__OBSOLETE: return obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE;
        case obodatamodelPackage.DANGLING_PROPERTY__REPLACED_BY: return obodatamodelPackage.OBSOLETABLE_OBJECT__REPLACED_BY;
        case obodatamodelPackage.DANGLING_PROPERTY__CONSIDER_REPLACEMENTS: return obodatamodelPackage.OBSOLETABLE_OBJECT__CONSIDER_REPLACEMENTS;
        case obodatamodelPackage.DANGLING_PROPERTY__CONSIDER_EXTENSION: return obodatamodelPackage.OBSOLETABLE_OBJECT__CONSIDER_EXTENSION;
        case obodatamodelPackage.DANGLING_PROPERTY__REPLACED_BY_EXTENSION: return obodatamodelPackage.OBSOLETABLE_OBJECT__REPLACED_BY_EXTENSION;
        case obodatamodelPackage.DANGLING_PROPERTY__OBSOLETE_EXTENSION: return obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE_EXTENSION;
        default: return -1;
      }
    }
    if (baseClass == DefinedObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.DANGLING_PROPERTY__DEFINITION: return obodatamodelPackage.DEFINED_OBJECT__DEFINITION;
        case obodatamodelPackage.DANGLING_PROPERTY__DEF_DBXREFS: return obodatamodelPackage.DEFINED_OBJECT__DEF_DBXREFS;
        case obodatamodelPackage.DANGLING_PROPERTY__DEFINITION_EXTENSION: return obodatamodelPackage.DEFINED_OBJECT__DEFINITION_EXTENSION;
        default: return -1;
      }
    }
    if (baseClass == SubsetObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.DANGLING_PROPERTY__SUBSETS: return obodatamodelPackage.SUBSET_OBJECT__SUBSETS;
        case obodatamodelPackage.DANGLING_PROPERTY__CATEGORY_EXTENSIONS: return obodatamodelPackage.SUBSET_OBJECT__CATEGORY_EXTENSIONS;
        default: return -1;
      }
    }
    if (baseClass == AnnotatedObject.class) {
      switch (derivedFeatureID) {
        default: return -1;
      }
    }
    if (baseClass == br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable.class) {
      switch (derivedFeatureID) {
        default: return -1;
      }
    }
    if (baseClass == OBOObject.class) {
      switch (derivedFeatureID) {
        default: return -1;
      }
    }
    if (baseClass == OBOProperty.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.DANGLING_PROPERTY__CYCLIC: return obodatamodelPackage.OBO_PROPERTY__CYCLIC;
        case obodatamodelPackage.DANGLING_PROPERTY__SYMMETRIC: return obodatamodelPackage.OBO_PROPERTY__SYMMETRIC;
        case obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE: return obodatamodelPackage.OBO_PROPERTY__TRANSITIVE;
        case obodatamodelPackage.DANGLING_PROPERTY__REFLEXIVE: return obodatamodelPackage.OBO_PROPERTY__REFLEXIVE;
        case obodatamodelPackage.DANGLING_PROPERTY__ALWAYS_IMPLIES_INVERSE: return obodatamodelPackage.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE;
        case obodatamodelPackage.DANGLING_PROPERTY__DUMMY: return obodatamodelPackage.OBO_PROPERTY__DUMMY;
        case obodatamodelPackage.DANGLING_PROPERTY__METADATA_TAG: return obodatamodelPackage.OBO_PROPERTY__METADATA_TAG;
        case obodatamodelPackage.DANGLING_PROPERTY__CYCLIC_EXTENSION: return obodatamodelPackage.OBO_PROPERTY__CYCLIC_EXTENSION;
        case obodatamodelPackage.DANGLING_PROPERTY__SYMMETRIC_EXTENSION: return obodatamodelPackage.OBO_PROPERTY__SYMMETRIC_EXTENSION;
        case obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE_EXTENSION: return obodatamodelPackage.OBO_PROPERTY__TRANSITIVE_EXTENSION;
        case obodatamodelPackage.DANGLING_PROPERTY__REFLEXIVE_EXTENSION: return obodatamodelPackage.OBO_PROPERTY__REFLEXIVE_EXTENSION;
        case obodatamodelPackage.DANGLING_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION: return obodatamodelPackage.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION;
        case obodatamodelPackage.DANGLING_PROPERTY__DOMAIN_EXTENSION: return obodatamodelPackage.OBO_PROPERTY__DOMAIN_EXTENSION;
        case obodatamodelPackage.DANGLING_PROPERTY__RANGE_EXTENSION: return obodatamodelPackage.OBO_PROPERTY__RANGE_EXTENSION;
        case obodatamodelPackage.DANGLING_PROPERTY__RANGE: return obodatamodelPackage.OBO_PROPERTY__RANGE;
        case obodatamodelPackage.DANGLING_PROPERTY__DOMAIN: return obodatamodelPackage.OBO_PROPERTY__DOMAIN;
        case obodatamodelPackage.DANGLING_PROPERTY__NON_INHERITABLE: return obodatamodelPackage.OBO_PROPERTY__NON_INHERITABLE;
        case obodatamodelPackage.DANGLING_PROPERTY__DISJOINT_OVER: return obodatamodelPackage.OBO_PROPERTY__DISJOINT_OVER;
        case obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE_OVER: return obodatamodelPackage.OBO_PROPERTY__TRANSITIVE_OVER;
        case obodatamodelPackage.DANGLING_PROPERTY__HOLDS_OVER_CHAINS: return obodatamodelPackage.OBO_PROPERTY__HOLDS_OVER_CHAINS;
        default: return -1;
      }
    }
    return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
    if (baseClass == ModificationMetadataObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY: return obodatamodelPackage.DANGLING_PROPERTY__CREATED_BY;
        case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION: return obodatamodelPackage.DANGLING_PROPERTY__CREATED_BY_EXTENSION;
        case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY: return obodatamodelPackage.DANGLING_PROPERTY__MODIFIED_BY;
        case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION: return obodatamodelPackage.DANGLING_PROPERTY__MODIFIED_BY_EXTENSION;
        case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE: return obodatamodelPackage.DANGLING_PROPERTY__CREATION_DATE;
        case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION: return obodatamodelPackage.DANGLING_PROPERTY__CREATION_DATE_EXTENSION;
        case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE: return obodatamodelPackage.DANGLING_PROPERTY__MODIFICATION_DATE;
        case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION: return obodatamodelPackage.DANGLING_PROPERTY__MODIFICATION_DATE_EXTENSION;
        default: return -1;
      }
    }
    if (baseClass == MultiIDObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.MULTI_ID_OBJECT__SECONDARY_IDS: return obodatamodelPackage.DANGLING_PROPERTY__SECONDARY_IDS;
        case obodatamodelPackage.MULTI_ID_OBJECT__SECONDARY_ID_EXTENSION: return obodatamodelPackage.DANGLING_PROPERTY__SECONDARY_ID_EXTENSION;
        default: return -1;
      }
    }
    if (baseClass == SynonymedObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.SYNONYMED_OBJECT__SYNONYMS: return obodatamodelPackage.DANGLING_PROPERTY__SYNONYMS;
        default: return -1;
      }
    }
    if (baseClass == DbxrefedObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.DBXREFED_OBJECT__DBXREFS: return obodatamodelPackage.DANGLING_PROPERTY__DBXREFS;
        default: return -1;
      }
    }
    if (baseClass == CommentedObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.COMMENTED_OBJECT__COMMENT: return obodatamodelPackage.DANGLING_PROPERTY__COMMENT;
        case obodatamodelPackage.COMMENTED_OBJECT__COMMENT_EXTENSION: return obodatamodelPackage.DANGLING_PROPERTY__COMMENT_EXTENSION;
        default: return -1;
      }
    }
    if (baseClass == ObsoletableObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE: return obodatamodelPackage.DANGLING_PROPERTY__OBSOLETE;
        case obodatamodelPackage.OBSOLETABLE_OBJECT__REPLACED_BY: return obodatamodelPackage.DANGLING_PROPERTY__REPLACED_BY;
        case obodatamodelPackage.OBSOLETABLE_OBJECT__CONSIDER_REPLACEMENTS: return obodatamodelPackage.DANGLING_PROPERTY__CONSIDER_REPLACEMENTS;
        case obodatamodelPackage.OBSOLETABLE_OBJECT__CONSIDER_EXTENSION: return obodatamodelPackage.DANGLING_PROPERTY__CONSIDER_EXTENSION;
        case obodatamodelPackage.OBSOLETABLE_OBJECT__REPLACED_BY_EXTENSION: return obodatamodelPackage.DANGLING_PROPERTY__REPLACED_BY_EXTENSION;
        case obodatamodelPackage.OBSOLETABLE_OBJECT__OBSOLETE_EXTENSION: return obodatamodelPackage.DANGLING_PROPERTY__OBSOLETE_EXTENSION;
        default: return -1;
      }
    }
    if (baseClass == DefinedObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.DEFINED_OBJECT__DEFINITION: return obodatamodelPackage.DANGLING_PROPERTY__DEFINITION;
        case obodatamodelPackage.DEFINED_OBJECT__DEF_DBXREFS: return obodatamodelPackage.DANGLING_PROPERTY__DEF_DBXREFS;
        case obodatamodelPackage.DEFINED_OBJECT__DEFINITION_EXTENSION: return obodatamodelPackage.DANGLING_PROPERTY__DEFINITION_EXTENSION;
        default: return -1;
      }
    }
    if (baseClass == SubsetObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.SUBSET_OBJECT__SUBSETS: return obodatamodelPackage.DANGLING_PROPERTY__SUBSETS;
        case obodatamodelPackage.SUBSET_OBJECT__CATEGORY_EXTENSIONS: return obodatamodelPackage.DANGLING_PROPERTY__CATEGORY_EXTENSIONS;
        default: return -1;
      }
    }
    if (baseClass == AnnotatedObject.class) {
      switch (baseFeatureID) {
        default: return -1;
      }
    }
    if (baseClass == br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable.class) {
      switch (baseFeatureID) {
        default: return -1;
      }
    }
    if (baseClass == OBOObject.class) {
      switch (baseFeatureID) {
        default: return -1;
      }
    }
    if (baseClass == OBOProperty.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.OBO_PROPERTY__CYCLIC: return obodatamodelPackage.DANGLING_PROPERTY__CYCLIC;
        case obodatamodelPackage.OBO_PROPERTY__SYMMETRIC: return obodatamodelPackage.DANGLING_PROPERTY__SYMMETRIC;
        case obodatamodelPackage.OBO_PROPERTY__TRANSITIVE: return obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE;
        case obodatamodelPackage.OBO_PROPERTY__REFLEXIVE: return obodatamodelPackage.DANGLING_PROPERTY__REFLEXIVE;
        case obodatamodelPackage.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE: return obodatamodelPackage.DANGLING_PROPERTY__ALWAYS_IMPLIES_INVERSE;
        case obodatamodelPackage.OBO_PROPERTY__DUMMY: return obodatamodelPackage.DANGLING_PROPERTY__DUMMY;
        case obodatamodelPackage.OBO_PROPERTY__METADATA_TAG: return obodatamodelPackage.DANGLING_PROPERTY__METADATA_TAG;
        case obodatamodelPackage.OBO_PROPERTY__CYCLIC_EXTENSION: return obodatamodelPackage.DANGLING_PROPERTY__CYCLIC_EXTENSION;
        case obodatamodelPackage.OBO_PROPERTY__SYMMETRIC_EXTENSION: return obodatamodelPackage.DANGLING_PROPERTY__SYMMETRIC_EXTENSION;
        case obodatamodelPackage.OBO_PROPERTY__TRANSITIVE_EXTENSION: return obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE_EXTENSION;
        case obodatamodelPackage.OBO_PROPERTY__REFLEXIVE_EXTENSION: return obodatamodelPackage.DANGLING_PROPERTY__REFLEXIVE_EXTENSION;
        case obodatamodelPackage.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION: return obodatamodelPackage.DANGLING_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION;
        case obodatamodelPackage.OBO_PROPERTY__DOMAIN_EXTENSION: return obodatamodelPackage.DANGLING_PROPERTY__DOMAIN_EXTENSION;
        case obodatamodelPackage.OBO_PROPERTY__RANGE_EXTENSION: return obodatamodelPackage.DANGLING_PROPERTY__RANGE_EXTENSION;
        case obodatamodelPackage.OBO_PROPERTY__RANGE: return obodatamodelPackage.DANGLING_PROPERTY__RANGE;
        case obodatamodelPackage.OBO_PROPERTY__DOMAIN: return obodatamodelPackage.DANGLING_PROPERTY__DOMAIN;
        case obodatamodelPackage.OBO_PROPERTY__NON_INHERITABLE: return obodatamodelPackage.DANGLING_PROPERTY__NON_INHERITABLE;
        case obodatamodelPackage.OBO_PROPERTY__DISJOINT_OVER: return obodatamodelPackage.DANGLING_PROPERTY__DISJOINT_OVER;
        case obodatamodelPackage.OBO_PROPERTY__TRANSITIVE_OVER: return obodatamodelPackage.DANGLING_PROPERTY__TRANSITIVE_OVER;
        case obodatamodelPackage.OBO_PROPERTY__HOLDS_OVER_CHAINS: return obodatamodelPackage.DANGLING_PROPERTY__HOLDS_OVER_CHAINS;
        default: return -1;
      }
    }
    return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (createdBy: ");
    result.append(createdBy);
    result.append(", modifiedBy: ");
    result.append(modifiedBy);
    result.append(", creationDate: ");
    result.append(creationDate);
    result.append(", modificationDate: ");
    result.append(modificationDate);
    result.append(", secondaryIds: ");
    result.append(secondaryIds);
    result.append(", comment: ");
    result.append(comment);
    result.append(", obsolete: ");
    result.append(obsolete);
    result.append(", definition: ");
    result.append(definition);
    result.append(", cyclic: ");
    result.append(cyclic);
    result.append(", symmetric: ");
    result.append(symmetric);
    result.append(", transitive: ");
    result.append(transitive);
    result.append(", reflexive: ");
    result.append(reflexive);
    result.append(", alwaysImpliesInverse: ");
    result.append(alwaysImpliesInverse);
    result.append(", dummy: ");
    result.append(dummy);
    result.append(", metadataTag: ");
    result.append(metadataTag);
    result.append(", nonInheritable: ");
    result.append(nonInheritable);
    result.append(')');
    return result.toString();
  }

} //DanglingPropertyImpl
