/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;

import java.math.BigInteger;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Synonym</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getUNKNOWN_SCOPE <em>UNKNOWN SCOPE</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getRELATED_SYNONYM <em>RELATED SYNONYM</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getEXACT_SYNONYM <em>EXACT SYNONYM</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getNARROW_SYNONYM <em>NARROW SYNONYM</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getBROAD_SYNONYM <em>BROAD SYNONYM</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getSynonymType <em>Synonym Type</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getNestedValue <em>Nested Value</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getScope <em>Scope</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getXrefs <em>Xrefs</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getText <em>Text</em>}</li>
 * </ul>
 *
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getSynonym()
 * @model
 * @generated
 */
public interface Synonym extends br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable, Serializable, br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable, IdentifiableObject {
  /**
   * Returns the value of the '<em><b>UNKNOWN SCOPE</b></em>' attribute.
   * The default value is <code>"-1"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>UNKNOWN SCOPE</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>UNKNOWN SCOPE</em>' attribute.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getSynonym_UNKNOWN_SCOPE()
   * @model default="-1" required="true" changeable="false"
   * @generated
   */
  BigInteger getUNKNOWN_SCOPE();

  /**
   * Returns the value of the '<em><b>RELATED SYNONYM</b></em>' attribute.
   * The default value is <code>"0"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>RELATED SYNONYM</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>RELATED SYNONYM</em>' attribute.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getSynonym_RELATED_SYNONYM()
   * @model default="0" required="true" changeable="false"
   * @generated
   */
  BigInteger getRELATED_SYNONYM();

  /**
   * Returns the value of the '<em><b>EXACT SYNONYM</b></em>' attribute.
   * The default value is <code>"1"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>EXACT SYNONYM</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>EXACT SYNONYM</em>' attribute.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getSynonym_EXACT_SYNONYM()
   * @model default="1" required="true" changeable="false"
   * @generated
   */
  BigInteger getEXACT_SYNONYM();

  /**
   * Returns the value of the '<em><b>NARROW SYNONYM</b></em>' attribute.
   * The default value is <code>"2"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>NARROW SYNONYM</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>NARROW SYNONYM</em>' attribute.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getSynonym_NARROW_SYNONYM()
   * @model default="2" required="true" changeable="false"
   * @generated
   */
  BigInteger getNARROW_SYNONYM();

  /**
   * Returns the value of the '<em><b>BROAD SYNONYM</b></em>' attribute.
   * The default value is <code>"3"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>BROAD SYNONYM</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>BROAD SYNONYM</em>' attribute.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getSynonym_BROAD_SYNONYM()
   * @model default="3" required="true" changeable="false"
   * @generated
   */
  BigInteger getBROAD_SYNONYM();

  /**
   * Returns the value of the '<em><b>Synonym Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Synonym Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Synonym Type</em>' containment reference.
   * @see #setSynonymType(SynonymType)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getSynonym_SynonymType()
   * @model containment="true"
   * @generated
   */
  SynonymType getSynonymType();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getSynonymType <em>Synonym Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Synonym Type</em>' containment reference.
   * @see #getSynonymType()
   * @generated
   */
  void setSynonymType(SynonymType value);

  /**
   * Returns the value of the '<em><b>Nested Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Nested Value</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nested Value</em>' containment reference.
   * @see #setNestedValue(NestedValue)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getSynonym_NestedValue()
   * @model containment="true"
   * @generated
   */
  NestedValue getNestedValue();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getNestedValue <em>Nested Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Nested Value</em>' containment reference.
   * @see #getNestedValue()
   * @generated
   */
  void setNestedValue(NestedValue value);

  /**
   * Returns the value of the '<em><b>Scope</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Scope</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Scope</em>' attribute.
   * @see #setScope(BigInteger)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getSynonym_Scope()
   * @model required="true"
   * @generated
   */
  BigInteger getScope();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getScope <em>Scope</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Scope</em>' attribute.
   * @see #getScope()
   * @generated
   */
  void setScope(BigInteger value);

  /**
   * Returns the value of the '<em><b>Xrefs</b></em>' reference list.
   * The list contents are of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Xrefs</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Xrefs</em>' reference list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getSynonym_Xrefs()
   * @model ordered="false"
   * @generated
   */
  EList<Dbxref> getXrefs();

  /**
   * Returns the value of the '<em><b>Text</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Text</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Text</em>' attribute.
   * @see #setText(String)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getSynonym_Text()
   * @model required="true"
   * @generated
   */
  String getText();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getText <em>Text</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Text</em>' attribute.
   * @see #getText()
   * @generated
   */
  void setText(String value);

} // Synonym
