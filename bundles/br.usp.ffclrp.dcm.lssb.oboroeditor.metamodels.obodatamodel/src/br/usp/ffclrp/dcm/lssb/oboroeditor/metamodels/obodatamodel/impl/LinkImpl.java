/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Link;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PathCapable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Serializable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Link</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkImpl#getId <em>Id</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkImpl#isAnonymous <em>Anonymous</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkImpl#getChild <em>Child</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkImpl#getType <em>Type</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkImpl#getNestedValue <em>Nested Value</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkImpl#getNamespace <em>Namespace</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class LinkImpl extends ImpliableImpl implements Link {
  /**
   * The default value of the '{@link #getId() <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getId()
   * @generated
   * @ordered
   */
  protected static final String ID_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getId()
   * @generated
   * @ordered
   */
  protected String id = ID_EDEFAULT;

  /**
   * The default value of the '{@link #isAnonymous() <em>Anonymous</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isAnonymous()
   * @generated
   * @ordered
   */
  protected static final boolean ANONYMOUS_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isAnonymous() <em>Anonymous</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isAnonymous()
   * @generated
   * @ordered
   */
  protected boolean anonymous = ANONYMOUS_EDEFAULT;

  /**
   * The cached value of the '{@link #getParent() <em>Parent</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParent()
   * @generated
   * @ordered
   */
  protected LinkedObject parent;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected OBOProperty type;

  /**
   * The cached value of the '{@link #getNestedValue() <em>Nested Value</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNestedValue()
   * @generated
   * @ordered
   */
  protected NestedValue nestedValue;

  /**
   * The cached value of the '{@link #getNamespace() <em>Namespace</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNamespace()
   * @generated
   * @ordered
   */
  protected Namespace namespace;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected LinkImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.LINK;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getId() {
    return id;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setId(String newId) {
    String oldId = id;
    id = newId;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.LINK__ID, oldId, id));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isAnonymous() {
    return anonymous;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAnonymous(boolean newAnonymous) {
    boolean oldAnonymous = anonymous;
    anonymous = newAnonymous;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.LINK__ANONYMOUS, oldAnonymous, anonymous));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LinkedObject getChild() {
    if (eContainerFeatureID() != obodatamodelPackage.LINK__CHILD) return null;
    return (LinkedObject)eInternalContainer();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetChild(LinkedObject newChild, NotificationChain msgs) {
    msgs = eBasicSetContainer((InternalEObject)newChild, obodatamodelPackage.LINK__CHILD, msgs);
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setChild(LinkedObject newChild) {
    if (newChild != eInternalContainer() || (eContainerFeatureID() != obodatamodelPackage.LINK__CHILD && newChild != null)) {
      if (EcoreUtil.isAncestor(this, newChild))
        throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
      NotificationChain msgs = null;
      if (eInternalContainer() != null)
        msgs = eBasicRemoveFromContainer(msgs);
      if (newChild != null)
        msgs = ((InternalEObject)newChild).eInverseAdd(this, obodatamodelPackage.LINKED_OBJECT__PARENTS, LinkedObject.class, msgs);
      msgs = basicSetChild(newChild, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.LINK__CHILD, newChild, newChild));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LinkedObject getParent() {
    if (parent != null && parent.eIsProxy()) {
      InternalEObject oldParent = (InternalEObject)parent;
      parent = (LinkedObject)eResolveProxy(oldParent);
      if (parent != oldParent) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, obodatamodelPackage.LINK__PARENT, oldParent, parent));
      }
    }
    return parent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LinkedObject basicGetParent() {
    return parent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetParent(LinkedObject newParent, NotificationChain msgs) {
    LinkedObject oldParent = parent;
    parent = newParent;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.LINK__PARENT, oldParent, newParent);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParent(LinkedObject newParent) {
    if (newParent != parent) {
      NotificationChain msgs = null;
      if (parent != null)
        msgs = ((InternalEObject)parent).eInverseRemove(this, obodatamodelPackage.LINKED_OBJECT__CHILDREN, LinkedObject.class, msgs);
      if (newParent != null)
        msgs = ((InternalEObject)newParent).eInverseAdd(this, obodatamodelPackage.LINKED_OBJECT__CHILDREN, LinkedObject.class, msgs);
      msgs = basicSetParent(newParent, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.LINK__PARENT, newParent, newParent));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBOProperty getType() {
    if (type != null && type.eIsProxy()) {
      InternalEObject oldType = (InternalEObject)type;
      type = (OBOProperty)eResolveProxy(oldType);
      if (type != oldType) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, obodatamodelPackage.LINK__TYPE, oldType, type));
      }
    }
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBOProperty basicGetType() {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(OBOProperty newType) {
    OBOProperty oldType = type;
    type = newType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.LINK__TYPE, oldType, type));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getNestedValue() {
    return nestedValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetNestedValue(NestedValue newNestedValue, NotificationChain msgs) {
    NestedValue oldNestedValue = nestedValue;
    nestedValue = newNestedValue;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.LINK__NESTED_VALUE, oldNestedValue, newNestedValue);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNestedValue(NestedValue newNestedValue) {
    if (newNestedValue != nestedValue) {
      NotificationChain msgs = null;
      if (nestedValue != null)
        msgs = ((InternalEObject)nestedValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.LINK__NESTED_VALUE, null, msgs);
      if (newNestedValue != null)
        msgs = ((InternalEObject)newNestedValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.LINK__NESTED_VALUE, null, msgs);
      msgs = basicSetNestedValue(newNestedValue, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.LINK__NESTED_VALUE, newNestedValue, newNestedValue));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Namespace getNamespace() {
    if (namespace != null && namespace.eIsProxy()) {
      InternalEObject oldNamespace = (InternalEObject)namespace;
      namespace = (Namespace)eResolveProxy(oldNamespace);
      if (namespace != oldNamespace) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, obodatamodelPackage.LINK__NAMESPACE, oldNamespace, namespace));
      }
    }
    return namespace;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Namespace basicGetNamespace() {
    return namespace;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNamespace(Namespace newNamespace) {
    Namespace oldNamespace = namespace;
    namespace = newNamespace;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.LINK__NAMESPACE, oldNamespace, namespace));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.LINK__CHILD:
        if (eInternalContainer() != null)
          msgs = eBasicRemoveFromContainer(msgs);
        return basicSetChild((LinkedObject)otherEnd, msgs);
      case obodatamodelPackage.LINK__PARENT:
        if (parent != null)
          msgs = ((InternalEObject)parent).eInverseRemove(this, obodatamodelPackage.LINKED_OBJECT__CHILDREN, LinkedObject.class, msgs);
        return basicSetParent((LinkedObject)otherEnd, msgs);
    }
    return super.eInverseAdd(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.LINK__CHILD:
        return basicSetChild(null, msgs);
      case obodatamodelPackage.LINK__PARENT:
        return basicSetParent(null, msgs);
      case obodatamodelPackage.LINK__NESTED_VALUE:
        return basicSetNestedValue(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
    switch (eContainerFeatureID()) {
      case obodatamodelPackage.LINK__CHILD:
        return eInternalContainer().eInverseRemove(this, obodatamodelPackage.LINKED_OBJECT__PARENTS, LinkedObject.class, msgs);
    }
    return super.eBasicRemoveFromContainerFeature(msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.LINK__ID:
        return getId();
      case obodatamodelPackage.LINK__ANONYMOUS:
        return isAnonymous();
      case obodatamodelPackage.LINK__CHILD:
        return getChild();
      case obodatamodelPackage.LINK__PARENT:
        if (resolve) return getParent();
        return basicGetParent();
      case obodatamodelPackage.LINK__TYPE:
        if (resolve) return getType();
        return basicGetType();
      case obodatamodelPackage.LINK__NESTED_VALUE:
        return getNestedValue();
      case obodatamodelPackage.LINK__NAMESPACE:
        if (resolve) return getNamespace();
        return basicGetNamespace();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.LINK__ID:
        setId((String)newValue);
        return;
      case obodatamodelPackage.LINK__ANONYMOUS:
        setAnonymous((Boolean)newValue);
        return;
      case obodatamodelPackage.LINK__CHILD:
        setChild((LinkedObject)newValue);
        return;
      case obodatamodelPackage.LINK__PARENT:
        setParent((LinkedObject)newValue);
        return;
      case obodatamodelPackage.LINK__TYPE:
        setType((OBOProperty)newValue);
        return;
      case obodatamodelPackage.LINK__NESTED_VALUE:
        setNestedValue((NestedValue)newValue);
        return;
      case obodatamodelPackage.LINK__NAMESPACE:
        setNamespace((Namespace)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.LINK__ID:
        setId(ID_EDEFAULT);
        return;
      case obodatamodelPackage.LINK__ANONYMOUS:
        setAnonymous(ANONYMOUS_EDEFAULT);
        return;
      case obodatamodelPackage.LINK__CHILD:
        setChild((LinkedObject)null);
        return;
      case obodatamodelPackage.LINK__PARENT:
        setParent((LinkedObject)null);
        return;
      case obodatamodelPackage.LINK__TYPE:
        setType((OBOProperty)null);
        return;
      case obodatamodelPackage.LINK__NESTED_VALUE:
        setNestedValue((NestedValue)null);
        return;
      case obodatamodelPackage.LINK__NAMESPACE:
        setNamespace((Namespace)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.LINK__ID:
        return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
      case obodatamodelPackage.LINK__ANONYMOUS:
        return anonymous != ANONYMOUS_EDEFAULT;
      case obodatamodelPackage.LINK__CHILD:
        return getChild() != null;
      case obodatamodelPackage.LINK__PARENT:
        return parent != null;
      case obodatamodelPackage.LINK__TYPE:
        return type != null;
      case obodatamodelPackage.LINK__NESTED_VALUE:
        return nestedValue != null;
      case obodatamodelPackage.LINK__NAMESPACE:
        return namespace != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
    if (baseClass == IdentifiableObject.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.LINK__ID: return obodatamodelPackage.IDENTIFIABLE_OBJECT__ID;
        case obodatamodelPackage.LINK__ANONYMOUS: return obodatamodelPackage.IDENTIFIABLE_OBJECT__ANONYMOUS;
        default: return -1;
      }
    }
    if (baseClass == Serializable.class) {
      switch (derivedFeatureID) {
        default: return -1;
      }
    }
    if (baseClass == br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable.class) {
      switch (derivedFeatureID) {
        default: return -1;
      }
    }
    if (baseClass == Relationship.class) {
      switch (derivedFeatureID) {
        case obodatamodelPackage.LINK__CHILD: return obodatamodelPackage.RELATIONSHIP__CHILD;
        case obodatamodelPackage.LINK__PARENT: return obodatamodelPackage.RELATIONSHIP__PARENT;
        case obodatamodelPackage.LINK__TYPE: return obodatamodelPackage.RELATIONSHIP__TYPE;
        case obodatamodelPackage.LINK__NESTED_VALUE: return obodatamodelPackage.RELATIONSHIP__NESTED_VALUE;
        default: return -1;
      }
    }
    if (baseClass == PathCapable.class) {
      switch (derivedFeatureID) {
        default: return -1;
      }
    }
    return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
    if (baseClass == IdentifiableObject.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.IDENTIFIABLE_OBJECT__ID: return obodatamodelPackage.LINK__ID;
        case obodatamodelPackage.IDENTIFIABLE_OBJECT__ANONYMOUS: return obodatamodelPackage.LINK__ANONYMOUS;
        default: return -1;
      }
    }
    if (baseClass == Serializable.class) {
      switch (baseFeatureID) {
        default: return -1;
      }
    }
    if (baseClass == br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable.class) {
      switch (baseFeatureID) {
        default: return -1;
      }
    }
    if (baseClass == Relationship.class) {
      switch (baseFeatureID) {
        case obodatamodelPackage.RELATIONSHIP__CHILD: return obodatamodelPackage.LINK__CHILD;
        case obodatamodelPackage.RELATIONSHIP__PARENT: return obodatamodelPackage.LINK__PARENT;
        case obodatamodelPackage.RELATIONSHIP__TYPE: return obodatamodelPackage.LINK__TYPE;
        case obodatamodelPackage.RELATIONSHIP__NESTED_VALUE: return obodatamodelPackage.LINK__NESTED_VALUE;
        default: return -1;
      }
    }
    if (baseClass == PathCapable.class) {
      switch (baseFeatureID) {
        default: return -1;
      }
    }
    return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (id: ");
    result.append(id);
    result.append(", anonymous: ");
    result.append(anonymous);
    result.append(')');
    return result.toString();
  }

} //LinkImpl
