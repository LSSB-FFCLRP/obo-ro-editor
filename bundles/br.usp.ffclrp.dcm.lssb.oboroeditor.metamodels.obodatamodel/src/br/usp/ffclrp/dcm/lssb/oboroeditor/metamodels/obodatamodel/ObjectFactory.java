/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object Factory</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getObjectFactory()
 * @model
 * @generated
 */
public interface ObjectFactory extends Serializable, br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable {
} // ObjectFactory
