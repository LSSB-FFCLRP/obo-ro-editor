/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Root Algorithm</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getRootAlgorithm()
 * @model
 * @generated
 */
public interface RootAlgorithm extends EObject {
} // RootAlgorithm
