/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MultiIDObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Multi ID Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.MultiIDObjectImpl#getSecondaryIds <em>Secondary Ids</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.MultiIDObjectImpl#getSecondaryIdExtension <em>Secondary Id Extension</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class MultiIDObjectImpl extends IdentifiedObjectImpl implements MultiIDObject {
  /**
   * The cached value of the '{@link #getSecondaryIds() <em>Secondary Ids</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSecondaryIds()
   * @generated
   * @ordered
   */
  protected EList<String> secondaryIds;

  /**
   * The cached value of the '{@link #getSecondaryIdExtension() <em>Secondary Id Extension</em>}' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSecondaryIdExtension()
   * @generated
   * @ordered
   */
  protected EMap<String, NestedValue> secondaryIdExtension;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MultiIDObjectImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.MULTI_ID_OBJECT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getSecondaryIds() {
    if (secondaryIds == null) {
      secondaryIds = new EDataTypeUniqueEList<String>(String.class, this, obodatamodelPackage.MULTI_ID_OBJECT__SECONDARY_IDS);
    }
    return secondaryIds;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EMap<String, NestedValue> getSecondaryIdExtension() {
    if (secondaryIdExtension == null) {
      secondaryIdExtension = new EcoreEMap<String,NestedValue>(obodatamodelPackage.Literals.STRING_TO_NESTED_VALUE_MAP, StringToNestedValueMapImpl.class, this, obodatamodelPackage.MULTI_ID_OBJECT__SECONDARY_ID_EXTENSION);
    }
    return secondaryIdExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.MULTI_ID_OBJECT__SECONDARY_ID_EXTENSION:
        return ((InternalEList<?>)getSecondaryIdExtension()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.MULTI_ID_OBJECT__SECONDARY_IDS:
        return getSecondaryIds();
      case obodatamodelPackage.MULTI_ID_OBJECT__SECONDARY_ID_EXTENSION:
        if (coreType) return getSecondaryIdExtension();
        else return getSecondaryIdExtension().map();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.MULTI_ID_OBJECT__SECONDARY_IDS:
        getSecondaryIds().clear();
        getSecondaryIds().addAll((Collection<? extends String>)newValue);
        return;
      case obodatamodelPackage.MULTI_ID_OBJECT__SECONDARY_ID_EXTENSION:
        ((EStructuralFeature.Setting)getSecondaryIdExtension()).set(newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.MULTI_ID_OBJECT__SECONDARY_IDS:
        getSecondaryIds().clear();
        return;
      case obodatamodelPackage.MULTI_ID_OBJECT__SECONDARY_ID_EXTENSION:
        getSecondaryIdExtension().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.MULTI_ID_OBJECT__SECONDARY_IDS:
        return secondaryIds != null && !secondaryIds.isEmpty();
      case obodatamodelPackage.MULTI_ID_OBJECT__SECONDARY_ID_EXTENSION:
        return secondaryIdExtension != null && !secondaryIdExtension.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (secondaryIds: ");
    result.append(secondaryIds);
    result.append(')');
    return result.toString();
  }

} //MultiIDObjectImpl
