/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Modification Metadata Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ModificationMetadataObjectImpl#getCreatedBy <em>Created By</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ModificationMetadataObjectImpl#getCreatedByExtension <em>Created By Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ModificationMetadataObjectImpl#getModifiedBy <em>Modified By</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ModificationMetadataObjectImpl#getModifiedByExtension <em>Modified By Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ModificationMetadataObjectImpl#getCreationDate <em>Creation Date</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ModificationMetadataObjectImpl#getCreationDateExtension <em>Creation Date Extension</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ModificationMetadataObjectImpl#getModificationDate <em>Modification Date</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ModificationMetadataObjectImpl#getModificationDateExtension <em>Modification Date Extension</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModificationMetadataObjectImpl extends MinimalEObjectImpl.Container implements ModificationMetadataObject {
  /**
   * The default value of the '{@link #getCreatedBy() <em>Created By</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCreatedBy()
   * @generated
   * @ordered
   */
  protected static final String CREATED_BY_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getCreatedBy() <em>Created By</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCreatedBy()
   * @generated
   * @ordered
   */
  protected String createdBy = CREATED_BY_EDEFAULT;

  /**
   * The cached value of the '{@link #getCreatedByExtension() <em>Created By Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCreatedByExtension()
   * @generated
   * @ordered
   */
  protected NestedValue createdByExtension;

  /**
   * The default value of the '{@link #getModifiedBy() <em>Modified By</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModifiedBy()
   * @generated
   * @ordered
   */
  protected static final String MODIFIED_BY_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getModifiedBy() <em>Modified By</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModifiedBy()
   * @generated
   * @ordered
   */
  protected String modifiedBy = MODIFIED_BY_EDEFAULT;

  /**
   * The cached value of the '{@link #getModifiedByExtension() <em>Modified By Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModifiedByExtension()
   * @generated
   * @ordered
   */
  protected NestedValue modifiedByExtension;

  /**
   * The default value of the '{@link #getCreationDate() <em>Creation Date</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCreationDate()
   * @generated
   * @ordered
   */
  protected static final Date CREATION_DATE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCreationDate() <em>Creation Date</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCreationDate()
   * @generated
   * @ordered
   */
  protected Date creationDate = CREATION_DATE_EDEFAULT;

  /**
   * The cached value of the '{@link #getCreationDateExtension() <em>Creation Date Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCreationDateExtension()
   * @generated
   * @ordered
   */
  protected NestedValue creationDateExtension;

  /**
   * The default value of the '{@link #getModificationDate() <em>Modification Date</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModificationDate()
   * @generated
   * @ordered
   */
  protected static final Date MODIFICATION_DATE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getModificationDate() <em>Modification Date</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModificationDate()
   * @generated
   * @ordered
   */
  protected Date modificationDate = MODIFICATION_DATE_EDEFAULT;

  /**
   * The cached value of the '{@link #getModificationDateExtension() <em>Modification Date Extension</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModificationDateExtension()
   * @generated
   * @ordered
   */
  protected NestedValue modificationDateExtension;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ModificationMetadataObjectImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCreatedBy() {
    return createdBy;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCreatedBy(String newCreatedBy) {
    String oldCreatedBy = createdBy;
    createdBy = newCreatedBy;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY, oldCreatedBy, createdBy));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getCreatedByExtension() {
    return createdByExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCreatedByExtension(NestedValue newCreatedByExtension, NotificationChain msgs) {
    NestedValue oldCreatedByExtension = createdByExtension;
    createdByExtension = newCreatedByExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION, oldCreatedByExtension, newCreatedByExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCreatedByExtension(NestedValue newCreatedByExtension) {
    if (newCreatedByExtension != createdByExtension) {
      NotificationChain msgs = null;
      if (createdByExtension != null)
        msgs = ((InternalEObject)createdByExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION, null, msgs);
      if (newCreatedByExtension != null)
        msgs = ((InternalEObject)newCreatedByExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION, null, msgs);
      msgs = basicSetCreatedByExtension(newCreatedByExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION, newCreatedByExtension, newCreatedByExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getModifiedBy() {
    return modifiedBy;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setModifiedBy(String newModifiedBy) {
    String oldModifiedBy = modifiedBy;
    modifiedBy = newModifiedBy;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY, oldModifiedBy, modifiedBy));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getModifiedByExtension() {
    return modifiedByExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetModifiedByExtension(NestedValue newModifiedByExtension, NotificationChain msgs) {
    NestedValue oldModifiedByExtension = modifiedByExtension;
    modifiedByExtension = newModifiedByExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION, oldModifiedByExtension, newModifiedByExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setModifiedByExtension(NestedValue newModifiedByExtension) {
    if (newModifiedByExtension != modifiedByExtension) {
      NotificationChain msgs = null;
      if (modifiedByExtension != null)
        msgs = ((InternalEObject)modifiedByExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION, null, msgs);
      if (newModifiedByExtension != null)
        msgs = ((InternalEObject)newModifiedByExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION, null, msgs);
      msgs = basicSetModifiedByExtension(newModifiedByExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION, newModifiedByExtension, newModifiedByExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Date getCreationDate() {
    return creationDate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCreationDate(Date newCreationDate) {
    Date oldCreationDate = creationDate;
    creationDate = newCreationDate;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE, oldCreationDate, creationDate));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getCreationDateExtension() {
    return creationDateExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCreationDateExtension(NestedValue newCreationDateExtension, NotificationChain msgs) {
    NestedValue oldCreationDateExtension = creationDateExtension;
    creationDateExtension = newCreationDateExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION, oldCreationDateExtension, newCreationDateExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCreationDateExtension(NestedValue newCreationDateExtension) {
    if (newCreationDateExtension != creationDateExtension) {
      NotificationChain msgs = null;
      if (creationDateExtension != null)
        msgs = ((InternalEObject)creationDateExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION, null, msgs);
      if (newCreationDateExtension != null)
        msgs = ((InternalEObject)newCreationDateExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION, null, msgs);
      msgs = basicSetCreationDateExtension(newCreationDateExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION, newCreationDateExtension, newCreationDateExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Date getModificationDate() {
    return modificationDate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setModificationDate(Date newModificationDate) {
    Date oldModificationDate = modificationDate;
    modificationDate = newModificationDate;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE, oldModificationDate, modificationDate));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NestedValue getModificationDateExtension() {
    return modificationDateExtension;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetModificationDateExtension(NestedValue newModificationDateExtension, NotificationChain msgs) {
    NestedValue oldModificationDateExtension = modificationDateExtension;
    modificationDateExtension = newModificationDateExtension;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION, oldModificationDateExtension, newModificationDateExtension);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setModificationDateExtension(NestedValue newModificationDateExtension) {
    if (newModificationDateExtension != modificationDateExtension) {
      NotificationChain msgs = null;
      if (modificationDateExtension != null)
        msgs = ((InternalEObject)modificationDateExtension).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION, null, msgs);
      if (newModificationDateExtension != null)
        msgs = ((InternalEObject)newModificationDateExtension).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION, null, msgs);
      msgs = basicSetModificationDateExtension(newModificationDateExtension, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION, newModificationDateExtension, newModificationDateExtension));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION:
        return basicSetCreatedByExtension(null, msgs);
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION:
        return basicSetModifiedByExtension(null, msgs);
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION:
        return basicSetCreationDateExtension(null, msgs);
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION:
        return basicSetModificationDateExtension(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY:
        return getCreatedBy();
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION:
        return getCreatedByExtension();
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY:
        return getModifiedBy();
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION:
        return getModifiedByExtension();
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE:
        return getCreationDate();
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION:
        return getCreationDateExtension();
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE:
        return getModificationDate();
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION:
        return getModificationDateExtension();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY:
        setCreatedBy((String)newValue);
        return;
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION:
        setCreatedByExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY:
        setModifiedBy((String)newValue);
        return;
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION:
        setModifiedByExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE:
        setCreationDate((Date)newValue);
        return;
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION:
        setCreationDateExtension((NestedValue)newValue);
        return;
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE:
        setModificationDate((Date)newValue);
        return;
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION:
        setModificationDateExtension((NestedValue)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY:
        setCreatedBy(CREATED_BY_EDEFAULT);
        return;
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION:
        setCreatedByExtension((NestedValue)null);
        return;
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY:
        setModifiedBy(MODIFIED_BY_EDEFAULT);
        return;
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION:
        setModifiedByExtension((NestedValue)null);
        return;
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE:
        setCreationDate(CREATION_DATE_EDEFAULT);
        return;
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION:
        setCreationDateExtension((NestedValue)null);
        return;
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE:
        setModificationDate(MODIFICATION_DATE_EDEFAULT);
        return;
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION:
        setModificationDateExtension((NestedValue)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY:
        return CREATED_BY_EDEFAULT == null ? createdBy != null : !CREATED_BY_EDEFAULT.equals(createdBy);
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION:
        return createdByExtension != null;
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY:
        return MODIFIED_BY_EDEFAULT == null ? modifiedBy != null : !MODIFIED_BY_EDEFAULT.equals(modifiedBy);
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION:
        return modifiedByExtension != null;
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE:
        return CREATION_DATE_EDEFAULT == null ? creationDate != null : !CREATION_DATE_EDEFAULT.equals(creationDate);
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION:
        return creationDateExtension != null;
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE:
        return MODIFICATION_DATE_EDEFAULT == null ? modificationDate != null : !MODIFICATION_DATE_EDEFAULT.equals(modificationDate);
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION:
        return modificationDateExtension != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (createdBy: ");
    result.append(createdBy);
    result.append(", modifiedBy: ");
    result.append(modifiedBy);
    result.append(", creationDate: ");
    result.append(creationDate);
    result.append(", modificationDate: ");
    result.append(modificationDate);
    result.append(')');
    return result.toString();
  }

} //ModificationMetadataObjectImpl
