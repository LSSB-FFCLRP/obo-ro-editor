/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface obodatamodelPackage extends EPackage {
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "obodatamodel";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://dcm.ffclrp.usp.br/lssb/oboroeditor/metamodels/obodatamodel.ecore";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "OBODatamodel";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  obodatamodelPackage eINSTANCE = br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl.init();

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiableObjectImpl <em>Identifiable Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiableObjectImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getIdentifiableObject()
   * @generated
   */
  int IDENTIFIABLE_OBJECT = 11;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiedObjectImpl <em>Identified Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiedObjectImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getIdentifiedObject()
   * @generated
   */
  int IDENTIFIED_OBJECT = 12;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl <em>Annotated Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getAnnotatedObject()
   * @generated
   */
  int ANNOTATED_OBJECT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.CommentedObjectImpl <em>Commented Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.CommentedObjectImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getCommentedObject()
   * @generated
   */
  int COMMENTED_OBJECT = 1;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkedObjectImpl <em>Linked Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkedObjectImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getLinkedObject()
   * @generated
   */
  int LINKED_OBJECT = 19;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingObjectImpl <em>Dangling Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingObjectImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getDanglingObject()
   * @generated
   */
  int DANGLING_OBJECT = 2;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl <em>Dangling Property</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getDanglingProperty()
   * @generated
   */
  int DANGLING_PROPERTY = 3;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.TypeImpl <em>Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.TypeImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getType()
   * @generated
   */
  int TYPE = 43;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DatatypeImpl <em>Datatype</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DatatypeImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getDatatype()
   * @generated
   */
  int DATATYPE = 4;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.CloneableImpl <em>Cloneable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.CloneableImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getCloneable()
   * @generated
   */
  int CLONEABLE = 48;

  /**
   * The number of structural features of the '<em>Cloneable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLONEABLE_FEATURE_COUNT = 0;

  /**
   * The number of operations of the '<em>Cloneable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CLONEABLE_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ValueImpl <em>Value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ValueImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getValue()
   * @generated
   */
  int VALUE = 45;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE__TYPE = CLONEABLE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_FEATURE_COUNT = CLONEABLE_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_OPERATION_COUNT = CLONEABLE_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIED_OBJECT__TYPE = VALUE__TYPE;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIED_OBJECT__ID = VALUE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIED_OBJECT__ANONYMOUS = VALUE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIED_OBJECT__NAMESPACE = VALUE_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Namespace Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIED_OBJECT__NAMESPACE_EXTENSION = VALUE_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Type Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIED_OBJECT__TYPE_EXTENSION = VALUE_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Built In</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIED_OBJECT__BUILT_IN = VALUE_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIED_OBJECT__NAME = VALUE_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Name Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIED_OBJECT__NAME_EXTENSION = VALUE_FEATURE_COUNT + 7;

  /**
   * The feature id for the '<em><b>Id Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIED_OBJECT__ID_EXTENSION = VALUE_FEATURE_COUNT + 8;

  /**
   * The feature id for the '<em><b>Anonymous Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION = VALUE_FEATURE_COUNT + 9;

  /**
   * The feature id for the '<em><b>Property Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIED_OBJECT__PROPERTY_VALUES = VALUE_FEATURE_COUNT + 10;

  /**
   * The number of structural features of the '<em>Identified Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIED_OBJECT_FEATURE_COUNT = VALUE_FEATURE_COUNT + 11;

  /**
   * The number of operations of the '<em>Identified Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIED_OBJECT_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__TYPE = IDENTIFIED_OBJECT__TYPE;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__ID = IDENTIFIED_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__ANONYMOUS = IDENTIFIED_OBJECT__ANONYMOUS;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__NAMESPACE = IDENTIFIED_OBJECT__NAMESPACE;

  /**
   * The feature id for the '<em><b>Namespace Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__NAMESPACE_EXTENSION = IDENTIFIED_OBJECT__NAMESPACE_EXTENSION;

  /**
   * The feature id for the '<em><b>Type Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__TYPE_EXTENSION = IDENTIFIED_OBJECT__TYPE_EXTENSION;

  /**
   * The feature id for the '<em><b>Built In</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__BUILT_IN = IDENTIFIED_OBJECT__BUILT_IN;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__NAME = IDENTIFIED_OBJECT__NAME;

  /**
   * The feature id for the '<em><b>Name Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__NAME_EXTENSION = IDENTIFIED_OBJECT__NAME_EXTENSION;

  /**
   * The feature id for the '<em><b>Id Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__ID_EXTENSION = IDENTIFIED_OBJECT__ID_EXTENSION;

  /**
   * The feature id for the '<em><b>Anonymous Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__ANONYMOUS_EXTENSION = IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION;

  /**
   * The feature id for the '<em><b>Property Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__PROPERTY_VALUES = IDENTIFIED_OBJECT__PROPERTY_VALUES;

  /**
   * The feature id for the '<em><b>Created By</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__CREATED_BY = IDENTIFIED_OBJECT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Created By Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__CREATED_BY_EXTENSION = IDENTIFIED_OBJECT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Modified By</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__MODIFIED_BY = IDENTIFIED_OBJECT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Modified By Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__MODIFIED_BY_EXTENSION = IDENTIFIED_OBJECT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Creation Date</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__CREATION_DATE = IDENTIFIED_OBJECT_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Creation Date Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__CREATION_DATE_EXTENSION = IDENTIFIED_OBJECT_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Modification Date</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__MODIFICATION_DATE = IDENTIFIED_OBJECT_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Modification Date Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__MODIFICATION_DATE_EXTENSION = IDENTIFIED_OBJECT_FEATURE_COUNT + 7;

  /**
   * The feature id for the '<em><b>Secondary Ids</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__SECONDARY_IDS = IDENTIFIED_OBJECT_FEATURE_COUNT + 8;

  /**
   * The feature id for the '<em><b>Secondary Id Extension</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__SECONDARY_ID_EXTENSION = IDENTIFIED_OBJECT_FEATURE_COUNT + 9;

  /**
   * The feature id for the '<em><b>Synonyms</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__SYNONYMS = IDENTIFIED_OBJECT_FEATURE_COUNT + 10;

  /**
   * The feature id for the '<em><b>Dbxrefs</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__DBXREFS = IDENTIFIED_OBJECT_FEATURE_COUNT + 11;

  /**
   * The feature id for the '<em><b>Comment</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__COMMENT = IDENTIFIED_OBJECT_FEATURE_COUNT + 12;

  /**
   * The feature id for the '<em><b>Comment Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__COMMENT_EXTENSION = IDENTIFIED_OBJECT_FEATURE_COUNT + 13;

  /**
   * The feature id for the '<em><b>Obsolete</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__OBSOLETE = IDENTIFIED_OBJECT_FEATURE_COUNT + 14;

  /**
   * The feature id for the '<em><b>Replaced By</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__REPLACED_BY = IDENTIFIED_OBJECT_FEATURE_COUNT + 15;

  /**
   * The feature id for the '<em><b>Consider Replacements</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__CONSIDER_REPLACEMENTS = IDENTIFIED_OBJECT_FEATURE_COUNT + 16;

  /**
   * The feature id for the '<em><b>Consider Extension</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__CONSIDER_EXTENSION = IDENTIFIED_OBJECT_FEATURE_COUNT + 17;

  /**
   * The feature id for the '<em><b>Replaced By Extension</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__REPLACED_BY_EXTENSION = IDENTIFIED_OBJECT_FEATURE_COUNT + 18;

  /**
   * The feature id for the '<em><b>Obsolete Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__OBSOLETE_EXTENSION = IDENTIFIED_OBJECT_FEATURE_COUNT + 19;

  /**
   * The feature id for the '<em><b>Definition</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__DEFINITION = IDENTIFIED_OBJECT_FEATURE_COUNT + 20;

  /**
   * The feature id for the '<em><b>Def Dbxrefs</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__DEF_DBXREFS = IDENTIFIED_OBJECT_FEATURE_COUNT + 21;

  /**
   * The feature id for the '<em><b>Definition Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__DEFINITION_EXTENSION = IDENTIFIED_OBJECT_FEATURE_COUNT + 22;

  /**
   * The feature id for the '<em><b>Subsets</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__SUBSETS = IDENTIFIED_OBJECT_FEATURE_COUNT + 23;

  /**
   * The feature id for the '<em><b>Category Extensions</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT__CATEGORY_EXTENSIONS = IDENTIFIED_OBJECT_FEATURE_COUNT + 24;

  /**
   * The number of structural features of the '<em>Annotated Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT_FEATURE_COUNT = IDENTIFIED_OBJECT_FEATURE_COUNT + 25;

  /**
   * The number of operations of the '<em>Annotated Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATED_OBJECT_OPERATION_COUNT = IDENTIFIED_OBJECT_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMENTED_OBJECT__TYPE = IDENTIFIED_OBJECT__TYPE;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMENTED_OBJECT__ID = IDENTIFIED_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMENTED_OBJECT__ANONYMOUS = IDENTIFIED_OBJECT__ANONYMOUS;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMENTED_OBJECT__NAMESPACE = IDENTIFIED_OBJECT__NAMESPACE;

  /**
   * The feature id for the '<em><b>Namespace Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMENTED_OBJECT__NAMESPACE_EXTENSION = IDENTIFIED_OBJECT__NAMESPACE_EXTENSION;

  /**
   * The feature id for the '<em><b>Type Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMENTED_OBJECT__TYPE_EXTENSION = IDENTIFIED_OBJECT__TYPE_EXTENSION;

  /**
   * The feature id for the '<em><b>Built In</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMENTED_OBJECT__BUILT_IN = IDENTIFIED_OBJECT__BUILT_IN;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMENTED_OBJECT__NAME = IDENTIFIED_OBJECT__NAME;

  /**
   * The feature id for the '<em><b>Name Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMENTED_OBJECT__NAME_EXTENSION = IDENTIFIED_OBJECT__NAME_EXTENSION;

  /**
   * The feature id for the '<em><b>Id Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMENTED_OBJECT__ID_EXTENSION = IDENTIFIED_OBJECT__ID_EXTENSION;

  /**
   * The feature id for the '<em><b>Anonymous Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMENTED_OBJECT__ANONYMOUS_EXTENSION = IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION;

  /**
   * The feature id for the '<em><b>Property Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMENTED_OBJECT__PROPERTY_VALUES = IDENTIFIED_OBJECT__PROPERTY_VALUES;

  /**
   * The feature id for the '<em><b>Comment</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMENTED_OBJECT__COMMENT = IDENTIFIED_OBJECT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Comment Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMENTED_OBJECT__COMMENT_EXTENSION = IDENTIFIED_OBJECT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Commented Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMENTED_OBJECT_FEATURE_COUNT = IDENTIFIED_OBJECT_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>Commented Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMENTED_OBJECT_OPERATION_COUNT = IDENTIFIED_OBJECT_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINKED_OBJECT__TYPE = IDENTIFIED_OBJECT__TYPE;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINKED_OBJECT__ID = IDENTIFIED_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINKED_OBJECT__ANONYMOUS = IDENTIFIED_OBJECT__ANONYMOUS;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINKED_OBJECT__NAMESPACE = IDENTIFIED_OBJECT__NAMESPACE;

  /**
   * The feature id for the '<em><b>Namespace Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINKED_OBJECT__NAMESPACE_EXTENSION = IDENTIFIED_OBJECT__NAMESPACE_EXTENSION;

  /**
   * The feature id for the '<em><b>Type Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINKED_OBJECT__TYPE_EXTENSION = IDENTIFIED_OBJECT__TYPE_EXTENSION;

  /**
   * The feature id for the '<em><b>Built In</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINKED_OBJECT__BUILT_IN = IDENTIFIED_OBJECT__BUILT_IN;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINKED_OBJECT__NAME = IDENTIFIED_OBJECT__NAME;

  /**
   * The feature id for the '<em><b>Name Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINKED_OBJECT__NAME_EXTENSION = IDENTIFIED_OBJECT__NAME_EXTENSION;

  /**
   * The feature id for the '<em><b>Id Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINKED_OBJECT__ID_EXTENSION = IDENTIFIED_OBJECT__ID_EXTENSION;

  /**
   * The feature id for the '<em><b>Anonymous Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINKED_OBJECT__ANONYMOUS_EXTENSION = IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION;

  /**
   * The feature id for the '<em><b>Property Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINKED_OBJECT__PROPERTY_VALUES = IDENTIFIED_OBJECT__PROPERTY_VALUES;

  /**
   * The feature id for the '<em><b>Parents</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINKED_OBJECT__PARENTS = IDENTIFIED_OBJECT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Children</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINKED_OBJECT__CHILDREN = IDENTIFIED_OBJECT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Linked Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINKED_OBJECT_FEATURE_COUNT = IDENTIFIED_OBJECT_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>Linked Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINKED_OBJECT_OPERATION_COUNT = IDENTIFIED_OBJECT_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_OBJECT__TYPE = LINKED_OBJECT__TYPE;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_OBJECT__ID = LINKED_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_OBJECT__ANONYMOUS = LINKED_OBJECT__ANONYMOUS;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_OBJECT__NAMESPACE = LINKED_OBJECT__NAMESPACE;

  /**
   * The feature id for the '<em><b>Namespace Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_OBJECT__NAMESPACE_EXTENSION = LINKED_OBJECT__NAMESPACE_EXTENSION;

  /**
   * The feature id for the '<em><b>Type Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_OBJECT__TYPE_EXTENSION = LINKED_OBJECT__TYPE_EXTENSION;

  /**
   * The feature id for the '<em><b>Built In</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_OBJECT__BUILT_IN = LINKED_OBJECT__BUILT_IN;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_OBJECT__NAME = LINKED_OBJECT__NAME;

  /**
   * The feature id for the '<em><b>Name Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_OBJECT__NAME_EXTENSION = LINKED_OBJECT__NAME_EXTENSION;

  /**
   * The feature id for the '<em><b>Id Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_OBJECT__ID_EXTENSION = LINKED_OBJECT__ID_EXTENSION;

  /**
   * The feature id for the '<em><b>Anonymous Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_OBJECT__ANONYMOUS_EXTENSION = LINKED_OBJECT__ANONYMOUS_EXTENSION;

  /**
   * The feature id for the '<em><b>Property Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_OBJECT__PROPERTY_VALUES = LINKED_OBJECT__PROPERTY_VALUES;

  /**
   * The feature id for the '<em><b>Parents</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_OBJECT__PARENTS = LINKED_OBJECT__PARENTS;

  /**
   * The feature id for the '<em><b>Children</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_OBJECT__CHILDREN = LINKED_OBJECT__CHILDREN;

  /**
   * The feature id for the '<em><b>Dangling</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_OBJECT__DANGLING = LINKED_OBJECT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Dangling Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_OBJECT_FEATURE_COUNT = LINKED_OBJECT_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Dangling Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_OBJECT_OPERATION_COUNT = LINKED_OBJECT_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__TYPE = LINKED_OBJECT__TYPE;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__ID = LINKED_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__ANONYMOUS = LINKED_OBJECT__ANONYMOUS;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__NAMESPACE = LINKED_OBJECT__NAMESPACE;

  /**
   * The feature id for the '<em><b>Namespace Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__NAMESPACE_EXTENSION = LINKED_OBJECT__NAMESPACE_EXTENSION;

  /**
   * The feature id for the '<em><b>Type Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__TYPE_EXTENSION = LINKED_OBJECT__TYPE_EXTENSION;

  /**
   * The feature id for the '<em><b>Built In</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__BUILT_IN = LINKED_OBJECT__BUILT_IN;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__NAME = LINKED_OBJECT__NAME;

  /**
   * The feature id for the '<em><b>Name Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__NAME_EXTENSION = LINKED_OBJECT__NAME_EXTENSION;

  /**
   * The feature id for the '<em><b>Id Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__ID_EXTENSION = LINKED_OBJECT__ID_EXTENSION;

  /**
   * The feature id for the '<em><b>Anonymous Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__ANONYMOUS_EXTENSION = LINKED_OBJECT__ANONYMOUS_EXTENSION;

  /**
   * The feature id for the '<em><b>Property Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__PROPERTY_VALUES = LINKED_OBJECT__PROPERTY_VALUES;

  /**
   * The feature id for the '<em><b>Parents</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__PARENTS = LINKED_OBJECT__PARENTS;

  /**
   * The feature id for the '<em><b>Children</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__CHILDREN = LINKED_OBJECT__CHILDREN;

  /**
   * The feature id for the '<em><b>Created By</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__CREATED_BY = LINKED_OBJECT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Created By Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__CREATED_BY_EXTENSION = LINKED_OBJECT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Modified By</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__MODIFIED_BY = LINKED_OBJECT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Modified By Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__MODIFIED_BY_EXTENSION = LINKED_OBJECT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Creation Date</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__CREATION_DATE = LINKED_OBJECT_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Creation Date Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__CREATION_DATE_EXTENSION = LINKED_OBJECT_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Modification Date</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__MODIFICATION_DATE = LINKED_OBJECT_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Modification Date Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__MODIFICATION_DATE_EXTENSION = LINKED_OBJECT_FEATURE_COUNT + 7;

  /**
   * The feature id for the '<em><b>Secondary Ids</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__SECONDARY_IDS = LINKED_OBJECT_FEATURE_COUNT + 8;

  /**
   * The feature id for the '<em><b>Secondary Id Extension</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__SECONDARY_ID_EXTENSION = LINKED_OBJECT_FEATURE_COUNT + 9;

  /**
   * The feature id for the '<em><b>Synonyms</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__SYNONYMS = LINKED_OBJECT_FEATURE_COUNT + 10;

  /**
   * The feature id for the '<em><b>Dbxrefs</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__DBXREFS = LINKED_OBJECT_FEATURE_COUNT + 11;

  /**
   * The feature id for the '<em><b>Comment</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__COMMENT = LINKED_OBJECT_FEATURE_COUNT + 12;

  /**
   * The feature id for the '<em><b>Comment Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__COMMENT_EXTENSION = LINKED_OBJECT_FEATURE_COUNT + 13;

  /**
   * The feature id for the '<em><b>Obsolete</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__OBSOLETE = LINKED_OBJECT_FEATURE_COUNT + 14;

  /**
   * The feature id for the '<em><b>Replaced By</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__REPLACED_BY = LINKED_OBJECT_FEATURE_COUNT + 15;

  /**
   * The feature id for the '<em><b>Consider Replacements</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__CONSIDER_REPLACEMENTS = LINKED_OBJECT_FEATURE_COUNT + 16;

  /**
   * The feature id for the '<em><b>Consider Extension</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__CONSIDER_EXTENSION = LINKED_OBJECT_FEATURE_COUNT + 17;

  /**
   * The feature id for the '<em><b>Replaced By Extension</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__REPLACED_BY_EXTENSION = LINKED_OBJECT_FEATURE_COUNT + 18;

  /**
   * The feature id for the '<em><b>Obsolete Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__OBSOLETE_EXTENSION = LINKED_OBJECT_FEATURE_COUNT + 19;

  /**
   * The feature id for the '<em><b>Definition</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__DEFINITION = LINKED_OBJECT_FEATURE_COUNT + 20;

  /**
   * The feature id for the '<em><b>Def Dbxrefs</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__DEF_DBXREFS = LINKED_OBJECT_FEATURE_COUNT + 21;

  /**
   * The feature id for the '<em><b>Definition Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__DEFINITION_EXTENSION = LINKED_OBJECT_FEATURE_COUNT + 22;

  /**
   * The feature id for the '<em><b>Subsets</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__SUBSETS = LINKED_OBJECT_FEATURE_COUNT + 23;

  /**
   * The feature id for the '<em><b>Category Extensions</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__CATEGORY_EXTENSIONS = LINKED_OBJECT_FEATURE_COUNT + 24;

  /**
   * The feature id for the '<em><b>Cyclic</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__CYCLIC = LINKED_OBJECT_FEATURE_COUNT + 25;

  /**
   * The feature id for the '<em><b>Symmetric</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__SYMMETRIC = LINKED_OBJECT_FEATURE_COUNT + 26;

  /**
   * The feature id for the '<em><b>Transitive</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__TRANSITIVE = LINKED_OBJECT_FEATURE_COUNT + 27;

  /**
   * The feature id for the '<em><b>Reflexive</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__REFLEXIVE = LINKED_OBJECT_FEATURE_COUNT + 28;

  /**
   * The feature id for the '<em><b>Always Implies Inverse</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__ALWAYS_IMPLIES_INVERSE = LINKED_OBJECT_FEATURE_COUNT + 29;

  /**
   * The feature id for the '<em><b>Dummy</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__DUMMY = LINKED_OBJECT_FEATURE_COUNT + 30;

  /**
   * The feature id for the '<em><b>Metadata Tag</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__METADATA_TAG = LINKED_OBJECT_FEATURE_COUNT + 31;

  /**
   * The feature id for the '<em><b>Cyclic Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__CYCLIC_EXTENSION = LINKED_OBJECT_FEATURE_COUNT + 32;

  /**
   * The feature id for the '<em><b>Symmetric Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__SYMMETRIC_EXTENSION = LINKED_OBJECT_FEATURE_COUNT + 33;

  /**
   * The feature id for the '<em><b>Transitive Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__TRANSITIVE_EXTENSION = LINKED_OBJECT_FEATURE_COUNT + 34;

  /**
   * The feature id for the '<em><b>Reflexive Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__REFLEXIVE_EXTENSION = LINKED_OBJECT_FEATURE_COUNT + 35;

  /**
   * The feature id for the '<em><b>Always Implies Inverse Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION = LINKED_OBJECT_FEATURE_COUNT + 36;

  /**
   * The feature id for the '<em><b>Domain Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__DOMAIN_EXTENSION = LINKED_OBJECT_FEATURE_COUNT + 37;

  /**
   * The feature id for the '<em><b>Range Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__RANGE_EXTENSION = LINKED_OBJECT_FEATURE_COUNT + 38;

  /**
   * The feature id for the '<em><b>Range</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__RANGE = LINKED_OBJECT_FEATURE_COUNT + 39;

  /**
   * The feature id for the '<em><b>Domain</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__DOMAIN = LINKED_OBJECT_FEATURE_COUNT + 40;

  /**
   * The feature id for the '<em><b>Non Inheritable</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__NON_INHERITABLE = LINKED_OBJECT_FEATURE_COUNT + 41;

  /**
   * The feature id for the '<em><b>Disjoint Over</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__DISJOINT_OVER = LINKED_OBJECT_FEATURE_COUNT + 42;

  /**
   * The feature id for the '<em><b>Transitive Over</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__TRANSITIVE_OVER = LINKED_OBJECT_FEATURE_COUNT + 43;

  /**
   * The feature id for the '<em><b>Holds Over Chains</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY__HOLDS_OVER_CHAINS = LINKED_OBJECT_FEATURE_COUNT + 44;

  /**
   * The number of structural features of the '<em>Dangling Property</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY_FEATURE_COUNT = LINKED_OBJECT_FEATURE_COUNT + 45;

  /**
   * The number of operations of the '<em>Dangling Property</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DANGLING_PROPERTY_OPERATION_COUNT = LINKED_OBJECT_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__TYPE = IDENTIFIED_OBJECT__TYPE;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__ID = IDENTIFIED_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__ANONYMOUS = IDENTIFIED_OBJECT__ANONYMOUS;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__NAMESPACE = IDENTIFIED_OBJECT__NAMESPACE;

  /**
   * The feature id for the '<em><b>Namespace Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__NAMESPACE_EXTENSION = IDENTIFIED_OBJECT__NAMESPACE_EXTENSION;

  /**
   * The feature id for the '<em><b>Type Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__TYPE_EXTENSION = IDENTIFIED_OBJECT__TYPE_EXTENSION;

  /**
   * The feature id for the '<em><b>Built In</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__BUILT_IN = IDENTIFIED_OBJECT__BUILT_IN;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__NAME = IDENTIFIED_OBJECT__NAME;

  /**
   * The feature id for the '<em><b>Name Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__NAME_EXTENSION = IDENTIFIED_OBJECT__NAME_EXTENSION;

  /**
   * The feature id for the '<em><b>Id Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__ID_EXTENSION = IDENTIFIED_OBJECT__ID_EXTENSION;

  /**
   * The feature id for the '<em><b>Anonymous Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__ANONYMOUS_EXTENSION = IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION;

  /**
   * The feature id for the '<em><b>Property Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__PROPERTY_VALUES = IDENTIFIED_OBJECT__PROPERTY_VALUES;

  /**
   * The number of structural features of the '<em>Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_FEATURE_COUNT = IDENTIFIED_OBJECT_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_OPERATION_COUNT = IDENTIFIED_OBJECT_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE__TYPE = TYPE__TYPE;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE__ID = TYPE__ID;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE__ANONYMOUS = TYPE__ANONYMOUS;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE__NAMESPACE = TYPE__NAMESPACE;

  /**
   * The feature id for the '<em><b>Namespace Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE__NAMESPACE_EXTENSION = TYPE__NAMESPACE_EXTENSION;

  /**
   * The feature id for the '<em><b>Type Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE__TYPE_EXTENSION = TYPE__TYPE_EXTENSION;

  /**
   * The feature id for the '<em><b>Built In</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE__BUILT_IN = TYPE__BUILT_IN;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE__NAME = TYPE__NAME;

  /**
   * The feature id for the '<em><b>Name Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE__NAME_EXTENSION = TYPE__NAME_EXTENSION;

  /**
   * The feature id for the '<em><b>Id Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE__ID_EXTENSION = TYPE__ID_EXTENSION;

  /**
   * The feature id for the '<em><b>Anonymous Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE__ANONYMOUS_EXTENSION = TYPE__ANONYMOUS_EXTENSION;

  /**
   * The feature id for the '<em><b>Property Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE__PROPERTY_VALUES = TYPE__PROPERTY_VALUES;

  /**
   * The feature id for the '<em><b>Abstract</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE__ABSTRACT = TYPE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Datatype</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE_FEATURE_COUNT = TYPE_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Datatype</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE_OPERATION_COUNT = TYPE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DatatypeValueImpl <em>Datatype Value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DatatypeValueImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getDatatypeValue()
   * @generated
   */
  int DATATYPE_VALUE = 5;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE_VALUE__TYPE = VALUE__TYPE;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE_VALUE__VALUE = VALUE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Datatype Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Datatype Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATATYPE_VALUE_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DbxrefImpl <em>Dbxref</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DbxrefImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getDbxref()
   * @generated
   */
  int DBXREF = 6;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DBXREF__ID = CLONEABLE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DBXREF__ANONYMOUS = CLONEABLE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Nested Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DBXREF__NESTED_VALUE = CLONEABLE_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Def Ref</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DBXREF__DEF_REF = CLONEABLE_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DBXREF__DESC = CLONEABLE_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DBXREF__TYPE = CLONEABLE_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Synonym</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DBXREF__SYNONYM = CLONEABLE_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Database ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DBXREF__DATABASE_ID = CLONEABLE_FEATURE_COUNT + 7;

  /**
   * The feature id for the '<em><b>Database</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DBXREF__DATABASE = CLONEABLE_FEATURE_COUNT + 8;

  /**
   * The number of structural features of the '<em>Dbxref</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DBXREF_FEATURE_COUNT = CLONEABLE_FEATURE_COUNT + 9;

  /**
   * The number of operations of the '<em>Dbxref</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DBXREF_OPERATION_COUNT = CLONEABLE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DbxrefedObjectImpl <em>Dbxrefed Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DbxrefedObjectImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getDbxrefedObject()
   * @generated
   */
  int DBXREFED_OBJECT = 7;

  /**
   * The feature id for the '<em><b>Dbxrefs</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DBXREFED_OBJECT__DBXREFS = 0;

  /**
   * The number of structural features of the '<em>Dbxrefed Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DBXREFED_OBJECT_FEATURE_COUNT = 1;

  /**
   * The number of operations of the '<em>Dbxrefed Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DBXREFED_OBJECT_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DefinedObjectImpl <em>Defined Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DefinedObjectImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getDefinedObject()
   * @generated
   */
  int DEFINED_OBJECT = 8;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINED_OBJECT__TYPE = IDENTIFIED_OBJECT__TYPE;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINED_OBJECT__ID = IDENTIFIED_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINED_OBJECT__ANONYMOUS = IDENTIFIED_OBJECT__ANONYMOUS;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINED_OBJECT__NAMESPACE = IDENTIFIED_OBJECT__NAMESPACE;

  /**
   * The feature id for the '<em><b>Namespace Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINED_OBJECT__NAMESPACE_EXTENSION = IDENTIFIED_OBJECT__NAMESPACE_EXTENSION;

  /**
   * The feature id for the '<em><b>Type Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINED_OBJECT__TYPE_EXTENSION = IDENTIFIED_OBJECT__TYPE_EXTENSION;

  /**
   * The feature id for the '<em><b>Built In</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINED_OBJECT__BUILT_IN = IDENTIFIED_OBJECT__BUILT_IN;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINED_OBJECT__NAME = IDENTIFIED_OBJECT__NAME;

  /**
   * The feature id for the '<em><b>Name Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINED_OBJECT__NAME_EXTENSION = IDENTIFIED_OBJECT__NAME_EXTENSION;

  /**
   * The feature id for the '<em><b>Id Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINED_OBJECT__ID_EXTENSION = IDENTIFIED_OBJECT__ID_EXTENSION;

  /**
   * The feature id for the '<em><b>Anonymous Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINED_OBJECT__ANONYMOUS_EXTENSION = IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION;

  /**
   * The feature id for the '<em><b>Property Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINED_OBJECT__PROPERTY_VALUES = IDENTIFIED_OBJECT__PROPERTY_VALUES;

  /**
   * The feature id for the '<em><b>Definition</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINED_OBJECT__DEFINITION = IDENTIFIED_OBJECT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Def Dbxrefs</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINED_OBJECT__DEF_DBXREFS = IDENTIFIED_OBJECT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Definition Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINED_OBJECT__DEFINITION_EXTENSION = IDENTIFIED_OBJECT_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Defined Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINED_OBJECT_FEATURE_COUNT = IDENTIFIED_OBJECT_FEATURE_COUNT + 3;

  /**
   * The number of operations of the '<em>Defined Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEFINED_OBJECT_OPERATION_COUNT = IDENTIFIED_OBJECT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.FieldPathImpl <em>Field Path</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.FieldPathImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getFieldPath()
   * @generated
   */
  int FIELD_PATH = 9;

  /**
   * The number of structural features of the '<em>Field Path</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_PATH_FEATURE_COUNT = 0;

  /**
   * The number of operations of the '<em>Field Path</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_PATH_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.FieldPathSpecImpl <em>Field Path Spec</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.FieldPathSpecImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getFieldPathSpec()
   * @generated
   */
  int FIELD_PATH_SPEC = 10;

  /**
   * The number of structural features of the '<em>Field Path Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_PATH_SPEC_FEATURE_COUNT = 0;

  /**
   * The number of operations of the '<em>Field Path Spec</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIELD_PATH_SPEC_OPERATION_COUNT = 0;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIABLE_OBJECT__ID = 0;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIABLE_OBJECT__ANONYMOUS = 1;

  /**
   * The number of structural features of the '<em>Identifiable Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIABLE_OBJECT_FEATURE_COUNT = 2;

  /**
   * The number of operations of the '<em>Identifiable Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIABLE_OBJECT_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiedObjectIndexImpl <em>Identified Object Index</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiedObjectIndexImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getIdentifiedObjectIndex()
   * @generated
   */
  int IDENTIFIED_OBJECT_INDEX = 13;

  /**
   * The number of structural features of the '<em>Identified Object Index</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT = 0;

  /**
   * The number of operations of the '<em>Identified Object Index</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIED_OBJECT_INDEX_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ImpliableImpl <em>Impliable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ImpliableImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getImpliable()
   * @generated
   */
  int IMPLIABLE = 14;

  /**
   * The feature id for the '<em><b>Implied</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPLIABLE__IMPLIED = 0;

  /**
   * The number of structural features of the '<em>Impliable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPLIABLE_FEATURE_COUNT = 1;

  /**
   * The number of operations of the '<em>Impliable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMPLIABLE_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOObjectImpl <em>OBO Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOObjectImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getOBOObject()
   * @generated
   */
  int OBO_OBJECT = 27;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__TYPE = ANNOTATED_OBJECT__TYPE;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__ID = ANNOTATED_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__ANONYMOUS = ANNOTATED_OBJECT__ANONYMOUS;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__NAMESPACE = ANNOTATED_OBJECT__NAMESPACE;

  /**
   * The feature id for the '<em><b>Namespace Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__NAMESPACE_EXTENSION = ANNOTATED_OBJECT__NAMESPACE_EXTENSION;

  /**
   * The feature id for the '<em><b>Type Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__TYPE_EXTENSION = ANNOTATED_OBJECT__TYPE_EXTENSION;

  /**
   * The feature id for the '<em><b>Built In</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__BUILT_IN = ANNOTATED_OBJECT__BUILT_IN;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__NAME = ANNOTATED_OBJECT__NAME;

  /**
   * The feature id for the '<em><b>Name Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__NAME_EXTENSION = ANNOTATED_OBJECT__NAME_EXTENSION;

  /**
   * The feature id for the '<em><b>Id Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__ID_EXTENSION = ANNOTATED_OBJECT__ID_EXTENSION;

  /**
   * The feature id for the '<em><b>Anonymous Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__ANONYMOUS_EXTENSION = ANNOTATED_OBJECT__ANONYMOUS_EXTENSION;

  /**
   * The feature id for the '<em><b>Property Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__PROPERTY_VALUES = ANNOTATED_OBJECT__PROPERTY_VALUES;

  /**
   * The feature id for the '<em><b>Created By</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__CREATED_BY = ANNOTATED_OBJECT__CREATED_BY;

  /**
   * The feature id for the '<em><b>Created By Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__CREATED_BY_EXTENSION = ANNOTATED_OBJECT__CREATED_BY_EXTENSION;

  /**
   * The feature id for the '<em><b>Modified By</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__MODIFIED_BY = ANNOTATED_OBJECT__MODIFIED_BY;

  /**
   * The feature id for the '<em><b>Modified By Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__MODIFIED_BY_EXTENSION = ANNOTATED_OBJECT__MODIFIED_BY_EXTENSION;

  /**
   * The feature id for the '<em><b>Creation Date</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__CREATION_DATE = ANNOTATED_OBJECT__CREATION_DATE;

  /**
   * The feature id for the '<em><b>Creation Date Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__CREATION_DATE_EXTENSION = ANNOTATED_OBJECT__CREATION_DATE_EXTENSION;

  /**
   * The feature id for the '<em><b>Modification Date</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__MODIFICATION_DATE = ANNOTATED_OBJECT__MODIFICATION_DATE;

  /**
   * The feature id for the '<em><b>Modification Date Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__MODIFICATION_DATE_EXTENSION = ANNOTATED_OBJECT__MODIFICATION_DATE_EXTENSION;

  /**
   * The feature id for the '<em><b>Secondary Ids</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__SECONDARY_IDS = ANNOTATED_OBJECT__SECONDARY_IDS;

  /**
   * The feature id for the '<em><b>Secondary Id Extension</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__SECONDARY_ID_EXTENSION = ANNOTATED_OBJECT__SECONDARY_ID_EXTENSION;

  /**
   * The feature id for the '<em><b>Synonyms</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__SYNONYMS = ANNOTATED_OBJECT__SYNONYMS;

  /**
   * The feature id for the '<em><b>Dbxrefs</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__DBXREFS = ANNOTATED_OBJECT__DBXREFS;

  /**
   * The feature id for the '<em><b>Comment</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__COMMENT = ANNOTATED_OBJECT__COMMENT;

  /**
   * The feature id for the '<em><b>Comment Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__COMMENT_EXTENSION = ANNOTATED_OBJECT__COMMENT_EXTENSION;

  /**
   * The feature id for the '<em><b>Obsolete</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__OBSOLETE = ANNOTATED_OBJECT__OBSOLETE;

  /**
   * The feature id for the '<em><b>Replaced By</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__REPLACED_BY = ANNOTATED_OBJECT__REPLACED_BY;

  /**
   * The feature id for the '<em><b>Consider Replacements</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__CONSIDER_REPLACEMENTS = ANNOTATED_OBJECT__CONSIDER_REPLACEMENTS;

  /**
   * The feature id for the '<em><b>Consider Extension</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__CONSIDER_EXTENSION = ANNOTATED_OBJECT__CONSIDER_EXTENSION;

  /**
   * The feature id for the '<em><b>Replaced By Extension</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__REPLACED_BY_EXTENSION = ANNOTATED_OBJECT__REPLACED_BY_EXTENSION;

  /**
   * The feature id for the '<em><b>Obsolete Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__OBSOLETE_EXTENSION = ANNOTATED_OBJECT__OBSOLETE_EXTENSION;

  /**
   * The feature id for the '<em><b>Definition</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__DEFINITION = ANNOTATED_OBJECT__DEFINITION;

  /**
   * The feature id for the '<em><b>Def Dbxrefs</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__DEF_DBXREFS = ANNOTATED_OBJECT__DEF_DBXREFS;

  /**
   * The feature id for the '<em><b>Definition Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__DEFINITION_EXTENSION = ANNOTATED_OBJECT__DEFINITION_EXTENSION;

  /**
   * The feature id for the '<em><b>Subsets</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__SUBSETS = ANNOTATED_OBJECT__SUBSETS;

  /**
   * The feature id for the '<em><b>Category Extensions</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__CATEGORY_EXTENSIONS = ANNOTATED_OBJECT__CATEGORY_EXTENSIONS;

  /**
   * The feature id for the '<em><b>Parents</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__PARENTS = ANNOTATED_OBJECT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Children</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT__CHILDREN = ANNOTATED_OBJECT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>OBO Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT_FEATURE_COUNT = ANNOTATED_OBJECT_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>OBO Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_OBJECT_OPERATION_COUNT = ANNOTATED_OBJECT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.InstanceImpl <em>Instance</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.InstanceImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getInstance()
   * @generated
   */
  int INSTANCE = 15;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__TYPE = OBO_OBJECT__TYPE;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__ID = OBO_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__ANONYMOUS = OBO_OBJECT__ANONYMOUS;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__NAMESPACE = OBO_OBJECT__NAMESPACE;

  /**
   * The feature id for the '<em><b>Namespace Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__NAMESPACE_EXTENSION = OBO_OBJECT__NAMESPACE_EXTENSION;

  /**
   * The feature id for the '<em><b>Type Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__TYPE_EXTENSION = OBO_OBJECT__TYPE_EXTENSION;

  /**
   * The feature id for the '<em><b>Built In</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__BUILT_IN = OBO_OBJECT__BUILT_IN;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__NAME = OBO_OBJECT__NAME;

  /**
   * The feature id for the '<em><b>Name Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__NAME_EXTENSION = OBO_OBJECT__NAME_EXTENSION;

  /**
   * The feature id for the '<em><b>Id Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__ID_EXTENSION = OBO_OBJECT__ID_EXTENSION;

  /**
   * The feature id for the '<em><b>Anonymous Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__ANONYMOUS_EXTENSION = OBO_OBJECT__ANONYMOUS_EXTENSION;

  /**
   * The feature id for the '<em><b>Property Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__PROPERTY_VALUES = OBO_OBJECT__PROPERTY_VALUES;

  /**
   * The feature id for the '<em><b>Created By</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__CREATED_BY = OBO_OBJECT__CREATED_BY;

  /**
   * The feature id for the '<em><b>Created By Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__CREATED_BY_EXTENSION = OBO_OBJECT__CREATED_BY_EXTENSION;

  /**
   * The feature id for the '<em><b>Modified By</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__MODIFIED_BY = OBO_OBJECT__MODIFIED_BY;

  /**
   * The feature id for the '<em><b>Modified By Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__MODIFIED_BY_EXTENSION = OBO_OBJECT__MODIFIED_BY_EXTENSION;

  /**
   * The feature id for the '<em><b>Creation Date</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__CREATION_DATE = OBO_OBJECT__CREATION_DATE;

  /**
   * The feature id for the '<em><b>Creation Date Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__CREATION_DATE_EXTENSION = OBO_OBJECT__CREATION_DATE_EXTENSION;

  /**
   * The feature id for the '<em><b>Modification Date</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__MODIFICATION_DATE = OBO_OBJECT__MODIFICATION_DATE;

  /**
   * The feature id for the '<em><b>Modification Date Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__MODIFICATION_DATE_EXTENSION = OBO_OBJECT__MODIFICATION_DATE_EXTENSION;

  /**
   * The feature id for the '<em><b>Secondary Ids</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__SECONDARY_IDS = OBO_OBJECT__SECONDARY_IDS;

  /**
   * The feature id for the '<em><b>Secondary Id Extension</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__SECONDARY_ID_EXTENSION = OBO_OBJECT__SECONDARY_ID_EXTENSION;

  /**
   * The feature id for the '<em><b>Synonyms</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__SYNONYMS = OBO_OBJECT__SYNONYMS;

  /**
   * The feature id for the '<em><b>Dbxrefs</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__DBXREFS = OBO_OBJECT__DBXREFS;

  /**
   * The feature id for the '<em><b>Comment</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__COMMENT = OBO_OBJECT__COMMENT;

  /**
   * The feature id for the '<em><b>Comment Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__COMMENT_EXTENSION = OBO_OBJECT__COMMENT_EXTENSION;

  /**
   * The feature id for the '<em><b>Obsolete</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__OBSOLETE = OBO_OBJECT__OBSOLETE;

  /**
   * The feature id for the '<em><b>Replaced By</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__REPLACED_BY = OBO_OBJECT__REPLACED_BY;

  /**
   * The feature id for the '<em><b>Consider Replacements</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__CONSIDER_REPLACEMENTS = OBO_OBJECT__CONSIDER_REPLACEMENTS;

  /**
   * The feature id for the '<em><b>Consider Extension</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__CONSIDER_EXTENSION = OBO_OBJECT__CONSIDER_EXTENSION;

  /**
   * The feature id for the '<em><b>Replaced By Extension</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__REPLACED_BY_EXTENSION = OBO_OBJECT__REPLACED_BY_EXTENSION;

  /**
   * The feature id for the '<em><b>Obsolete Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__OBSOLETE_EXTENSION = OBO_OBJECT__OBSOLETE_EXTENSION;

  /**
   * The feature id for the '<em><b>Definition</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__DEFINITION = OBO_OBJECT__DEFINITION;

  /**
   * The feature id for the '<em><b>Def Dbxrefs</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__DEF_DBXREFS = OBO_OBJECT__DEF_DBXREFS;

  /**
   * The feature id for the '<em><b>Definition Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__DEFINITION_EXTENSION = OBO_OBJECT__DEFINITION_EXTENSION;

  /**
   * The feature id for the '<em><b>Subsets</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__SUBSETS = OBO_OBJECT__SUBSETS;

  /**
   * The feature id for the '<em><b>Category Extensions</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__CATEGORY_EXTENSIONS = OBO_OBJECT__CATEGORY_EXTENSIONS;

  /**
   * The feature id for the '<em><b>Parents</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__PARENTS = OBO_OBJECT__PARENTS;

  /**
   * The feature id for the '<em><b>Children</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__CHILDREN = OBO_OBJECT__CHILDREN;

  /**
   * The feature id for the '<em><b>Obo Properties Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE__OBO_PROPERTIES_VALUES = OBO_OBJECT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Instance</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE_FEATURE_COUNT = OBO_OBJECT_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Instance</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCE_OPERATION_COUNT = OBO_OBJECT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkImpl <em>Link</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getLink()
   * @generated
   */
  int LINK = 16;

  /**
   * The feature id for the '<em><b>Implied</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK__IMPLIED = IMPLIABLE__IMPLIED;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK__ID = IMPLIABLE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK__ANONYMOUS = IMPLIABLE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Child</b></em>' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK__CHILD = IMPLIABLE_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Parent</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK__PARENT = IMPLIABLE_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK__TYPE = IMPLIABLE_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Nested Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK__NESTED_VALUE = IMPLIABLE_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK__NAMESPACE = IMPLIABLE_FEATURE_COUNT + 6;

  /**
   * The number of structural features of the '<em>Link</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_FEATURE_COUNT = IMPLIABLE_FEATURE_COUNT + 7;

  /**
   * The number of operations of the '<em>Link</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_OPERATION_COUNT = IMPLIABLE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkDatabaseImpl <em>Link Database</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkDatabaseImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getLinkDatabase()
   * @generated
   */
  int LINK_DATABASE = 17;

  /**
   * The feature id for the '<em><b>Session</b></em>' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_DATABASE__SESSION = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Objects</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_DATABASE__OBJECTS = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Properties</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_DATABASE__PROPERTIES = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Link Database</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_DATABASE_FEATURE_COUNT = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 3;

  /**
   * The number of operations of the '<em>Link Database</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_DATABASE_OPERATION_COUNT = IDENTIFIED_OBJECT_INDEX_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkLinkedObjectImpl <em>Link Linked Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkLinkedObjectImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getLinkLinkedObject()
   * @generated
   */
  int LINK_LINKED_OBJECT = 18;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_LINKED_OBJECT__TYPE = LINKED_OBJECT__TYPE;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_LINKED_OBJECT__ID = LINKED_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_LINKED_OBJECT__ANONYMOUS = LINKED_OBJECT__ANONYMOUS;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_LINKED_OBJECT__NAMESPACE = LINKED_OBJECT__NAMESPACE;

  /**
   * The feature id for the '<em><b>Namespace Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_LINKED_OBJECT__NAMESPACE_EXTENSION = LINKED_OBJECT__NAMESPACE_EXTENSION;

  /**
   * The feature id for the '<em><b>Type Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_LINKED_OBJECT__TYPE_EXTENSION = LINKED_OBJECT__TYPE_EXTENSION;

  /**
   * The feature id for the '<em><b>Built In</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_LINKED_OBJECT__BUILT_IN = LINKED_OBJECT__BUILT_IN;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_LINKED_OBJECT__NAME = LINKED_OBJECT__NAME;

  /**
   * The feature id for the '<em><b>Name Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_LINKED_OBJECT__NAME_EXTENSION = LINKED_OBJECT__NAME_EXTENSION;

  /**
   * The feature id for the '<em><b>Id Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_LINKED_OBJECT__ID_EXTENSION = LINKED_OBJECT__ID_EXTENSION;

  /**
   * The feature id for the '<em><b>Anonymous Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_LINKED_OBJECT__ANONYMOUS_EXTENSION = LINKED_OBJECT__ANONYMOUS_EXTENSION;

  /**
   * The feature id for the '<em><b>Property Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_LINKED_OBJECT__PROPERTY_VALUES = LINKED_OBJECT__PROPERTY_VALUES;

  /**
   * The feature id for the '<em><b>Parents</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_LINKED_OBJECT__PARENTS = LINKED_OBJECT__PARENTS;

  /**
   * The feature id for the '<em><b>Children</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_LINKED_OBJECT__CHILDREN = LINKED_OBJECT__CHILDREN;

  /**
   * The feature id for the '<em><b>Link</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_LINKED_OBJECT__LINK = LINKED_OBJECT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Link Linked Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_LINKED_OBJECT_FEATURE_COUNT = LINKED_OBJECT_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Link Linked Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LINK_LINKED_OBJECT_OPERATION_COUNT = LINKED_OBJECT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ModificationMetadataObjectImpl <em>Modification Metadata Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ModificationMetadataObjectImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getModificationMetadataObject()
   * @generated
   */
  int MODIFICATION_METADATA_OBJECT = 20;

  /**
   * The feature id for the '<em><b>Created By</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFICATION_METADATA_OBJECT__CREATED_BY = 0;

  /**
   * The feature id for the '<em><b>Created By Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION = 1;

  /**
   * The feature id for the '<em><b>Modified By</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFICATION_METADATA_OBJECT__MODIFIED_BY = 2;

  /**
   * The feature id for the '<em><b>Modified By Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION = 3;

  /**
   * The feature id for the '<em><b>Creation Date</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFICATION_METADATA_OBJECT__CREATION_DATE = 4;

  /**
   * The feature id for the '<em><b>Creation Date Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION = 5;

  /**
   * The feature id for the '<em><b>Modification Date</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE = 6;

  /**
   * The feature id for the '<em><b>Modification Date Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION = 7;

  /**
   * The number of structural features of the '<em>Modification Metadata Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFICATION_METADATA_OBJECT_FEATURE_COUNT = 8;

  /**
   * The number of operations of the '<em>Modification Metadata Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODIFICATION_METADATA_OBJECT_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.MultiIDObjectImpl <em>Multi ID Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.MultiIDObjectImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getMultiIDObject()
   * @generated
   */
  int MULTI_ID_OBJECT = 21;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTI_ID_OBJECT__TYPE = IDENTIFIED_OBJECT__TYPE;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTI_ID_OBJECT__ID = IDENTIFIED_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTI_ID_OBJECT__ANONYMOUS = IDENTIFIED_OBJECT__ANONYMOUS;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTI_ID_OBJECT__NAMESPACE = IDENTIFIED_OBJECT__NAMESPACE;

  /**
   * The feature id for the '<em><b>Namespace Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTI_ID_OBJECT__NAMESPACE_EXTENSION = IDENTIFIED_OBJECT__NAMESPACE_EXTENSION;

  /**
   * The feature id for the '<em><b>Type Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTI_ID_OBJECT__TYPE_EXTENSION = IDENTIFIED_OBJECT__TYPE_EXTENSION;

  /**
   * The feature id for the '<em><b>Built In</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTI_ID_OBJECT__BUILT_IN = IDENTIFIED_OBJECT__BUILT_IN;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTI_ID_OBJECT__NAME = IDENTIFIED_OBJECT__NAME;

  /**
   * The feature id for the '<em><b>Name Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTI_ID_OBJECT__NAME_EXTENSION = IDENTIFIED_OBJECT__NAME_EXTENSION;

  /**
   * The feature id for the '<em><b>Id Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTI_ID_OBJECT__ID_EXTENSION = IDENTIFIED_OBJECT__ID_EXTENSION;

  /**
   * The feature id for the '<em><b>Anonymous Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTI_ID_OBJECT__ANONYMOUS_EXTENSION = IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION;

  /**
   * The feature id for the '<em><b>Property Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTI_ID_OBJECT__PROPERTY_VALUES = IDENTIFIED_OBJECT__PROPERTY_VALUES;

  /**
   * The feature id for the '<em><b>Secondary Ids</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTI_ID_OBJECT__SECONDARY_IDS = IDENTIFIED_OBJECT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Secondary Id Extension</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTI_ID_OBJECT__SECONDARY_ID_EXTENSION = IDENTIFIED_OBJECT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Multi ID Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTI_ID_OBJECT_FEATURE_COUNT = IDENTIFIED_OBJECT_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>Multi ID Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTI_ID_OBJECT_OPERATION_COUNT = IDENTIFIED_OBJECT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.MutableLinkDatabaseImpl <em>Mutable Link Database</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.MutableLinkDatabaseImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getMutableLinkDatabase()
   * @generated
   */
  int MUTABLE_LINK_DATABASE = 22;

  /**
   * The feature id for the '<em><b>Session</b></em>' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MUTABLE_LINK_DATABASE__SESSION = LINK_DATABASE__SESSION;

  /**
   * The feature id for the '<em><b>Objects</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MUTABLE_LINK_DATABASE__OBJECTS = LINK_DATABASE__OBJECTS;

  /**
   * The feature id for the '<em><b>Properties</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MUTABLE_LINK_DATABASE__PROPERTIES = LINK_DATABASE__PROPERTIES;

  /**
   * The feature id for the '<em><b>Identified Object Index</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MUTABLE_LINK_DATABASE__IDENTIFIED_OBJECT_INDEX = LINK_DATABASE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Mutable Link Database</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MUTABLE_LINK_DATABASE_FEATURE_COUNT = LINK_DATABASE_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Mutable Link Database</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MUTABLE_LINK_DATABASE_OPERATION_COUNT = LINK_DATABASE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.NamespaceImpl <em>Namespace</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.NamespaceImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getNamespace()
   * @generated
   */
  int NAMESPACE = 23;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAMESPACE__ID = 0;

  /**
   * The feature id for the '<em><b>Path</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAMESPACE__PATH = 1;

  /**
   * The feature id for the '<em><b>Private Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAMESPACE__PRIVATE_ID = 2;

  /**
   * The number of structural features of the '<em>Namespace</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAMESPACE_FEATURE_COUNT = 3;

  /**
   * The number of operations of the '<em>Namespace</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAMESPACE_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.NamespacedObjectImpl <em>Namespaced Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.NamespacedObjectImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getNamespacedObject()
   * @generated
   */
  int NAMESPACED_OBJECT = 24;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAMESPACED_OBJECT__NAMESPACE = 0;

  /**
   * The feature id for the '<em><b>Namespace Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAMESPACED_OBJECT__NAMESPACE_EXTENSION = 1;

  /**
   * The number of structural features of the '<em>Namespaced Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAMESPACED_OBJECT_FEATURE_COUNT = 2;

  /**
   * The number of operations of the '<em>Namespaced Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAMESPACED_OBJECT_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.NestedValueImpl <em>Nested Value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.NestedValueImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getNestedValue()
   * @generated
   */
  int NESTED_VALUE = 25;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_VALUE__NAME = CLONEABLE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Property Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_VALUE__PROPERTY_VALUES = CLONEABLE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Suggested Comment</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_VALUE__SUGGESTED_COMMENT = CLONEABLE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Nested Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_VALUE_FEATURE_COUNT = CLONEABLE_FEATURE_COUNT + 3;

  /**
   * The number of operations of the '<em>Nested Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NESTED_VALUE_OPERATION_COUNT = CLONEABLE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOClassImpl <em>OBO Class</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOClassImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getOBOClass()
   * @generated
   */
  int OBO_CLASS = 26;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__TYPE = OBO_OBJECT__TYPE;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__ID = OBO_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__ANONYMOUS = OBO_OBJECT__ANONYMOUS;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__NAMESPACE = OBO_OBJECT__NAMESPACE;

  /**
   * The feature id for the '<em><b>Namespace Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__NAMESPACE_EXTENSION = OBO_OBJECT__NAMESPACE_EXTENSION;

  /**
   * The feature id for the '<em><b>Type Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__TYPE_EXTENSION = OBO_OBJECT__TYPE_EXTENSION;

  /**
   * The feature id for the '<em><b>Built In</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__BUILT_IN = OBO_OBJECT__BUILT_IN;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__NAME = OBO_OBJECT__NAME;

  /**
   * The feature id for the '<em><b>Name Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__NAME_EXTENSION = OBO_OBJECT__NAME_EXTENSION;

  /**
   * The feature id for the '<em><b>Id Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__ID_EXTENSION = OBO_OBJECT__ID_EXTENSION;

  /**
   * The feature id for the '<em><b>Anonymous Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__ANONYMOUS_EXTENSION = OBO_OBJECT__ANONYMOUS_EXTENSION;

  /**
   * The feature id for the '<em><b>Property Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__PROPERTY_VALUES = OBO_OBJECT__PROPERTY_VALUES;

  /**
   * The feature id for the '<em><b>Created By</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__CREATED_BY = OBO_OBJECT__CREATED_BY;

  /**
   * The feature id for the '<em><b>Created By Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__CREATED_BY_EXTENSION = OBO_OBJECT__CREATED_BY_EXTENSION;

  /**
   * The feature id for the '<em><b>Modified By</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__MODIFIED_BY = OBO_OBJECT__MODIFIED_BY;

  /**
   * The feature id for the '<em><b>Modified By Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__MODIFIED_BY_EXTENSION = OBO_OBJECT__MODIFIED_BY_EXTENSION;

  /**
   * The feature id for the '<em><b>Creation Date</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__CREATION_DATE = OBO_OBJECT__CREATION_DATE;

  /**
   * The feature id for the '<em><b>Creation Date Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__CREATION_DATE_EXTENSION = OBO_OBJECT__CREATION_DATE_EXTENSION;

  /**
   * The feature id for the '<em><b>Modification Date</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__MODIFICATION_DATE = OBO_OBJECT__MODIFICATION_DATE;

  /**
   * The feature id for the '<em><b>Modification Date Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__MODIFICATION_DATE_EXTENSION = OBO_OBJECT__MODIFICATION_DATE_EXTENSION;

  /**
   * The feature id for the '<em><b>Secondary Ids</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__SECONDARY_IDS = OBO_OBJECT__SECONDARY_IDS;

  /**
   * The feature id for the '<em><b>Secondary Id Extension</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__SECONDARY_ID_EXTENSION = OBO_OBJECT__SECONDARY_ID_EXTENSION;

  /**
   * The feature id for the '<em><b>Synonyms</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__SYNONYMS = OBO_OBJECT__SYNONYMS;

  /**
   * The feature id for the '<em><b>Dbxrefs</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__DBXREFS = OBO_OBJECT__DBXREFS;

  /**
   * The feature id for the '<em><b>Comment</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__COMMENT = OBO_OBJECT__COMMENT;

  /**
   * The feature id for the '<em><b>Comment Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__COMMENT_EXTENSION = OBO_OBJECT__COMMENT_EXTENSION;

  /**
   * The feature id for the '<em><b>Obsolete</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__OBSOLETE = OBO_OBJECT__OBSOLETE;

  /**
   * The feature id for the '<em><b>Replaced By</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__REPLACED_BY = OBO_OBJECT__REPLACED_BY;

  /**
   * The feature id for the '<em><b>Consider Replacements</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__CONSIDER_REPLACEMENTS = OBO_OBJECT__CONSIDER_REPLACEMENTS;

  /**
   * The feature id for the '<em><b>Consider Extension</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__CONSIDER_EXTENSION = OBO_OBJECT__CONSIDER_EXTENSION;

  /**
   * The feature id for the '<em><b>Replaced By Extension</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__REPLACED_BY_EXTENSION = OBO_OBJECT__REPLACED_BY_EXTENSION;

  /**
   * The feature id for the '<em><b>Obsolete Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__OBSOLETE_EXTENSION = OBO_OBJECT__OBSOLETE_EXTENSION;

  /**
   * The feature id for the '<em><b>Definition</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__DEFINITION = OBO_OBJECT__DEFINITION;

  /**
   * The feature id for the '<em><b>Def Dbxrefs</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__DEF_DBXREFS = OBO_OBJECT__DEF_DBXREFS;

  /**
   * The feature id for the '<em><b>Definition Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__DEFINITION_EXTENSION = OBO_OBJECT__DEFINITION_EXTENSION;

  /**
   * The feature id for the '<em><b>Subsets</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__SUBSETS = OBO_OBJECT__SUBSETS;

  /**
   * The feature id for the '<em><b>Category Extensions</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__CATEGORY_EXTENSIONS = OBO_OBJECT__CATEGORY_EXTENSIONS;

  /**
   * The feature id for the '<em><b>Parents</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__PARENTS = OBO_OBJECT__PARENTS;

  /**
   * The feature id for the '<em><b>Children</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS__CHILDREN = OBO_OBJECT__CHILDREN;

  /**
   * The number of structural features of the '<em>OBO Class</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS_FEATURE_COUNT = OBO_OBJECT_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>OBO Class</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_CLASS_OPERATION_COUNT = OBO_OBJECT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl <em>OBO Property</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getOBOProperty()
   * @generated
   */
  int OBO_PROPERTY = 28;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__TYPE = OBO_OBJECT__TYPE;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__ID = OBO_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__ANONYMOUS = OBO_OBJECT__ANONYMOUS;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__NAMESPACE = OBO_OBJECT__NAMESPACE;

  /**
   * The feature id for the '<em><b>Namespace Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__NAMESPACE_EXTENSION = OBO_OBJECT__NAMESPACE_EXTENSION;

  /**
   * The feature id for the '<em><b>Type Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__TYPE_EXTENSION = OBO_OBJECT__TYPE_EXTENSION;

  /**
   * The feature id for the '<em><b>Built In</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__BUILT_IN = OBO_OBJECT__BUILT_IN;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__NAME = OBO_OBJECT__NAME;

  /**
   * The feature id for the '<em><b>Name Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__NAME_EXTENSION = OBO_OBJECT__NAME_EXTENSION;

  /**
   * The feature id for the '<em><b>Id Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__ID_EXTENSION = OBO_OBJECT__ID_EXTENSION;

  /**
   * The feature id for the '<em><b>Anonymous Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__ANONYMOUS_EXTENSION = OBO_OBJECT__ANONYMOUS_EXTENSION;

  /**
   * The feature id for the '<em><b>Property Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__PROPERTY_VALUES = OBO_OBJECT__PROPERTY_VALUES;

  /**
   * The feature id for the '<em><b>Created By</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__CREATED_BY = OBO_OBJECT__CREATED_BY;

  /**
   * The feature id for the '<em><b>Created By Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__CREATED_BY_EXTENSION = OBO_OBJECT__CREATED_BY_EXTENSION;

  /**
   * The feature id for the '<em><b>Modified By</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__MODIFIED_BY = OBO_OBJECT__MODIFIED_BY;

  /**
   * The feature id for the '<em><b>Modified By Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__MODIFIED_BY_EXTENSION = OBO_OBJECT__MODIFIED_BY_EXTENSION;

  /**
   * The feature id for the '<em><b>Creation Date</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__CREATION_DATE = OBO_OBJECT__CREATION_DATE;

  /**
   * The feature id for the '<em><b>Creation Date Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__CREATION_DATE_EXTENSION = OBO_OBJECT__CREATION_DATE_EXTENSION;

  /**
   * The feature id for the '<em><b>Modification Date</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__MODIFICATION_DATE = OBO_OBJECT__MODIFICATION_DATE;

  /**
   * The feature id for the '<em><b>Modification Date Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__MODIFICATION_DATE_EXTENSION = OBO_OBJECT__MODIFICATION_DATE_EXTENSION;

  /**
   * The feature id for the '<em><b>Secondary Ids</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__SECONDARY_IDS = OBO_OBJECT__SECONDARY_IDS;

  /**
   * The feature id for the '<em><b>Secondary Id Extension</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__SECONDARY_ID_EXTENSION = OBO_OBJECT__SECONDARY_ID_EXTENSION;

  /**
   * The feature id for the '<em><b>Synonyms</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__SYNONYMS = OBO_OBJECT__SYNONYMS;

  /**
   * The feature id for the '<em><b>Dbxrefs</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__DBXREFS = OBO_OBJECT__DBXREFS;

  /**
   * The feature id for the '<em><b>Comment</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__COMMENT = OBO_OBJECT__COMMENT;

  /**
   * The feature id for the '<em><b>Comment Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__COMMENT_EXTENSION = OBO_OBJECT__COMMENT_EXTENSION;

  /**
   * The feature id for the '<em><b>Obsolete</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__OBSOLETE = OBO_OBJECT__OBSOLETE;

  /**
   * The feature id for the '<em><b>Replaced By</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__REPLACED_BY = OBO_OBJECT__REPLACED_BY;

  /**
   * The feature id for the '<em><b>Consider Replacements</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__CONSIDER_REPLACEMENTS = OBO_OBJECT__CONSIDER_REPLACEMENTS;

  /**
   * The feature id for the '<em><b>Consider Extension</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__CONSIDER_EXTENSION = OBO_OBJECT__CONSIDER_EXTENSION;

  /**
   * The feature id for the '<em><b>Replaced By Extension</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__REPLACED_BY_EXTENSION = OBO_OBJECT__REPLACED_BY_EXTENSION;

  /**
   * The feature id for the '<em><b>Obsolete Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__OBSOLETE_EXTENSION = OBO_OBJECT__OBSOLETE_EXTENSION;

  /**
   * The feature id for the '<em><b>Definition</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__DEFINITION = OBO_OBJECT__DEFINITION;

  /**
   * The feature id for the '<em><b>Def Dbxrefs</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__DEF_DBXREFS = OBO_OBJECT__DEF_DBXREFS;

  /**
   * The feature id for the '<em><b>Definition Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__DEFINITION_EXTENSION = OBO_OBJECT__DEFINITION_EXTENSION;

  /**
   * The feature id for the '<em><b>Subsets</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__SUBSETS = OBO_OBJECT__SUBSETS;

  /**
   * The feature id for the '<em><b>Category Extensions</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__CATEGORY_EXTENSIONS = OBO_OBJECT__CATEGORY_EXTENSIONS;

  /**
   * The feature id for the '<em><b>Parents</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__PARENTS = OBO_OBJECT__PARENTS;

  /**
   * The feature id for the '<em><b>Children</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__CHILDREN = OBO_OBJECT__CHILDREN;

  /**
   * The feature id for the '<em><b>Cyclic</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__CYCLIC = OBO_OBJECT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Symmetric</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__SYMMETRIC = OBO_OBJECT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Transitive</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__TRANSITIVE = OBO_OBJECT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Reflexive</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__REFLEXIVE = OBO_OBJECT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Always Implies Inverse</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE = OBO_OBJECT_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Dummy</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__DUMMY = OBO_OBJECT_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Metadata Tag</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__METADATA_TAG = OBO_OBJECT_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Cyclic Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__CYCLIC_EXTENSION = OBO_OBJECT_FEATURE_COUNT + 7;

  /**
   * The feature id for the '<em><b>Symmetric Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__SYMMETRIC_EXTENSION = OBO_OBJECT_FEATURE_COUNT + 8;

  /**
   * The feature id for the '<em><b>Transitive Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__TRANSITIVE_EXTENSION = OBO_OBJECT_FEATURE_COUNT + 9;

  /**
   * The feature id for the '<em><b>Reflexive Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__REFLEXIVE_EXTENSION = OBO_OBJECT_FEATURE_COUNT + 10;

  /**
   * The feature id for the '<em><b>Always Implies Inverse Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION = OBO_OBJECT_FEATURE_COUNT + 11;

  /**
   * The feature id for the '<em><b>Domain Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__DOMAIN_EXTENSION = OBO_OBJECT_FEATURE_COUNT + 12;

  /**
   * The feature id for the '<em><b>Range Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__RANGE_EXTENSION = OBO_OBJECT_FEATURE_COUNT + 13;

  /**
   * The feature id for the '<em><b>Range</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__RANGE = OBO_OBJECT_FEATURE_COUNT + 14;

  /**
   * The feature id for the '<em><b>Domain</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__DOMAIN = OBO_OBJECT_FEATURE_COUNT + 15;

  /**
   * The feature id for the '<em><b>Non Inheritable</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__NON_INHERITABLE = OBO_OBJECT_FEATURE_COUNT + 16;

  /**
   * The feature id for the '<em><b>Disjoint Over</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__DISJOINT_OVER = OBO_OBJECT_FEATURE_COUNT + 17;

  /**
   * The feature id for the '<em><b>Transitive Over</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__TRANSITIVE_OVER = OBO_OBJECT_FEATURE_COUNT + 18;

  /**
   * The feature id for the '<em><b>Holds Over Chains</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY__HOLDS_OVER_CHAINS = OBO_OBJECT_FEATURE_COUNT + 19;

  /**
   * The number of structural features of the '<em>OBO Property</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY_FEATURE_COUNT = OBO_OBJECT_FEATURE_COUNT + 20;

  /**
   * The number of operations of the '<em>OBO Property</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY_OPERATION_COUNT = OBO_OBJECT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBORestrictionImpl <em>OBO Restriction</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBORestrictionImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getOBORestriction()
   * @generated
   */
  int OBO_RESTRICTION = 29;

  /**
   * The feature id for the '<em><b>Implied</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_RESTRICTION__IMPLIED = LINK__IMPLIED;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_RESTRICTION__ID = LINK__ID;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_RESTRICTION__ANONYMOUS = LINK__ANONYMOUS;

  /**
   * The feature id for the '<em><b>Child</b></em>' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_RESTRICTION__CHILD = LINK__CHILD;

  /**
   * The feature id for the '<em><b>Parent</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_RESTRICTION__PARENT = LINK__PARENT;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_RESTRICTION__TYPE = LINK__TYPE;

  /**
   * The feature id for the '<em><b>Nested Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_RESTRICTION__NESTED_VALUE = LINK__NESTED_VALUE;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_RESTRICTION__NAMESPACE = LINK__NAMESPACE;

  /**
   * The feature id for the '<em><b>Cardinality</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_RESTRICTION__CARDINALITY = LINK_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Max Cardinality</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_RESTRICTION__MAX_CARDINALITY = LINK_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Min Cardinality</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_RESTRICTION__MIN_CARDINALITY = LINK_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Completes</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_RESTRICTION__COMPLETES = LINK_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Inverse Completes</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_RESTRICTION__INVERSE_COMPLETES = LINK_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Necessarily True</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_RESTRICTION__NECESSARILY_TRUE = LINK_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Inverse Necessarily True</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_RESTRICTION__INVERSE_NECESSARILY_TRUE = LINK_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Additional Arguments</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_RESTRICTION__ADDITIONAL_ARGUMENTS = LINK_FEATURE_COUNT + 7;

  /**
   * The number of structural features of the '<em>OBO Restriction</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_RESTRICTION_FEATURE_COUNT = LINK_FEATURE_COUNT + 8;

  /**
   * The number of operations of the '<em>OBO Restriction</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_RESTRICTION_OPERATION_COUNT = LINK_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOSessionImpl <em>OBO Session</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOSessionImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getOBOSession()
   * @generated
   */
  int OBO_SESSION = 30;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_SESSION__ID = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Objects</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_SESSION__OBJECTS = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Link Database</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_SESSION__LINK_DATABASE = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Default Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_SESSION__DEFAULT_NAMESPACE = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Namespaces</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_SESSION__NAMESPACES = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Property Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_SESSION__PROPERTY_VALUES = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Unknown Stanzas</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_SESSION__UNKNOWN_STANZAS = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Subsets</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_SESSION__SUBSETS = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 7;

  /**
   * The feature id for the '<em><b>Categories</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_SESSION__CATEGORIES = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 8;

  /**
   * The feature id for the '<em><b>Synonym Types</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_SESSION__SYNONYM_TYPES = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 9;

  /**
   * The feature id for the '<em><b>Synonym Categories</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_SESSION__SYNONYM_CATEGORIES = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 10;

  /**
   * The feature id for the '<em><b>Current Filenames</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_SESSION__CURRENT_FILENAMES = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 11;

  /**
   * The feature id for the '<em><b>Current User</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_SESSION__CURRENT_USER = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 12;

  /**
   * The feature id for the '<em><b>Id Spaces</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_SESSION__ID_SPACES = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 13;

  /**
   * The feature id for the '<em><b>Load Remark</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_SESSION__LOAD_REMARK = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 14;

  /**
   * The feature id for the '<em><b>All Dbx Refs Container</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_SESSION__ALL_DBX_REFS_CONTAINER = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 15;

  /**
   * The number of structural features of the '<em>OBO Session</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_SESSION_FEATURE_COUNT = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 16;

  /**
   * The number of operations of the '<em>OBO Session</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_SESSION_OPERATION_COUNT = IDENTIFIED_OBJECT_INDEX_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SerializableImpl <em>Serializable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SerializableImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getSerializable()
   * @generated
   */
  int SERIALIZABLE = 49;

  /**
   * The number of structural features of the '<em>Serializable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SERIALIZABLE_FEATURE_COUNT = 0;

  /**
   * The number of operations of the '<em>Serializable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SERIALIZABLE_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObjectFactoryImpl <em>Object Factory</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObjectFactoryImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getObjectFactory()
   * @generated
   */
  int OBJECT_FACTORY = 31;

  /**
   * The number of structural features of the '<em>Object Factory</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBJECT_FACTORY_FEATURE_COUNT = SERIALIZABLE_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>Object Factory</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBJECT_FACTORY_OPERATION_COUNT = SERIALIZABLE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObjectFieldImpl <em>Object Field</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObjectFieldImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getObjectField()
   * @generated
   */
  int OBJECT_FIELD = 32;

  /**
   * The feature id for the '<em><b>Values</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBJECT_FIELD__VALUES = 0;

  /**
   * The number of structural features of the '<em>Object Field</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBJECT_FIELD_FEATURE_COUNT = 1;

  /**
   * The number of operations of the '<em>Object Field</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBJECT_FIELD_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObsoletableObjectImpl <em>Obsoletable Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObsoletableObjectImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getObsoletableObject()
   * @generated
   */
  int OBSOLETABLE_OBJECT = 33;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT__TYPE = IDENTIFIED_OBJECT__TYPE;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT__ID = IDENTIFIED_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT__ANONYMOUS = IDENTIFIED_OBJECT__ANONYMOUS;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT__NAMESPACE = IDENTIFIED_OBJECT__NAMESPACE;

  /**
   * The feature id for the '<em><b>Namespace Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT__NAMESPACE_EXTENSION = IDENTIFIED_OBJECT__NAMESPACE_EXTENSION;

  /**
   * The feature id for the '<em><b>Type Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT__TYPE_EXTENSION = IDENTIFIED_OBJECT__TYPE_EXTENSION;

  /**
   * The feature id for the '<em><b>Built In</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT__BUILT_IN = IDENTIFIED_OBJECT__BUILT_IN;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT__NAME = IDENTIFIED_OBJECT__NAME;

  /**
   * The feature id for the '<em><b>Name Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT__NAME_EXTENSION = IDENTIFIED_OBJECT__NAME_EXTENSION;

  /**
   * The feature id for the '<em><b>Id Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT__ID_EXTENSION = IDENTIFIED_OBJECT__ID_EXTENSION;

  /**
   * The feature id for the '<em><b>Anonymous Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT__ANONYMOUS_EXTENSION = IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION;

  /**
   * The feature id for the '<em><b>Property Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT__PROPERTY_VALUES = IDENTIFIED_OBJECT__PROPERTY_VALUES;

  /**
   * The feature id for the '<em><b>Obsolete</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT__OBSOLETE = IDENTIFIED_OBJECT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Replaced By</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT__REPLACED_BY = IDENTIFIED_OBJECT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Consider Replacements</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT__CONSIDER_REPLACEMENTS = IDENTIFIED_OBJECT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Consider Extension</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT__CONSIDER_EXTENSION = IDENTIFIED_OBJECT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Replaced By Extension</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT__REPLACED_BY_EXTENSION = IDENTIFIED_OBJECT_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Obsolete Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT__OBSOLETE_EXTENSION = IDENTIFIED_OBJECT_FEATURE_COUNT + 5;

  /**
   * The number of structural features of the '<em>Obsoletable Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT_FEATURE_COUNT = IDENTIFIED_OBJECT_FEATURE_COUNT + 6;

  /**
   * The number of operations of the '<em>Obsoletable Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT_OPERATION_COUNT = IDENTIFIED_OBJECT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.PathCapableImpl <em>Path Capable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.PathCapableImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getPathCapable()
   * @generated
   */
  int PATH_CAPABLE = 34;

  /**
   * The number of structural features of the '<em>Path Capable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATH_CAPABLE_FEATURE_COUNT = 0;

  /**
   * The number of operations of the '<em>Path Capable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PATH_CAPABLE_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.PropertyValueImpl <em>Property Value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.PropertyValueImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getPropertyValue()
   * @generated
   */
  int PROPERTY_VALUE = 35;

  /**
   * The feature id for the '<em><b>Property</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_VALUE__PROPERTY = CLONEABLE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_VALUE__VALUE = CLONEABLE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Line Number</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_VALUE__LINE_NUMBER = CLONEABLE_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Filename</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_VALUE__FILENAME = CLONEABLE_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Line</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_VALUE__LINE = CLONEABLE_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>Property Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_VALUE_FEATURE_COUNT = CLONEABLE_FEATURE_COUNT + 5;

  /**
   * The number of operations of the '<em>Property Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_VALUE_OPERATION_COUNT = CLONEABLE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.RelationshipImpl <em>Relationship</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.RelationshipImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getRelationship()
   * @generated
   */
  int RELATIONSHIP = 36;

  /**
   * The feature id for the '<em><b>Child</b></em>' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONSHIP__CHILD = SERIALIZABLE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Parent</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONSHIP__PARENT = SERIALIZABLE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONSHIP__TYPE = SERIALIZABLE_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Nested Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONSHIP__NESTED_VALUE = SERIALIZABLE_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Relationship</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONSHIP_FEATURE_COUNT = SERIALIZABLE_FEATURE_COUNT + 4;

  /**
   * The number of operations of the '<em>Relationship</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RELATIONSHIP_OPERATION_COUNT = SERIALIZABLE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.RootAlgorithmImpl <em>Root Algorithm</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.RootAlgorithmImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getRootAlgorithm()
   * @generated
   */
  int ROOT_ALGORITHM = 37;

  /**
   * The number of structural features of the '<em>Root Algorithm</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ROOT_ALGORITHM_FEATURE_COUNT = 0;

  /**
   * The number of operations of the '<em>Root Algorithm</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ROOT_ALGORITHM_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SubsetObjectImpl <em>Subset Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SubsetObjectImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getSubsetObject()
   * @generated
   */
  int SUBSET_OBJECT = 38;

  /**
   * The feature id for the '<em><b>Subsets</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBSET_OBJECT__SUBSETS = 0;

  /**
   * The feature id for the '<em><b>Category Extensions</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBSET_OBJECT__CATEGORY_EXTENSIONS = 1;

  /**
   * The number of structural features of the '<em>Subset Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBSET_OBJECT_FEATURE_COUNT = 2;

  /**
   * The number of operations of the '<em>Subset Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBSET_OBJECT_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymImpl <em>Synonym</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getSynonym()
   * @generated
   */
  int SYNONYM = 39;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYM__ID = CLONEABLE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYM__ANONYMOUS = CLONEABLE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>UNKNOWN SCOPE</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYM__UNKNOWN_SCOPE = CLONEABLE_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>RELATED SYNONYM</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYM__RELATED_SYNONYM = CLONEABLE_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>EXACT SYNONYM</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYM__EXACT_SYNONYM = CLONEABLE_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>NARROW SYNONYM</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYM__NARROW_SYNONYM = CLONEABLE_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>BROAD SYNONYM</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYM__BROAD_SYNONYM = CLONEABLE_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Synonym Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYM__SYNONYM_TYPE = CLONEABLE_FEATURE_COUNT + 7;

  /**
   * The feature id for the '<em><b>Nested Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYM__NESTED_VALUE = CLONEABLE_FEATURE_COUNT + 8;

  /**
   * The feature id for the '<em><b>Scope</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYM__SCOPE = CLONEABLE_FEATURE_COUNT + 9;

  /**
   * The feature id for the '<em><b>Xrefs</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYM__XREFS = CLONEABLE_FEATURE_COUNT + 10;

  /**
   * The feature id for the '<em><b>Text</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYM__TEXT = CLONEABLE_FEATURE_COUNT + 11;

  /**
   * The number of structural features of the '<em>Synonym</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYM_FEATURE_COUNT = CLONEABLE_FEATURE_COUNT + 12;

  /**
   * The number of operations of the '<em>Synonym</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYM_OPERATION_COUNT = CLONEABLE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymTypeImpl <em>Synonym Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymTypeImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getSynonymType()
   * @generated
   */
  int SYNONYM_TYPE = 40;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYM_TYPE__ID = CLONEABLE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYM_TYPE__NAME = CLONEABLE_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Scope</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYM_TYPE__SCOPE = CLONEABLE_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Synonym Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYM_TYPE_FEATURE_COUNT = CLONEABLE_FEATURE_COUNT + 3;

  /**
   * The number of operations of the '<em>Synonym Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYM_TYPE_OPERATION_COUNT = CLONEABLE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymedObjectImpl <em>Synonymed Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymedObjectImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getSynonymedObject()
   * @generated
   */
  int SYNONYMED_OBJECT = 41;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYMED_OBJECT__TYPE = IDENTIFIED_OBJECT__TYPE;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYMED_OBJECT__ID = IDENTIFIED_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYMED_OBJECT__ANONYMOUS = IDENTIFIED_OBJECT__ANONYMOUS;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYMED_OBJECT__NAMESPACE = IDENTIFIED_OBJECT__NAMESPACE;

  /**
   * The feature id for the '<em><b>Namespace Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYMED_OBJECT__NAMESPACE_EXTENSION = IDENTIFIED_OBJECT__NAMESPACE_EXTENSION;

  /**
   * The feature id for the '<em><b>Type Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYMED_OBJECT__TYPE_EXTENSION = IDENTIFIED_OBJECT__TYPE_EXTENSION;

  /**
   * The feature id for the '<em><b>Built In</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYMED_OBJECT__BUILT_IN = IDENTIFIED_OBJECT__BUILT_IN;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYMED_OBJECT__NAME = IDENTIFIED_OBJECT__NAME;

  /**
   * The feature id for the '<em><b>Name Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYMED_OBJECT__NAME_EXTENSION = IDENTIFIED_OBJECT__NAME_EXTENSION;

  /**
   * The feature id for the '<em><b>Id Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYMED_OBJECT__ID_EXTENSION = IDENTIFIED_OBJECT__ID_EXTENSION;

  /**
   * The feature id for the '<em><b>Anonymous Extension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYMED_OBJECT__ANONYMOUS_EXTENSION = IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION;

  /**
   * The feature id for the '<em><b>Property Values</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYMED_OBJECT__PROPERTY_VALUES = IDENTIFIED_OBJECT__PROPERTY_VALUES;

  /**
   * The feature id for the '<em><b>Synonyms</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYMED_OBJECT__SYNONYMS = IDENTIFIED_OBJECT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Synonymed Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYMED_OBJECT_FEATURE_COUNT = IDENTIFIED_OBJECT_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Synonymed Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SYNONYMED_OBJECT_OPERATION_COUNT = IDENTIFIED_OBJECT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.TermSubsetImpl <em>Term Subset</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.TermSubsetImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getTermSubset()
   * @generated
   */
  int TERM_SUBSET = 42;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM_SUBSET__NAME = CLONEABLE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Desc</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM_SUBSET__DESC = CLONEABLE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Term Subset</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM_SUBSET_FEATURE_COUNT = CLONEABLE_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>Term Subset</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM_SUBSET_OPERATION_COUNT = CLONEABLE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.UnknownStanzaImpl <em>Unknown Stanza</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.UnknownStanzaImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getUnknownStanza()
   * @generated
   */
  int UNKNOWN_STANZA = 44;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNKNOWN_STANZA__NAMESPACE = 0;

  /**
   * The feature id for the '<em><b>Property Values</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNKNOWN_STANZA__PROPERTY_VALUES = 1;

  /**
   * The feature id for the '<em><b>Nested Values</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNKNOWN_STANZA__NESTED_VALUES = 2;

  /**
   * The feature id for the '<em><b>Stanza</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNKNOWN_STANZA__STANZA = 3;

  /**
   * The number of structural features of the '<em>Unknown Stanza</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNKNOWN_STANZA_FEATURE_COUNT = 4;

  /**
   * The number of operations of the '<em>Unknown Stanza</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNKNOWN_STANZA_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ValueDatabaseImpl <em>Value Database</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ValueDatabaseImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getValueDatabase()
   * @generated
   */
  int VALUE_DATABASE = 46;

  /**
   * The feature id for the '<em><b>Objects</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_DATABASE__OBJECTS = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Value Database</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_DATABASE_FEATURE_COUNT = IDENTIFIED_OBJECT_INDEX_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Value Database</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_DATABASE_OPERATION_COUNT = IDENTIFIED_OBJECT_INDEX_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ValueLinkImpl <em>Value Link</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ValueLinkImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getValueLink()
   * @generated
   */
  int VALUE_LINK = 47;

  /**
   * The feature id for the '<em><b>Implied</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_LINK__IMPLIED = LINK__IMPLIED;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_LINK__ID = LINK__ID;

  /**
   * The feature id for the '<em><b>Anonymous</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_LINK__ANONYMOUS = LINK__ANONYMOUS;

  /**
   * The feature id for the '<em><b>Child</b></em>' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_LINK__CHILD = LINK__CHILD;

  /**
   * The feature id for the '<em><b>Parent</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_LINK__PARENT = LINK__PARENT;

  /**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_LINK__TYPE = LINK__TYPE;

  /**
   * The feature id for the '<em><b>Nested Value</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_LINK__NESTED_VALUE = LINK__NESTED_VALUE;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_LINK__NAMESPACE = LINK__NAMESPACE;

  /**
   * The feature id for the '<em><b>Value</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_LINK__VALUE = LINK_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Value Link</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_LINK_FEATURE_COUNT = LINK_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Value Link</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VALUE_LINK_OPERATION_COUNT = LINK_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ComparableImpl <em>Comparable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ComparableImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getComparable()
   * @generated
   */
  int COMPARABLE = 50;

  /**
   * The number of structural features of the '<em>Comparable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARABLE_FEATURE_COUNT = 0;

  /**
   * The number of operations of the '<em>Comparable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPARABLE_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.StringToNestedValueMapImpl <em>String To Nested Value Map</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.StringToNestedValueMapImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getStringToNestedValueMap()
   * @generated
   */
  int STRING_TO_NESTED_VALUE_MAP = 51;

  /**
   * The feature id for the '<em><b>Key</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_TO_NESTED_VALUE_MAP__KEY = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_TO_NESTED_VALUE_MAP__VALUE = 1;

  /**
   * The number of structural features of the '<em>String To Nested Value Map</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_TO_NESTED_VALUE_MAP_FEATURE_COUNT = 2;

  /**
   * The number of operations of the '<em>String To Nested Value Map</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int STRING_TO_NESTED_VALUE_MAP_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObsoletableObjectToNestedValueMapImpl <em>Obsoletable Object To Nested Value Map</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObsoletableObjectToNestedValueMapImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getObsoletableObjectToNestedValueMap()
   * @generated
   */
  int OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP = 52;

  /**
   * The feature id for the '<em><b>Key</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP__KEY = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP__VALUE = 1;

  /**
   * The number of structural features of the '<em>Obsoletable Object To Nested Value Map</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP_FEATURE_COUNT = 2;

  /**
   * The number of operations of the '<em>Obsoletable Object To Nested Value Map</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.TermSubsetToNestedValueMapImpl <em>Term Subset To Nested Value Map</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.TermSubsetToNestedValueMapImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getTermSubsetToNestedValueMap()
   * @generated
   */
  int TERM_SUBSET_TO_NESTED_VALUE_MAP = 53;

  /**
   * The feature id for the '<em><b>Key</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM_SUBSET_TO_NESTED_VALUE_MAP__KEY = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM_SUBSET_TO_NESTED_VALUE_MAP__VALUE = 1;

  /**
   * The number of structural features of the '<em>Term Subset To Nested Value Map</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM_SUBSET_TO_NESTED_VALUE_MAP_FEATURE_COUNT = 2;

  /**
   * The number of operations of the '<em>Term Subset To Nested Value Map</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TERM_SUBSET_TO_NESTED_VALUE_MAP_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdSpacesMapImpl <em>Id Spaces Map</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdSpacesMapImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getIdSpacesMap()
   * @generated
   */
  int ID_SPACES_MAP = 54;

  /**
   * The feature id for the '<em><b>Key</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ID_SPACES_MAP__KEY = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ID_SPACES_MAP__VALUE = 1;

  /**
   * The number of structural features of the '<em>Id Spaces Map</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ID_SPACES_MAP_FEATURE_COUNT = 2;

  /**
   * The number of operations of the '<em>Id Spaces Map</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ID_SPACES_MAP_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ListOfPropertiesImpl <em>List Of Properties</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ListOfPropertiesImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getListOfProperties()
   * @generated
   */
  int LIST_OF_PROPERTIES = 55;

  /**
   * The feature id for the '<em><b>Properties</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LIST_OF_PROPERTIES__PROPERTIES = 0;

  /**
   * The number of structural features of the '<em>List Of Properties</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LIST_OF_PROPERTIES_FEATURE_COUNT = 1;

  /**
   * The number of operations of the '<em>List Of Properties</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LIST_OF_PROPERTIES_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OboPropertyValueImpl <em>Obo Property Value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OboPropertyValueImpl
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getOboPropertyValue()
   * @generated
   */
  int OBO_PROPERTY_VALUE = 56;

  /**
   * The feature id for the '<em><b>Property</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY_VALUE__PROPERTY = 0;

  /**
   * The feature id for the '<em><b>Value</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY_VALUE__VALUE = 1;

  /**
   * The number of structural features of the '<em>Obo Property Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY_VALUE_FEATURE_COUNT = 2;

  /**
   * The number of operations of the '<em>Obo Property Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OBO_PROPERTY_VALUE_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '<em>Date</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see java.util.Date
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getDate()
   * @generated
   */
  int DATE = 57;

  /**
   * The meta object id for the '<em>Ecore Object</em>' data type.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EObject
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getEcoreObject()
   * @generated
   */
  int ECORE_OBJECT = 58;


  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.AnnotatedObject <em>Annotated Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Annotated Object</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.AnnotatedObject
   * @generated
   */
  EClass getAnnotatedObject();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.CommentedObject <em>Commented Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Commented Object</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.CommentedObject
   * @generated
   */
  EClass getCommentedObject();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.CommentedObject#getComment <em>Comment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Comment</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.CommentedObject#getComment()
   * @see #getCommentedObject()
   * @generated
   */
  EAttribute getCommentedObject_Comment();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.CommentedObject#getCommentExtension <em>Comment Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Comment Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.CommentedObject#getCommentExtension()
   * @see #getCommentedObject()
   * @generated
   */
  EReference getCommentedObject_CommentExtension();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingObject <em>Dangling Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Dangling Object</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingObject
   * @generated
   */
  EClass getDanglingObject();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingObject#isDangling <em>Dangling</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dangling</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingObject#isDangling()
   * @see #getDanglingObject()
   * @generated
   */
  EAttribute getDanglingObject_Dangling();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingProperty <em>Dangling Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Dangling Property</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingProperty
   * @generated
   */
  EClass getDanglingProperty();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Datatype <em>Datatype</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Datatype</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Datatype
   * @generated
   */
  EClass getDatatype();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Datatype#isAbstract <em>Abstract</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Abstract</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Datatype#isAbstract()
   * @see #getDatatype()
   * @generated
   */
  EAttribute getDatatype_Abstract();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DatatypeValue <em>Datatype Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Datatype Value</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DatatypeValue
   * @generated
   */
  EClass getDatatypeValue();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DatatypeValue#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DatatypeValue#getValue()
   * @see #getDatatypeValue()
   * @generated
   */
  EAttribute getDatatypeValue_Value();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref <em>Dbxref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Dbxref</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref
   * @generated
   */
  EClass getDbxref();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref#getNestedValue <em>Nested Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Nested Value</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref#getNestedValue()
   * @see #getDbxref()
   * @generated
   */
  EReference getDbxref_NestedValue();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref#isDefRef <em>Def Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Def Ref</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref#isDefRef()
   * @see #getDbxref()
   * @generated
   */
  EAttribute getDbxref_DefRef();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref#getDesc <em>Desc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Desc</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref#getDesc()
   * @see #getDbxref()
   * @generated
   */
  EAttribute getDbxref_Desc();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref#getType()
   * @see #getDbxref()
   * @generated
   */
  EAttribute getDbxref_Type();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref#getSynonym <em>Synonym</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Synonym</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref#getSynonym()
   * @see #getDbxref()
   * @generated
   */
  EReference getDbxref_Synonym();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref#getDatabaseID <em>Database ID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Database ID</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref#getDatabaseID()
   * @see #getDbxref()
   * @generated
   */
  EAttribute getDbxref_DatabaseID();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref#getDatabase <em>Database</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Database</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref#getDatabase()
   * @see #getDbxref()
   * @generated
   */
  EAttribute getDbxref_Database();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DbxrefedObject <em>Dbxrefed Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Dbxrefed Object</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DbxrefedObject
   * @generated
   */
  EClass getDbxrefedObject();

  /**
   * Returns the meta object for the reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DbxrefedObject#getDbxrefs <em>Dbxrefs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Dbxrefs</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DbxrefedObject#getDbxrefs()
   * @see #getDbxrefedObject()
   * @generated
   */
  EReference getDbxrefedObject_Dbxrefs();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject <em>Defined Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Defined Object</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject
   * @generated
   */
  EClass getDefinedObject();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject#getDefinition <em>Definition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Definition</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject#getDefinition()
   * @see #getDefinedObject()
   * @generated
   */
  EAttribute getDefinedObject_Definition();

  /**
   * Returns the meta object for the reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject#getDefDbxrefs <em>Def Dbxrefs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Def Dbxrefs</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject#getDefDbxrefs()
   * @see #getDefinedObject()
   * @generated
   */
  EReference getDefinedObject_DefDbxrefs();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject#getDefinitionExtension <em>Definition Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Definition Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject#getDefinitionExtension()
   * @see #getDefinedObject()
   * @generated
   */
  EReference getDefinedObject_DefinitionExtension();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPath <em>Field Path</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Field Path</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPath
   * @generated
   */
  EClass getFieldPath();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPathSpec <em>Field Path Spec</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Field Path Spec</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPathSpec
   * @generated
   */
  EClass getFieldPathSpec();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiableObject <em>Identifiable Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Identifiable Object</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiableObject
   * @generated
   */
  EClass getIdentifiableObject();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiableObject#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Id</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiableObject#getId()
   * @see #getIdentifiableObject()
   * @generated
   */
  EAttribute getIdentifiableObject_Id();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiableObject#isAnonymous <em>Anonymous</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Anonymous</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiableObject#isAnonymous()
   * @see #getIdentifiableObject()
   * @generated
   */
  EAttribute getIdentifiableObject_Anonymous();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject <em>Identified Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Identified Object</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject
   * @generated
   */
  EClass getIdentifiedObject();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject#getTypeExtension <em>Type Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject#getTypeExtension()
   * @see #getIdentifiedObject()
   * @generated
   */
  EReference getIdentifiedObject_TypeExtension();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject#isBuiltIn <em>Built In</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Built In</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject#isBuiltIn()
   * @see #getIdentifiedObject()
   * @generated
   */
  EAttribute getIdentifiedObject_BuiltIn();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject#getName()
   * @see #getIdentifiedObject()
   * @generated
   */
  EAttribute getIdentifiedObject_Name();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject#getNameExtension <em>Name Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Name Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject#getNameExtension()
   * @see #getIdentifiedObject()
   * @generated
   */
  EReference getIdentifiedObject_NameExtension();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject#getIdExtension <em>Id Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Id Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject#getIdExtension()
   * @see #getIdentifiedObject()
   * @generated
   */
  EReference getIdentifiedObject_IdExtension();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject#getAnonymousExtension <em>Anonymous Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Anonymous Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject#getAnonymousExtension()
   * @see #getIdentifiedObject()
   * @generated
   */
  EReference getIdentifiedObject_AnonymousExtension();

  /**
   * Returns the meta object for the containment reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject#getPropertyValues <em>Property Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Property Values</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject#getPropertyValues()
   * @see #getIdentifiedObject()
   * @generated
   */
  EReference getIdentifiedObject_PropertyValues();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObjectIndex <em>Identified Object Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Identified Object Index</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObjectIndex
   * @generated
   */
  EClass getIdentifiedObjectIndex();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Impliable <em>Impliable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Impliable</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Impliable
   * @generated
   */
  EClass getImpliable();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Impliable#isImplied <em>Implied</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Implied</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Impliable#isImplied()
   * @see #getImpliable()
   * @generated
   */
  EAttribute getImpliable_Implied();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Instance <em>Instance</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Instance</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Instance
   * @generated
   */
  EClass getInstance();

  /**
   * Returns the meta object for the containment reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Instance#getOboPropertiesValues <em>Obo Properties Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Obo Properties Values</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Instance#getOboPropertiesValues()
   * @see #getInstance()
   * @generated
   */
  EReference getInstance_OboPropertiesValues();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Link <em>Link</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Link</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Link
   * @generated
   */
  EClass getLink();

  /**
   * Returns the meta object for the reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Link#getNamespace <em>Namespace</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Namespace</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Link#getNamespace()
   * @see #getLink()
   * @generated
   */
  EReference getLink_Namespace();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase <em>Link Database</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Link Database</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase
   * @generated
   */
  EClass getLinkDatabase();

  /**
   * Returns the meta object for the container reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase#getSession <em>Session</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the container reference '<em>Session</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase#getSession()
   * @see #getLinkDatabase()
   * @generated
   */
  EReference getLinkDatabase_Session();

  /**
   * Returns the meta object for the reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase#getObjects <em>Objects</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Objects</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase#getObjects()
   * @see #getLinkDatabase()
   * @generated
   */
  EReference getLinkDatabase_Objects();

  /**
   * Returns the meta object for the reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase#getProperties <em>Properties</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Properties</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase#getProperties()
   * @see #getLinkDatabase()
   * @generated
   */
  EReference getLinkDatabase_Properties();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkLinkedObject <em>Link Linked Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Link Linked Object</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkLinkedObject
   * @generated
   */
  EClass getLinkLinkedObject();

  /**
   * Returns the meta object for the reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkLinkedObject#getLink <em>Link</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Link</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkLinkedObject#getLink()
   * @see #getLinkLinkedObject()
   * @generated
   */
  EReference getLinkLinkedObject_Link();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject <em>Linked Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Linked Object</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject
   * @generated
   */
  EClass getLinkedObject();

  /**
   * Returns the meta object for the containment reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject#getParents <em>Parents</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Parents</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject#getParents()
   * @see #getLinkedObject()
   * @generated
   */
  EReference getLinkedObject_Parents();

  /**
   * Returns the meta object for the reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject#getChildren <em>Children</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Children</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject#getChildren()
   * @see #getLinkedObject()
   * @generated
   */
  EReference getLinkedObject_Children();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject <em>Modification Metadata Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Modification Metadata Object</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject
   * @generated
   */
  EClass getModificationMetadataObject();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getCreatedBy <em>Created By</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Created By</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getCreatedBy()
   * @see #getModificationMetadataObject()
   * @generated
   */
  EAttribute getModificationMetadataObject_CreatedBy();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getCreatedByExtension <em>Created By Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Created By Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getCreatedByExtension()
   * @see #getModificationMetadataObject()
   * @generated
   */
  EReference getModificationMetadataObject_CreatedByExtension();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getModifiedBy <em>Modified By</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Modified By</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getModifiedBy()
   * @see #getModificationMetadataObject()
   * @generated
   */
  EAttribute getModificationMetadataObject_ModifiedBy();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getModifiedByExtension <em>Modified By Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Modified By Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getModifiedByExtension()
   * @see #getModificationMetadataObject()
   * @generated
   */
  EReference getModificationMetadataObject_ModifiedByExtension();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getCreationDate <em>Creation Date</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Creation Date</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getCreationDate()
   * @see #getModificationMetadataObject()
   * @generated
   */
  EAttribute getModificationMetadataObject_CreationDate();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getCreationDateExtension <em>Creation Date Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Creation Date Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getCreationDateExtension()
   * @see #getModificationMetadataObject()
   * @generated
   */
  EReference getModificationMetadataObject_CreationDateExtension();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getModificationDate <em>Modification Date</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Modification Date</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getModificationDate()
   * @see #getModificationMetadataObject()
   * @generated
   */
  EAttribute getModificationMetadataObject_ModificationDate();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getModificationDateExtension <em>Modification Date Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Modification Date Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject#getModificationDateExtension()
   * @see #getModificationMetadataObject()
   * @generated
   */
  EReference getModificationMetadataObject_ModificationDateExtension();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MultiIDObject <em>Multi ID Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Multi ID Object</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MultiIDObject
   * @generated
   */
  EClass getMultiIDObject();

  /**
   * Returns the meta object for the attribute list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MultiIDObject#getSecondaryIds <em>Secondary Ids</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Secondary Ids</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MultiIDObject#getSecondaryIds()
   * @see #getMultiIDObject()
   * @generated
   */
  EAttribute getMultiIDObject_SecondaryIds();

  /**
   * Returns the meta object for the map '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MultiIDObject#getSecondaryIdExtension <em>Secondary Id Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the map '<em>Secondary Id Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MultiIDObject#getSecondaryIdExtension()
   * @see #getMultiIDObject()
   * @generated
   */
  EReference getMultiIDObject_SecondaryIdExtension();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MutableLinkDatabase <em>Mutable Link Database</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Mutable Link Database</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MutableLinkDatabase
   * @generated
   */
  EClass getMutableLinkDatabase();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MutableLinkDatabase#getIdentifiedObjectIndex <em>Identified Object Index</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Identified Object Index</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MutableLinkDatabase#getIdentifiedObjectIndex()
   * @see #getMutableLinkDatabase()
   * @generated
   */
  EReference getMutableLinkDatabase_IdentifiedObjectIndex();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace <em>Namespace</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Namespace</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace
   * @generated
   */
  EClass getNamespace();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Id</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace#getId()
   * @see #getNamespace()
   * @generated
   */
  EAttribute getNamespace_Id();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace#getPath <em>Path</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Path</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace#getPath()
   * @see #getNamespace()
   * @generated
   */
  EAttribute getNamespace_Path();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace#getPrivateId <em>Private Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Private Id</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace#getPrivateId()
   * @see #getNamespace()
   * @generated
   */
  EAttribute getNamespace_PrivateId();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NamespacedObject <em>Namespaced Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Namespaced Object</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NamespacedObject
   * @generated
   */
  EClass getNamespacedObject();

  /**
   * Returns the meta object for the reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NamespacedObject#getNamespace <em>Namespace</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Namespace</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NamespacedObject#getNamespace()
   * @see #getNamespacedObject()
   * @generated
   */
  EReference getNamespacedObject_Namespace();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NamespacedObject#getNamespaceExtension <em>Namespace Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Namespace Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NamespacedObject#getNamespaceExtension()
   * @see #getNamespacedObject()
   * @generated
   */
  EReference getNamespacedObject_NamespaceExtension();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue <em>Nested Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Nested Value</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue
   * @generated
   */
  EClass getNestedValue();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue#getName()
   * @see #getNestedValue()
   * @generated
   */
  EAttribute getNestedValue_Name();

  /**
   * Returns the meta object for the containment reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue#getPropertyValues <em>Property Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Property Values</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue#getPropertyValues()
   * @see #getNestedValue()
   * @generated
   */
  EReference getNestedValue_PropertyValues();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue#getSuggestedComment <em>Suggested Comment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Suggested Comment</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue#getSuggestedComment()
   * @see #getNestedValue()
   * @generated
   */
  EAttribute getNestedValue_SuggestedComment();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOClass <em>OBO Class</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>OBO Class</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOClass
   * @generated
   */
  EClass getOBOClass();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOObject <em>OBO Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>OBO Object</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOObject
   * @generated
   */
  EClass getOBOObject();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty <em>OBO Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>OBO Property</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty
   * @generated
   */
  EClass getOBOProperty();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isCyclic <em>Cyclic</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Cyclic</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isCyclic()
   * @see #getOBOProperty()
   * @generated
   */
  EAttribute getOBOProperty_Cyclic();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isSymmetric <em>Symmetric</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Symmetric</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isSymmetric()
   * @see #getOBOProperty()
   * @generated
   */
  EAttribute getOBOProperty_Symmetric();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isTransitive <em>Transitive</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Transitive</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isTransitive()
   * @see #getOBOProperty()
   * @generated
   */
  EAttribute getOBOProperty_Transitive();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isReflexive <em>Reflexive</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Reflexive</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isReflexive()
   * @see #getOBOProperty()
   * @generated
   */
  EAttribute getOBOProperty_Reflexive();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isAlwaysImpliesInverse <em>Always Implies Inverse</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Always Implies Inverse</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isAlwaysImpliesInverse()
   * @see #getOBOProperty()
   * @generated
   */
  EAttribute getOBOProperty_AlwaysImpliesInverse();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isDummy <em>Dummy</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dummy</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isDummy()
   * @see #getOBOProperty()
   * @generated
   */
  EAttribute getOBOProperty_Dummy();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isMetadataTag <em>Metadata Tag</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Metadata Tag</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isMetadataTag()
   * @see #getOBOProperty()
   * @generated
   */
  EAttribute getOBOProperty_MetadataTag();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getCyclicExtension <em>Cyclic Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Cyclic Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getCyclicExtension()
   * @see #getOBOProperty()
   * @generated
   */
  EReference getOBOProperty_CyclicExtension();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getSymmetricExtension <em>Symmetric Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Symmetric Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getSymmetricExtension()
   * @see #getOBOProperty()
   * @generated
   */
  EReference getOBOProperty_SymmetricExtension();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getTransitiveExtension <em>Transitive Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Transitive Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getTransitiveExtension()
   * @see #getOBOProperty()
   * @generated
   */
  EReference getOBOProperty_TransitiveExtension();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getReflexiveExtension <em>Reflexive Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Reflexive Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getReflexiveExtension()
   * @see #getOBOProperty()
   * @generated
   */
  EReference getOBOProperty_ReflexiveExtension();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getAlwaysImpliesInverseExtension <em>Always Implies Inverse Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Always Implies Inverse Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getAlwaysImpliesInverseExtension()
   * @see #getOBOProperty()
   * @generated
   */
  EReference getOBOProperty_AlwaysImpliesInverseExtension();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getDomainExtension <em>Domain Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Domain Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getDomainExtension()
   * @see #getOBOProperty()
   * @generated
   */
  EReference getOBOProperty_DomainExtension();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getRangeExtension <em>Range Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Range Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getRangeExtension()
   * @see #getOBOProperty()
   * @generated
   */
  EReference getOBOProperty_RangeExtension();

  /**
   * Returns the meta object for the reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getRange <em>Range</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Range</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getRange()
   * @see #getOBOProperty()
   * @generated
   */
  EReference getOBOProperty_Range();

  /**
   * Returns the meta object for the reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getDomain <em>Domain</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Domain</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getDomain()
   * @see #getOBOProperty()
   * @generated
   */
  EReference getOBOProperty_Domain();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isNonInheritable <em>Non Inheritable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Non Inheritable</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#isNonInheritable()
   * @see #getOBOProperty()
   * @generated
   */
  EAttribute getOBOProperty_NonInheritable();

  /**
   * Returns the meta object for the reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getDisjointOver <em>Disjoint Over</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Disjoint Over</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getDisjointOver()
   * @see #getOBOProperty()
   * @generated
   */
  EReference getOBOProperty_DisjointOver();

  /**
   * Returns the meta object for the reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getTransitiveOver <em>Transitive Over</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Transitive Over</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getTransitiveOver()
   * @see #getOBOProperty()
   * @generated
   */
  EReference getOBOProperty_TransitiveOver();

  /**
   * Returns the meta object for the containment reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getHoldsOverChains <em>Holds Over Chains</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Holds Over Chains</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty#getHoldsOverChains()
   * @see #getOBOProperty()
   * @generated
   */
  EReference getOBOProperty_HoldsOverChains();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction <em>OBO Restriction</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>OBO Restriction</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction
   * @generated
   */
  EClass getOBORestriction();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#getCardinality <em>Cardinality</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Cardinality</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#getCardinality()
   * @see #getOBORestriction()
   * @generated
   */
  EAttribute getOBORestriction_Cardinality();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#getMaxCardinality <em>Max Cardinality</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Max Cardinality</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#getMaxCardinality()
   * @see #getOBORestriction()
   * @generated
   */
  EAttribute getOBORestriction_MaxCardinality();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#getMinCardinality <em>Min Cardinality</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Min Cardinality</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#getMinCardinality()
   * @see #getOBORestriction()
   * @generated
   */
  EAttribute getOBORestriction_MinCardinality();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#isCompletes <em>Completes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Completes</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#isCompletes()
   * @see #getOBORestriction()
   * @generated
   */
  EAttribute getOBORestriction_Completes();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#isInverseCompletes <em>Inverse Completes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Inverse Completes</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#isInverseCompletes()
   * @see #getOBORestriction()
   * @generated
   */
  EAttribute getOBORestriction_InverseCompletes();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#isNecessarilyTrue <em>Necessarily True</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Necessarily True</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#isNecessarilyTrue()
   * @see #getOBORestriction()
   * @generated
   */
  EAttribute getOBORestriction_NecessarilyTrue();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#isInverseNecessarilyTrue <em>Inverse Necessarily True</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Inverse Necessarily True</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#isInverseNecessarilyTrue()
   * @see #getOBORestriction()
   * @generated
   */
  EAttribute getOBORestriction_InverseNecessarilyTrue();

  /**
   * Returns the meta object for the reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#getAdditionalArguments <em>Additional Arguments</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Additional Arguments</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction#getAdditionalArguments()
   * @see #getOBORestriction()
   * @generated
   */
  EReference getOBORestriction_AdditionalArguments();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession <em>OBO Session</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>OBO Session</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession
   * @generated
   */
  EClass getOBOSession();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Id</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getId()
   * @see #getOBOSession()
   * @generated
   */
  EAttribute getOBOSession_Id();

  /**
   * Returns the meta object for the containment reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getObjects <em>Objects</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Objects</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getObjects()
   * @see #getOBOSession()
   * @generated
   */
  EReference getOBOSession_Objects();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getLinkDatabase <em>Link Database</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Link Database</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getLinkDatabase()
   * @see #getOBOSession()
   * @generated
   */
  EReference getOBOSession_LinkDatabase();

  /**
   * Returns the meta object for the reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getDefaultNamespace <em>Default Namespace</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Default Namespace</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getDefaultNamespace()
   * @see #getOBOSession()
   * @generated
   */
  EReference getOBOSession_DefaultNamespace();

  /**
   * Returns the meta object for the containment reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getNamespaces <em>Namespaces</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Namespaces</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getNamespaces()
   * @see #getOBOSession()
   * @generated
   */
  EReference getOBOSession_Namespaces();

  /**
   * Returns the meta object for the containment reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getPropertyValues <em>Property Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Property Values</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getPropertyValues()
   * @see #getOBOSession()
   * @generated
   */
  EReference getOBOSession_PropertyValues();

  /**
   * Returns the meta object for the containment reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getUnknownStanzas <em>Unknown Stanzas</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Unknown Stanzas</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getUnknownStanzas()
   * @see #getOBOSession()
   * @generated
   */
  EReference getOBOSession_UnknownStanzas();

  /**
   * Returns the meta object for the containment reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getSubsets <em>Subsets</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Subsets</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getSubsets()
   * @see #getOBOSession()
   * @generated
   */
  EReference getOBOSession_Subsets();

  /**
   * Returns the meta object for the containment reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getCategories <em>Categories</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Categories</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getCategories()
   * @see #getOBOSession()
   * @generated
   */
  EReference getOBOSession_Categories();

  /**
   * Returns the meta object for the containment reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getSynonymTypes <em>Synonym Types</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Synonym Types</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getSynonymTypes()
   * @see #getOBOSession()
   * @generated
   */
  EReference getOBOSession_SynonymTypes();

  /**
   * Returns the meta object for the containment reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getSynonymCategories <em>Synonym Categories</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Synonym Categories</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getSynonymCategories()
   * @see #getOBOSession()
   * @generated
   */
  EReference getOBOSession_SynonymCategories();

  /**
   * Returns the meta object for the attribute list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getCurrentFilenames <em>Current Filenames</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Current Filenames</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getCurrentFilenames()
   * @see #getOBOSession()
   * @generated
   */
  EAttribute getOBOSession_CurrentFilenames();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getCurrentUser <em>Current User</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Current User</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getCurrentUser()
   * @see #getOBOSession()
   * @generated
   */
  EAttribute getOBOSession_CurrentUser();

  /**
   * Returns the meta object for the map '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getIdSpaces <em>Id Spaces</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the map '<em>Id Spaces</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getIdSpaces()
   * @see #getOBOSession()
   * @generated
   */
  EReference getOBOSession_IdSpaces();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getLoadRemark <em>Load Remark</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Load Remark</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getLoadRemark()
   * @see #getOBOSession()
   * @generated
   */
  EAttribute getOBOSession_LoadRemark();

  /**
   * Returns the meta object for the containment reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getAllDbxRefsContainer <em>All Dbx Refs Container</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>All Dbx Refs Container</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getAllDbxRefsContainer()
   * @see #getOBOSession()
   * @generated
   */
  EReference getOBOSession_AllDbxRefsContainer();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectFactory <em>Object Factory</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Object Factory</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectFactory
   * @generated
   */
  EClass getObjectFactory();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectField <em>Object Field</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Object Field</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectField
   * @generated
   */
  EClass getObjectField();

  /**
   * Returns the meta object for the attribute list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectField#getValues <em>Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Values</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectField#getValues()
   * @see #getObjectField()
   * @generated
   */
  EAttribute getObjectField_Values();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject <em>Obsoletable Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Obsoletable Object</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject
   * @generated
   */
  EClass getObsoletableObject();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject#isObsolete <em>Obsolete</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Obsolete</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject#isObsolete()
   * @see #getObsoletableObject()
   * @generated
   */
  EAttribute getObsoletableObject_Obsolete();

  /**
   * Returns the meta object for the reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject#getReplacedBy <em>Replaced By</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Replaced By</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject#getReplacedBy()
   * @see #getObsoletableObject()
   * @generated
   */
  EReference getObsoletableObject_ReplacedBy();

  /**
   * Returns the meta object for the reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject#getConsiderReplacements <em>Consider Replacements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Consider Replacements</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject#getConsiderReplacements()
   * @see #getObsoletableObject()
   * @generated
   */
  EReference getObsoletableObject_ConsiderReplacements();

  /**
   * Returns the meta object for the map '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject#getConsiderExtension <em>Consider Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the map '<em>Consider Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject#getConsiderExtension()
   * @see #getObsoletableObject()
   * @generated
   */
  EReference getObsoletableObject_ConsiderExtension();

  /**
   * Returns the meta object for the map '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject#getReplacedByExtension <em>Replaced By Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the map '<em>Replaced By Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject#getReplacedByExtension()
   * @see #getObsoletableObject()
   * @generated
   */
  EReference getObsoletableObject_ReplacedByExtension();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject#getObsoleteExtension <em>Obsolete Extension</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Obsolete Extension</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject#getObsoleteExtension()
   * @see #getObsoletableObject()
   * @generated
   */
  EReference getObsoletableObject_ObsoleteExtension();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PathCapable <em>Path Capable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Path Capable</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PathCapable
   * @generated
   */
  EClass getPathCapable();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue <em>Property Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Property Value</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue
   * @generated
   */
  EClass getPropertyValue();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue#getProperty <em>Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Property</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue#getProperty()
   * @see #getPropertyValue()
   * @generated
   */
  EAttribute getPropertyValue_Property();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue#getValue()
   * @see #getPropertyValue()
   * @generated
   */
  EAttribute getPropertyValue_Value();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue#getLineNumber <em>Line Number</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Line Number</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue#getLineNumber()
   * @see #getPropertyValue()
   * @generated
   */
  EAttribute getPropertyValue_LineNumber();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue#getFilename <em>Filename</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Filename</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue#getFilename()
   * @see #getPropertyValue()
   * @generated
   */
  EAttribute getPropertyValue_Filename();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue#getLine <em>Line</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Line</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue#getLine()
   * @see #getPropertyValue()
   * @generated
   */
  EAttribute getPropertyValue_Line();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship <em>Relationship</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Relationship</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship
   * @generated
   */
  EClass getRelationship();

  /**
   * Returns the meta object for the container reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship#getChild <em>Child</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the container reference '<em>Child</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship#getChild()
   * @see #getRelationship()
   * @generated
   */
  EReference getRelationship_Child();

  /**
   * Returns the meta object for the reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship#getParent <em>Parent</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Parent</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship#getParent()
   * @see #getRelationship()
   * @generated
   */
  EReference getRelationship_Parent();

  /**
   * Returns the meta object for the reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Type</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship#getType()
   * @see #getRelationship()
   * @generated
   */
  EReference getRelationship_Type();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship#getNestedValue <em>Nested Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Nested Value</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship#getNestedValue()
   * @see #getRelationship()
   * @generated
   */
  EReference getRelationship_NestedValue();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.RootAlgorithm <em>Root Algorithm</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Root Algorithm</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.RootAlgorithm
   * @generated
   */
  EClass getRootAlgorithm();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SubsetObject <em>Subset Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Subset Object</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SubsetObject
   * @generated
   */
  EClass getSubsetObject();

  /**
   * Returns the meta object for the reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SubsetObject#getSubsets <em>Subsets</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Subsets</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SubsetObject#getSubsets()
   * @see #getSubsetObject()
   * @generated
   */
  EReference getSubsetObject_Subsets();

  /**
   * Returns the meta object for the map '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SubsetObject#getCategoryExtensions <em>Category Extensions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the map '<em>Category Extensions</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SubsetObject#getCategoryExtensions()
   * @see #getSubsetObject()
   * @generated
   */
  EReference getSubsetObject_CategoryExtensions();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym <em>Synonym</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Synonym</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym
   * @generated
   */
  EClass getSynonym();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getUNKNOWN_SCOPE <em>UNKNOWN SCOPE</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>UNKNOWN SCOPE</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getUNKNOWN_SCOPE()
   * @see #getSynonym()
   * @generated
   */
  EAttribute getSynonym_UNKNOWN_SCOPE();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getRELATED_SYNONYM <em>RELATED SYNONYM</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>RELATED SYNONYM</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getRELATED_SYNONYM()
   * @see #getSynonym()
   * @generated
   */
  EAttribute getSynonym_RELATED_SYNONYM();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getEXACT_SYNONYM <em>EXACT SYNONYM</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>EXACT SYNONYM</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getEXACT_SYNONYM()
   * @see #getSynonym()
   * @generated
   */
  EAttribute getSynonym_EXACT_SYNONYM();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getNARROW_SYNONYM <em>NARROW SYNONYM</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>NARROW SYNONYM</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getNARROW_SYNONYM()
   * @see #getSynonym()
   * @generated
   */
  EAttribute getSynonym_NARROW_SYNONYM();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getBROAD_SYNONYM <em>BROAD SYNONYM</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>BROAD SYNONYM</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getBROAD_SYNONYM()
   * @see #getSynonym()
   * @generated
   */
  EAttribute getSynonym_BROAD_SYNONYM();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getSynonymType <em>Synonym Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Synonym Type</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getSynonymType()
   * @see #getSynonym()
   * @generated
   */
  EReference getSynonym_SynonymType();

  /**
   * Returns the meta object for the containment reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getNestedValue <em>Nested Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Nested Value</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getNestedValue()
   * @see #getSynonym()
   * @generated
   */
  EReference getSynonym_NestedValue();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getScope <em>Scope</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Scope</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getScope()
   * @see #getSynonym()
   * @generated
   */
  EAttribute getSynonym_Scope();

  /**
   * Returns the meta object for the reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getXrefs <em>Xrefs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Xrefs</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getXrefs()
   * @see #getSynonym()
   * @generated
   */
  EReference getSynonym_Xrefs();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getText <em>Text</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Text</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym#getText()
   * @see #getSynonym()
   * @generated
   */
  EAttribute getSynonym_Text();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType <em>Synonym Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Synonym Type</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType
   * @generated
   */
  EClass getSynonymType();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Id</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType#getId()
   * @see #getSynonymType()
   * @generated
   */
  EAttribute getSynonymType_Id();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType#getName()
   * @see #getSynonymType()
   * @generated
   */
  EAttribute getSynonymType_Name();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType#getScope <em>Scope</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Scope</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType#getScope()
   * @see #getSynonymType()
   * @generated
   */
  EAttribute getSynonymType_Scope();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymedObject <em>Synonymed Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Synonymed Object</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymedObject
   * @generated
   */
  EClass getSynonymedObject();

  /**
   * Returns the meta object for the containment reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymedObject#getSynonyms <em>Synonyms</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Synonyms</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymedObject#getSynonyms()
   * @see #getSynonymedObject()
   * @generated
   */
  EReference getSynonymedObject_Synonyms();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset <em>Term Subset</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Term Subset</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset
   * @generated
   */
  EClass getTermSubset();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset#getName()
   * @see #getTermSubset()
   * @generated
   */
  EAttribute getTermSubset_Name();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset#getDesc <em>Desc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Desc</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset#getDesc()
   * @see #getTermSubset()
   * @generated
   */
  EAttribute getTermSubset_Desc();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Type <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Type
   * @generated
   */
  EClass getType();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza <em>Unknown Stanza</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Unknown Stanza</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza
   * @generated
   */
  EClass getUnknownStanza();

  /**
   * Returns the meta object for the reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza#getNamespace <em>Namespace</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Namespace</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza#getNamespace()
   * @see #getUnknownStanza()
   * @generated
   */
  EReference getUnknownStanza_Namespace();

  /**
   * Returns the meta object for the reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza#getPropertyValues <em>Property Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Property Values</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza#getPropertyValues()
   * @see #getUnknownStanza()
   * @generated
   */
  EReference getUnknownStanza_PropertyValues();

  /**
   * Returns the meta object for the reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza#getNestedValues <em>Nested Values</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Nested Values</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza#getNestedValues()
   * @see #getUnknownStanza()
   * @generated
   */
  EReference getUnknownStanza_NestedValues();

  /**
   * Returns the meta object for the attribute '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza#getStanza <em>Stanza</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Stanza</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza#getStanza()
   * @see #getUnknownStanza()
   * @generated
   */
  EAttribute getUnknownStanza_Stanza();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Value <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Value</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Value
   * @generated
   */
  EClass getValue();

  /**
   * Returns the meta object for the reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Value#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Type</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Value#getType()
   * @see #getValue()
   * @generated
   */
  EReference getValue_Type();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueDatabase <em>Value Database</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Value Database</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueDatabase
   * @generated
   */
  EClass getValueDatabase();

  /**
   * Returns the meta object for the reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueDatabase#getObjects <em>Objects</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Objects</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueDatabase#getObjects()
   * @see #getValueDatabase()
   * @generated
   */
  EReference getValueDatabase_Objects();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueLink <em>Value Link</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Value Link</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueLink
   * @generated
   */
  EClass getValueLink();

  /**
   * Returns the meta object for the reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueLink#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Value</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueLink#getValue()
   * @see #getValueLink()
   * @generated
   */
  EReference getValueLink_Value();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable <em>Cloneable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Cloneable</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable
   * @generated
   */
  EClass getCloneable();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Serializable <em>Serializable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Serializable</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Serializable
   * @generated
   */
  EClass getSerializable();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable <em>Comparable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Comparable</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable
   * @generated
   */
  EClass getComparable();

  /**
   * Returns the meta object for class '{@link java.util.Map.Entry <em>String To Nested Value Map</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>String To Nested Value Map</em>'.
   * @see java.util.Map.Entry
   * @model keyDataType="org.eclipse.emf.ecore.EString" keyRequired="true"
   *        valueType="br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue" valueRequired="true"
   * @generated
   */
  EClass getStringToNestedValueMap();

  /**
   * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Key</em>'.
   * @see java.util.Map.Entry
   * @see #getStringToNestedValueMap()
   * @generated
   */
  EAttribute getStringToNestedValueMap_Key();

  /**
   * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Value</em>'.
   * @see java.util.Map.Entry
   * @see #getStringToNestedValueMap()
   * @generated
   */
  EReference getStringToNestedValueMap_Value();

  /**
   * Returns the meta object for class '{@link java.util.Map.Entry <em>Obsoletable Object To Nested Value Map</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Obsoletable Object To Nested Value Map</em>'.
   * @see java.util.Map.Entry
   * @model keyType="br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject" keyRequired="true"
   *        valueType="br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue" valueRequired="true"
   * @generated
   */
  EClass getObsoletableObjectToNestedValueMap();

  /**
   * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Key</em>'.
   * @see java.util.Map.Entry
   * @see #getObsoletableObjectToNestedValueMap()
   * @generated
   */
  EReference getObsoletableObjectToNestedValueMap_Key();

  /**
   * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Value</em>'.
   * @see java.util.Map.Entry
   * @see #getObsoletableObjectToNestedValueMap()
   * @generated
   */
  EReference getObsoletableObjectToNestedValueMap_Value();

  /**
   * Returns the meta object for class '{@link java.util.Map.Entry <em>Term Subset To Nested Value Map</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Term Subset To Nested Value Map</em>'.
   * @see java.util.Map.Entry
   * @model keyType="br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset" keyRequired="true"
   *        valueType="br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue" valueRequired="true"
   * @generated
   */
  EClass getTermSubsetToNestedValueMap();

  /**
   * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Key</em>'.
   * @see java.util.Map.Entry
   * @see #getTermSubsetToNestedValueMap()
   * @generated
   */
  EReference getTermSubsetToNestedValueMap_Key();

  /**
   * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Value</em>'.
   * @see java.util.Map.Entry
   * @see #getTermSubsetToNestedValueMap()
   * @generated
   */
  EReference getTermSubsetToNestedValueMap_Value();

  /**
   * Returns the meta object for class '{@link java.util.Map.Entry <em>Id Spaces Map</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Id Spaces Map</em>'.
   * @see java.util.Map.Entry
   * @model keyDataType="org.eclipse.emf.ecore.EString" keyRequired="true"
   *        valueDataType="org.eclipse.emf.ecore.EString" valueRequired="true"
   * @generated
   */
  EClass getIdSpacesMap();

  /**
   * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Key</em>'.
   * @see java.util.Map.Entry
   * @see #getIdSpacesMap()
   * @generated
   */
  EAttribute getIdSpacesMap_Key();

  /**
   * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see java.util.Map.Entry
   * @see #getIdSpacesMap()
   * @generated
   */
  EAttribute getIdSpacesMap_Value();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ListOfProperties <em>List Of Properties</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>List Of Properties</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ListOfProperties
   * @generated
   */
  EClass getListOfProperties();

  /**
   * Returns the meta object for the reference list '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ListOfProperties#getProperties <em>Properties</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Properties</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ListOfProperties#getProperties()
   * @see #getListOfProperties()
   * @generated
   */
  EReference getListOfProperties_Properties();

  /**
   * Returns the meta object for class '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OboPropertyValue <em>Obo Property Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Obo Property Value</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OboPropertyValue
   * @generated
   */
  EClass getOboPropertyValue();

  /**
   * Returns the meta object for the reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OboPropertyValue#getProperty <em>Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Property</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OboPropertyValue#getProperty()
   * @see #getOboPropertyValue()
   * @generated
   */
  EReference getOboPropertyValue_Property();

  /**
   * Returns the meta object for the reference '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OboPropertyValue#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Value</em>'.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OboPropertyValue#getValue()
   * @see #getOboPropertyValue()
   * @generated
   */
  EReference getOboPropertyValue_Value();

  /**
   * Returns the meta object for data type '{@link java.util.Date <em>Date</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Date</em>'.
   * @see java.util.Date
   * @model instanceClass="java.util.Date"
   * @generated
   */
  EDataType getDate();

  /**
   * Returns the meta object for data type '{@link org.eclipse.emf.ecore.EObject <em>Ecore Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for data type '<em>Ecore Object</em>'.
   * @see org.eclipse.emf.ecore.EObject
   * @model instanceClass="org.eclipse.emf.ecore.EObject"
   * @generated
   */
  EDataType getEcoreObject();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  obodatamodelFactory getobodatamodelFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each operation of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals {
    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl <em>Annotated Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.AnnotatedObjectImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getAnnotatedObject()
     * @generated
     */
    EClass ANNOTATED_OBJECT = eINSTANCE.getAnnotatedObject();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.CommentedObjectImpl <em>Commented Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.CommentedObjectImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getCommentedObject()
     * @generated
     */
    EClass COMMENTED_OBJECT = eINSTANCE.getCommentedObject();

    /**
     * The meta object literal for the '<em><b>Comment</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMMENTED_OBJECT__COMMENT = eINSTANCE.getCommentedObject_Comment();

    /**
     * The meta object literal for the '<em><b>Comment Extension</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMMENTED_OBJECT__COMMENT_EXTENSION = eINSTANCE.getCommentedObject_CommentExtension();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingObjectImpl <em>Dangling Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingObjectImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getDanglingObject()
     * @generated
     */
    EClass DANGLING_OBJECT = eINSTANCE.getDanglingObject();

    /**
     * The meta object literal for the '<em><b>Dangling</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DANGLING_OBJECT__DANGLING = eINSTANCE.getDanglingObject_Dangling();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl <em>Dangling Property</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DanglingPropertyImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getDanglingProperty()
     * @generated
     */
    EClass DANGLING_PROPERTY = eINSTANCE.getDanglingProperty();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DatatypeImpl <em>Datatype</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DatatypeImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getDatatype()
     * @generated
     */
    EClass DATATYPE = eINSTANCE.getDatatype();

    /**
     * The meta object literal for the '<em><b>Abstract</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DATATYPE__ABSTRACT = eINSTANCE.getDatatype_Abstract();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DatatypeValueImpl <em>Datatype Value</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DatatypeValueImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getDatatypeValue()
     * @generated
     */
    EClass DATATYPE_VALUE = eINSTANCE.getDatatypeValue();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DATATYPE_VALUE__VALUE = eINSTANCE.getDatatypeValue_Value();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DbxrefImpl <em>Dbxref</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DbxrefImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getDbxref()
     * @generated
     */
    EClass DBXREF = eINSTANCE.getDbxref();

    /**
     * The meta object literal for the '<em><b>Nested Value</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DBXREF__NESTED_VALUE = eINSTANCE.getDbxref_NestedValue();

    /**
     * The meta object literal for the '<em><b>Def Ref</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DBXREF__DEF_REF = eINSTANCE.getDbxref_DefRef();

    /**
     * The meta object literal for the '<em><b>Desc</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DBXREF__DESC = eINSTANCE.getDbxref_Desc();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DBXREF__TYPE = eINSTANCE.getDbxref_Type();

    /**
     * The meta object literal for the '<em><b>Synonym</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DBXREF__SYNONYM = eINSTANCE.getDbxref_Synonym();

    /**
     * The meta object literal for the '<em><b>Database ID</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DBXREF__DATABASE_ID = eINSTANCE.getDbxref_DatabaseID();

    /**
     * The meta object literal for the '<em><b>Database</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DBXREF__DATABASE = eINSTANCE.getDbxref_Database();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DbxrefedObjectImpl <em>Dbxrefed Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DbxrefedObjectImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getDbxrefedObject()
     * @generated
     */
    EClass DBXREFED_OBJECT = eINSTANCE.getDbxrefedObject();

    /**
     * The meta object literal for the '<em><b>Dbxrefs</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DBXREFED_OBJECT__DBXREFS = eINSTANCE.getDbxrefedObject_Dbxrefs();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DefinedObjectImpl <em>Defined Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.DefinedObjectImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getDefinedObject()
     * @generated
     */
    EClass DEFINED_OBJECT = eINSTANCE.getDefinedObject();

    /**
     * The meta object literal for the '<em><b>Definition</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DEFINED_OBJECT__DEFINITION = eINSTANCE.getDefinedObject_Definition();

    /**
     * The meta object literal for the '<em><b>Def Dbxrefs</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DEFINED_OBJECT__DEF_DBXREFS = eINSTANCE.getDefinedObject_DefDbxrefs();

    /**
     * The meta object literal for the '<em><b>Definition Extension</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DEFINED_OBJECT__DEFINITION_EXTENSION = eINSTANCE.getDefinedObject_DefinitionExtension();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.FieldPathImpl <em>Field Path</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.FieldPathImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getFieldPath()
     * @generated
     */
    EClass FIELD_PATH = eINSTANCE.getFieldPath();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.FieldPathSpecImpl <em>Field Path Spec</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.FieldPathSpecImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getFieldPathSpec()
     * @generated
     */
    EClass FIELD_PATH_SPEC = eINSTANCE.getFieldPathSpec();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiableObjectImpl <em>Identifiable Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiableObjectImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getIdentifiableObject()
     * @generated
     */
    EClass IDENTIFIABLE_OBJECT = eINSTANCE.getIdentifiableObject();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute IDENTIFIABLE_OBJECT__ID = eINSTANCE.getIdentifiableObject_Id();

    /**
     * The meta object literal for the '<em><b>Anonymous</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute IDENTIFIABLE_OBJECT__ANONYMOUS = eINSTANCE.getIdentifiableObject_Anonymous();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiedObjectImpl <em>Identified Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiedObjectImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getIdentifiedObject()
     * @generated
     */
    EClass IDENTIFIED_OBJECT = eINSTANCE.getIdentifiedObject();

    /**
     * The meta object literal for the '<em><b>Type Extension</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IDENTIFIED_OBJECT__TYPE_EXTENSION = eINSTANCE.getIdentifiedObject_TypeExtension();

    /**
     * The meta object literal for the '<em><b>Built In</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute IDENTIFIED_OBJECT__BUILT_IN = eINSTANCE.getIdentifiedObject_BuiltIn();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute IDENTIFIED_OBJECT__NAME = eINSTANCE.getIdentifiedObject_Name();

    /**
     * The meta object literal for the '<em><b>Name Extension</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IDENTIFIED_OBJECT__NAME_EXTENSION = eINSTANCE.getIdentifiedObject_NameExtension();

    /**
     * The meta object literal for the '<em><b>Id Extension</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IDENTIFIED_OBJECT__ID_EXTENSION = eINSTANCE.getIdentifiedObject_IdExtension();

    /**
     * The meta object literal for the '<em><b>Anonymous Extension</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION = eINSTANCE.getIdentifiedObject_AnonymousExtension();

    /**
     * The meta object literal for the '<em><b>Property Values</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IDENTIFIED_OBJECT__PROPERTY_VALUES = eINSTANCE.getIdentifiedObject_PropertyValues();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiedObjectIndexImpl <em>Identified Object Index</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdentifiedObjectIndexImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getIdentifiedObjectIndex()
     * @generated
     */
    EClass IDENTIFIED_OBJECT_INDEX = eINSTANCE.getIdentifiedObjectIndex();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ImpliableImpl <em>Impliable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ImpliableImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getImpliable()
     * @generated
     */
    EClass IMPLIABLE = eINSTANCE.getImpliable();

    /**
     * The meta object literal for the '<em><b>Implied</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute IMPLIABLE__IMPLIED = eINSTANCE.getImpliable_Implied();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.InstanceImpl <em>Instance</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.InstanceImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getInstance()
     * @generated
     */
    EClass INSTANCE = eINSTANCE.getInstance();

    /**
     * The meta object literal for the '<em><b>Obo Properties Values</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INSTANCE__OBO_PROPERTIES_VALUES = eINSTANCE.getInstance_OboPropertiesValues();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkImpl <em>Link</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getLink()
     * @generated
     */
    EClass LINK = eINSTANCE.getLink();

    /**
     * The meta object literal for the '<em><b>Namespace</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LINK__NAMESPACE = eINSTANCE.getLink_Namespace();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkDatabaseImpl <em>Link Database</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkDatabaseImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getLinkDatabase()
     * @generated
     */
    EClass LINK_DATABASE = eINSTANCE.getLinkDatabase();

    /**
     * The meta object literal for the '<em><b>Session</b></em>' container reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LINK_DATABASE__SESSION = eINSTANCE.getLinkDatabase_Session();

    /**
     * The meta object literal for the '<em><b>Objects</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LINK_DATABASE__OBJECTS = eINSTANCE.getLinkDatabase_Objects();

    /**
     * The meta object literal for the '<em><b>Properties</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LINK_DATABASE__PROPERTIES = eINSTANCE.getLinkDatabase_Properties();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkLinkedObjectImpl <em>Link Linked Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkLinkedObjectImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getLinkLinkedObject()
     * @generated
     */
    EClass LINK_LINKED_OBJECT = eINSTANCE.getLinkLinkedObject();

    /**
     * The meta object literal for the '<em><b>Link</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LINK_LINKED_OBJECT__LINK = eINSTANCE.getLinkLinkedObject_Link();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkedObjectImpl <em>Linked Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.LinkedObjectImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getLinkedObject()
     * @generated
     */
    EClass LINKED_OBJECT = eINSTANCE.getLinkedObject();

    /**
     * The meta object literal for the '<em><b>Parents</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LINKED_OBJECT__PARENTS = eINSTANCE.getLinkedObject_Parents();

    /**
     * The meta object literal for the '<em><b>Children</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LINKED_OBJECT__CHILDREN = eINSTANCE.getLinkedObject_Children();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ModificationMetadataObjectImpl <em>Modification Metadata Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ModificationMetadataObjectImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getModificationMetadataObject()
     * @generated
     */
    EClass MODIFICATION_METADATA_OBJECT = eINSTANCE.getModificationMetadataObject();

    /**
     * The meta object literal for the '<em><b>Created By</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MODIFICATION_METADATA_OBJECT__CREATED_BY = eINSTANCE.getModificationMetadataObject_CreatedBy();

    /**
     * The meta object literal for the '<em><b>Created By Extension</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION = eINSTANCE.getModificationMetadataObject_CreatedByExtension();

    /**
     * The meta object literal for the '<em><b>Modified By</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MODIFICATION_METADATA_OBJECT__MODIFIED_BY = eINSTANCE.getModificationMetadataObject_ModifiedBy();

    /**
     * The meta object literal for the '<em><b>Modified By Extension</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION = eINSTANCE.getModificationMetadataObject_ModifiedByExtension();

    /**
     * The meta object literal for the '<em><b>Creation Date</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MODIFICATION_METADATA_OBJECT__CREATION_DATE = eINSTANCE.getModificationMetadataObject_CreationDate();

    /**
     * The meta object literal for the '<em><b>Creation Date Extension</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION = eINSTANCE.getModificationMetadataObject_CreationDateExtension();

    /**
     * The meta object literal for the '<em><b>Modification Date</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE = eINSTANCE.getModificationMetadataObject_ModificationDate();

    /**
     * The meta object literal for the '<em><b>Modification Date Extension</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION = eINSTANCE.getModificationMetadataObject_ModificationDateExtension();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.MultiIDObjectImpl <em>Multi ID Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.MultiIDObjectImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getMultiIDObject()
     * @generated
     */
    EClass MULTI_ID_OBJECT = eINSTANCE.getMultiIDObject();

    /**
     * The meta object literal for the '<em><b>Secondary Ids</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MULTI_ID_OBJECT__SECONDARY_IDS = eINSTANCE.getMultiIDObject_SecondaryIds();

    /**
     * The meta object literal for the '<em><b>Secondary Id Extension</b></em>' map feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MULTI_ID_OBJECT__SECONDARY_ID_EXTENSION = eINSTANCE.getMultiIDObject_SecondaryIdExtension();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.MutableLinkDatabaseImpl <em>Mutable Link Database</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.MutableLinkDatabaseImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getMutableLinkDatabase()
     * @generated
     */
    EClass MUTABLE_LINK_DATABASE = eINSTANCE.getMutableLinkDatabase();

    /**
     * The meta object literal for the '<em><b>Identified Object Index</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MUTABLE_LINK_DATABASE__IDENTIFIED_OBJECT_INDEX = eINSTANCE.getMutableLinkDatabase_IdentifiedObjectIndex();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.NamespaceImpl <em>Namespace</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.NamespaceImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getNamespace()
     * @generated
     */
    EClass NAMESPACE = eINSTANCE.getNamespace();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NAMESPACE__ID = eINSTANCE.getNamespace_Id();

    /**
     * The meta object literal for the '<em><b>Path</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NAMESPACE__PATH = eINSTANCE.getNamespace_Path();

    /**
     * The meta object literal for the '<em><b>Private Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NAMESPACE__PRIVATE_ID = eINSTANCE.getNamespace_PrivateId();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.NamespacedObjectImpl <em>Namespaced Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.NamespacedObjectImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getNamespacedObject()
     * @generated
     */
    EClass NAMESPACED_OBJECT = eINSTANCE.getNamespacedObject();

    /**
     * The meta object literal for the '<em><b>Namespace</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NAMESPACED_OBJECT__NAMESPACE = eINSTANCE.getNamespacedObject_Namespace();

    /**
     * The meta object literal for the '<em><b>Namespace Extension</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NAMESPACED_OBJECT__NAMESPACE_EXTENSION = eINSTANCE.getNamespacedObject_NamespaceExtension();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.NestedValueImpl <em>Nested Value</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.NestedValueImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getNestedValue()
     * @generated
     */
    EClass NESTED_VALUE = eINSTANCE.getNestedValue();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NESTED_VALUE__NAME = eINSTANCE.getNestedValue_Name();

    /**
     * The meta object literal for the '<em><b>Property Values</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference NESTED_VALUE__PROPERTY_VALUES = eINSTANCE.getNestedValue_PropertyValues();

    /**
     * The meta object literal for the '<em><b>Suggested Comment</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NESTED_VALUE__SUGGESTED_COMMENT = eINSTANCE.getNestedValue_SuggestedComment();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOClassImpl <em>OBO Class</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOClassImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getOBOClass()
     * @generated
     */
    EClass OBO_CLASS = eINSTANCE.getOBOClass();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOObjectImpl <em>OBO Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOObjectImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getOBOObject()
     * @generated
     */
    EClass OBO_OBJECT = eINSTANCE.getOBOObject();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl <em>OBO Property</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOPropertyImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getOBOProperty()
     * @generated
     */
    EClass OBO_PROPERTY = eINSTANCE.getOBOProperty();

    /**
     * The meta object literal for the '<em><b>Cyclic</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBO_PROPERTY__CYCLIC = eINSTANCE.getOBOProperty_Cyclic();

    /**
     * The meta object literal for the '<em><b>Symmetric</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBO_PROPERTY__SYMMETRIC = eINSTANCE.getOBOProperty_Symmetric();

    /**
     * The meta object literal for the '<em><b>Transitive</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBO_PROPERTY__TRANSITIVE = eINSTANCE.getOBOProperty_Transitive();

    /**
     * The meta object literal for the '<em><b>Reflexive</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBO_PROPERTY__REFLEXIVE = eINSTANCE.getOBOProperty_Reflexive();

    /**
     * The meta object literal for the '<em><b>Always Implies Inverse</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE = eINSTANCE.getOBOProperty_AlwaysImpliesInverse();

    /**
     * The meta object literal for the '<em><b>Dummy</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBO_PROPERTY__DUMMY = eINSTANCE.getOBOProperty_Dummy();

    /**
     * The meta object literal for the '<em><b>Metadata Tag</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBO_PROPERTY__METADATA_TAG = eINSTANCE.getOBOProperty_MetadataTag();

    /**
     * The meta object literal for the '<em><b>Cyclic Extension</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_PROPERTY__CYCLIC_EXTENSION = eINSTANCE.getOBOProperty_CyclicExtension();

    /**
     * The meta object literal for the '<em><b>Symmetric Extension</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_PROPERTY__SYMMETRIC_EXTENSION = eINSTANCE.getOBOProperty_SymmetricExtension();

    /**
     * The meta object literal for the '<em><b>Transitive Extension</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_PROPERTY__TRANSITIVE_EXTENSION = eINSTANCE.getOBOProperty_TransitiveExtension();

    /**
     * The meta object literal for the '<em><b>Reflexive Extension</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_PROPERTY__REFLEXIVE_EXTENSION = eINSTANCE.getOBOProperty_ReflexiveExtension();

    /**
     * The meta object literal for the '<em><b>Always Implies Inverse Extension</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION = eINSTANCE.getOBOProperty_AlwaysImpliesInverseExtension();

    /**
     * The meta object literal for the '<em><b>Domain Extension</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_PROPERTY__DOMAIN_EXTENSION = eINSTANCE.getOBOProperty_DomainExtension();

    /**
     * The meta object literal for the '<em><b>Range Extension</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_PROPERTY__RANGE_EXTENSION = eINSTANCE.getOBOProperty_RangeExtension();

    /**
     * The meta object literal for the '<em><b>Range</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_PROPERTY__RANGE = eINSTANCE.getOBOProperty_Range();

    /**
     * The meta object literal for the '<em><b>Domain</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_PROPERTY__DOMAIN = eINSTANCE.getOBOProperty_Domain();

    /**
     * The meta object literal for the '<em><b>Non Inheritable</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBO_PROPERTY__NON_INHERITABLE = eINSTANCE.getOBOProperty_NonInheritable();

    /**
     * The meta object literal for the '<em><b>Disjoint Over</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_PROPERTY__DISJOINT_OVER = eINSTANCE.getOBOProperty_DisjointOver();

    /**
     * The meta object literal for the '<em><b>Transitive Over</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_PROPERTY__TRANSITIVE_OVER = eINSTANCE.getOBOProperty_TransitiveOver();

    /**
     * The meta object literal for the '<em><b>Holds Over Chains</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_PROPERTY__HOLDS_OVER_CHAINS = eINSTANCE.getOBOProperty_HoldsOverChains();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBORestrictionImpl <em>OBO Restriction</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBORestrictionImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getOBORestriction()
     * @generated
     */
    EClass OBO_RESTRICTION = eINSTANCE.getOBORestriction();

    /**
     * The meta object literal for the '<em><b>Cardinality</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBO_RESTRICTION__CARDINALITY = eINSTANCE.getOBORestriction_Cardinality();

    /**
     * The meta object literal for the '<em><b>Max Cardinality</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBO_RESTRICTION__MAX_CARDINALITY = eINSTANCE.getOBORestriction_MaxCardinality();

    /**
     * The meta object literal for the '<em><b>Min Cardinality</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBO_RESTRICTION__MIN_CARDINALITY = eINSTANCE.getOBORestriction_MinCardinality();

    /**
     * The meta object literal for the '<em><b>Completes</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBO_RESTRICTION__COMPLETES = eINSTANCE.getOBORestriction_Completes();

    /**
     * The meta object literal for the '<em><b>Inverse Completes</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBO_RESTRICTION__INVERSE_COMPLETES = eINSTANCE.getOBORestriction_InverseCompletes();

    /**
     * The meta object literal for the '<em><b>Necessarily True</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBO_RESTRICTION__NECESSARILY_TRUE = eINSTANCE.getOBORestriction_NecessarilyTrue();

    /**
     * The meta object literal for the '<em><b>Inverse Necessarily True</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBO_RESTRICTION__INVERSE_NECESSARILY_TRUE = eINSTANCE.getOBORestriction_InverseNecessarilyTrue();

    /**
     * The meta object literal for the '<em><b>Additional Arguments</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_RESTRICTION__ADDITIONAL_ARGUMENTS = eINSTANCE.getOBORestriction_AdditionalArguments();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOSessionImpl <em>OBO Session</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OBOSessionImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getOBOSession()
     * @generated
     */
    EClass OBO_SESSION = eINSTANCE.getOBOSession();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBO_SESSION__ID = eINSTANCE.getOBOSession_Id();

    /**
     * The meta object literal for the '<em><b>Objects</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_SESSION__OBJECTS = eINSTANCE.getOBOSession_Objects();

    /**
     * The meta object literal for the '<em><b>Link Database</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_SESSION__LINK_DATABASE = eINSTANCE.getOBOSession_LinkDatabase();

    /**
     * The meta object literal for the '<em><b>Default Namespace</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_SESSION__DEFAULT_NAMESPACE = eINSTANCE.getOBOSession_DefaultNamespace();

    /**
     * The meta object literal for the '<em><b>Namespaces</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_SESSION__NAMESPACES = eINSTANCE.getOBOSession_Namespaces();

    /**
     * The meta object literal for the '<em><b>Property Values</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_SESSION__PROPERTY_VALUES = eINSTANCE.getOBOSession_PropertyValues();

    /**
     * The meta object literal for the '<em><b>Unknown Stanzas</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_SESSION__UNKNOWN_STANZAS = eINSTANCE.getOBOSession_UnknownStanzas();

    /**
     * The meta object literal for the '<em><b>Subsets</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_SESSION__SUBSETS = eINSTANCE.getOBOSession_Subsets();

    /**
     * The meta object literal for the '<em><b>Categories</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_SESSION__CATEGORIES = eINSTANCE.getOBOSession_Categories();

    /**
     * The meta object literal for the '<em><b>Synonym Types</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_SESSION__SYNONYM_TYPES = eINSTANCE.getOBOSession_SynonymTypes();

    /**
     * The meta object literal for the '<em><b>Synonym Categories</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_SESSION__SYNONYM_CATEGORIES = eINSTANCE.getOBOSession_SynonymCategories();

    /**
     * The meta object literal for the '<em><b>Current Filenames</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBO_SESSION__CURRENT_FILENAMES = eINSTANCE.getOBOSession_CurrentFilenames();

    /**
     * The meta object literal for the '<em><b>Current User</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBO_SESSION__CURRENT_USER = eINSTANCE.getOBOSession_CurrentUser();

    /**
     * The meta object literal for the '<em><b>Id Spaces</b></em>' map feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_SESSION__ID_SPACES = eINSTANCE.getOBOSession_IdSpaces();

    /**
     * The meta object literal for the '<em><b>Load Remark</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBO_SESSION__LOAD_REMARK = eINSTANCE.getOBOSession_LoadRemark();

    /**
     * The meta object literal for the '<em><b>All Dbx Refs Container</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_SESSION__ALL_DBX_REFS_CONTAINER = eINSTANCE.getOBOSession_AllDbxRefsContainer();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObjectFactoryImpl <em>Object Factory</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObjectFactoryImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getObjectFactory()
     * @generated
     */
    EClass OBJECT_FACTORY = eINSTANCE.getObjectFactory();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObjectFieldImpl <em>Object Field</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObjectFieldImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getObjectField()
     * @generated
     */
    EClass OBJECT_FIELD = eINSTANCE.getObjectField();

    /**
     * The meta object literal for the '<em><b>Values</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBJECT_FIELD__VALUES = eINSTANCE.getObjectField_Values();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObsoletableObjectImpl <em>Obsoletable Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObsoletableObjectImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getObsoletableObject()
     * @generated
     */
    EClass OBSOLETABLE_OBJECT = eINSTANCE.getObsoletableObject();

    /**
     * The meta object literal for the '<em><b>Obsolete</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OBSOLETABLE_OBJECT__OBSOLETE = eINSTANCE.getObsoletableObject_Obsolete();

    /**
     * The meta object literal for the '<em><b>Replaced By</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBSOLETABLE_OBJECT__REPLACED_BY = eINSTANCE.getObsoletableObject_ReplacedBy();

    /**
     * The meta object literal for the '<em><b>Consider Replacements</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBSOLETABLE_OBJECT__CONSIDER_REPLACEMENTS = eINSTANCE.getObsoletableObject_ConsiderReplacements();

    /**
     * The meta object literal for the '<em><b>Consider Extension</b></em>' map feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBSOLETABLE_OBJECT__CONSIDER_EXTENSION = eINSTANCE.getObsoletableObject_ConsiderExtension();

    /**
     * The meta object literal for the '<em><b>Replaced By Extension</b></em>' map feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBSOLETABLE_OBJECT__REPLACED_BY_EXTENSION = eINSTANCE.getObsoletableObject_ReplacedByExtension();

    /**
     * The meta object literal for the '<em><b>Obsolete Extension</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBSOLETABLE_OBJECT__OBSOLETE_EXTENSION = eINSTANCE.getObsoletableObject_ObsoleteExtension();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.PathCapableImpl <em>Path Capable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.PathCapableImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getPathCapable()
     * @generated
     */
    EClass PATH_CAPABLE = eINSTANCE.getPathCapable();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.PropertyValueImpl <em>Property Value</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.PropertyValueImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getPropertyValue()
     * @generated
     */
    EClass PROPERTY_VALUE = eINSTANCE.getPropertyValue();

    /**
     * The meta object literal for the '<em><b>Property</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PROPERTY_VALUE__PROPERTY = eINSTANCE.getPropertyValue_Property();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PROPERTY_VALUE__VALUE = eINSTANCE.getPropertyValue_Value();

    /**
     * The meta object literal for the '<em><b>Line Number</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PROPERTY_VALUE__LINE_NUMBER = eINSTANCE.getPropertyValue_LineNumber();

    /**
     * The meta object literal for the '<em><b>Filename</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PROPERTY_VALUE__FILENAME = eINSTANCE.getPropertyValue_Filename();

    /**
     * The meta object literal for the '<em><b>Line</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PROPERTY_VALUE__LINE = eINSTANCE.getPropertyValue_Line();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.RelationshipImpl <em>Relationship</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.RelationshipImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getRelationship()
     * @generated
     */
    EClass RELATIONSHIP = eINSTANCE.getRelationship();

    /**
     * The meta object literal for the '<em><b>Child</b></em>' container reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RELATIONSHIP__CHILD = eINSTANCE.getRelationship_Child();

    /**
     * The meta object literal for the '<em><b>Parent</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RELATIONSHIP__PARENT = eINSTANCE.getRelationship_Parent();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RELATIONSHIP__TYPE = eINSTANCE.getRelationship_Type();

    /**
     * The meta object literal for the '<em><b>Nested Value</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RELATIONSHIP__NESTED_VALUE = eINSTANCE.getRelationship_NestedValue();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.RootAlgorithmImpl <em>Root Algorithm</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.RootAlgorithmImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getRootAlgorithm()
     * @generated
     */
    EClass ROOT_ALGORITHM = eINSTANCE.getRootAlgorithm();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SubsetObjectImpl <em>Subset Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SubsetObjectImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getSubsetObject()
     * @generated
     */
    EClass SUBSET_OBJECT = eINSTANCE.getSubsetObject();

    /**
     * The meta object literal for the '<em><b>Subsets</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SUBSET_OBJECT__SUBSETS = eINSTANCE.getSubsetObject_Subsets();

    /**
     * The meta object literal for the '<em><b>Category Extensions</b></em>' map feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SUBSET_OBJECT__CATEGORY_EXTENSIONS = eINSTANCE.getSubsetObject_CategoryExtensions();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymImpl <em>Synonym</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getSynonym()
     * @generated
     */
    EClass SYNONYM = eINSTANCE.getSynonym();

    /**
     * The meta object literal for the '<em><b>UNKNOWN SCOPE</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SYNONYM__UNKNOWN_SCOPE = eINSTANCE.getSynonym_UNKNOWN_SCOPE();

    /**
     * The meta object literal for the '<em><b>RELATED SYNONYM</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SYNONYM__RELATED_SYNONYM = eINSTANCE.getSynonym_RELATED_SYNONYM();

    /**
     * The meta object literal for the '<em><b>EXACT SYNONYM</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SYNONYM__EXACT_SYNONYM = eINSTANCE.getSynonym_EXACT_SYNONYM();

    /**
     * The meta object literal for the '<em><b>NARROW SYNONYM</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SYNONYM__NARROW_SYNONYM = eINSTANCE.getSynonym_NARROW_SYNONYM();

    /**
     * The meta object literal for the '<em><b>BROAD SYNONYM</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SYNONYM__BROAD_SYNONYM = eINSTANCE.getSynonym_BROAD_SYNONYM();

    /**
     * The meta object literal for the '<em><b>Synonym Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SYNONYM__SYNONYM_TYPE = eINSTANCE.getSynonym_SynonymType();

    /**
     * The meta object literal for the '<em><b>Nested Value</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SYNONYM__NESTED_VALUE = eINSTANCE.getSynonym_NestedValue();

    /**
     * The meta object literal for the '<em><b>Scope</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SYNONYM__SCOPE = eINSTANCE.getSynonym_Scope();

    /**
     * The meta object literal for the '<em><b>Xrefs</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SYNONYM__XREFS = eINSTANCE.getSynonym_Xrefs();

    /**
     * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SYNONYM__TEXT = eINSTANCE.getSynonym_Text();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymTypeImpl <em>Synonym Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymTypeImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getSynonymType()
     * @generated
     */
    EClass SYNONYM_TYPE = eINSTANCE.getSynonymType();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SYNONYM_TYPE__ID = eINSTANCE.getSynonymType_Id();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SYNONYM_TYPE__NAME = eINSTANCE.getSynonymType_Name();

    /**
     * The meta object literal for the '<em><b>Scope</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SYNONYM_TYPE__SCOPE = eINSTANCE.getSynonymType_Scope();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymedObjectImpl <em>Synonymed Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SynonymedObjectImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getSynonymedObject()
     * @generated
     */
    EClass SYNONYMED_OBJECT = eINSTANCE.getSynonymedObject();

    /**
     * The meta object literal for the '<em><b>Synonyms</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SYNONYMED_OBJECT__SYNONYMS = eINSTANCE.getSynonymedObject_Synonyms();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.TermSubsetImpl <em>Term Subset</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.TermSubsetImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getTermSubset()
     * @generated
     */
    EClass TERM_SUBSET = eINSTANCE.getTermSubset();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TERM_SUBSET__NAME = eINSTANCE.getTermSubset_Name();

    /**
     * The meta object literal for the '<em><b>Desc</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TERM_SUBSET__DESC = eINSTANCE.getTermSubset_Desc();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.TypeImpl <em>Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.TypeImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getType()
     * @generated
     */
    EClass TYPE = eINSTANCE.getType();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.UnknownStanzaImpl <em>Unknown Stanza</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.UnknownStanzaImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getUnknownStanza()
     * @generated
     */
    EClass UNKNOWN_STANZA = eINSTANCE.getUnknownStanza();

    /**
     * The meta object literal for the '<em><b>Namespace</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference UNKNOWN_STANZA__NAMESPACE = eINSTANCE.getUnknownStanza_Namespace();

    /**
     * The meta object literal for the '<em><b>Property Values</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference UNKNOWN_STANZA__PROPERTY_VALUES = eINSTANCE.getUnknownStanza_PropertyValues();

    /**
     * The meta object literal for the '<em><b>Nested Values</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference UNKNOWN_STANZA__NESTED_VALUES = eINSTANCE.getUnknownStanza_NestedValues();

    /**
     * The meta object literal for the '<em><b>Stanza</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute UNKNOWN_STANZA__STANZA = eINSTANCE.getUnknownStanza_Stanza();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ValueImpl <em>Value</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ValueImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getValue()
     * @generated
     */
    EClass VALUE = eINSTANCE.getValue();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VALUE__TYPE = eINSTANCE.getValue_Type();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ValueDatabaseImpl <em>Value Database</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ValueDatabaseImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getValueDatabase()
     * @generated
     */
    EClass VALUE_DATABASE = eINSTANCE.getValueDatabase();

    /**
     * The meta object literal for the '<em><b>Objects</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VALUE_DATABASE__OBJECTS = eINSTANCE.getValueDatabase_Objects();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ValueLinkImpl <em>Value Link</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ValueLinkImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getValueLink()
     * @generated
     */
    EClass VALUE_LINK = eINSTANCE.getValueLink();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VALUE_LINK__VALUE = eINSTANCE.getValueLink_Value();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.CloneableImpl <em>Cloneable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.CloneableImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getCloneable()
     * @generated
     */
    EClass CLONEABLE = eINSTANCE.getCloneable();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SerializableImpl <em>Serializable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.SerializableImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getSerializable()
     * @generated
     */
    EClass SERIALIZABLE = eINSTANCE.getSerializable();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ComparableImpl <em>Comparable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ComparableImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getComparable()
     * @generated
     */
    EClass COMPARABLE = eINSTANCE.getComparable();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.StringToNestedValueMapImpl <em>String To Nested Value Map</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.StringToNestedValueMapImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getStringToNestedValueMap()
     * @generated
     */
    EClass STRING_TO_NESTED_VALUE_MAP = eINSTANCE.getStringToNestedValueMap();

    /**
     * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute STRING_TO_NESTED_VALUE_MAP__KEY = eINSTANCE.getStringToNestedValueMap_Key();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference STRING_TO_NESTED_VALUE_MAP__VALUE = eINSTANCE.getStringToNestedValueMap_Value();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObsoletableObjectToNestedValueMapImpl <em>Obsoletable Object To Nested Value Map</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ObsoletableObjectToNestedValueMapImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getObsoletableObjectToNestedValueMap()
     * @generated
     */
    EClass OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP = eINSTANCE.getObsoletableObjectToNestedValueMap();

    /**
     * The meta object literal for the '<em><b>Key</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP__KEY = eINSTANCE.getObsoletableObjectToNestedValueMap_Key();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP__VALUE = eINSTANCE.getObsoletableObjectToNestedValueMap_Value();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.TermSubsetToNestedValueMapImpl <em>Term Subset To Nested Value Map</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.TermSubsetToNestedValueMapImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getTermSubsetToNestedValueMap()
     * @generated
     */
    EClass TERM_SUBSET_TO_NESTED_VALUE_MAP = eINSTANCE.getTermSubsetToNestedValueMap();

    /**
     * The meta object literal for the '<em><b>Key</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TERM_SUBSET_TO_NESTED_VALUE_MAP__KEY = eINSTANCE.getTermSubsetToNestedValueMap_Key();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TERM_SUBSET_TO_NESTED_VALUE_MAP__VALUE = eINSTANCE.getTermSubsetToNestedValueMap_Value();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdSpacesMapImpl <em>Id Spaces Map</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.IdSpacesMapImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getIdSpacesMap()
     * @generated
     */
    EClass ID_SPACES_MAP = eINSTANCE.getIdSpacesMap();

    /**
     * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ID_SPACES_MAP__KEY = eINSTANCE.getIdSpacesMap_Key();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ID_SPACES_MAP__VALUE = eINSTANCE.getIdSpacesMap_Value();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ListOfPropertiesImpl <em>List Of Properties</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.ListOfPropertiesImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getListOfProperties()
     * @generated
     */
    EClass LIST_OF_PROPERTIES = eINSTANCE.getListOfProperties();

    /**
     * The meta object literal for the '<em><b>Properties</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LIST_OF_PROPERTIES__PROPERTIES = eINSTANCE.getListOfProperties_Properties();

    /**
     * The meta object literal for the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OboPropertyValueImpl <em>Obo Property Value</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.OboPropertyValueImpl
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getOboPropertyValue()
     * @generated
     */
    EClass OBO_PROPERTY_VALUE = eINSTANCE.getOboPropertyValue();

    /**
     * The meta object literal for the '<em><b>Property</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_PROPERTY_VALUE__PROPERTY = eINSTANCE.getOboPropertyValue_Property();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OBO_PROPERTY_VALUE__VALUE = eINSTANCE.getOboPropertyValue_Value();

    /**
     * The meta object literal for the '<em>Date</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see java.util.Date
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getDate()
     * @generated
     */
    EDataType DATE = eINSTANCE.getDate();

    /**
     * The meta object literal for the '<em>Ecore Object</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.eclipse.emf.ecore.EObject
     * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.impl.obodatamodelPackageImpl#getEcoreObject()
     * @generated
     */
    EDataType ECORE_OBJECT = eINSTANCE.getEcoreObject();

  }

} //obodatamodelPackage
