/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Link Database</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase#getSession <em>Session</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase#getObjects <em>Objects</em>}</li>
 *   <li>{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase#getProperties <em>Properties</em>}</li>
 * </ul>
 *
 * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getLinkDatabase()
 * @model
 * @generated
 */
public interface LinkDatabase extends IdentifiedObjectIndex, Serializable, br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable {
  /**
   * Returns the value of the '<em><b>Session</b></em>' container reference.
   * It is bidirectional and its opposite is '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getLinkDatabase <em>Link Database</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Session</em>' container reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Session</em>' container reference.
   * @see #setSession(OBOSession)
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getLinkDatabase_Session()
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession#getLinkDatabase
   * @model opposite="linkDatabase" transient="false"
   * @generated
   */
  OBOSession getSession();

  /**
   * Sets the value of the '{@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase#getSession <em>Session</em>}' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Session</em>' container reference.
   * @see #getSession()
   * @generated
   */
  void setSession(OBOSession value);

  /**
   * Returns the value of the '<em><b>Objects</b></em>' reference list.
   * The list contents are of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Objects</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Objects</em>' reference list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getLinkDatabase_Objects()
   * @model ordered="false"
   * @generated
   */
  EList<IdentifiedObject> getObjects();

  /**
   * Returns the value of the '<em><b>Properties</b></em>' reference list.
   * The list contents are of type {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Properties</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Properties</em>' reference list.
   * @see br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage#getLinkDatabase_Properties()
   * @model ordered="false"
   * @generated
   */
  EList<OBOProperty> getProperties();

} // LinkDatabase
