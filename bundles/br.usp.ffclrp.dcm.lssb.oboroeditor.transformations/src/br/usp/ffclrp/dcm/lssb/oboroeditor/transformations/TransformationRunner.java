package br.usp.ffclrp.dcm.lssb.oboroeditor.transformations;


import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.m2m.atl.core.IExtractor;
import org.eclipse.m2m.atl.core.IInjector;
import org.eclipse.m2m.atl.core.IModel;
import org.eclipse.m2m.atl.core.IReferenceModel;
import org.eclipse.m2m.atl.core.ModelFactory;
import org.eclipse.m2m.atl.core.emf.EMFExtractor;
import org.eclipse.m2m.atl.core.emf.EMFInjector;
import org.eclipse.m2m.atl.core.emf.EMFModel;
import org.eclipse.m2m.atl.core.emf.EMFModelFactory;
import org.eclipse.m2m.atl.core.emf.EMFReferenceModel;
import org.eclipse.m2m.atl.core.launch.ILauncher;
import org.eclipse.m2m.atl.engine.emfvm.launch.EMFVMLauncher;
import org.eclipse.uml2.uml.UMLPackage;

public class TransformationRunner {

  // TRANSFORMATION MODULES
  public static final String UML2ODM_BASIC_TRANSFORMATION_MODULE_URI =
      "platform:/plugin/br.usp.ffclrp.dcm.lssb.oboroeditor.transformations"
          + "/resources/UML2OBODatamodel.asm";

  public static final String UML2ODM_BFO_SUPERIMPOSITION_MODULE_URI =
      "platform:/plugin/br.usp.ffclrp.dcm.lssb.oboroeditor.transformations"
          + "/resources/UML2OBODatamodel-BFOSuperimposition.asm";

  public static final String UML2ODM_ROCORE_SUPERIMPOSITION_MODULE_URI =
      "platform:/plugin/br.usp.ffclrp.dcm.lssb.oboroeditor.transformations"
          + "/resources/UML2OBODatamodel-ROCoreSuperimposition.asm";
  
  public static final String ODM_ENRICHMENT_MODULE =
      "platform:/plugin/br.usp.ffclrp.dcm.lssb.oboroeditor.transformations"
          + "/resources/OBODatamodelMerging.asm";

  // METAMODELS
  public static final String UML_METAMODEL_URI = UMLPackage.eNS_URI;
  public static final String ODM_METAMODEL_URI =
      "platform:/plugin/br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel/model/obodatamodel.ecore";
  
  // PROFILES (IModels)
  public static final String OBO_PROFILE_URI =
      "platform:/plugin/br.usp.ffclrp.dcm.lssb.uml.profiles.obodatamodel"
      + "/resources/profile/obodatamodel.profile.uml";  
  /*public static final String OBO_PROFILE_URI =
      "http://dcm.ffclrp.usp.br/lssb/uml/profiles/obodatamodel.profile.uml";*/
  
  public static final String BFO_PROFILE_URI =
      "platform:/plugin/br.usp.ffclrp.lssb.uml.profiles.bfo"
      + "/resources/bfo-minimal/bfo-minimal.profile.uml";
  public static final String ROCORE_PROFILE_URI =
      "platform:/plugin/br.usp.ffclrp.lssb.uml.profiles.ro"
      + "/resources/ROCore/ROCore.profile.uml";

  /** The URI of the ROCore ontology represented as a ODM model */
  public static final String ROCORE_OBO_URI = 
      "platform:/plugin/br.usp.ffclrp.dcm.lssb.oboroeditor.transformations/resources/rocore/core.obodatamodel";

  private static final String ODM2UML_BASIC_TRANSFORMATION_MODULE_URI=
      "platform:/plugin/br.usp.ffclrp.dcm.lssb.oboroeditor.transformations"
          + "/resources/OBODatamodel2UML.asm";
  private static final String ODM2UML_BFO_SUPERIMPOSITION_MODULE_URI =
      "platform:/plugin/br.usp.ffclrp.dcm.lssb.oboroeditor.transformations"
          + "/resources/OBODatamodel2UML-BFOSuperimposition.asm";

  private static final String ODM2UML_ROCORE_SUPERIMPOSITION_MODULE_URI = 
      "platform:/plugin/br.usp.ffclrp.dcm.lssb.oboroeditor.transformations"
          + "/resources/OBODatamodel2UML-ROCoreSuperimposition.asm";
  
  static EMFModelFactory modelFactory;
  static EMFVMLauncher transformationLauncher;
  static EMFInjector injector;
  static EMFExtractor extractor;
  
  protected static void initialize() {
    modelFactory = new EMFModelFactory();
    transformationLauncher = new EMFVMLauncher();
    injector = new EMFInjector();
    extractor = new EMFExtractor();
  }
  
  protected static void unload() {
    modelFactory = null;
    transformationLauncher = null;
    injector = null;
    extractor = null;
  }


  public static void transformUML2OboRoOdmFile(InputStream umlSource, OutputStream odmTarget,
      IProgressMonitor monitor) {
    
    if(monitor != null) {
      monitor.subTask("UML2ODM-ROCore superimposed transformation");
    }
    
    try {
      /*
       * Initializations
       */
      initialize();
      

      //URL umlUrl = new URL(UML_METAMODEL_URI);
      URL odmUrl = new URL(ODM_METAMODEL_URI);


      // Load the transformation definition
      URL basicModuleUrl = new URL(UML2ODM_BASIC_TRANSFORMATION_MODULE_URI);
      InputStream transformationInputStream = basicModuleUrl.openConnection().getInputStream();
      Object basicModule = transformationLauncher.loadModule(transformationInputStream);
      
  
      URL bfoModuleUrl = new URL(UML2ODM_BFO_SUPERIMPOSITION_MODULE_URI);
      InputStream bfoModuleInputStream = bfoModuleUrl.openConnection().getInputStream();
      Object bfoModule = transformationLauncher.loadModule(bfoModuleInputStream);
    

      URL transformationModuleUrl = new URL(UML2ODM_ROCORE_SUPERIMPOSITION_MODULE_URI);
      InputStream rocoreModuleInputStream = transformationModuleUrl.openConnection().getInputStream();
      Object rocoreModule = transformationLauncher.loadModule(rocoreModuleInputStream);
              

      URL oboProfileUri = new URL(OBO_PROFILE_URI);
      InputStream oboProfileSource = oboProfileUri.openConnection().getInputStream();
      
      URL bfoProfileUri = new URL(BFO_PROFILE_URI);
      InputStream bfoProfileSource = bfoProfileUri.openConnection().getInputStream();


      URL rocoreProfileUri = new URL(ROCORE_PROFILE_URI);
      InputStream rocoreProfileSource = rocoreProfileUri.openConnection().getInputStream();

      URL rocoreOboUri = new URL(ROCORE_OBO_URI);
      InputStream rocoreOboSource = rocoreOboUri.openConnection().getInputStream();

      /*
       * Load metamodels 
       */
      IReferenceModel odmMetamodel = modelFactory.newReferenceModel();
      injector.inject(odmMetamodel, odmUrl.openConnection().getInputStream(),
          new HashMap<String,Object>());
      IModel odmModel = modelFactory.newModel(odmMetamodel);
      

      IReferenceModel umlMetamodel = modelFactory.newReferenceModel();
      injector.inject(umlMetamodel, UML_METAMODEL_URI, new HashMap<String,Object>());

      /*
       * Load models
       */
      
      
      System.out.println("começando a  injetar modelo uml");
      IModel umlModel = modelFactory.newModel(umlMetamodel);
      injector.inject(umlModel, umlSource, new HashMap<String,Object>());

      System.out.println("terminou injeção modelo uml");
      
      IModel oboProfile = modelFactory.newModel(umlMetamodel);
      injector.inject(oboProfile, oboProfileSource, new HashMap<String,Object>());

      IModel bfoProfile = modelFactory.newModel(umlMetamodel);
      injector.inject(bfoProfile, bfoProfileSource, new HashMap<String,Object>());

      IModel rocoreProfile = modelFactory.newModel(umlMetamodel);
      injector.inject(bfoProfile, rocoreProfileSource, new HashMap<String,Object>());
      

      IModel rocoreObo = modelFactory.newModel(odmMetamodel);
      injector.inject(rocoreObo, rocoreOboSource, new HashMap<String,Object>());


      /*
       * Run transformation
       */
      //create OUT: OBO from IN: UML, PROFILE: UML, BFOPROFILE : UML, ROPROFILE : UML, 
      // ROCORE : OBO;

      transformationLauncher.initialize(new HashMap<String, Object>());
      transformationLauncher.addInModel(umlModel, "IN", "UML");
      transformationLauncher.addInModel(oboProfile, "PROFILE", "UML");
      transformationLauncher.addInModel(bfoProfile, "BFOPROFILE", "UML");
      transformationLauncher.addInModel(rocoreProfile, "ROPROFILE", "UML");
      transformationLauncher.addInModel(rocoreObo, "ROCORE", "OBO");
      transformationLauncher.addOutModel(odmModel, "OUT", "OBO");


      System.out.println("começando a transformaçõa");

      transformationLauncher.launch(ILauncher.RUN_MODE, monitor, new HashMap<String, Object>(),
          basicModule
          ,bfoModule
          ,rocoreModule
          );

      /*
       * extract model
       */
      extractor.extract(odmModel, odmTarget, new HashMap<String,Object>());
      odmTarget.close();
      /*
       * Unload all models and metamodels (EMF-specific)
       */
      EMFModelFactory emfModelFactory = (EMFModelFactory) modelFactory;
      emfModelFactory.unload((EMFModel) umlModel);
      emfModelFactory.unload((EMFModel) odmModel);
      emfModelFactory.unload((EMFReferenceModel) odmMetamodel);
      emfModelFactory.unload((EMFReferenceModel) umlMetamodel);
      unload();
      
    } catch (Exception e) {
      System.err.println(e);
      e.printStackTrace();
      unload();
    }
  }



  
  
  public static void transformEnrichOdmFile(InputStream newerOdmIS, 
      InputStream olderOdmIS, OutputStream odmTargetOS,
      IProgressMonitor monitor) {
    
    if(monitor != null) {
      monitor.subTask("ODM Enrichment");
    }
    
    try {
      /*
       * Initializations
       */
      initialize();


      // Load the transformation definition
      URL basicModuleUrl = new URL(ODM_ENRICHMENT_MODULE);
      InputStream transformationInputStream = basicModuleUrl.openConnection().getInputStream();
      Object basicModule = transformationLauncher.loadModule(transformationInputStream);
      
       /*
       * Load metamodels
       */
      URL odmUrl = new URL(ODM_METAMODEL_URI);
      
      IReferenceModel odmMetamodel = modelFactory.newReferenceModel();
      injector.inject(odmMetamodel, odmUrl.openConnection().getInputStream(),
          new HashMap<String,Object>());
      
      /*
       * Load models
       */
      IModel newerModel = modelFactory.newModel(odmMetamodel);
      injector.inject(newerModel, newerOdmIS, new HashMap<String,Object>());
      
      IModel olderModel = modelFactory.newModel(odmMetamodel);
      injector.inject(olderModel, olderOdmIS, new HashMap<String,Object>());
      
      IModel odmTargetModel = modelFactory.newModel(odmMetamodel);

      /*
       * Run transformation
       */
      //create OUT : OBO from Newer : OBO, Older : OBO;
      transformationLauncher.initialize(new HashMap<String, Object>());
      transformationLauncher.addInModel(newerModel, "Newer", "OBO");
      transformationLauncher.addInModel(olderModel, "Older", "OBO");
      transformationLauncher.addOutModel(odmTargetModel, "OUT", "OBO");

      transformationLauncher.launch(ILauncher.RUN_MODE, monitor, 
          new HashMap<String, Object>(),
          basicModule);

      /*
       * extract model
       */
      extractor.extract(odmTargetModel, odmTargetOS, new HashMap<String,Object>());
      odmTargetOS.close();
      /*
       * Unload all models and metamodels (EMF-specific)
       */
      EMFModelFactory emfModelFactory = (EMFModelFactory) modelFactory;
      emfModelFactory.unload((EMFModel) odmTargetModel);
      emfModelFactory.unload((EMFModel) newerModel);
      emfModelFactory.unload((EMFModel) olderModel);
      emfModelFactory.unload((EMFReferenceModel) odmMetamodel);
      unload();
      
    } catch (Exception e) {
      System.err.println(e);
      e.printStackTrace();
    }
    
  
  }
  
  public static void transformOboRoOdm2UmlFile(InputStream odmSource, OutputStream umlTarget) {
    transformOboRoOdm2UmlFile(odmSource, umlTarget,null);
  }

  public static void transformOboRoOdm2UmlFile(InputStream odmSource, OutputStream umlTarget,
      IProgressMonitor monitor) {
    
    if(monitor != null) {
      monitor.subTask("ODM2UML-ROCore superimposed transformation");
    }
    
    try {
      /*
       * Initializations
       */
      initialize();
      

      //URL umlUrl = new URL(UML_METAMODEL_URI);
      URL odmUrl = new URL(ODM_METAMODEL_URI);


      // Load the transformation definition
      URL basicModuleUrl = new URL(ODM2UML_BASIC_TRANSFORMATION_MODULE_URI);
      InputStream transformationInputStream = basicModuleUrl.openConnection().getInputStream();
      Object basicModule = transformationLauncher.loadModule(transformationInputStream);
      
  
      URL bfoModuleUrl = new URL(ODM2UML_BFO_SUPERIMPOSITION_MODULE_URI);
      InputStream bfoModuleInputStream = bfoModuleUrl.openConnection().getInputStream();
      Object bfoModule = transformationLauncher.loadModule(bfoModuleInputStream);
    

      URL transformationModuleUrl = new URL(ODM2UML_ROCORE_SUPERIMPOSITION_MODULE_URI);
      InputStream rocoreModuleInputStream = transformationModuleUrl.openConnection().getInputStream();
      Object rocoreModule = transformationLauncher.loadModule(rocoreModuleInputStream);
              

      URL oboProfileUri = new URL(OBO_PROFILE_URI);
      InputStream oboProfileSource = oboProfileUri.openConnection().getInputStream();
      
      URL bfoProfileUri = new URL(BFO_PROFILE_URI);
      InputStream bfoProfileSource = bfoProfileUri.openConnection().getInputStream();


      URL rocoreProfileUri = new URL(ROCORE_PROFILE_URI);
      InputStream rocoreProfileSource = rocoreProfileUri.openConnection().getInputStream();

      /*URL rocoreOboUri = new URL(ROCORE_OBO_URI);
      InputStream rocoreOboSource = rocoreOboUri.openConnection().getInputStream();*/

      /*
       * Load metamodels 
       */
      IReferenceModel odmMetamodel = modelFactory.newReferenceModel();
      injector.inject(odmMetamodel, odmUrl.openConnection().getInputStream(),
          new HashMap<String,Object>());
      
      

      IReferenceModel umlMetamodel = modelFactory.newReferenceModel();
      injector.inject(umlMetamodel, UML_METAMODEL_URI, new HashMap<String,Object>());
      IModel umlModel = modelFactory.newModel(umlMetamodel);
      /*
       * Load models
       */
      
      
      System.out.println("começando a  injetar modelo odm");
      IModel odmModel = modelFactory.newModel(odmMetamodel);
      injector.inject(odmModel, odmSource, new HashMap<String,Object>());

      System.out.println("terminou injeção modelo odm");
      
      EMFModel oboProfile = (EMFModel) modelFactory.newModel((EMFReferenceModel) umlMetamodel);
      injector.inject(oboProfile, oboProfileSource, new HashMap<String, Object>());
      oboProfile.getResource().setURI(
          URI.createURI(OBO_PROFILE_URI, true));

      EMFModel bfoProfile = (EMFModel) modelFactory.newModel((EMFReferenceModel)umlMetamodel);
      injector.inject(bfoProfile, bfoProfileSource, new HashMap<String, Object>());
      bfoProfile.getResource().setURI(
          URI.createURI(BFO_PROFILE_URI, true));

      
      EMFModel rocoreProfile = (EMFModel) modelFactory.newModel((EMFReferenceModel)umlMetamodel);
      injector.inject(rocoreProfile, rocoreProfileSource, new HashMap<String, Object>());
      rocoreProfile.getResource().setURI(
          URI.createURI(ROCORE_OBO_URI, true));


      /*
       * Run transformation
       */

      transformationLauncher.initialize(new HashMap<String, Object>());
      transformationLauncher.addInModel(odmModel, "IN", "OBO");
      transformationLauncher.addInModel(oboProfile, "PROFILE", "UML");
      transformationLauncher.addInModel(bfoProfile, "BFOPROFILE", "UML");
      transformationLauncher.addInModel(rocoreProfile, "ROPROFILE", "UML");
      transformationLauncher.addOutModel(umlModel, "OUT", "UML");


      transformationLauncher.launch(ILauncher.RUN_MODE, monitor, new HashMap<String, Object>(),
          basicModule
          ,bfoModule
          ,rocoreModule
          );

      /*
       * extract model
       */
      extractor.extract(umlModel, umlTarget, new HashMap<String,Object>());
      umlTarget.close();
      /*
       * Unload all models and metamodels (EMF-specific)
       */
      EMFModelFactory emfModelFactory = (EMFModelFactory) modelFactory;
      emfModelFactory.unload((EMFModel) umlModel);
      emfModelFactory.unload((EMFModel) odmModel);
      emfModelFactory.unload((EMFReferenceModel) odmMetamodel);
      emfModelFactory.unload((EMFReferenceModel) umlMetamodel);
      unload();
      
    } catch (Exception e) {
      System.out.println("EXCESSAAAAAAAAAAAAAAAAOOOOOO");
      System.err.println(e);
      e.printStackTrace();
      unload();
    }
  }
  
  
}
