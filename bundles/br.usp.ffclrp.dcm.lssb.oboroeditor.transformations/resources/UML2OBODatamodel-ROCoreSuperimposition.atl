-- @nsURI UML=http://www.eclipse.org/uml2/4.0.0/UML
-- @nsURI OBO=http://dcm.ffclrp.usp.br/lssb/oboroeditor/metamodels/obodatamodel.ecore
-- superimposed on UML2OBODatamodel and UML2OBODatamodel-BFOSuporimposition;


module UML2OBODatamodelROCoreSuperimposition;
create OUT: OBO from IN: UML, PROFILE: UML, BFOPROFILE : UML, ROPROFILE : UML, ROCORE : OBO;




--- A map to translate ROCore Type IDs to ROCore profile stereotype names
helper def: idToStereotypeNameMap : Map(String, String) = 
       	Map{
        	('BFO:0000050','partOf'),
        	('BFO:0000051','hasPart'),
        	('BFO:0000054','realizedIn'),
        	('BFO:0000055','realizes'),
        	('BFO:0000066','occursIn'),
        	('BFO:0000067','containProcess'),
        	('RO:0000052','inheresIn'),
        	('RO:0000053','bearerOf'),
        	('RO:0000056','participatesIn'),
        	('RO:0000057','hasParticipant'),
        	('RO:0000058','isConcretizedAs'),
        	('RO:0000059','concretizes'),
        	('RO:0000079','functionOf'),
        	('RO:0000080','qualityOf'),
        	('RO:0000081','roleOf'),
        	('RO:0000085','hasFunction'),
        	('RO:0000086','hasQuality'),
        	('RO:0000087','hasRole'),
        	('RO:0000091','hasDisposition'),
        	('RO:0000092','dispositionOf'),
        	('RO:0001000','derivesFrom'),
        	('RO:0001001','derivesInto'),
        	('RO:0001015','locationOf'),
        	('RO:0001025','locatedIn'),
        	('RO:0002000','2DBoundaryOf'),
        	('RO:0002002','has2DBoundary'),
        	('RO:0002350','memberOf'),
        	('RO:0002351','hasMember')
    	};
    	
    	
--- A map to translate ROCore profile stereotype names to ROCore Type IDs
helper def: stereotypeNameToIdMap : Map(String, String) = 
    thisModule.idToStereotypeNameMap.getKeys()-> iterate(key; acc: Map(OclAny, OclAny) =
			Map{} | acc -> including(thisModule.idToStereotypeNameMap.get(key), key));


helper def: convertStereotypeNameToId(name : String) : String =
    thisModule.stereotypeNameToIdMap.get(name);


helper context UML!Stereotype def: isROCoreStereotype() : Boolean = 
    self.profile.name = 'ROCore';




rule Association_ROCore {
    from 
    	input : UML!Association in IN (
    	    input.getAppliedStereotypes()
						->exists(s | s.profile.name = 'ROCore')
		)
	using {
    	    roStereotype : UML!Stereotype = input.getAppliedStereotypes()
						->select(s | s.profile.name = 'ROCore')->first();
			oboPropertyId : String = thisModule.stereotypeNameToIdMap.get(roStereotype.name);
	}
    to
    	output : OBO!OBORestriction (
    	    type <- thisModule.resolveTemp(OBO!OBOProperty.allInstancesFrom('ROCORE')
    	    				->select(p | p.id = oboPropertyId)->first(),'output').debug(),
    	    child <- input.memberEnd->last().type,
    	    parent <- input.memberEnd->first().type,
    	    namespace <- if input.namespace.oclIsKindOf(UML!Package) 
						and input.isStereotypedWith('OBONamespace')
						then
							input.namespace
						else
							OclUndefined
						endif--,
			-- completes <- input.getStereotypeValue('OBORestriction','intersection')
    	)
    do {
        oboPropertyId.debug();
    }
}




-- ------------------------------------------------------------------------
rule CopyAnnotatedObject {
	from 
		input : OBO!AnnotatedObject in ROCORE
			(input.oclIsTypeOf(OBO!AnnotatedObject))
	to
		output : OBO!AnnotatedObject(
			id <- input.id,
			name <- input.name,
			builtIn <- input.builtIn
		)
	do {
		OBO!OBOSession.allInstancesFrom('OUT')->first().objects->append(output);
	}
}


rule CopyDefaultClass {
	from 
		input : OBO!OBOClass in ROCORE
	to
		output : OBO!OBOClass(
			id <- input.id,
			name <- input.name,
			builtIn <- input.builtIn
		)
	do {
		OBO!OBOSession.allInstancesFrom('OUT')->first().objects->append(output);
	}
}

rule CopyDefaultProperty {
	from 
		input : OBO!OBOProperty in ROCORE
	to
		output : OBO!OBOProperty in OUT (
			id <- input.id,
			name <-  input.name,
			builtIn <- input.builtIn,
			namespace <- input.namespace
			
		)
	do {
		OBO!OBOSession.allInstancesFrom('OUT')->first().objects->append(output);
	}
}

rule CopyDefaultNamespace {
	from 
		input : OBO!Namespace in ROCORE
	to
		output : OBO!Namespace(
			id <- input.id,
			path <- input.path,
			privateId <- input.privateId
		)
	do {
		OBO!OBOSession.allInstancesFrom('OUT')->first().namespaces <- OBO!OBOSession.allInstancesFrom('OUT')->first().namespaces->append(output);
	}
}
