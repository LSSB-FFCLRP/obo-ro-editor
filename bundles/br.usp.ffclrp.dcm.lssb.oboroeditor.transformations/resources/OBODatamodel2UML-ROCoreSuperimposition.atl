-- @nsURI UML=http://www.eclipse.org/uml2/4.0.0/UML
-- @nsURI OBO=http://dcm.ffclrp.usp.br/lssb/oboroeditor/metamodels/obodatamodel.ecore
-- superimposed on UML2OBODatamodel;


module OBODatamodel2UMLROCoreSuperimposition;
create OUT: UML from IN: OBO, PROFILE: UML, ROPROFILE : UML;





helper def: idToStereotypeNameMap : Map(String, String) = 
       	Map{
        	('BFO:0000050','partOf'),
        	('BFO:0000051','hasPart'),
        	('BFO:0000054','realizedIn'),
        	('BFO:0000055','realizes'),
        	('BFO:0000066','occursIn'),
        	('BFO:0000067','containProcess'),
        	('RO:0000052','inheresIn'),
        	('RO:0000053','bearerOf'),
        	('RO:0000056','participatesIn'),
        	('RO:0000057','hasParticipant'),
        	('RO:0000058','isConcretizedAs'),
        	('RO:0000059','concretizes'),
        	('RO:0000079','functionOf'),
        	('RO:0000080','qualityOf'),
        	('RO:0000081','roleOf'),
        	('RO:0000085','hasFunction'),
        	('RO:0000086','hasQuality'),
        	('RO:0000087','hasRole'),
        	('RO:0000091','hasDisposition'),
        	('RO:0000092','dispositionOf'),
        	('RO:0001000','derivesFrom'),
        	('RO:0001001','derivesInto'),
        	('RO:0001015','locationOf'),
        	('RO:0001025','locatedIn'),
        	('RO:0002000','2DBoundaryOf'),
        	('RO:0002002','has2DBoundary'),
        	('RO:0002350','memberOf'),
        	('RO:0002351','hasMember')
    	};

helper def: convertIdToStereotypeName(id : String) : String =
    thisModule.idToStereotypeNameMap.get(id);


helper def: isROCoreType(id : String) : Boolean = 
    not thisModule.idToStereotypeNameMap.get(id).oclIsUndefined();












rule OBORestriction {
	from
		input: OBO!OBORestriction (not input.type.id.endsWith('is_a') and not thisModule.isROCoreType(input.type.id))
	using {
		stereotype: UML!Stereotype = thisModule.stereotypes.get('OBORestriction');
	}
	to
		output: UML!Association (
			package <- if not input.namespace.oclIsUndefined() 
					   then input.namespace 
					   else thisModule.resolveTemp(
					       OBO!OBOSession.allInstances()->first(),
					       'output')  
					   endif,
			name <- input.id,
			endType <- Set{sourceProperty,
						targetProperty},
			package <- input.namespace,
			memberEnd <-  sourceProperty,
			memberEnd <- targetProperty
			-- navigableOwnedEnd <- targetProperty,
		),
		sourceProperty: UML!Property ( -- the `source` of the relation
		    name <- thisModule.sanitizeString(input.type.name + '_' + input.parent.name),
		    isUnique <- true,
		    type <- input.parent
		),
		targetProperty: UML!Property ( -- the `target`of the relation
		    name <- 'target',
		    type <- input.child,
		    isUnique <- true,
		    owningAssociation <- output
		)
	do {
	    
		output.applyStereotype(stereotype); 
		output.setValue(stereotype, 'type', thisModule.resolveTemp(input.type,'output')); 
		output.setValue(stereotype, 'intersection', input.completes); -- is a intersection?
		        
		        
	}
}




rule OBORestriction_ROCore {
	from
		input: OBO!OBORestriction (not input.type.id.endsWith('is_a') and thisModule.isROCoreType(input.type.id))
	using {
		stereotype: UML!Stereotype = thisModule.stereotypes.get('OBORestriction');
		roStereotype : UML!Stereotype = thisModule.stereotypes
					.get(thisModule.convertIdToStereotypeName(input.type.id));		
		session : OBO!OBOSession = OBO!OBOSession.allInstancesFrom('OUT')->first();
		roCoreProfile : UML!Profile =  UML!Profile.allInstances() -> select(p | p
				.name = 'ROCore') -> first();
	}
	to
		output: UML!Association (
			package <- if not input.namespace.oclIsUndefined() 
					   then input.namespace 
					   else thisModule.resolveTemp(
					       OBO!OBOSession.allInstances()->first(),
					       'output')  
					   endif,
			name <- input.id,
			endType <- Set{sourceProperty,
						targetProperty},
			package <- input.namespace,
			memberEnd <-  sourceProperty,
			memberEnd <- targetProperty
			-- navigableOwnedEnd <- targetProperty,
		),
		sourceProperty: UML!Property ( -- the `source` of the relation
		    name <- input.type.name + '_' + input.parent.name,
		    isUnique <- true,
		    type <- input.parent
		),
		targetProperty: UML!Property ( -- the `target`of the relation
		    name <- 'target',
		    type <- input.child,
		    isUnique <- true,
		    owningAssociation <- output
		)
	do {
	    -- apply profile if ROCore restriction exists
		session.applyProfile(roCoreProfile);
	    
		output.applyStereotype(stereotype); 
		output.setValue(stereotype, 'type', thisModule.resolveTemp(input.type,'output')); 
		output.setValue(stereotype, 'intersection', input.completes); -- is a intersection?
		        
		        
		output.applyStereotype(roStereotype); 
	}
}



