# Viewpoints for OBO-RO Editor

Papyrus viewpoints can be used to constrain diagrams elements to a particular class of users, also allowing to define new kinds of diagrams as domain-specific views based on UML and SysML. Such  new diagrams can provide different names for a element, as well as new palette items and figures.

## Usage

Papyrus viewpoints can be using two lists: *stakeholder* and *viewpoint*. The *stakeholder list* presents categories of users. Each of those categories have one or more viewpoints associated to it, according to the concerns of the corresponding user.  When a class of user is selected in the *stakeholder list*, the *viewpoint list* is populated with associated viewpoints, that can be applied in the edited model. Viewpoints  can have a *multiplicity* checkbox checked, allowing opnly one diagram of a kind defined for each model element.  

More information about the usage of viewpoints in Papyrus can be found in the dedicated [Eclipse Help Page](https://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.papyrus.infra.viewpoints.doc%2Ftarget%2Fgenerated-eclipse-help%2Fviewpoints.html&cp=66_1_0_3_1&anchor=Walkthrough)

## New stakeholders

This Papyrus configuration defines the following new stakeholders:

- **Ontology Developer**: An individual that is interested in the elements that are marked with stereotypes defined by the RO/OWL profile, thus, that are  formalized by the OBO Relations Ontology. 
  Viewpoints of interest:
  - Ontologic Concepts

## New viewpoints

This Papyrus configuration defines the following new viewpoints:

- **Relations Ontology Concepts**: 

## New diagrams

- **OBO-RO Entities**: 


## About palette customization

Customized palettes can be found in `workspace/.metadata/.plugins/org.eclipse.papyrus.uml.diagram.common` and then imported to the model.


## Criação de um novo architecture model:
- Criar um papyrus architecture model
- No architecture model:
    - <right_click> -> Load Resource... -> "platform:/plugin/org.eclipse.papyrus.uml.architecture/model/uml.architecture"