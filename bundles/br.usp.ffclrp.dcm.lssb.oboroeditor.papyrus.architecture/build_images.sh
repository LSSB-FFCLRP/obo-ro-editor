#!/bin/bash

IMG_DIR="resources/img/";
INKSCAPE_EXPORT_DPI=300;
RESIZE_GEOMETRY="16x16";
AUX_FILE_NAME="aux.png";

for SVG  in ./${IMG_DIR}*.svg; do 
    filename="$( basename ${SVG} )"
    GIF="${IMG_DIR}${filename%.*}-${RESIZE_GEOMETRY}.gif"

    if [[ "${SVG}" -nt "${GIF}" ]]; then
        echo "${SVG}" is newer than "${GIF}": Exporting
        inkscape -D -z --export-dpi=${INKSCAPE_EXPORT_DPI} --file=${SVG} "--export-png=${AUX_FILE_NAME}"
        convert "${AUX_FILE_NAME}" -resize "${RESIZE_GEOMETRY}" "${GIF}"
    else
        echo "${SVG}" is older than "${GIF}: Passing"
    fi
done
 