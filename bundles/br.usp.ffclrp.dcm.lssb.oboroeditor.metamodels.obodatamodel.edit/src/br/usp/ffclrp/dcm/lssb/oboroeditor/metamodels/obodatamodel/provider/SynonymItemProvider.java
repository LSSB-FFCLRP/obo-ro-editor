/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.provider;


import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelFactory;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SynonymItemProvider extends CloneableItemProvider {
  /**
   * This constructs an instance from a factory and a notifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SynonymItemProvider(AdapterFactory adapterFactory) {
    super(adapterFactory);
  }

  /**
   * This returns the property descriptors for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
    if (itemPropertyDescriptors == null) {
      super.getPropertyDescriptors(object);

      addIdPropertyDescriptor(object);
      addAnonymousPropertyDescriptor(object);
      addUNKNOWN_SCOPEPropertyDescriptor(object);
      addRELATED_SYNONYMPropertyDescriptor(object);
      addEXACT_SYNONYMPropertyDescriptor(object);
      addNARROW_SYNONYMPropertyDescriptor(object);
      addBROAD_SYNONYMPropertyDescriptor(object);
      addScopePropertyDescriptor(object);
      addXrefsPropertyDescriptor(object);
      addTextPropertyDescriptor(object);
    }
    return itemPropertyDescriptors;
  }

  /**
   * This adds a property descriptor for the Id feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addIdPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_IdentifiableObject_id_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_IdentifiableObject_id_feature", "_UI_IdentifiableObject_type"),
         obodatamodelPackage.Literals.IDENTIFIABLE_OBJECT__ID,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Anonymous feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addAnonymousPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_IdentifiableObject_anonymous_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_IdentifiableObject_anonymous_feature", "_UI_IdentifiableObject_type"),
         obodatamodelPackage.Literals.IDENTIFIABLE_OBJECT__ANONYMOUS,
         true,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the UNKNOWN SCOPE feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addUNKNOWN_SCOPEPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Synonym_UNKNOWN_SCOPE_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Synonym_UNKNOWN_SCOPE_feature", "_UI_Synonym_type"),
         obodatamodelPackage.Literals.SYNONYM__UNKNOWN_SCOPE,
         false,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the RELATED SYNONYM feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addRELATED_SYNONYMPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Synonym_RELATED_SYNONYM_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Synonym_RELATED_SYNONYM_feature", "_UI_Synonym_type"),
         obodatamodelPackage.Literals.SYNONYM__RELATED_SYNONYM,
         false,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the EXACT SYNONYM feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addEXACT_SYNONYMPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Synonym_EXACT_SYNONYM_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Synonym_EXACT_SYNONYM_feature", "_UI_Synonym_type"),
         obodatamodelPackage.Literals.SYNONYM__EXACT_SYNONYM,
         false,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the NARROW SYNONYM feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addNARROW_SYNONYMPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Synonym_NARROW_SYNONYM_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Synonym_NARROW_SYNONYM_feature", "_UI_Synonym_type"),
         obodatamodelPackage.Literals.SYNONYM__NARROW_SYNONYM,
         false,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the BROAD SYNONYM feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addBROAD_SYNONYMPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Synonym_BROAD_SYNONYM_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Synonym_BROAD_SYNONYM_feature", "_UI_Synonym_type"),
         obodatamodelPackage.Literals.SYNONYM__BROAD_SYNONYM,
         false,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Scope feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addScopePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Synonym_scope_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Synonym_scope_feature", "_UI_Synonym_type"),
         obodatamodelPackage.Literals.SYNONYM__SCOPE,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Xrefs feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addXrefsPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Synonym_xrefs_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Synonym_xrefs_feature", "_UI_Synonym_type"),
         obodatamodelPackage.Literals.SYNONYM__XREFS,
         true,
         false,
         true,
         null,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Text feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addTextPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_Synonym_text_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_Synonym_text_feature", "_UI_Synonym_type"),
         obodatamodelPackage.Literals.SYNONYM__TEXT,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
   * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
   * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
    if (childrenFeatures == null) {
      super.getChildrenFeatures(object);
      childrenFeatures.add(obodatamodelPackage.Literals.SYNONYM__SYNONYM_TYPE);
      childrenFeatures.add(obodatamodelPackage.Literals.SYNONYM__NESTED_VALUE);
    }
    return childrenFeatures;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EStructuralFeature getChildFeature(Object object, Object child) {
    // Check the type of the specified child object and return the proper feature to use for
    // adding (see {@link AddCommand}) it as a child.

    return super.getChildFeature(object, child);
  }

  /**
   * This returns Synonym.gif.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object getImage(Object object) {
    return overlayImage(object, getResourceLocator().getImage("full/obj16/Synonym"));
  }

  /**
   * This returns the label text for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getText(Object object) {
    String label = ((Synonym)object).getId();
    return label == null || label.length() == 0 ?
      getString("_UI_Synonym_type") :
      getString("_UI_Synonym_type") + " " + label;
  }
  

  /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void notifyChanged(Notification notification) {
    updateChildren(notification);

    switch (notification.getFeatureID(Synonym.class)) {
      case obodatamodelPackage.SYNONYM__ID:
      case obodatamodelPackage.SYNONYM__ANONYMOUS:
      case obodatamodelPackage.SYNONYM__UNKNOWN_SCOPE:
      case obodatamodelPackage.SYNONYM__RELATED_SYNONYM:
      case obodatamodelPackage.SYNONYM__EXACT_SYNONYM:
      case obodatamodelPackage.SYNONYM__NARROW_SYNONYM:
      case obodatamodelPackage.SYNONYM__BROAD_SYNONYM:
      case obodatamodelPackage.SYNONYM__SCOPE:
      case obodatamodelPackage.SYNONYM__TEXT:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
        return;
      case obodatamodelPackage.SYNONYM__SYNONYM_TYPE:
      case obodatamodelPackage.SYNONYM__NESTED_VALUE:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
        return;
    }
    super.notifyChanged(notification);
  }

  /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
    super.collectNewChildDescriptors(newChildDescriptors, object);

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.SYNONYM__SYNONYM_TYPE,
         obodatamodelFactory.eINSTANCE.createSynonymType()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.SYNONYM__NESTED_VALUE,
         obodatamodelFactory.eINSTANCE.createNestedValue()));
  }

}
