/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.provider;


import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelFactory;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class IdentifiedObjectItemProvider extends ValueItemProvider {
  /**
   * This constructs an instance from a factory and a notifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IdentifiedObjectItemProvider(AdapterFactory adapterFactory) {
    super(adapterFactory);
  }

  /**
   * This returns the property descriptors for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
    if (itemPropertyDescriptors == null) {
      super.getPropertyDescriptors(object);

      addIdPropertyDescriptor(object);
      addAnonymousPropertyDescriptor(object);
      addNamespacePropertyDescriptor(object);
      addBuiltInPropertyDescriptor(object);
      addNamePropertyDescriptor(object);
    }
    return itemPropertyDescriptors;
  }

  /**
   * This adds a property descriptor for the Id feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addIdPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_IdentifiableObject_id_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_IdentifiableObject_id_feature", "_UI_IdentifiableObject_type"),
         obodatamodelPackage.Literals.IDENTIFIABLE_OBJECT__ID,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Anonymous feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addAnonymousPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_IdentifiableObject_anonymous_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_IdentifiableObject_anonymous_feature", "_UI_IdentifiableObject_type"),
         obodatamodelPackage.Literals.IDENTIFIABLE_OBJECT__ANONYMOUS,
         true,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Namespace feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addNamespacePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_NamespacedObject_namespace_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_NamespacedObject_namespace_feature", "_UI_NamespacedObject_type"),
         obodatamodelPackage.Literals.NAMESPACED_OBJECT__NAMESPACE,
         true,
         false,
         true,
         null,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Built In feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addBuiltInPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_IdentifiedObject_builtIn_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_IdentifiedObject_builtIn_feature", "_UI_IdentifiedObject_type"),
         obodatamodelPackage.Literals.IDENTIFIED_OBJECT__BUILT_IN,
         true,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Name feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addNamePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_IdentifiedObject_name_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_IdentifiedObject_name_feature", "_UI_IdentifiedObject_type"),
         obodatamodelPackage.Literals.IDENTIFIED_OBJECT__NAME,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
   * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
   * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
    if (childrenFeatures == null) {
      super.getChildrenFeatures(object);
      childrenFeatures.add(obodatamodelPackage.Literals.NAMESPACED_OBJECT__NAMESPACE_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.IDENTIFIED_OBJECT__TYPE_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.IDENTIFIED_OBJECT__NAME_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.IDENTIFIED_OBJECT__ID_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.IDENTIFIED_OBJECT__PROPERTY_VALUES);
    }
    return childrenFeatures;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EStructuralFeature getChildFeature(Object object, Object child) {
    // Check the type of the specified child object and return the proper feature to use for
    // adding (see {@link AddCommand}) it as a child.

    return super.getChildFeature(object, child);
  }

  /**
   * This returns the label text for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getText(Object object) {
    String label = ((IdentifiedObject)object).getName();
    return label == null || label.length() == 0 ?
      getString("_UI_IdentifiedObject_type") :
      getString("_UI_IdentifiedObject_type") + " " + label;
  }
  

  /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void notifyChanged(Notification notification) {
    updateChildren(notification);

    switch (notification.getFeatureID(IdentifiedObject.class)) {
      case obodatamodelPackage.IDENTIFIED_OBJECT__ID:
      case obodatamodelPackage.IDENTIFIED_OBJECT__ANONYMOUS:
      case obodatamodelPackage.IDENTIFIED_OBJECT__BUILT_IN:
      case obodatamodelPackage.IDENTIFIED_OBJECT__NAME:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
        return;
      case obodatamodelPackage.IDENTIFIED_OBJECT__NAMESPACE_EXTENSION:
      case obodatamodelPackage.IDENTIFIED_OBJECT__TYPE_EXTENSION:
      case obodatamodelPackage.IDENTIFIED_OBJECT__NAME_EXTENSION:
      case obodatamodelPackage.IDENTIFIED_OBJECT__ID_EXTENSION:
      case obodatamodelPackage.IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION:
      case obodatamodelPackage.IDENTIFIED_OBJECT__PROPERTY_VALUES:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
        return;
    }
    super.notifyChanged(notification);
  }

  /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
    super.collectNewChildDescriptors(newChildDescriptors, object);

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.NAMESPACED_OBJECT__NAMESPACE_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.IDENTIFIED_OBJECT__TYPE_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.IDENTIFIED_OBJECT__NAME_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.IDENTIFIED_OBJECT__ID_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.IDENTIFIED_OBJECT__PROPERTY_VALUES,
         obodatamodelFactory.eINSTANCE.createPropertyValue()));
  }

  /**
   * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
    Object childFeature = feature;
    Object childObject = child;

    boolean qualify =
      childFeature == obodatamodelPackage.Literals.NAMESPACED_OBJECT__NAMESPACE_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.IDENTIFIED_OBJECT__TYPE_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.IDENTIFIED_OBJECT__NAME_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.IDENTIFIED_OBJECT__ID_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION;

    if (qualify) {
      return getString
        ("_UI_CreateChild_text2",
         new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
    }
    return super.getCreateChildText(owner, feature, child, selection);
  }

}
