/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.provider;


import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelFactory;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class OBOSessionItemProvider extends IdentifiedObjectIndexItemProvider {
  /**
   * This constructs an instance from a factory and a notifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBOSessionItemProvider(AdapterFactory adapterFactory) {
    super(adapterFactory);
  }

  /**
   * This returns the property descriptors for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
    if (itemPropertyDescriptors == null) {
      super.getPropertyDescriptors(object);

      addIdPropertyDescriptor(object);
      addDefaultNamespacePropertyDescriptor(object);
      addCurrentFilenamesPropertyDescriptor(object);
      addCurrentUserPropertyDescriptor(object);
      addLoadRemarkPropertyDescriptor(object);
    }
    return itemPropertyDescriptors;
  }

  /**
   * This adds a property descriptor for the Id feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addIdPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBOSession_id_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBOSession_id_feature", "_UI_OBOSession_type"),
         obodatamodelPackage.Literals.OBO_SESSION__ID,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Default Namespace feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addDefaultNamespacePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBOSession_defaultNamespace_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBOSession_defaultNamespace_feature", "_UI_OBOSession_type"),
         obodatamodelPackage.Literals.OBO_SESSION__DEFAULT_NAMESPACE,
         true,
         false,
         true,
         null,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Current Filenames feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addCurrentFilenamesPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBOSession_currentFilenames_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBOSession_currentFilenames_feature", "_UI_OBOSession_type"),
         obodatamodelPackage.Literals.OBO_SESSION__CURRENT_FILENAMES,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Current User feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addCurrentUserPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBOSession_currentUser_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBOSession_currentUser_feature", "_UI_OBOSession_type"),
         obodatamodelPackage.Literals.OBO_SESSION__CURRENT_USER,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Load Remark feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addLoadRemarkPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBOSession_loadRemark_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBOSession_loadRemark_feature", "_UI_OBOSession_type"),
         obodatamodelPackage.Literals.OBO_SESSION__LOAD_REMARK,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
   * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
   * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
    if (childrenFeatures == null) {
      super.getChildrenFeatures(object);
      childrenFeatures.add(obodatamodelPackage.Literals.OBO_SESSION__OBJECTS);
      childrenFeatures.add(obodatamodelPackage.Literals.OBO_SESSION__LINK_DATABASE);
      childrenFeatures.add(obodatamodelPackage.Literals.OBO_SESSION__NAMESPACES);
      childrenFeatures.add(obodatamodelPackage.Literals.OBO_SESSION__PROPERTY_VALUES);
      childrenFeatures.add(obodatamodelPackage.Literals.OBO_SESSION__UNKNOWN_STANZAS);
      childrenFeatures.add(obodatamodelPackage.Literals.OBO_SESSION__SUBSETS);
      childrenFeatures.add(obodatamodelPackage.Literals.OBO_SESSION__CATEGORIES);
      childrenFeatures.add(obodatamodelPackage.Literals.OBO_SESSION__SYNONYM_TYPES);
      childrenFeatures.add(obodatamodelPackage.Literals.OBO_SESSION__SYNONYM_CATEGORIES);
      childrenFeatures.add(obodatamodelPackage.Literals.OBO_SESSION__ID_SPACES);
      childrenFeatures.add(obodatamodelPackage.Literals.OBO_SESSION__ALL_DBX_REFS_CONTAINER);
    }
    return childrenFeatures;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EStructuralFeature getChildFeature(Object object, Object child) {
    // Check the type of the specified child object and return the proper feature to use for
    // adding (see {@link AddCommand}) it as a child.

    return super.getChildFeature(object, child);
  }

  /**
   * This returns OBOSession.gif.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object getImage(Object object) {
    return overlayImage(object, getResourceLocator().getImage("full/obj16/OBOSession"));
  }

  /**
   * This returns the label text for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getText(Object object) {
    String label = ((OBOSession)object).getId();
    return label == null || label.length() == 0 ?
      getString("_UI_OBOSession_type") :
      getString("_UI_OBOSession_type") + " " + label;
  }
  

  /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void notifyChanged(Notification notification) {
    updateChildren(notification);

    switch (notification.getFeatureID(OBOSession.class)) {
      case obodatamodelPackage.OBO_SESSION__ID:
      case obodatamodelPackage.OBO_SESSION__CURRENT_FILENAMES:
      case obodatamodelPackage.OBO_SESSION__CURRENT_USER:
      case obodatamodelPackage.OBO_SESSION__LOAD_REMARK:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
        return;
      case obodatamodelPackage.OBO_SESSION__OBJECTS:
      case obodatamodelPackage.OBO_SESSION__LINK_DATABASE:
      case obodatamodelPackage.OBO_SESSION__NAMESPACES:
      case obodatamodelPackage.OBO_SESSION__PROPERTY_VALUES:
      case obodatamodelPackage.OBO_SESSION__UNKNOWN_STANZAS:
      case obodatamodelPackage.OBO_SESSION__SUBSETS:
      case obodatamodelPackage.OBO_SESSION__CATEGORIES:
      case obodatamodelPackage.OBO_SESSION__SYNONYM_TYPES:
      case obodatamodelPackage.OBO_SESSION__SYNONYM_CATEGORIES:
      case obodatamodelPackage.OBO_SESSION__ID_SPACES:
      case obodatamodelPackage.OBO_SESSION__ALL_DBX_REFS_CONTAINER:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
        return;
    }
    super.notifyChanged(notification);
  }

  /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
    super.collectNewChildDescriptors(newChildDescriptors, object);

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_SESSION__OBJECTS,
         obodatamodelFactory.eINSTANCE.createAnnotatedObject()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_SESSION__OBJECTS,
         obodatamodelFactory.eINSTANCE.createDanglingObject()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_SESSION__OBJECTS,
         obodatamodelFactory.eINSTANCE.createDanglingProperty()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_SESSION__OBJECTS,
         obodatamodelFactory.eINSTANCE.createDatatype()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_SESSION__OBJECTS,
         obodatamodelFactory.eINSTANCE.createInstance()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_SESSION__OBJECTS,
         obodatamodelFactory.eINSTANCE.createLinkLinkedObject()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_SESSION__OBJECTS,
         obodatamodelFactory.eINSTANCE.createOBOClass()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_SESSION__OBJECTS,
         obodatamodelFactory.eINSTANCE.createOBOProperty()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_SESSION__OBJECTS,
         obodatamodelFactory.eINSTANCE.createSynonymedObject()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_SESSION__LINK_DATABASE,
         obodatamodelFactory.eINSTANCE.createLinkDatabase()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_SESSION__LINK_DATABASE,
         obodatamodelFactory.eINSTANCE.createMutableLinkDatabase()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_SESSION__NAMESPACES,
         obodatamodelFactory.eINSTANCE.createNamespace()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_SESSION__PROPERTY_VALUES,
         obodatamodelFactory.eINSTANCE.createPropertyValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_SESSION__UNKNOWN_STANZAS,
         obodatamodelFactory.eINSTANCE.createUnknownStanza()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_SESSION__SUBSETS,
         obodatamodelFactory.eINSTANCE.createTermSubset()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_SESSION__CATEGORIES,
         obodatamodelFactory.eINSTANCE.createTermSubset()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_SESSION__SYNONYM_TYPES,
         obodatamodelFactory.eINSTANCE.createSynonymType()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_SESSION__SYNONYM_CATEGORIES,
         obodatamodelFactory.eINSTANCE.createSynonymType()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_SESSION__ID_SPACES,
         obodatamodelFactory.eINSTANCE.create(obodatamodelPackage.Literals.ID_SPACES_MAP)));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_SESSION__ALL_DBX_REFS_CONTAINER,
         obodatamodelFactory.eINSTANCE.createDbxref()));
  }

  /**
   * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
    Object childFeature = feature;
    Object childObject = child;

    boolean qualify =
      childFeature == obodatamodelPackage.Literals.OBO_SESSION__SUBSETS ||
      childFeature == obodatamodelPackage.Literals.OBO_SESSION__CATEGORIES ||
      childFeature == obodatamodelPackage.Literals.OBO_SESSION__SYNONYM_TYPES ||
      childFeature == obodatamodelPackage.Literals.OBO_SESSION__SYNONYM_CATEGORIES;

    if (qualify) {
      return getString
        ("_UI_CreateChild_text2",
         new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
    }
    return super.getCreateChildText(owner, feature, child, selection);
  }

}
