/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.provider;


import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.AnnotatedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelFactory;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.AnnotatedObject} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AnnotatedObjectItemProvider extends IdentifiedObjectItemProvider {
  /**
   * This constructs an instance from a factory and a notifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AnnotatedObjectItemProvider(AdapterFactory adapterFactory) {
    super(adapterFactory);
  }

  /**
   * This returns the property descriptors for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
    if (itemPropertyDescriptors == null) {
      super.getPropertyDescriptors(object);

      addCreatedByPropertyDescriptor(object);
      addModifiedByPropertyDescriptor(object);
      addCreationDatePropertyDescriptor(object);
      addModificationDatePropertyDescriptor(object);
      addSecondaryIdsPropertyDescriptor(object);
      addDbxrefsPropertyDescriptor(object);
      addCommentPropertyDescriptor(object);
      addObsoletePropertyDescriptor(object);
      addReplacedByPropertyDescriptor(object);
      addConsiderReplacementsPropertyDescriptor(object);
      addDefinitionPropertyDescriptor(object);
      addDefDbxrefsPropertyDescriptor(object);
      addSubsetsPropertyDescriptor(object);
    }
    return itemPropertyDescriptors;
  }

  /**
   * This adds a property descriptor for the Created By feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addCreatedByPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_ModificationMetadataObject_createdBy_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_ModificationMetadataObject_createdBy_feature", "_UI_ModificationMetadataObject_type"),
         obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__CREATED_BY,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Modified By feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addModifiedByPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_ModificationMetadataObject_modifiedBy_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_ModificationMetadataObject_modifiedBy_feature", "_UI_ModificationMetadataObject_type"),
         obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__MODIFIED_BY,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Creation Date feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addCreationDatePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_ModificationMetadataObject_creationDate_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_ModificationMetadataObject_creationDate_feature", "_UI_ModificationMetadataObject_type"),
         obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__CREATION_DATE,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Modification Date feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addModificationDatePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_ModificationMetadataObject_modificationDate_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_ModificationMetadataObject_modificationDate_feature", "_UI_ModificationMetadataObject_type"),
         obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Secondary Ids feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addSecondaryIdsPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_MultiIDObject_secondaryIds_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_MultiIDObject_secondaryIds_feature", "_UI_MultiIDObject_type"),
         obodatamodelPackage.Literals.MULTI_ID_OBJECT__SECONDARY_IDS,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Dbxrefs feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addDbxrefsPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_DbxrefedObject_dbxrefs_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_DbxrefedObject_dbxrefs_feature", "_UI_DbxrefedObject_type"),
         obodatamodelPackage.Literals.DBXREFED_OBJECT__DBXREFS,
         true,
         false,
         true,
         null,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Comment feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addCommentPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_CommentedObject_comment_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_CommentedObject_comment_feature", "_UI_CommentedObject_type"),
         obodatamodelPackage.Literals.COMMENTED_OBJECT__COMMENT,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Obsolete feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addObsoletePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_ObsoletableObject_obsolete_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_ObsoletableObject_obsolete_feature", "_UI_ObsoletableObject_type"),
         obodatamodelPackage.Literals.OBSOLETABLE_OBJECT__OBSOLETE,
         true,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Replaced By feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addReplacedByPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_ObsoletableObject_replacedBy_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_ObsoletableObject_replacedBy_feature", "_UI_ObsoletableObject_type"),
         obodatamodelPackage.Literals.OBSOLETABLE_OBJECT__REPLACED_BY,
         true,
         false,
         true,
         null,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Consider Replacements feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addConsiderReplacementsPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_ObsoletableObject_considerReplacements_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_ObsoletableObject_considerReplacements_feature", "_UI_ObsoletableObject_type"),
         obodatamodelPackage.Literals.OBSOLETABLE_OBJECT__CONSIDER_REPLACEMENTS,
         true,
         false,
         true,
         null,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Definition feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addDefinitionPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_DefinedObject_definition_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_DefinedObject_definition_feature", "_UI_DefinedObject_type"),
         obodatamodelPackage.Literals.DEFINED_OBJECT__DEFINITION,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Def Dbxrefs feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addDefDbxrefsPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_DefinedObject_defDbxrefs_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_DefinedObject_defDbxrefs_feature", "_UI_DefinedObject_type"),
         obodatamodelPackage.Literals.DEFINED_OBJECT__DEF_DBXREFS,
         true,
         false,
         true,
         null,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Subsets feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addSubsetsPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_SubsetObject_subsets_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_SubsetObject_subsets_feature", "_UI_SubsetObject_type"),
         obodatamodelPackage.Literals.SUBSET_OBJECT__SUBSETS,
         true,
         false,
         true,
         null,
         null,
         null));
  }

  /**
   * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
   * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
   * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
    if (childrenFeatures == null) {
      super.getChildrenFeatures(object);
      childrenFeatures.add(obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.MULTI_ID_OBJECT__SECONDARY_ID_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.SYNONYMED_OBJECT__SYNONYMS);
      childrenFeatures.add(obodatamodelPackage.Literals.COMMENTED_OBJECT__COMMENT_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.OBSOLETABLE_OBJECT__CONSIDER_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.OBSOLETABLE_OBJECT__REPLACED_BY_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.OBSOLETABLE_OBJECT__OBSOLETE_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.DEFINED_OBJECT__DEFINITION_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.SUBSET_OBJECT__CATEGORY_EXTENSIONS);
    }
    return childrenFeatures;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EStructuralFeature getChildFeature(Object object, Object child) {
    // Check the type of the specified child object and return the proper feature to use for
    // adding (see {@link AddCommand}) it as a child.

    return super.getChildFeature(object, child);
  }

  /**
   * This returns AnnotatedObject.gif.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object getImage(Object object) {
    return overlayImage(object, getResourceLocator().getImage("full/obj16/AnnotatedObject"));
  }

  /**
   * This returns the label text for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getText(Object object) {
    String label = ((AnnotatedObject)object).getName();
    return label == null || label.length() == 0 ?
      getString("_UI_AnnotatedObject_type") :
      getString("_UI_AnnotatedObject_type") + " " + label;
  }
  

  /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void notifyChanged(Notification notification) {
    updateChildren(notification);

    switch (notification.getFeatureID(AnnotatedObject.class)) {
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATED_BY:
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFIED_BY:
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATION_DATE:
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFICATION_DATE:
      case obodatamodelPackage.ANNOTATED_OBJECT__SECONDARY_IDS:
      case obodatamodelPackage.ANNOTATED_OBJECT__COMMENT:
      case obodatamodelPackage.ANNOTATED_OBJECT__OBSOLETE:
      case obodatamodelPackage.ANNOTATED_OBJECT__DEFINITION:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
        return;
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATED_BY_EXTENSION:
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFIED_BY_EXTENSION:
      case obodatamodelPackage.ANNOTATED_OBJECT__CREATION_DATE_EXTENSION:
      case obodatamodelPackage.ANNOTATED_OBJECT__MODIFICATION_DATE_EXTENSION:
      case obodatamodelPackage.ANNOTATED_OBJECT__SECONDARY_ID_EXTENSION:
      case obodatamodelPackage.ANNOTATED_OBJECT__SYNONYMS:
      case obodatamodelPackage.ANNOTATED_OBJECT__COMMENT_EXTENSION:
      case obodatamodelPackage.ANNOTATED_OBJECT__CONSIDER_EXTENSION:
      case obodatamodelPackage.ANNOTATED_OBJECT__REPLACED_BY_EXTENSION:
      case obodatamodelPackage.ANNOTATED_OBJECT__OBSOLETE_EXTENSION:
      case obodatamodelPackage.ANNOTATED_OBJECT__DEFINITION_EXTENSION:
      case obodatamodelPackage.ANNOTATED_OBJECT__CATEGORY_EXTENSIONS:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
        return;
    }
    super.notifyChanged(notification);
  }

  /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
    super.collectNewChildDescriptors(newChildDescriptors, object);

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.MULTI_ID_OBJECT__SECONDARY_ID_EXTENSION,
         obodatamodelFactory.eINSTANCE.create(obodatamodelPackage.Literals.STRING_TO_NESTED_VALUE_MAP)));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.SYNONYMED_OBJECT__SYNONYMS,
         obodatamodelFactory.eINSTANCE.createSynonym()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.COMMENTED_OBJECT__COMMENT_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBSOLETABLE_OBJECT__CONSIDER_EXTENSION,
         obodatamodelFactory.eINSTANCE.create(obodatamodelPackage.Literals.OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP)));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBSOLETABLE_OBJECT__REPLACED_BY_EXTENSION,
         obodatamodelFactory.eINSTANCE.create(obodatamodelPackage.Literals.OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP)));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBSOLETABLE_OBJECT__OBSOLETE_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.DEFINED_OBJECT__DEFINITION_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.SUBSET_OBJECT__CATEGORY_EXTENSIONS,
         obodatamodelFactory.eINSTANCE.create(obodatamodelPackage.Literals.TERM_SUBSET_TO_NESTED_VALUE_MAP)));
  }

  /**
   * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
    Object childFeature = feature;
    Object childObject = child;

    boolean qualify =
      childFeature == obodatamodelPackage.Literals.NAMESPACED_OBJECT__NAMESPACE_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.IDENTIFIED_OBJECT__TYPE_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.IDENTIFIED_OBJECT__NAME_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.IDENTIFIED_OBJECT__ID_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.COMMENTED_OBJECT__COMMENT_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.OBSOLETABLE_OBJECT__OBSOLETE_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.DEFINED_OBJECT__DEFINITION_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.OBSOLETABLE_OBJECT__CONSIDER_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.OBSOLETABLE_OBJECT__REPLACED_BY_EXTENSION;

    if (qualify) {
      return getString
        ("_UI_CreateChild_text2",
         new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
    }
    return super.getCreateChildText(owner, feature, child, selection);
  }

}
