/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.provider;


import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelFactory;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class OBOPropertyItemProvider extends OBOObjectItemProvider {
  /**
   * This constructs an instance from a factory and a notifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBOPropertyItemProvider(AdapterFactory adapterFactory) {
    super(adapterFactory);
  }

  /**
   * This returns the property descriptors for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
    if (itemPropertyDescriptors == null) {
      super.getPropertyDescriptors(object);

      addCyclicPropertyDescriptor(object);
      addSymmetricPropertyDescriptor(object);
      addTransitivePropertyDescriptor(object);
      addReflexivePropertyDescriptor(object);
      addAlwaysImpliesInversePropertyDescriptor(object);
      addDummyPropertyDescriptor(object);
      addMetadataTagPropertyDescriptor(object);
      addRangePropertyDescriptor(object);
      addDomainPropertyDescriptor(object);
      addNonInheritablePropertyDescriptor(object);
      addDisjointOverPropertyDescriptor(object);
      addTransitiveOverPropertyDescriptor(object);
    }
    return itemPropertyDescriptors;
  }

  /**
   * This adds a property descriptor for the Cyclic feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addCyclicPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBOProperty_cyclic_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBOProperty_cyclic_feature", "_UI_OBOProperty_type"),
         obodatamodelPackage.Literals.OBO_PROPERTY__CYCLIC,
         true,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Symmetric feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addSymmetricPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBOProperty_symmetric_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBOProperty_symmetric_feature", "_UI_OBOProperty_type"),
         obodatamodelPackage.Literals.OBO_PROPERTY__SYMMETRIC,
         true,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Transitive feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addTransitivePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBOProperty_transitive_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBOProperty_transitive_feature", "_UI_OBOProperty_type"),
         obodatamodelPackage.Literals.OBO_PROPERTY__TRANSITIVE,
         true,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Reflexive feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addReflexivePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBOProperty_reflexive_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBOProperty_reflexive_feature", "_UI_OBOProperty_type"),
         obodatamodelPackage.Literals.OBO_PROPERTY__REFLEXIVE,
         true,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Always Implies Inverse feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addAlwaysImpliesInversePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBOProperty_alwaysImpliesInverse_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBOProperty_alwaysImpliesInverse_feature", "_UI_OBOProperty_type"),
         obodatamodelPackage.Literals.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE,
         true,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Dummy feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addDummyPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBOProperty_dummy_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBOProperty_dummy_feature", "_UI_OBOProperty_type"),
         obodatamodelPackage.Literals.OBO_PROPERTY__DUMMY,
         true,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Metadata Tag feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addMetadataTagPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBOProperty_metadataTag_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBOProperty_metadataTag_feature", "_UI_OBOProperty_type"),
         obodatamodelPackage.Literals.OBO_PROPERTY__METADATA_TAG,
         true,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Range feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addRangePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBOProperty_range_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBOProperty_range_feature", "_UI_OBOProperty_type"),
         obodatamodelPackage.Literals.OBO_PROPERTY__RANGE,
         true,
         false,
         true,
         null,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Domain feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addDomainPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBOProperty_domain_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBOProperty_domain_feature", "_UI_OBOProperty_type"),
         obodatamodelPackage.Literals.OBO_PROPERTY__DOMAIN,
         true,
         false,
         true,
         null,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Non Inheritable feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addNonInheritablePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBOProperty_nonInheritable_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBOProperty_nonInheritable_feature", "_UI_OBOProperty_type"),
         obodatamodelPackage.Literals.OBO_PROPERTY__NON_INHERITABLE,
         true,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Disjoint Over feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addDisjointOverPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBOProperty_disjointOver_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBOProperty_disjointOver_feature", "_UI_OBOProperty_type"),
         obodatamodelPackage.Literals.OBO_PROPERTY__DISJOINT_OVER,
         true,
         false,
         true,
         null,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Transitive Over feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addTransitiveOverPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBOProperty_transitiveOver_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBOProperty_transitiveOver_feature", "_UI_OBOProperty_type"),
         obodatamodelPackage.Literals.OBO_PROPERTY__TRANSITIVE_OVER,
         true,
         false,
         true,
         null,
         null,
         null));
  }

  /**
   * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
   * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
   * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
    if (childrenFeatures == null) {
      super.getChildrenFeatures(object);
      childrenFeatures.add(obodatamodelPackage.Literals.OBO_PROPERTY__CYCLIC_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.OBO_PROPERTY__SYMMETRIC_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.OBO_PROPERTY__TRANSITIVE_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.OBO_PROPERTY__REFLEXIVE_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.OBO_PROPERTY__DOMAIN_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.OBO_PROPERTY__RANGE_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.OBO_PROPERTY__HOLDS_OVER_CHAINS);
    }
    return childrenFeatures;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EStructuralFeature getChildFeature(Object object, Object child) {
    // Check the type of the specified child object and return the proper feature to use for
    // adding (see {@link AddCommand}) it as a child.

    return super.getChildFeature(object, child);
  }

  /**
   * This returns OBOProperty.gif.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object getImage(Object object) {
    return overlayImage(object, getResourceLocator().getImage("full/obj16/OBOProperty"));
  }

  /**
   * This returns the label text for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getText(Object object) {
    String label = ((OBOProperty)object).getName();
    return label == null || label.length() == 0 ?
      getString("_UI_OBOProperty_type") :
      getString("_UI_OBOProperty_type") + " " + label;
  }
  

  /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void notifyChanged(Notification notification) {
    updateChildren(notification);

    switch (notification.getFeatureID(OBOProperty.class)) {
      case obodatamodelPackage.OBO_PROPERTY__CYCLIC:
      case obodatamodelPackage.OBO_PROPERTY__SYMMETRIC:
      case obodatamodelPackage.OBO_PROPERTY__TRANSITIVE:
      case obodatamodelPackage.OBO_PROPERTY__REFLEXIVE:
      case obodatamodelPackage.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE:
      case obodatamodelPackage.OBO_PROPERTY__DUMMY:
      case obodatamodelPackage.OBO_PROPERTY__METADATA_TAG:
      case obodatamodelPackage.OBO_PROPERTY__NON_INHERITABLE:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
        return;
      case obodatamodelPackage.OBO_PROPERTY__CYCLIC_EXTENSION:
      case obodatamodelPackage.OBO_PROPERTY__SYMMETRIC_EXTENSION:
      case obodatamodelPackage.OBO_PROPERTY__TRANSITIVE_EXTENSION:
      case obodatamodelPackage.OBO_PROPERTY__REFLEXIVE_EXTENSION:
      case obodatamodelPackage.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION:
      case obodatamodelPackage.OBO_PROPERTY__DOMAIN_EXTENSION:
      case obodatamodelPackage.OBO_PROPERTY__RANGE_EXTENSION:
      case obodatamodelPackage.OBO_PROPERTY__HOLDS_OVER_CHAINS:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
        return;
    }
    super.notifyChanged(notification);
  }

  /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
    super.collectNewChildDescriptors(newChildDescriptors, object);

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_PROPERTY__CYCLIC_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_PROPERTY__SYMMETRIC_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_PROPERTY__TRANSITIVE_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_PROPERTY__REFLEXIVE_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_PROPERTY__DOMAIN_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_PROPERTY__RANGE_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.OBO_PROPERTY__HOLDS_OVER_CHAINS,
         obodatamodelFactory.eINSTANCE.createListOfProperties()));
  }

  /**
   * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
    Object childFeature = feature;
    Object childObject = child;

    boolean qualify =
      childFeature == obodatamodelPackage.Literals.NAMESPACED_OBJECT__NAMESPACE_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.IDENTIFIED_OBJECT__TYPE_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.IDENTIFIED_OBJECT__NAME_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.IDENTIFIED_OBJECT__ID_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.IDENTIFIED_OBJECT__ANONYMOUS_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.COMMENTED_OBJECT__COMMENT_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.OBSOLETABLE_OBJECT__OBSOLETE_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.DEFINED_OBJECT__DEFINITION_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.OBO_PROPERTY__CYCLIC_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.OBO_PROPERTY__SYMMETRIC_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.OBO_PROPERTY__TRANSITIVE_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.OBO_PROPERTY__REFLEXIVE_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.OBO_PROPERTY__ALWAYS_IMPLIES_INVERSE_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.OBO_PROPERTY__DOMAIN_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.OBO_PROPERTY__RANGE_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.OBSOLETABLE_OBJECT__CONSIDER_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.OBSOLETABLE_OBJECT__REPLACED_BY_EXTENSION;

    if (qualify) {
      return getString
        ("_UI_CreateChild_text2",
         new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
    }
    return super.getCreateChildText(owner, feature, child, selection);
  }

}
