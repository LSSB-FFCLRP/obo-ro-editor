/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.provider;


import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelFactory;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ModificationMetadataObjectItemProvider 
  extends ItemProviderAdapter
  implements
    IEditingDomainItemProvider,
    IStructuredItemContentProvider,
    ITreeItemContentProvider,
    IItemLabelProvider,
    IItemPropertySource {
  /**
   * This constructs an instance from a factory and a notifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ModificationMetadataObjectItemProvider(AdapterFactory adapterFactory) {
    super(adapterFactory);
  }

  /**
   * This returns the property descriptors for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
    if (itemPropertyDescriptors == null) {
      super.getPropertyDescriptors(object);

      addCreatedByPropertyDescriptor(object);
      addModifiedByPropertyDescriptor(object);
      addCreationDatePropertyDescriptor(object);
      addModificationDatePropertyDescriptor(object);
    }
    return itemPropertyDescriptors;
  }

  /**
   * This adds a property descriptor for the Created By feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addCreatedByPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_ModificationMetadataObject_createdBy_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_ModificationMetadataObject_createdBy_feature", "_UI_ModificationMetadataObject_type"),
         obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__CREATED_BY,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Modified By feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addModifiedByPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_ModificationMetadataObject_modifiedBy_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_ModificationMetadataObject_modifiedBy_feature", "_UI_ModificationMetadataObject_type"),
         obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__MODIFIED_BY,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Creation Date feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addCreationDatePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_ModificationMetadataObject_creationDate_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_ModificationMetadataObject_creationDate_feature", "_UI_ModificationMetadataObject_type"),
         obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__CREATION_DATE,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Modification Date feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addModificationDatePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_ModificationMetadataObject_modificationDate_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_ModificationMetadataObject_modificationDate_feature", "_UI_ModificationMetadataObject_type"),
         obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
   * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
   * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
    if (childrenFeatures == null) {
      super.getChildrenFeatures(object);
      childrenFeatures.add(obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION);
      childrenFeatures.add(obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION);
    }
    return childrenFeatures;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EStructuralFeature getChildFeature(Object object, Object child) {
    // Check the type of the specified child object and return the proper feature to use for
    // adding (see {@link AddCommand}) it as a child.

    return super.getChildFeature(object, child);
  }

  /**
   * This returns ModificationMetadataObject.gif.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object getImage(Object object) {
    return overlayImage(object, getResourceLocator().getImage("full/obj16/ModificationMetadataObject"));
  }

  /**
   * This returns the label text for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getText(Object object) {
    String label = ((ModificationMetadataObject)object).getCreatedBy();
    return label == null || label.length() == 0 ?
      getString("_UI_ModificationMetadataObject_type") :
      getString("_UI_ModificationMetadataObject_type") + " " + label;
  }
  

  /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void notifyChanged(Notification notification) {
    updateChildren(notification);

    switch (notification.getFeatureID(ModificationMetadataObject.class)) {
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY:
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY:
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE:
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
        return;
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION:
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION:
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION:
      case obodatamodelPackage.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
        return;
    }
    super.notifyChanged(notification);
  }

  /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
    super.collectNewChildDescriptors(newChildDescriptors, object);

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));

    newChildDescriptors.add
      (createChildParameter
        (obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION,
         obodatamodelFactory.eINSTANCE.createNestedValue()));
  }

  /**
   * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
    Object childFeature = feature;
    Object childObject = child;

    boolean qualify =
      childFeature == obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__CREATED_BY_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__MODIFIED_BY_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__CREATION_DATE_EXTENSION ||
      childFeature == obodatamodelPackage.Literals.MODIFICATION_METADATA_OBJECT__MODIFICATION_DATE_EXTENSION;

    if (qualify) {
      return getString
        ("_UI_CreateChild_text2",
         new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
    }
    return super.getCreateChildText(owner, feature, child, selection);
  }

  /**
   * Return the resource locator for this item provider's resources.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ResourceLocator getResourceLocator() {
    return OBODatamodelEditPlugin.INSTANCE;
  }

}
