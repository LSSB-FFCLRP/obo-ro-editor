/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.provider;


import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class OBORestrictionItemProvider extends LinkItemProvider {
  /**
   * This constructs an instance from a factory and a notifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBORestrictionItemProvider(AdapterFactory adapterFactory) {
    super(adapterFactory);
  }

  /**
   * This returns the property descriptors for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
    if (itemPropertyDescriptors == null) {
      super.getPropertyDescriptors(object);

      addCardinalityPropertyDescriptor(object);
      addMaxCardinalityPropertyDescriptor(object);
      addMinCardinalityPropertyDescriptor(object);
      addCompletesPropertyDescriptor(object);
      addInverseCompletesPropertyDescriptor(object);
      addNecessarilyTruePropertyDescriptor(object);
      addInverseNecessarilyTruePropertyDescriptor(object);
      addAdditionalArgumentsPropertyDescriptor(object);
    }
    return itemPropertyDescriptors;
  }

  /**
   * This adds a property descriptor for the Cardinality feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addCardinalityPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBORestriction_cardinality_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBORestriction_cardinality_feature", "_UI_OBORestriction_type"),
         obodatamodelPackage.Literals.OBO_RESTRICTION__CARDINALITY,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Max Cardinality feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addMaxCardinalityPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBORestriction_maxCardinality_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBORestriction_maxCardinality_feature", "_UI_OBORestriction_type"),
         obodatamodelPackage.Literals.OBO_RESTRICTION__MAX_CARDINALITY,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Min Cardinality feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addMinCardinalityPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBORestriction_minCardinality_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBORestriction_minCardinality_feature", "_UI_OBORestriction_type"),
         obodatamodelPackage.Literals.OBO_RESTRICTION__MIN_CARDINALITY,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Completes feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addCompletesPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBORestriction_completes_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBORestriction_completes_feature", "_UI_OBORestriction_type"),
         obodatamodelPackage.Literals.OBO_RESTRICTION__COMPLETES,
         true,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Inverse Completes feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addInverseCompletesPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBORestriction_inverseCompletes_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBORestriction_inverseCompletes_feature", "_UI_OBORestriction_type"),
         obodatamodelPackage.Literals.OBO_RESTRICTION__INVERSE_COMPLETES,
         true,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Necessarily True feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addNecessarilyTruePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBORestriction_necessarilyTrue_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBORestriction_necessarilyTrue_feature", "_UI_OBORestriction_type"),
         obodatamodelPackage.Literals.OBO_RESTRICTION__NECESSARILY_TRUE,
         true,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Inverse Necessarily True feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addInverseNecessarilyTruePropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBORestriction_inverseNecessarilyTrue_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBORestriction_inverseNecessarilyTrue_feature", "_UI_OBORestriction_type"),
         obodatamodelPackage.Literals.OBO_RESTRICTION__INVERSE_NECESSARILY_TRUE,
         true,
         false,
         false,
         ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Additional Arguments feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addAdditionalArgumentsPropertyDescriptor(Object object) {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_OBORestriction_additionalArguments_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_OBORestriction_additionalArguments_feature", "_UI_OBORestriction_type"),
         obodatamodelPackage.Literals.OBO_RESTRICTION__ADDITIONAL_ARGUMENTS,
         true,
         false,
         true,
         null,
         null,
         null));
  }

  /**
   * This returns OBORestriction.gif.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object getImage(Object object) {
    return overlayImage(object, getResourceLocator().getImage("full/obj16/OBORestriction"));
  }

  /**
   * This returns the label text for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getText(Object object) {
    String label = ((OBORestriction)object).getId();
    return label == null || label.length() == 0 ?
      getString("_UI_OBORestriction_type") :
      getString("_UI_OBORestriction_type") + " " + label;
  }
  

  /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void notifyChanged(Notification notification) {
    updateChildren(notification);

    switch (notification.getFeatureID(OBORestriction.class)) {
      case obodatamodelPackage.OBO_RESTRICTION__CARDINALITY:
      case obodatamodelPackage.OBO_RESTRICTION__MAX_CARDINALITY:
      case obodatamodelPackage.OBO_RESTRICTION__MIN_CARDINALITY:
      case obodatamodelPackage.OBO_RESTRICTION__COMPLETES:
      case obodatamodelPackage.OBO_RESTRICTION__INVERSE_COMPLETES:
      case obodatamodelPackage.OBO_RESTRICTION__NECESSARILY_TRUE:
      case obodatamodelPackage.OBO_RESTRICTION__INVERSE_NECESSARILY_TRUE:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
        return;
    }
    super.notifyChanged(notification);
  }

  /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
    super.collectNewChildDescriptors(newChildDescriptors, object);
  }

}
