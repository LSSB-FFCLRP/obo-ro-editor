package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

/**
 * A number of methods to serialize and deserialize ODM models
 * @author cawal
 *
 */
public class OBODatamodelSerializer {

  public static void serialize(OBOSession session, String filePath) throws IOException {
    // Initialize the model
    obodatamodelPackage.eINSTANCE.eClass();

    // Register the XMI resource factory for the .xmi extension
    Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
    Map<String, Object> m = reg.getExtensionToFactoryMap();
    m.put("obodatamodel", new XMIResourceFactoryImpl());

    // Obtain a new resource set
    ResourceSet resSet = new ResourceSetImpl();
    resSet.getLoadOptions().put(XMIResource.OPTION_DEFER_IDREF_RESOLUTION, Boolean.TRUE);
    //resSet.getLoadOptions().put(XMIResource.OPTION_DEFER_ATTACHMENT,  Boolean.TRUE);
    resSet.getLoadOptions().put(XMIResource.OPTION_ENCODING, "UTF-8");
    // Get the resource
    Resource resource = resSet.createResource(URI.createURI(filePath));
    
    resource.getContents().add(session);
    resource.save(Collections.EMPTY_MAP);
  }
  
}
