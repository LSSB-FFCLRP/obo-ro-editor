package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors;

import java.io.OutputStream;
import java.util.Map;

import org.bbop.dataadapter.DataAdapterException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.m2m.atl.core.ATLCoreException;
import org.eclipse.m2m.atl.core.IExtractor;
import org.eclipse.m2m.atl.core.IModel;
import org.obo.dataadapter.OBOAdapter;
import org.obo.dataadapter.OBOFileAdapter;
import org.obo.datamodel.impl.DefaultOperationModel;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;
import edu.emory.mathcs.backport.java.util.Collections;


/**
 * This class implements operations that takes an ODM model and serializes it as an OBOFFF ontology
 * (extractor operations).
 * 
 * @author cawal
 *
 */
public class OBOFileExtractor implements IExtractor {

  @SuppressWarnings("unchecked")
  @Override
  public void extract(IModel sourceModel, String target) throws ATLCoreException {
    extract(sourceModel,target,Collections.emptyMap()); 
  }

  
  
 
  @Override
  public void extract(IModel sourceModel, String target, Map<String, Object> options)
      throws ATLCoreException {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void extract(IModel sourceModel, OutputStream target, Map<String, Object> options)
      throws ATLCoreException {
    // TODO Auto-generated method stub
    
  }




}
