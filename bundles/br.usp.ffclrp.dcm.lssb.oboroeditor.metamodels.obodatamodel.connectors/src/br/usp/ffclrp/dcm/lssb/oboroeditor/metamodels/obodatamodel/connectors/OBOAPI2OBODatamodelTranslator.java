package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceFactoryImpl;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.AnnotatedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.CommentedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingProperty;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Datatype;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DatatypeValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DbxrefedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPath;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPathSpec;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObjectIndex;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Impliable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Instance;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Link;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkLinkedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ListOfProperties;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MultiIDObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MutableLinkDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NamespacedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOClass;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectFactory;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectField;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OboPropertyValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PathCapable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.RootAlgorithm;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SubsetObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Type;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Value;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueLink;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelFactory;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;



/**
 * This class implements operations that takes a OBOFFF ontology and transforms it in an ODM model
 * (injection operations).
 * 
 * @author cawal
 *
 */
public class OBOAPI2OBODatamodelTranslator {

  final static int WRONG_ARGUMENTS = 1;

  
  private org.obo.datamodel.OBOSession source = null;
  private OBOSession target = null;
  private Map<Object,EObject> objectMap;
  
  public OBOAPI2OBODatamodelTranslator(org.obo.datamodel.OBOSession source) {
    if(source != null)
      this.source = source;
    else 
      throw new Error("Source session in null!");
    
    objectMap = new LinkedHashMap<Object, EObject>();
  }
  
  public OBOSession translate(){
    
    objectMap.clear();
    
    // create the ontology model object
    target = (OBOSession) getEquivalentEObject(source, objectMap);

    // cria e mapeia cada objeto da session
    // faz todas as ligações entre os objetos e passagem de atributos.
    linkObjects(objectMap);

    for (EObject o : objectMap.values()) {

      // get dbxrefs into containment tree
      if (o instanceof Dbxref) {
        target.getAllDbxRefsContainer().add((Dbxref) o);
      
      // warants that all IdentifiableObjects are in the containment tree
      } else if (o instanceof OBOObject) {
        target.getObjects().add((OBOObject)o);
      }
    }
    
    return target;
  }
  


  private static EObject getEquivalentEObject(Object object, Map<Object, EObject> objectMap) {
    EObject eo = null;
    if (!objectMap.containsKey(object)) {
      eo = createObjectOfEquivalentClass(object);
      objectMap.put(object, eo);
    } else {
      eo = objectMap.get(object);
    }

    return eo;

  }



  @SuppressWarnings({"unchecked", "rawtypes"})
  private static Collection getEquivalentCollection(Collection c, Map<Object, EObject> objectMap) {
    if(c == null) {
      c = Collections.emptyList();
    }
    Collection out = new ArrayList();
    for (Object o : c) {
      out.add(getEquivalentEObject(o, objectMap));
    }

    return out;
  }

  /**
   * Se encontrar uma classe que não foi contemplada, coloque a criação dela aqui
   * 
   * @param object
   * @return
   */
  private static EObject createObjectOfEquivalentClass(Object object) {
    EObject eobject = null;
    obodatamodelPackage.eINSTANCE.eClass();
    // Retrieve the default factory singleton
    obodatamodelFactory factory = obodatamodelFactory.eINSTANCE;


    // create object of most child subclass -------------------------------
    // OBOClass -> OBOObject, Type, Comparable
    if (object instanceof org.obo.datamodel.OBOClass) {
      eobject = factory.createOBOClass();

      // OBOProperty -> OBOObject
    } else if (object instanceof org.obo.datamodel.OBOProperty) {
      eobject = factory.createOBOProperty();

      // Instance -> OBOObject
    } else if (object instanceof org.obo.datamodel.Instance) {
      eobject = factory.createInstance();

      // OBOObject -> AnnotatedObject, LinkedObject, Comparable
    } else if (object instanceof org.obo.datamodel.OBOObject) {
      throw new RuntimeException(
          "org.obo.datamodel.OBOObject should be abstract! Cannot create instance!");
      // eobject = factory.createOBOObject();

      // AnnotatedObject -> IdentifiedObject, ModificationMetadataObject,
      // MultiIDObject, SynonymedObject, DbxrefedObject, CommentedObject,
      // ObsoletableObject, Cloneable, Serializable, DefinedObject, SubsetObject
    } else if (object instanceof org.obo.datamodel.AnnotatedObject) {
      eobject = factory.createAnnotatedObject();

      // Datatype -> Type
    } else if (object instanceof org.obo.datamodel.Datatype) {
      eobject = factory.createDatatype();

      // Type -> IdentifiedObject
    } else if (object instanceof org.obo.datamodel.Type) {
      throw new RuntimeException(
          "org.obo.datamodel.Type should be abstract! Cannot create instance!");
      // eobject = factory.createType();

      // MultiIDObject -> IdentifiedObject
    } else if (object instanceof org.obo.datamodel.MultiIDObject) {
      throw new RuntimeException(
          "org.obo.datamodel.MultiIDObject should be abstract! Cannot create instance!");
      // eobject = factory.createMultiIDObject();

      // SynonymedObject -> IdentifiedObject
    } else if (object instanceof org.obo.datamodel.SynonymedObject) {
      eobject = factory.createSynonymedObject();

      // CommentedObject -> IdentifiedObject
    } else if (object instanceof org.obo.datamodel.CommentedObject) {
      throw new RuntimeException(
          "org.obo.datamodel.CommentedObject should be abstract! Cannot create instance!");
      // eobject = factory.createCommentedObject();

      // ObsoletableObject -> IdentifiedObject
    } else if (object instanceof org.obo.datamodel.ObsoletableObject) {
      throw new RuntimeException(
          "org.obo.datamodel.ObsoletableObject should be abstract! Cannot create instance!");
      // eobject = factory.createObsoletableObject();

      // DefinedObject -> IdentifiedObject
    } else if (object instanceof org.obo.datamodel.DefinedObject) {
      throw new RuntimeException(
          "org.obo.datamodel.DefinedObject should be abstract! Cannot create instance!");
      // eobject = factory.createDefinedObject();

      // LinkLinkedObject -> LinkedObject
    } else if (object instanceof org.obo.datamodel.LinkLinkedObject) {
      eobject = factory.createLinkLinkedObject();

      // DanglingObject -> LinkedObject, Type
    } else if (object instanceof org.obo.datamodel.DanglingObject) {
      eobject = factory.createDanglingObject();

      // DanglingProperty -> LinkedObject, OBOProperty
    } else if (object instanceof org.obo.datamodel.DanglingProperty) {
      eobject = factory.createDanglingProperty();

      // LinkedObject -> IdentifiedObject, PathCapable
    } else if (object instanceof org.obo.datamodel.LinkedObject) {
      throw new RuntimeException(
          "org.obo.datamodel.LinkedObject should be abstract! Cannot create instance!");
      // eobject = factory.createLinkedObject();

      // IdentifiedObject -> IdentifiableObject, NamespacedObject, Value,
      // Cloneable, Serializable
    } else if (object instanceof org.obo.datamodel.IdentifiedObject) {
      throw new RuntimeException(
          "org.obo.datamodel.IdentifiedObject should be abstract! Cannot create instance!");
      // eobject = factory.createIdentifiedObject();

      // DatatypeValue -> Value
    } else if (object instanceof org.obo.datamodel.DatatypeValue) {
      eobject = factory.createDatatypeValue();

      // OBORestriction -> Link
    } else if (object instanceof org.obo.datamodel.OBORestriction) {
      eobject = factory.createOBORestriction();

      // ValueLink -> Link
    } else if (object instanceof org.obo.datamodel.ValueLink) {
      eobject = factory.createValueLink();

      // Link -> Impliable, IdentifiableObject, Relationship, PathCapable
    } else if (object instanceof org.obo.datamodel.Link) {
      throw new RuntimeException(
          "org.obo.datamodel.Link should be abstract! Cannot create instance!");
      // eobject = factory.createLink();

      // MutableLinkDatabase -> LinkDatabase
    } else if (object instanceof org.obo.datamodel.MutableLinkDatabase) {
      eobject = factory.createMutableLinkDatabase();

      // LinkDatabase -> IdentifiedObjectIndex, Serializable, Cloneable
    } else if (object instanceof org.obo.datamodel.LinkDatabase) {
      eobject = factory.createLinkDatabase();

      // OBOSession -> IdentifiedObjectIndex, Serializable
    } else if (object instanceof org.obo.datamodel.OBOSession) {
      eobject = factory.createOBOSession();

      // ValueDatabase -> IdentifiedObjectIndex
    } else if (object instanceof org.obo.datamodel.ValueDatabase) {
      eobject = factory.createValueDatabase();

      // Synonym -> Cloneable, Serializable, Comparable, IdentifiableObject
    } else if (object instanceof org.obo.datamodel.Synonym) {
      eobject = factory.createSynonym();

      // Dbxref -> Cloneable, Serializable, Comparable, IdentifiableObject
    } else if (object instanceof org.obo.datamodel.Dbxref) {
      eobject = factory.createDbxref();

      // ModificationMetadataObject
    } else if (object instanceof org.obo.datamodel.ModificationMetadataObject) {
      eobject = factory.createModificationMetadataObject();

      // DbxrefedObject
    } else if (object instanceof org.obo.datamodel.DbxrefedObject) {
      eobject = factory.createDbxrefedObject();

      // SubsetObject
    } else if (object instanceof org.obo.datamodel.SubsetObject) {
      eobject = factory.createSubsetObject();

      // IdentifiableObject
    } else if (object instanceof org.obo.datamodel.IdentifiableObject) {
      throw new RuntimeException(
          "org.obo.datamodel.IdentifiableObject should be abstract! Cannot create instance!");
      // eobject = factory.createIdentifiableObject();

      // NamespacedObject
    } else if (object instanceof org.obo.datamodel.NamespacedObject) {
      throw new RuntimeException(
          "org.obo.datamodel.NamespacedObject should be abstract! Cannot create instance!");
      // eobject = factory.createNamespacedObject();

      // Namespace
    } else if (object instanceof org.obo.datamodel.Namespace) {
      eobject = factory.createNamespace();

      // RootAlgorithm
    } else if (object instanceof org.obo.datamodel.RootAlgorithm) {
      eobject = factory.createRootAlgorithm();

      // UnknownStanza
    } else if (object instanceof org.obo.datamodel.UnknownStanza) {
      eobject = factory.createUnknownStanza();

      // IdentifiedObjectIndex
    } else if (object instanceof org.obo.datamodel.IdentifiedObjectIndex) {
      eobject = factory.createIdentifiedObjectIndex();

      // Impliable
    } else if (object instanceof org.obo.datamodel.Impliable) {
      throw new RuntimeException(
          "org.obo.datamodel.Impliable should be abstract! Cannot create instance!");
      // eobject = factory.createImpliable();

      // ObjectField
    } else if (object instanceof org.obo.datamodel.ObjectField) {
      eobject = factory.createObjectField();

      // FieldPath
    } else if (object instanceof org.obo.datamodel.FieldPath) {
      eobject = factory.createFieldPath();

      // PathCapable
    } else if (object instanceof org.obo.datamodel.PathCapable) {
      eobject = factory.createPathCapable();

      // FieldPathSpec
    } else if (object instanceof org.obo.datamodel.FieldPathSpec) {
      eobject = factory.createFieldPathSpec();

      // Value -> Cloneable, Serializable
    } else if (object instanceof org.obo.datamodel.Value) {
      eobject = factory.createValue();

      // TermSubset -> Cloneable, Serializable
    } else if (object instanceof org.obo.datamodel.TermSubset) {
      eobject = factory.createTermSubset();

      // SynonymType -> Cloneable
    } else if (object instanceof org.obo.datamodel.SynonymType) {
      eobject = factory.createSynonymType();

      // NestedValue -> Cloneable, Serializable
    } else if (object instanceof org.obo.datamodel.NestedValue) {
      eobject = factory.createNestedValue();

      // Relationship -> Serializable, Cloneable
    } else if (object instanceof org.obo.datamodel.Relationship) {
      throw new RuntimeException(
          "org.obo.datamodel.Relationship should be abstract! Cannot create instance!");
      // eobject = factory.createRelationship();

      // ObjectFactory -> Serializable, Cloneable
    } else if (object instanceof org.obo.datamodel.ObjectFactory) {
      eobject = factory.createObjectFactory();

      // PropertyValue -> Cloneable, Serializable
    } else if (object instanceof org.obo.datamodel.PropertyValue) {
      eobject = factory.createPropertyValue();

    } else if (object instanceof List<?>) {
      eobject = factory.createListOfProperties();

    } // --------------------------------------------------------------


    return eobject;
  }


  @SuppressWarnings({"rawtypes"})
  private static void linkObjects(Map<Object, EObject> objectMap) {

    for (int i = 0; i < objectMap.size(); i++) {// -----------------------
      Object[] array = objectMap.keySet().toArray();
      Object o = array[i];
      EObject eo = objectMap.get(o);


      if (eo instanceof AnnotatedObject) {
        passPropertiesOfAnnotatedObject((org.obo.datamodel.AnnotatedObject) o, objectMap);
      }

      if (eo instanceof CommentedObject) {
        passPropertiesOfCommentedObject((org.obo.datamodel.CommentedObject) o, objectMap);
      }

      if (eo instanceof DanglingObject) {
        passPropertiesOfDanglingObject((org.obo.datamodel.DanglingObject) o, objectMap);
      }

      if (eo instanceof DanglingProperty) {
        passPropertiesOfDanglingProperty((org.obo.datamodel.DanglingProperty) o, objectMap);
      }

      if (eo instanceof Datatype) {
        passPropertiesOfDatatype((org.obo.datamodel.Datatype) o, objectMap);
      }

      if (eo instanceof DatatypeValue) {
        passPropertiesOfDatatypeValue((org.obo.datamodel.DatatypeValue) o, objectMap);
      }

      if (eo instanceof DbxrefedObject) {
        passPropertiesOfDbxrefedObject((org.obo.datamodel.DbxrefedObject) o, objectMap);
      }

      if (eo instanceof Dbxref) {
        passPropertiesOfDbxref((org.obo.datamodel.Dbxref) o, objectMap);
      }

      if (eo instanceof DefinedObject) {
        passPropertiesOfDefinedObject((org.obo.datamodel.DefinedObject) o, objectMap);
      }

      if (eo instanceof FieldPath) {
        passPropertiesOfFieldPath((org.obo.datamodel.FieldPath) o, objectMap);
      }

      if (eo instanceof FieldPathSpec) {
        passPropertiesOfFieldPathSpec((org.obo.datamodel.FieldPathSpec) o, objectMap);
      }

      if (eo instanceof IdentifiableObject) {
        passPropertiesOfIdentifiableObject((org.obo.datamodel.IdentifiableObject) o, objectMap);
      }

      if (eo instanceof IdentifiedObjectIndex) {
        passPropertiesOfIdentifiedObjectIndex((org.obo.datamodel.IdentifiedObjectIndex) o,
            objectMap);
      }

      if (eo instanceof IdentifiedObject) {
        passPropertiesOfIdentifiedObject((org.obo.datamodel.IdentifiedObject) o, objectMap);
      }

      if (eo instanceof Impliable) {
        passPropertiesOfImpliable((org.obo.datamodel.Impliable) o, objectMap);
      }

      if (eo instanceof Instance) {
        passPropertiesOfInstance((org.obo.datamodel.Instance) o, objectMap);
      }

      if (eo instanceof LinkDatabase) {
        passPropertiesOfLinkDatabase((org.obo.datamodel.LinkDatabase) o, objectMap);
      }

      if (eo instanceof LinkedObject) {
        passPropertiesOfLinkedObject((org.obo.datamodel.LinkedObject) o, objectMap);
      }

      if (eo instanceof Link) {
        passPropertiesOfLink((org.obo.datamodel.Link) o, objectMap);
      }

      if (eo instanceof LinkLinkedObject) {
        passPropertiesOfLinkLinkedObject((org.obo.datamodel.LinkLinkedObject) o, objectMap);
      }

      if (eo instanceof ModificationMetadataObject) {
        passPropertiesOfModificationMetadataObject((org.obo.datamodel.ModificationMetadataObject) o,
            objectMap);
      }

      if (eo instanceof MultiIDObject) {
        passPropertiesOfMultiIDObject((org.obo.datamodel.MultiIDObject) o, objectMap);
      }

      if (eo instanceof MutableLinkDatabase) {
        passPropertiesOfMutableLinkDatabase((org.obo.datamodel.MutableLinkDatabase) o, objectMap);
      }

      if (eo instanceof NamespacedObject) {
        passPropertiesOfNamespacedObject((org.obo.datamodel.NamespacedObject) o, objectMap);
      }

      if (eo instanceof Namespace) {
        passPropertiesOfNamespace((org.obo.datamodel.Namespace) o, objectMap);
      }

      if (eo instanceof NestedValue) {
        passPropertiesOfNestedValue((org.obo.datamodel.NestedValue) o, objectMap);
      }

      if (eo instanceof ObjectFactory) {
        passPropertiesOfObjectFactory((org.obo.datamodel.ObjectFactory) o, objectMap);
      }

      if (eo instanceof ObjectField) {
        passPropertiesOfObjectField((org.obo.datamodel.ObjectField) o, objectMap);
      }

      if (eo instanceof OBOClass) {
        passPropertiesOfOBOClass((org.obo.datamodel.OBOClass) o, objectMap);
      }

      if (eo instanceof OBOObject) {
        passPropertiesOfOBOObject((org.obo.datamodel.OBOObject) o, objectMap);
      }

      if (eo instanceof OBOProperty) {
        passPropertiesOfOBOProperty((org.obo.datamodel.OBOProperty) o, objectMap);
      }

      if (eo instanceof OBORestriction) {
        passPropertiesOfOBORestriction((org.obo.datamodel.OBORestriction) o, objectMap);
      }

      if (eo instanceof OBOSession) {
        passPropertiesOfOBOSession((org.obo.datamodel.OBOSession) o, objectMap);
      }

      if (eo instanceof ObsoletableObject) {
        passPropertiesOfObsoletableObject((org.obo.datamodel.ObsoletableObject) o, objectMap);
      }

      if (eo instanceof PathCapable) {
        passPropertiesOfPathCapable((org.obo.datamodel.PathCapable) o, objectMap);
      }

      if (eo instanceof PropertyValue) {
        passPropertiesOfPropertyValue((org.obo.datamodel.PropertyValue) o, objectMap);
      }

      if (eo instanceof Relationship) {
        passPropertiesOfRelationship((org.obo.datamodel.Relationship) o, objectMap);
      }

      if (eo instanceof RootAlgorithm) {
        passPropertiesOfRootAlgorithm((org.obo.datamodel.RootAlgorithm) o, objectMap);
      }

      if (eo instanceof SubsetObject) {
        passPropertiesOfSubsetObject((org.obo.datamodel.SubsetObject) o, objectMap);
      }

      if (eo instanceof SynonymedObject) {
        passPropertiesOfSynonymedObject((org.obo.datamodel.SynonymedObject) o, objectMap);
      }

      if (eo instanceof Synonym) {
        passPropertiesOfSynonym((org.obo.datamodel.Synonym) o, objectMap);
      }

      if (eo instanceof SynonymType) {
        passPropertiesOfSynonymType((org.obo.datamodel.SynonymType) o, objectMap);
      }

      if (eo instanceof TermSubset) {
        passPropertiesOfTermSubset((org.obo.datamodel.TermSubset) o, objectMap);
      }

      if (eo instanceof Type) {
        passPropertiesOfType((org.obo.datamodel.Type) o, objectMap);
      }

      if (eo instanceof UnknownStanza) {
        passPropertiesOfUnknownStanza((org.obo.datamodel.UnknownStanza) o, objectMap);
      }

      if (eo instanceof ValueDatabase) {
        passPropertiesOfValueDatabase((org.obo.datamodel.ValueDatabase) o, objectMap);
      }

      if (eo instanceof Value) {
        passPropertiesOfValue((org.obo.datamodel.Value) o, objectMap);
      }

      if (eo instanceof ValueLink) {
        passPropertiesOfValueLink((org.obo.datamodel.ValueLink) o, objectMap);
      }


    } // end for over objectMap ---------------------------------------
  }



  public static boolean saveAsXmi(EObject ecoreModel, String outputFilepath) {
    // As of here we preparing to save the model content
    // Register the XMI resource factory for the .xmi extension
    Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
    Map<String, Object> m = reg.getExtensionToFactoryMap();
    m.put("xmi", new XMIResourceFactoryImpl());

    // Obtain a new resource set
    ResourceSet resSet = new ResourceSetImpl();
    resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("obodatamodel",
        new XMLResourceFactoryImpl());
    // create a resource
    Resource resource = resSet.createResource(URI.createURI(outputFilepath));


    // Get the first model element and cast it to the right type, in my
    // example everything is hierarchical included in this first node
    resource.getContents().add(ecoreModel);

    // now save the content.
    try {
      resource.save(Collections.EMPTY_MAP);
    } catch (IOException e) {
      e.printStackTrace();
      return false;
    }

    return true;
  }



  /************************************************************************* 
   * Methods related with
   * the transformation of the attributtes / links
   **************************************************************************/

  private static void passPropertiesOfAnnotatedObject(org.obo.datamodel.AnnotatedObject object,
      Map<Object, EObject> objectMap) {

    AnnotatedObject eobject = (AnnotatedObject) objectMap.get(object);

    if (eobject != null) {
    }

  }

  private static void passPropertiesOfCommentedObject(org.obo.datamodel.CommentedObject object,
      Map<Object, EObject> objectMap) {

    CommentedObject eobject = (CommentedObject) objectMap.get(object);

    if (eobject != null) {
      // comment - string
      eobject.setComment(object.getComment());

      // commentExtension
      eobject.setCommentExtension(
          (NestedValue) getEquivalentEObject(object.getCommentExtension(), objectMap));
    }

  }


  private static void passPropertiesOfDanglingObject(org.obo.datamodel.DanglingObject object,
      Map<Object, EObject> objectMap) {

    DanglingObject eobject = (DanglingObject) objectMap.get(object);

    if (eobject != null) {
      // dangling - boolean
      eobject.setDangling(object.isDangling());

    }

  }


  private static void passPropertiesOfDanglingProperty(org.obo.datamodel.DanglingProperty object,
      Map<Object, EObject> objectMap) {

    DanglingProperty eobject = (DanglingProperty) objectMap.get(object);

    if (eobject != null) {
    }

  }


  @SuppressWarnings("rawtypes")
  private static void passPropertiesOfDatatype(org.obo.datamodel.Datatype object,
      Map<Object, EObject> objectMap) {

    Datatype eobject = (Datatype) objectMap.get(object);

    if (eobject != null) {
      // abstract - boolean
      eobject.setAbstract(object.isAbstract());

    }

  }


  @SuppressWarnings("rawtypes")
  private static void passPropertiesOfDatatypeValue(org.obo.datamodel.DatatypeValue object,
      Map<Object, EObject> objectMap) {

    DatatypeValue eobject = (DatatypeValue) objectMap.get(object);

    if (eobject != null) {
      // value - string
      eobject.setValue(object.getValue());
    }

  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfDbxrefedObject(org.obo.datamodel.DbxrefedObject object,
      Map<Object, EObject> objectMap) {

    DbxrefedObject eobject = (DbxrefedObject) objectMap.get(object);

    if (eobject != null) {
      // dbxref
      eobject.getDbxrefs()
          .addAll((Collection<Dbxref>) getEquivalentCollection(object.getDbxrefs(), objectMap));
    }

  }


  private static void passPropertiesOfDbxref(org.obo.datamodel.Dbxref object,
      Map<Object, EObject> objectMap) {

    Dbxref eobject = (Dbxref) objectMap.get(object);

    if (eobject != null) {
      // property nestedValue : NestedValue[?] { composes };
      eobject
          .setNestedValue((NestedValue) getEquivalentEObject(object.getNestedValue(), objectMap));

      // property defRef : Boolean;
      eobject.setDefRef(object.isDefRef());

      // property desc : String ='';
      eobject.setDesc(object.getDesc());

      // property type : Type[?];
      eobject.setType(BigInteger.valueOf(object.getType()));

      // property synonym : Synonym[?];
      eobject.setSynonym((Synonym) getEquivalentEObject(object.getSynonym(), objectMap));

      // property databaseID : String = '';
      eobject.setDatabaseID(object.getDatabaseID());

      // property database : String = '';
      eobject.setDatabase(object.getDatabase());

    }

  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfDefinedObject(org.obo.datamodel.DefinedObject object,
      Map<Object, EObject> objectMap) {

    DefinedObject eobject = (DefinedObject) objectMap.get(object);

    if (eobject != null) {

      // property _'definition' : String[?];
      eobject.setDefinition(object.getDefinition());

      // property defDbxrefs : Dbxref[*] { composes };
      eobject.getDefDbxrefs()
          .addAll((Collection<Dbxref>) getEquivalentCollection(object.getDefDbxrefs(), objectMap));

      // property definitionExtension : NestedValue[?] {composes};
      eobject.setDefinitionExtension(
          (NestedValue) getEquivalentEObject(object.getDefinitionExtension(), objectMap));
    }

  }


  private static void passPropertiesOfFieldPath(org.obo.datamodel.FieldPath object,
      Map<Object, EObject> objectMap) {

    FieldPath eobject = (FieldPath) objectMap.get(object);

    if (eobject != null) {
    }

  }


  private static void passPropertiesOfFieldPathSpec(org.obo.datamodel.FieldPathSpec object,
      Map<Object, EObject> objectMap) {

    FieldPathSpec eobject = (FieldPathSpec) objectMap.get(object);

    if (eobject != null) {
    }

  }



  private static void passPropertiesOfIdentifiableObject(
      org.obo.datamodel.IdentifiableObject object, Map<Object, EObject> objectMap) {

    IdentifiableObject eobject = (IdentifiableObject) objectMap.get(object);

    if (eobject != null) {
      // id - string
      eobject.setId(object.getID());

      // anonymous - boolean
      eobject.setAnonymous(object.isAnonymous());
    }

  }



  private static void passPropertiesOfIdentifiedObjectIndex(
      org.obo.datamodel.IdentifiedObjectIndex object, Map<Object, EObject> objectMap) {

    IdentifiedObjectIndex eobject = (IdentifiedObjectIndex) objectMap.get(object);

    if (eobject != null) {
    }

  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfIdentifiedObject(org.obo.datamodel.IdentifiedObject object,
      Map<Object, EObject> objectMap) {

    IdentifiedObject eobject = (IdentifiedObject) objectMap.get(object);

    if (eobject != null) {

      // property typeExtension : NestedValue[?] { composes };
      eobject.setTypeExtension(
          (NestedValue) getEquivalentEObject(object.getTypeExtension(), objectMap));

      // property builtIn : Boolean;
      eobject.setBuiltIn(object.isBuiltIn());

      // property name : String;
      eobject.setName(object.getName());

      // property nameExtension : NestedValue[?] { composes };
      eobject.setNameExtension(
          (NestedValue) getEquivalentEObject(object.getNameExtension(), objectMap));

      // property idExtension : NestedValue[?] { composes };
      eobject
          .setIdExtension((NestedValue) getEquivalentEObject(object.getIDExtension(), objectMap));

      // property anonymousExtension : NestedValue[?] { composes };
      eobject.setAnonymousExtension(
          (NestedValue) getEquivalentEObject(object.getAnonymousExtension(), objectMap));

      // property propertyValues : PropertyValue[*] { composes };
      eobject.getPropertyValues()
          .addAll((Collection<PropertyValue>) getEquivalentCollection(object.getPropertyValues(),
              objectMap));



    }

  }


  private static void passPropertiesOfImpliable(org.obo.datamodel.Impliable object,
      Map<Object, EObject> objectMap) {

    Impliable eobject = (Impliable) objectMap.get(object);

    if (eobject != null) {
      // property implied : Boolean;
      eobject.setImplied(object.isImplied());
    }

  }


  @SuppressWarnings("rawtypes")
  private static void passPropertiesOfInstance(org.obo.datamodel.Instance object,
      Map<Object, EObject> objectMap) {

    Instance eobject = (Instance) objectMap.get(object);

    if (eobject != null) {
      obodatamodelPackage.eINSTANCE.eClass();
      // Retrieve the default factory singleton
      obodatamodelFactory factory = obodatamodelFactory.eINSTANCE;

      // property oboPropertiesValues : OboPropertyValue[*] {composes};
      for (org.obo.datamodel.OBOProperty p : object.getProperties()) {
        for (org.obo.datamodel.Value v : object.getValues(p)) {
          OboPropertyValue pv = factory.createOboPropertyValue();
          pv.setProperty((OBOProperty) getEquivalentEObject(p, objectMap));
          pv.setValue((Value) getEquivalentEObject(v, objectMap));
          eobject.getOboPropertiesValues().add(pv);
        }
      }


      eobject.setType((OBOClass) getEquivalentEObject(object.getType(), objectMap));
    }

  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfLinkDatabase(org.obo.datamodel.LinkDatabase object,
      Map<Object, EObject> objectMap) {

    LinkDatabase eobject = (LinkDatabase) objectMap.get(object);

    if (eobject != null) {
      // property objects : IdentifiedObject[*];
      eobject.getObjects().addAll(
          (Collection<IdentifiedObject>) getEquivalentCollection(object.getObjects(), objectMap));


      // property session#linkDatabase : OBOSession[?];
      eobject.setSession((OBOSession) getEquivalentEObject(object.getSession(), objectMap));

      // property objects : IdentifiedObject[*] { unique };
      eobject.getObjects().addAll(
          (Collection<IdentifiedObject>) getEquivalentCollection(object.getObjects(), objectMap));

      // property properties : OBOProperty[*] { unique composes};
      eobject.getProperties().addAll(
          (Collection<OBOProperty>) getEquivalentCollection(object.getProperties(), objectMap));
    }

  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfLinkedObject(org.obo.datamodel.LinkedObject object,
      Map<Object, EObject> objectMap) {

    LinkedObject eobject = (LinkedObject) objectMap.get(object);

    if (eobject != null) {
      // pass childrens
      eobject.getChildren().addAll(
          (Collection<Relationship>) getEquivalentCollection(object.getChildren(), objectMap));

      // pass parents
      eobject.getParents().addAll(
          (Collection<Relationship>) getEquivalentCollection(object.getParents(), objectMap));


    }

  }


  private static void passPropertiesOfLink(org.obo.datamodel.Link object,
      Map<Object, EObject> objectMap) {

    Link eobject = (Link) objectMap.get(object);

    if (eobject != null) {
      // property namespace : Namespace[?];
      eobject.setNamespace((Namespace) getEquivalentEObject(object.getNamespace(), objectMap));
    }

  }


  private static void passPropertiesOfLinkLinkedObject(org.obo.datamodel.LinkLinkedObject object,
      Map<Object, EObject> objectMap) {

    LinkLinkedObject eobject = (LinkLinkedObject) objectMap.get(object);

    if (eobject != null) {
      // property link : Link[?];
      eobject.setLink((Link) getEquivalentEObject(object.getLink(), objectMap));

    }

  }


  private static void passPropertiesOfModificationMetadataObject(
      org.obo.datamodel.ModificationMetadataObject object, Map<Object, EObject> objectMap) {

    ModificationMetadataObject eobject = (ModificationMetadataObject) objectMap.get(object);

    if (eobject != null) {

      // property createdBy : String[?];
      eobject.setCreatedBy(object.getCreatedBy());

      // property createdByExtension : NestedValue[?] { composes };
      eobject.setCreatedByExtension(
          (NestedValue) getEquivalentEObject(object.getCreatedByExtension(), objectMap));

      // --property creationDate : Date[?] { composes };
      eobject.setCreationDate(object.getCreationDate());
      // --property creationDateExtension : NestedValue[?] { composes };
      eobject.setCreationDateExtension(
          (NestedValue) getEquivalentEObject(object.getCreationDateExtension(), objectMap));

      // property modifiedBy : String[?];
      eobject.setModifiedBy(object.getModifiedBy());

      // property creationModifiedByExtension : NestedValue[?] { composes };
      eobject.setModifiedByExtension(
          (NestedValue) getEquivalentEObject(object.getModifiedByExtension(), objectMap));

      // --property modificationDate : Date[?] { composes };
      eobject.setModificationDate(object.getModificationDate());

      // --property modificationDateExtension : NestedValue[?] { composes };
      eobject.setModificationDateExtension(
          (NestedValue) getEquivalentEObject(object.getModificationDateExtension(), objectMap));

    }

  }


  private static void passPropertiesOfMultiIDObject(org.obo.datamodel.MultiIDObject object,
      Map<Object, EObject> objectMap) {

    MultiIDObject eobject = (MultiIDObject) objectMap.get(object);

    if (eobject != null) {

      // property secondaryIds : String[*] { composes unique };
      eobject.getSecondaryIds().addAll(object.getSecondaryIDs());

      // property secondaryIdExtension : StringToNestedValueMap[*] {composes};
      for (String secondaryId : object.getSecondaryIDs()) {
        org.obo.datamodel.NestedValue oNestedValue = object.getSecondaryIDExtension(secondaryId);
        if (oNestedValue != null) {
          eobject.getSecondaryIdExtension().put(secondaryId,
              (NestedValue) getEquivalentEObject(oNestedValue, objectMap));
        }
      }

    }

  }


  private static void passPropertiesOfMutableLinkDatabase(
      org.obo.datamodel.MutableLinkDatabase object, Map<Object, EObject> objectMap) {

    MutableLinkDatabase eobject = (MutableLinkDatabase) objectMap.get(object);

    if (eobject != null) {
      // property identifiedObjectIndex : IdentifiedObjectIndex[*] { composes };
      // eobject.setIdentifiedObjectIndex(
      // getEquivalentEObject(object.ge, objectMap)); não possui gets
    }

  }


  private static void passPropertiesOfNamespacedObject(org.obo.datamodel.NamespacedObject object,
      Map<Object, EObject> objectMap) {

    NamespacedObject eobject = (NamespacedObject) objectMap.get(object);

    if (eobject != null) {
      // property namespace : Namespace[?];
      eobject.setNamespace((Namespace) getEquivalentEObject(object.getNamespace(), objectMap));

      // property namespaceExtension : NestedValue[?] { composes };
      eobject.setNamespaceExtension(
          (NestedValue) getEquivalentEObject(object.getNamespaceExtension(), objectMap));
    }

  }



  private static void passPropertiesOfNamespace(org.obo.datamodel.Namespace object,
      Map<Object, EObject> objectMap) {

    Namespace eobject = (Namespace) objectMap.get(object);

    if (eobject != null) {
      // id - string
      eobject.setId(object.getID());

      // path - string
      eobject.setPath(object.getPath());

      // privateID - integer
      eobject.setPrivateId(BigInteger.valueOf(object.getPrivateID()));

    }

  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfNestedValue(org.obo.datamodel.NestedValue object,
      Map<Object, EObject> objectMap) {

    NestedValue eobject = (NestedValue) objectMap.get(object);

    if (eobject != null) {
      // name - string
      eobject.setName(object.getName());

      // propertyValues
      eobject.getPropertyValues()
          .addAll((Collection<PropertyValue>) getEquivalentCollection(object.getPropertyValues(),
              objectMap));

      // suggestedcomment - string
      eobject.setSuggestedComment(object.getSuggestedComment());

    }

  }


  private static void passPropertiesOfObjectFactory(org.obo.datamodel.ObjectFactory object,
      Map<Object, EObject> objectMap) {

    ObjectFactory eobject = (ObjectFactory) objectMap.get(object);

    if (eobject != null) {
    }

  }


  private static void passPropertiesOfObjectField(org.obo.datamodel.ObjectField object,
      Map<Object, EObject> objectMap) {

    ObjectField eobject = (ObjectField) objectMap.get(object);

    if (eobject != null) {
      // property values : EObject[*];
      // eobject.getValues().addAll((Collection<EObject>)
      // getEquivalentCollection(object., objectMap));
    }

  }


  private static void passPropertiesOfOBOClass(org.obo.datamodel.OBOClass object,
      Map<Object, EObject> objectMap) {

    OBOClass eobject = (OBOClass) objectMap.get(object);

    if (eobject != null) {
    }

  }


  private static void passPropertiesOfOBOObject(org.obo.datamodel.OBOObject object,
      Map<Object, EObject> objectMap) {

    OBOObject eobject = (OBOObject) objectMap.get(object);

    if (eobject != null) {
    }

  }


  private static void passPropertiesOfOBOProperty(org.obo.datamodel.OBOProperty object,
      Map<Object, EObject> objectMap) {

    OBOProperty eobject = (OBOProperty) objectMap.get(object);

    if (eobject != null) {

      // property nonInheritable : Boolean;
      eobject.setNonInheritable(object.isNonInheritable());


      // property cyclic : Boolean;
      eobject.setCyclic(object.isCyclic());

      // property symmetric : Boolean;
      eobject.setSymmetric(object.isSymmetric());

      // property transitive : Boolean;
      eobject.setTransitive(object.isTransitive());

      // property reflexive : Boolean;
      eobject.setReflexive(object.isReflexive());

      // property alwaysImpliesInverse : Boolean;
      eobject.setAlwaysImpliesInverse(object.isAlwaysImpliesInverse());

      // property dummy : Boolean;
      eobject.setDummy(object.isDummy());

      // property metadataTag : Boolean;
      eobject.setMetadataTag(object.isMetadataTag());

      // property cyclicExtension : NestedValue[?] { composes };
      eobject.setCyclicExtension(
          (NestedValue) getEquivalentEObject(object.getCyclicExtension(), objectMap));

      // property symmetricExtension : NestedValue[?] { composes };
      eobject.setSymmetricExtension(
          (NestedValue) getEquivalentEObject(object.getSymmetricExtension(), objectMap));

      // property transitiveExtension : NestedValue[?] { composes };
      eobject.setTransitiveExtension(
          (NestedValue) getEquivalentEObject(object.getTransitiveExtension(), objectMap));

      // property reflexiveExtension : NestedValue[?] { composes };
      eobject.setReflexiveExtension(
          (NestedValue) getEquivalentEObject(object.getReflexiveExtension(), objectMap));

      // property alwaysImpliesInverseExtension : NestedValue[?] { composes };
      eobject.setAlwaysImpliesInverseExtension(
          (NestedValue) getEquivalentEObject(object.getAlwaysImpliesInverseExtension(), objectMap));

      // property domainExtension : NestedValue[?] { composes };
      eobject.setDomainExtension(
          (NestedValue) getEquivalentEObject(object.getDomainExtension(), objectMap));

      // property rangeExtension : NestedValue[?] { composes };
      eobject.setRangeExtension(
          (NestedValue) getEquivalentEObject(object.getRangeExtension(), objectMap));

      // property range : Type[?];
      eobject.setRange((Type) getEquivalentEObject(object.getRange(), objectMap));

      // property domain : Type[?];
      eobject.setDomain((Type) getEquivalentEObject(object.getDomain(), objectMap));

      // property disjointOver : OBOProperty [?];
      eobject
          .setDisjointOver((OBOProperty) getEquivalentEObject(object.getDisjointOver(), objectMap));

      // property transitiveOver : OBOProperty [?];
      eobject.setTransitiveOver(
          (OBOProperty) getEquivalentEObject(object.getTransitiveOver(), objectMap));

      // property holdsOverChains : ListOfProperties[*] { composes };
      if (object.getHoldsOverChains() != null) {
        for (List<org.obo.datamodel.OBOProperty> list : object.getHoldsOverChains()) {
          ListOfProperties eList = (ListOfProperties) getEquivalentEObject(list, objectMap);
          for (org.obo.datamodel.OBOProperty p : list) {
            eList.getProperties().add((OBOProperty) getEquivalentEObject(p, objectMap));
          }


        }
      }
    }

  }

  @SuppressWarnings("unchecked")
  private static void passPropertiesOfOBORestriction(org.obo.datamodel.OBORestriction object,
      Map<Object, EObject> objectMap) {

    OBORestriction eobject = (OBORestriction) objectMap.get(object);

    if (eobject != null) {
      // property cardinality : Integer;
      if (object.getCardinality() != null) {
        eobject.setCardinality(BigInteger.valueOf(object.getCardinality()));
      }
      // property maxCardinality : Integer;
      if (object.getMaxCardinality() != null) {
        eobject.setMaxCardinality(BigInteger.valueOf(object.getMaxCardinality()));
      }

      // property minCardinality : Integer;
      if (object.getMinCardinality() != null) {
        eobject.setMinCardinality(BigInteger.valueOf(object.getMinCardinality()));
      }

      // property completes : Boolean;
      eobject.setCompletes(object.getCompletes());

      // property inverseCompletes : Boolean;
      eobject.setInverseCompletes(object.inverseCompletes());

      // property necessarilyTrue : Boolean;
      eobject.setNecessarilyTrue(object.isNecessarilyTrue());

      // property inverseNecessarilyTrue : Boolean;
      eobject.setInverseNecessarilyTrue(object.isInverseNecessarilyTrue());

      // property additionalArguments : LinkedObject[*] { !unique };
      if (object.getAdditionalArguments() != null) {
        eobject.getAdditionalArguments().addAll((Collection<LinkedObject>) getEquivalentCollection(
            object.getAdditionalArguments(), objectMap));
      }
    }

  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfOBOSession(org.obo.datamodel.OBOSession object,
      Map<Object, EObject> objectMap) {

    OBOSession eobject = (OBOSession) objectMap.get(object);

    if (eobject != null) {

      // pass identified objects
      eobject.getObjects().addAll(
          (Collection<IdentifiedObject>) getEquivalentCollection(object.getObjects(), objectMap));

      // pass LinkDataBase
      eobject.setLinkDatabase(
          (LinkDatabase) getEquivalentEObject(object.getLinkDatabase(), objectMap));

      // defaultNamespace
      eobject.setDefaultNamespace(
          (Namespace) getEquivalentEObject(object.getDefaultNamespace(), objectMap));

      // namespaces
      eobject.getNamespaces().addAll(
          (Collection<Namespace>) getEquivalentCollection(object.getNamespaces(), objectMap));

      // currentFilenames - string
      if(object.getCurrentFilenames() != null) {
        eobject.getCurrentFilenames().addAll(object.getCurrentFilenames());
      }
      // propertyValues
      eobject.getPropertyValues()
          .addAll((Collection<PropertyValue>) getEquivalentCollection(object.getPropertyValues(),
              objectMap));

      // unknownStanzas
      eobject.getUnknownStanzas()
          .addAll((Collection<UnknownStanza>) getEquivalentCollection(object.getUnknownStanzas(),
              objectMap));


      // subsets
      eobject.getSubsets()
          .addAll((Collection<TermSubset>) getEquivalentCollection(object.getSubsets(), objectMap));

      // categories
      // eobject.getCategories().addAll( (Collection<TermSubset>)
      // getEquivalentCollection(object.getCategory, objectMap));


      // synonymTypes
      eobject.getSynonymTypes().addAll(
          (Collection<SynonymType>) getEquivalentCollection(object.getSynonymTypes(), objectMap));

      // synonymCategories
      // eobject.getSynonymCategories().addAll( (Collection<SynonymType>)
      // getEquivalentCollection(object.getSynony, objectMap));

      // currentUser
      eobject.setCurrentUser(object.getCurrentUser());

      // idSpaces - string => string
      for (String idspace : object.getIDSpaces()) {
        String expanded = object.expandIDSpace(idspace);
        eobject.getIdSpaces().put(idspace, expanded);
      }

      // loadRemark - string
      eobject.setLoadRemark(object.getLoadRemark());


    }



  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfObsoletableObject(org.obo.datamodel.ObsoletableObject object,
      Map<Object, EObject> objectMap) {

    ObsoletableObject eobject = (ObsoletableObject) objectMap.get(object);

    if (eobject != null) {
      // property obsolete : Boolean;
      eobject.setObsolete(object.isObsolete());

      // property replacedBy : ObsoletableObject[*] { unique };
      eobject.getReplacedBy()
          .addAll((Collection<ObsoletableObject>) getEquivalentCollection(object.getReplacedBy(),
              objectMap));

      // property considerReplacements : ObsoletableObject[*] { unique };
      eobject.getConsiderReplacements().addAll(
          (Collection<ObsoletableObject>) getEquivalentCollection(object.getConsiderReplacements(),
              objectMap));

      // property considerExtension : ObsoletableObjectToNestedValueMap[*] { composes };
      for (org.obo.datamodel.ObsoletableObject o : object.getConsiderReplacements()) {
        org.obo.datamodel.NestedValue oNestedValue = object.getConsiderExtension(o);
        if (oNestedValue != null) {
          eobject.getConsiderExtension().put((ObsoletableObject) getEquivalentEObject(o, objectMap),
              (NestedValue) getEquivalentEObject(oNestedValue, objectMap));
        }
      }

      // property replacedByExtension : ObsoletableObjectToNestedValueMap[*] { composes };
      for (org.obo.datamodel.ObsoletableObject o : object.getReplacedBy()) {
        org.obo.datamodel.NestedValue oNestedValue = object.getReplacedByExtension(o);
        if (oNestedValue != null) {
          eobject.getReplacedByExtension().put(
              (ObsoletableObject) getEquivalentEObject(o, objectMap),
              (NestedValue) getEquivalentEObject(oNestedValue, objectMap));
        }
      }

      // property obsoleteExtension : NestedValue[?] { composes };
      eobject.setObsoleteExtension(
          (NestedValue) getEquivalentEObject(object.getObsoleteExtension(), objectMap));


    }

  }


  private static void passPropertiesOfPathCapable(org.obo.datamodel.PathCapable object,
      Map<Object, EObject> objectMap) {

    PathCapable eobject = (PathCapable) objectMap.get(object);

    if (eobject != null) {
    }

  }


  private static void passPropertiesOfPropertyValue(org.obo.datamodel.PropertyValue object,
      Map<Object, EObject> objectMap) {

    PropertyValue eobject = (PropertyValue) objectMap.get(object);

    if (eobject != null) {
      // property - string
      eobject.setProperty(object.getProperty());

      // value - string
      eobject.setValue(object.getValue());

      // lineNumber - integer
      eobject.setLineNumber(BigInteger.valueOf(object.getLineNumber()));

      // filename - String
      eobject.setFilename(object.getFilename());

      // line
      eobject.setLine(object.getLine());
    }

  }


  private static void passPropertiesOfRelationship(org.obo.datamodel.Relationship object,
      Map<Object, EObject> objectMap) {

    Relationship eobject = (Relationship) objectMap.get(object);

    if (eobject != null) {
      // type
      eobject.setType((OBOProperty) getEquivalentEObject(object.getType(), objectMap));

      // nestedValue
      eobject
          .setNestedValue((NestedValue) getEquivalentEObject(object.getNestedValue(), objectMap));
    }

  }



  private static void passPropertiesOfRootAlgorithm(org.obo.datamodel.RootAlgorithm object,
      Map<Object, EObject> objectMap) {

    RootAlgorithm eobject = (RootAlgorithm) objectMap.get(object);

    if (eobject != null) {

    }

  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfSubsetObject(org.obo.datamodel.SubsetObject object,
      Map<Object, EObject> objectMap) {

    SubsetObject eobject = (SubsetObject) objectMap.get(object);

    if (eobject != null) {
      // property subsets : TermSubset[*];
      eobject.getSubsets()
          .addAll((Collection<TermSubset>) getEquivalentCollection(object.getSubsets(), objectMap));

      // property categories : TermSubset[*];
      // eobject.getCategories().addAll((Collection<TermSubset>)
      // getEquivalentCollection(object., objectMap));

      // property categoryExtensions : TermSubsetToNestedValueMap[*] { composes };
      Set<org.obo.datamodel.TermSubset> subsets = object.getSubsets();
      if (subsets == null) {
        subsets = Collections.emptySet();
      }
      for (org.obo.datamodel.TermSubset o : subsets) {
        org.obo.datamodel.NestedValue oNestedValue = object.getCategoryExtension(o);
        if (oNestedValue != null) {
          eobject.getCategoryExtensions().put((TermSubset) getEquivalentEObject(o, objectMap),
              (NestedValue) getEquivalentEObject(oNestedValue, objectMap));
        }
      }
    }

  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfSynonymedObject(org.obo.datamodel.SynonymedObject object,
      Map<Object, EObject> objectMap) {

    SynonymedObject eobject = (SynonymedObject) objectMap.get(object);

    if (eobject != null) {
      // property synonyms : Synonym[*] { composes };
      eobject.getSynonyms()
          .addAll((Collection<Synonym>) getEquivalentCollection(object.getSynonyms(), objectMap));
    }

  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfSynonym(org.obo.datamodel.Synonym object,
      Map<Object, EObject> objectMap) {

    Synonym eobject = (Synonym) objectMap.get(object);

    if (eobject != null) {
      // property synonymType : SynonymType[?] { composes };
      eobject
          .setSynonymType((SynonymType) getEquivalentEObject(object.getSynonymType(), objectMap));

      // property nestedValue : NestedValue[?] { composes };
      eobject
          .setNestedValue((NestedValue) getEquivalentEObject(object.getNestedValue(), objectMap));

      // property scope : Integer;
      eobject.setScope(BigInteger.valueOf(object.getScope()));

      // property xrefs : Dbxref[?] { composes };
      eobject.getXrefs()
          .addAll((Collection<Dbxref>) getEquivalentCollection(object.getXrefs(), objectMap));

      // property text : String;
      eobject.setText(object.getText());
    }

  }


  private static void passPropertiesOfSynonymType(org.obo.datamodel.SynonymType object,
      Map<Object, EObject> objectMap) {

    SynonymType eobject = (SynonymType) objectMap.get(object);

    if (eobject != null) {

      // attribute id : String;
      eobject.setId(object.getID());

      // attribute name : String;
      eobject.setName(object.getName());

      // attribute scope : Integer;
      eobject.setScope(BigInteger.valueOf(object.getScope()));
    }

  }


  private static void passPropertiesOfTermSubset(org.obo.datamodel.TermSubset object,
      Map<Object, EObject> objectMap) {

    TermSubset eobject = (TermSubset) objectMap.get(object);

    if (eobject != null) {
      // property name : String;
      eobject.setName(object.getName());
      // property desc : String;
      eobject.setDesc(object.getDesc());
    }

  }


  @SuppressWarnings("rawtypes")
  private static void passPropertiesOfType(org.obo.datamodel.Type object,
      Map<Object, EObject> objectMap) {

    Type eobject = (Type) objectMap.get(object);

    if (eobject != null) {
    }

  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfUnknownStanza(org.obo.datamodel.UnknownStanza object,
      Map<Object, EObject> objectMap) {

    UnknownStanza eobject = (UnknownStanza) objectMap.get(object);

    if (eobject != null) {

      // namespace
      eobject.setNamespace((Namespace) getEquivalentEObject(object.getNamespace(), objectMap));

      // stanza - string
      eobject.setStanza(object.getStanza());

      // propertyValues
      eobject.getPropertyValues()
          .addAll((Collection<PropertyValue>) getEquivalentCollection(object.getPropertyValues(),
              objectMap));

      // nestedValues
      // precisa pensar nesse

    }

  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfValueDatabase(org.obo.datamodel.ValueDatabase object,
      Map<Object, EObject> objectMap) {

    ValueDatabase eobject = (ValueDatabase) objectMap.get(object);

    if (eobject != null) {
      // public Collection<IdentifiedObject> getObjects();
      eobject.getObjects().addAll(
          (Collection<IdentifiedObject>) getEquivalentCollection(object.getObjects(), objectMap));
    }

  }


  @SuppressWarnings("rawtypes")
  private static void passPropertiesOfValue(org.obo.datamodel.Value object,
      Map<Object, EObject> objectMap) {

    Value eobject = (Value) objectMap.get(object);

    if (eobject != null) {
      // property type : Type[?];
      //eobject.setType((Type) getEquivalentEObject(object.getType(), objectMap));
    }

  }


  private static void passPropertiesOfValueLink(org.obo.datamodel.ValueLink object,
      Map<Object, EObject> objectMap) {

    ValueLink eobject = (ValueLink) objectMap.get(object);

    if (eobject != null) {
      // property value : Value[?] { composes };
      eobject.setValue((Value) getEquivalentEObject(object.getValue(), objectMap));
    }

  }


  protected static void fixDbxrefs(org.obo.datamodel.OBOSession session) {
    for (org.obo.datamodel.IdentifiedObject io : session.getObjects()) {
      if (io instanceof org.obo.datamodel.DefinedObject) {
        org.obo.datamodel.DefinedObject dfo = (org.obo.datamodel.DefinedObject) io;
        org.obo.datamodel.Dbxref metacycRef = null;
        org.obo.datamodel.Dbxref brokenRef = null;
        org.obo.datamodel.Dbxref otherRef = null;
        int metacycCount = 0;
        int brokenCount = 0;

        for (org.obo.datamodel.Dbxref ref : dfo.getDefDbxrefs()) {
          if (ref.getDatabase().equalsIgnoreCase("metacyc")) {
            metacycCount++;
            metacycRef = ref;
          } else if (ref.getDatabase().length() == 0) {
            brokenCount++;
            brokenRef = ref;
          } else {
            otherRef = ref;
          }
        }
        if (brokenRef != null && metacycRef != null) {
          if (brokenCount > 1 || metacycCount > 1) {
            System.err.println("*!!!!! Probable broken ref at " + io.getID()
                + " cannot be automatically repaired. There are too many pieces.");
            continue;
          }
          dfo.removeDefDbxref(metacycRef);
          dfo.removeDefDbxref(brokenRef);
          System.out.println("* Repairing broken dbxref at " + dfo.getID() + ", merging dbxrefs "
              + metacycRef + " and " + brokenRef);
          metacycRef.setDatabaseID(metacycRef.getDatabaseID() + "," + brokenRef.getDatabaseID());
          dfo.addDefDbxref(metacycRef);
        } else if (dfo.getDefDbxrefs().size() == 2 && brokenRef != null && otherRef != null) {
          dfo.removeDefDbxref(otherRef);
          dfo.removeDefDbxref(brokenRef);
          System.out.println("* Repairing broken dbxref at " + dfo.getID() + ", merging dbxrefs "
              + otherRef + " and " + brokenRef);
          otherRef.setDatabaseID(otherRef.getDatabaseID() + "," + brokenRef.getDatabaseID());
          dfo.addDefDbxref(otherRef);
        } else if (brokenRef != null) {
          System.out.println("*!! Possible broken ref at " + dfo.getID()
              + " could not be automatically repaired.");
        }
      }
    }
  }



}
