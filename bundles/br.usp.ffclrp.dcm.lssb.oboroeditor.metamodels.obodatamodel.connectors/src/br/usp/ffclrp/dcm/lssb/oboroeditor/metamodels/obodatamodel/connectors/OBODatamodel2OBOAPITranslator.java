package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.AnnotatedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.CommentedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingProperty;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Datatype;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DatatypeValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Dbxref;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DbxrefedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPath;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.FieldPathSpec;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObjectIndex;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Impliable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Instance;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Link;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkLinkedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ListOfProperties;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ModificationMetadataObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MultiIDObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MutableLinkDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Namespace;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NamespacedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOClass;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOProperty;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectFactory;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObjectField;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OboPropertyValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PathCapable;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.PropertyValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.RootAlgorithm;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SubsetObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Synonym;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymType;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.SynonymedObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Type;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.UnknownStanza;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Value;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueDatabase;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ValueLink;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;


/**
 * This class implements operations that takes an ODM model and serializes it as an OBOFFF ontology
 * (extractor operations).
 * 
 * @author cawal
 *
 */
public class OBODatamodel2OBOAPITranslator {

  final static int WRONG_ARGUMENTS = 1;

  private OBOSession source = null;
  private org.obo.datamodel.OBOSession target = null;
  private Map<EObject,Object> objectMap;
  
  public OBODatamodel2OBOAPITranslator(OBOSession source) {
    if(source != null)
      this.source = source;
    else 
      throw new Error("Source session in null!");
    
    objectMap = new HashMap<EObject, Object>();
  }
  
  public org.obo.datamodel.OBOSession translate(){
    
    objectMap.clear();
    
    target = (org.obo.datamodel.OBOSession) getEquivalentObject(source, objectMap);

    mapBuiltInObjects(source, target, objectMap);

    // cria e mapeia cada objeto da session
    // faz todas as ligações entre os objetos e passagem de atributos.
    linkObjects(objectMap);
    
    
    return target;
  }
  
  public static org.obo.datamodel.OBOSession translate(OBOSession source) {
    OBODatamodel2OBOAPITranslator translator 
        = new OBODatamodel2OBOAPITranslator(source);
    return translator.translate();
  }

  // ==========================================================================




  private static Object getEquivalentObject(EObject eObject, Map<EObject, Object> objectMap) {
    Object o = null;
    if (!objectMap.containsKey(eObject)) {
      o = createObjectOfEquivalentClass(eObject, objectMap);
      objectMap.put(eObject, o);
    } else {
      o = objectMap.get(eObject);
    }
    return o;

  }



  @SuppressWarnings({"rawtypes", "unchecked"})
  private static Collection getEquivalentCollection(Collection c, Map<EObject, Object> objectMap) {

    Collection out = new ArrayList();
    for (EObject o : (Collection<EObject>) c) {
      out.add(getEquivalentObject(o, objectMap));
    }

    return out;
  }



  private static void mapBuiltInObjects(
      OBOSession eSession,
      org.obo.datamodel.OBOSession session, Map<EObject, Object> objectMap) {
    for (IdentifiedObject eo : eSession
        .getObjects()) {
      if (eo.isBuiltIn()) {
        org.obo.datamodel.IdentifiedObject o = session.getObject(eo.getId());
        objectMap.put(eo, o);
      }
    }
  }


  /**
   * Se encontrar uma classe que não foi contemplada, coloque a criação dela aqui
   * 
   * @param object
   * @return
   */
  private static Object createObjectOfEquivalentClass(EObject eObject,
      Map<EObject, Object> objectMap) {
    Object object = null;
    org.obo.datamodel.ObjectFactory factory =
        org.obo.datamodel.impl.DefaultObjectFactory.getFactory();



    if (eObject instanceof OBOSession) {
      object = factory.createSession();

    } else if (eObject instanceof OBOClass) {
      OBOClass clazz =
          (OBOClass) eObject;
      object = factory.createObject(clazz.getId(), org.obo.datamodel.OBOClass.OBO_CLASS, clazz.isAnonymous());

    } else if (eObject instanceof OBOProperty) {
      OBOProperty property =
          (OBOProperty) eObject;
      object =
          factory.createObject(property.getId(), org.obo.datamodel.OBOClass.OBO_PROPERTY, property.isAnonymous());

    } else if (eObject instanceof Instance) {
      Instance instance =
          (Instance) eObject;
      object =
          factory.createObject(instance.getId(), org.obo.datamodel.OBOClass.OBO_INSTANCE, instance.isAnonymous());

    } else if (eObject instanceof DanglingObject) {
      DanglingObject danglingObject =
          (DanglingObject) eObject;
      object = factory.createDanglingObject(danglingObject.getId(), false);


    } else if (eObject instanceof DanglingProperty) {
      DanglingObject danglingObject =
          (DanglingObject) eObject;
      object = factory.createDanglingObject(danglingObject.getId(), true);


    } else if (eObject instanceof OBORestriction) {
      OBORestriction restriction =
          (OBORestriction) eObject;
      org.obo.datamodel.LinkedObject child =
          (org.obo.datamodel.LinkedObject) getEquivalentObject(restriction.getChild(), objectMap);
      org.obo.datamodel.LinkedObject parent =
          (org.obo.datamodel.LinkedObject) getEquivalentObject(restriction.getParent(), objectMap);
      org.obo.datamodel.OBOProperty property =
          (org.obo.datamodel.OBOProperty) getEquivalentObject(restriction.getType(), objectMap);
      boolean implied = restriction.isImplied();
      org.obo.datamodel.OBORestriction r =
          factory.createOBORestriction(child, property, parent, implied);
      r.setCompletes(restriction.isCompletes());
      object = r;


    } else if (eObject instanceof SynonymType) {
      SynonymType synonym =
          (SynonymType) eObject;
      String id = synonym.getId();
      String name = synonym.getName();
      int scope = synonym.getScope().intValue();
      object = factory.createSynonymType(id, name, scope);


    } else if (eObject instanceof TermSubset) {
      TermSubset subset =
          (TermSubset) eObject;
      String name = subset.getName();
      String desc = subset.getDesc();
      object = factory.createSubset(name, desc);


    } else if (eObject instanceof Namespace) {
      Namespace namespace =
          (Namespace) eObject;
      String id = namespace.getId();
      String path = namespace.getPath();
      object = factory.createNamespace(id, path);


    } else if (eObject instanceof Synonym) {
      Synonym synonym =
          (Synonym) eObject;
      String text = synonym.getText();
      int scope = synonym.getScope().intValue();
      object = factory.createSynonym(text, scope);

    } else if (eObject instanceof Dbxref) {
      Dbxref dbxref =
          (Dbxref) eObject;
      String database = dbxref.getDatabase();
      String databaseId = dbxref.getDatabaseID();
      String desc = dbxref.getDesc();
      int type = dbxref.getType().intValue();
      org.obo.datamodel.Synonym targetSynonym =
          (org.obo.datamodel.Synonym) getEquivalentObject(dbxref.getSynonym(), objectMap);
      object = factory.createDbxref(database, databaseId, desc, type, targetSynonym);

    } else if (eObject instanceof NestedValue) {
      object = factory.createNestedValue();

    } else if (eObject instanceof PropertyValue) {
      PropertyValue propertyValue =
          (PropertyValue) eObject;
      String property = propertyValue.getProperty();
      String value = propertyValue.getValue();
      object = factory.createPropertyValue(property, value);

    }

    return object;
  }


  private static void linkObjects(Map<EObject, Object> objectMap) {

    for (int i = 0; i < objectMap.size(); i++) {// -----------------------
      Object[] array = objectMap.keySet().toArray();
      Object eo = array[i];



      if (eo instanceof AnnotatedObject) {
        passPropertiesOfAnnotatedObject(
            (AnnotatedObject) eo,
            objectMap);
      }

      if (eo instanceof CommentedObject) {
        passPropertiesOfCommentedObject(
            (CommentedObject) eo,
            objectMap);
      }

      if (eo instanceof DanglingObject) {
        passPropertiesOfDanglingObject(
            (DanglingObject) eo,
            objectMap);
      }

      if (eo instanceof DanglingProperty) {
        passPropertiesOfDanglingProperty(
            (DanglingProperty) eo,
            objectMap);
      }

      if (eo instanceof Datatype) {
        passPropertiesOfDatatype(
            (Datatype) eo, objectMap);
      }

      if (eo instanceof DatatypeValue) {
        passPropertiesOfDatatypeValue(
            (DatatypeValue) eo,
            objectMap);
      }

      if (eo instanceof DbxrefedObject) {
        passPropertiesOfDbxrefedObject(
            (DbxrefedObject) eo,
            objectMap);
      }

      if (eo instanceof Dbxref) {
        passPropertiesOfDbxref(
            (Dbxref) eo, objectMap);
      }

      if (eo instanceof DefinedObject) {
        passPropertiesOfDefinedObject(
            (DefinedObject) eo,
            objectMap);
      }

      if (eo instanceof FieldPath) {
        passPropertiesOfFieldPath(
            (FieldPath) eo, objectMap);
      }

      if (eo instanceof FieldPathSpec) {
        passPropertiesOfFieldPathSpec(
            (FieldPathSpec) eo,
            objectMap);
      }

      if (eo instanceof IdentifiableObject) {
        passPropertiesOfIdentifiableObject(
            (IdentifiableObject) eo,
            objectMap);
      }

      if (eo instanceof IdentifiedObjectIndex) {
        passPropertiesOfIdentifiedObjectIndex(
            (IdentifiedObjectIndex) eo,
            objectMap);
      }

      if (eo instanceof IdentifiedObject) {
        passPropertiesOfIdentifiedObject(
            (IdentifiedObject) eo,
            objectMap);
      }

      if (eo instanceof Impliable) {
        passPropertiesOfImpliable(
            (Impliable) eo, objectMap);
      }

      if (eo instanceof Instance) {
        passPropertiesOfInstance(
            (Instance) eo, objectMap);
      }

      if (eo instanceof LinkDatabase) {
        passPropertiesOfLinkDatabase(
            (LinkDatabase) eo,
            objectMap);
      }

      if (eo instanceof LinkedObject) {
        passPropertiesOfLinkedObject(
            (LinkedObject) eo,
            objectMap);
      }

      if (eo instanceof Link) {
        passPropertiesOfLink((Link) eo,
            objectMap);
      }

      if (eo instanceof LinkLinkedObject) {
        passPropertiesOfLinkLinkedObject(
            (LinkLinkedObject) eo,
            objectMap);
      }

      if (eo instanceof ModificationMetadataObject) {
        passPropertiesOfModificationMetadataObject(
            (ModificationMetadataObject) eo,
            objectMap);
      }

      if (eo instanceof MultiIDObject) {
        passPropertiesOfMultiIDObject(
            (MultiIDObject) eo,
            objectMap);
      }

      if (eo instanceof MutableLinkDatabase) {
        passPropertiesOfMutableLinkDatabase(
            (MutableLinkDatabase) eo,
            objectMap);
      }

      if (eo instanceof NamespacedObject) {
        passPropertiesOfNamespacedObject(
            (NamespacedObject) eo,
            objectMap);
      }

      if (eo instanceof Namespace) {
        passPropertiesOfNamespace(
            (Namespace) eo, objectMap);
      }

      if (eo instanceof NestedValue) {
        passPropertiesOfNestedValue(
            (NestedValue) eo, objectMap);
      }

      if (eo instanceof ObjectFactory) {
        passPropertiesOfObjectFactory(
            (ObjectFactory) eo,
            objectMap);
      }

      if (eo instanceof ObjectField) {
        passPropertiesOfObjectField(
            (ObjectField) eo, objectMap);
      }

      if (eo instanceof OBOClass) {
        passPropertiesOfOBOClass(
            (OBOClass) eo, objectMap);
      }

      if (eo instanceof OBOObject) {
        passPropertiesOfOBOObject(
            (OBOObject) eo, objectMap);
      }

      if (eo instanceof OBOProperty) {
        passPropertiesOfOBOProperty(
            (OBOProperty) eo, objectMap);
      }

      if (eo instanceof OBORestriction) {
        passPropertiesOfOBORestriction(
            (OBORestriction) eo,
            objectMap);
      }

      if (eo instanceof OBOSession) {
        passPropertiesOfOBOSession(
            (OBOSession) eo, objectMap);
      }

      if (eo instanceof ObsoletableObject) {
        passPropertiesOfObsoletableObject(
            (ObsoletableObject) eo,
            objectMap);
      }

      if (eo instanceof PathCapable) {
        passPropertiesOfPathCapable(
            (PathCapable) eo, objectMap);
      }

      if (eo instanceof PropertyValue) {
        passPropertiesOfPropertyValue(
            (PropertyValue) eo,
            objectMap);
      }

      if (eo instanceof Relationship) {
        passPropertiesOfRelationship(
            (Relationship) eo,
            objectMap);
      }

      if (eo instanceof RootAlgorithm) {
        passPropertiesOfRootAlgorithm(
            (RootAlgorithm) eo,
            objectMap);
      }

      if (eo instanceof SubsetObject) {
        passPropertiesOfSubsetObject(
            (SubsetObject) eo,
            objectMap);
      }

      if (eo instanceof SynonymedObject) {
        passPropertiesOfSynonymedObject(
            (SynonymedObject) eo,
            objectMap);
      }

      if (eo instanceof Synonym) {
        passPropertiesOfSynonym(
            (Synonym) eo, objectMap);
      }

      if (eo instanceof SynonymType) {
        passPropertiesOfSynonymType(
            (SynonymType) eo, objectMap);
      }

      if (eo instanceof TermSubset) {
        passPropertiesOfTermSubset(
            (TermSubset) eo, objectMap);
      }

      if (eo instanceof Type) {
        passPropertiesOfType((Type) eo,
            objectMap);
      }

      if (eo instanceof UnknownStanza) {
        passPropertiesOfUnknownStanza(
            (UnknownStanza) eo,
            objectMap);
      }

      if (eo instanceof ValueDatabase) {
        passPropertiesOfValueDatabase(
            (ValueDatabase) eo,
            objectMap);
      }

      if (eo instanceof Value) {
        passPropertiesOfValue((Value) eo,
            objectMap);
      }

      if (eo instanceof ValueLink) {
        passPropertiesOfValueLink(
            (ValueLink) eo, objectMap);
      }


    } // end for over objectMap ---------------------------------------
  }



  /*
   * ************************************************************************ Methods related with
   * the transformation of the attributtes / links
   **************************************************************************/

  private static void passPropertiesOfAnnotatedObject(
      AnnotatedObject eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.AnnotatedObject object =
        (org.obo.datamodel.AnnotatedObject) objectMap.get(eobject);

    if (object != null) {
    }

  }



  private static void passPropertiesOfCommentedObject(
      CommentedObject eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.CommentedObject object =
        (org.obo.datamodel.CommentedObject) objectMap.get(eobject);
    if (object != null) {
      // comment - string
      object.setComment(eobject.getComment());

      // commentExtension
      object.setCommentExtension((org.obo.datamodel.NestedValue) getEquivalentObject(
          eobject.getCommentExtension(), objectMap));
    }

  }


  private static void passPropertiesOfDanglingObject(
      DanglingObject eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.DanglingObject object =
        (org.obo.datamodel.DanglingObject) objectMap.get(eobject);

    if (object != null) {
      // dangling - boolean - NO CONSTRUTOR
    }

  }


  private static void passPropertiesOfDanglingProperty(
      DanglingProperty eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.DanglingProperty object =
        (org.obo.datamodel.DanglingProperty) objectMap.get(eobject);

    if (object != null) {
    }

  }


  @SuppressWarnings("rawtypes")
  private static void passPropertiesOfDatatype(
      Datatype eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.Datatype object = (org.obo.datamodel.Datatype) objectMap.get(eobject);
    if (object != null) {
      // abstract - boolean
      // object.setAbstract(eobject.isAbstract());

    }

  }


  @SuppressWarnings("rawtypes")
  private static void passPropertiesOfDatatypeValue(
      DatatypeValue eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.DatatypeValue object =
        (org.obo.datamodel.DatatypeValue) objectMap.get(eobject);

    if (object != null) {
      // value - string
      // eobject.setValue(object.getValue());
    }

  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfDbxrefedObject(
      DbxrefedObject eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.DbxrefedObject object =
        (org.obo.datamodel.DbxrefedObject) objectMap.get(eobject);

    if (object != null) {
      // dbxref
      object.getDbxrefs().addAll((Collection<org.obo.datamodel.Dbxref>) getEquivalentCollection(
          eobject.getDbxrefs(), objectMap));
    }

  }


  private static void passPropertiesOfDbxref(
      Dbxref eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.Dbxref object = (org.obo.datamodel.Dbxref) objectMap.get(eobject);
    if (object != null) {
      // property nestedValue : NestedValue[?] { composes };
      object.setNestedValue(
          (org.obo.datamodel.NestedValue) getEquivalentObject(eobject.getNestedValue(), objectMap));

      // property defRef : Boolean;
      // object.setDefRef(eobject.isDefRef());

      // property desc : String ='';
      object.setDesc(eobject.getDesc());


      // property type : Type[?];
      // object.setType(((org.obo.datamodel.Type)
      // getEquivalentObject(eobject.getType(), objectMap)));

      // property synonym : Synonym[?];
      object.setSynonym(
          (org.obo.datamodel.Synonym) getEquivalentObject(eobject.getSynonym(), objectMap));

      // property databaseID : String = '';
      object.setDatabaseID(eobject.getDatabaseID());

      // property database : String = '';
      object.setDatabase(eobject.getDatabase());

    }

  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfDefinedObject(
      DefinedObject eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.DefinedObject object =
        (org.obo.datamodel.DefinedObject) objectMap.get(eobject);

    if (object != null) {

      // property _'definition' : String[?];
      object.setDefinition(eobject.getDefinition());

      // property defDbxrefs : Dbxref[*] { composes };
      object.getDefDbxrefs().addAll((Collection<org.obo.datamodel.Dbxref>) getEquivalentCollection(
          eobject.getDefDbxrefs(), objectMap));

      // property definitionExtension : NestedValue[?] {composes};
      object.setDefinitionExtension((org.obo.datamodel.NestedValue) getEquivalentObject(
          eobject.getDefinitionExtension(), objectMap));
    }

  }


  private static void passPropertiesOfFieldPath(
      FieldPath eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.FieldPath object = (org.obo.datamodel.FieldPath) objectMap.get(eobject);

    if (object != null) {
    }

  }


  private static void passPropertiesOfFieldPathSpec(
      FieldPathSpec eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.FieldPathSpec object =
        (org.obo.datamodel.FieldPathSpec) objectMap.get(eobject);

    if (object != null) {
    }

  }



  private static void passPropertiesOfIdentifiableObject(
      IdentifiableObject eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.IdentifiableObject object =
        (org.obo.datamodel.IdentifiableObject) objectMap.get(eobject);

    if (object != null) {
      // id - string - CONSTRUTOR
      // anonymous - boolean - CONSTRUTOR
    }

  }



  private static void passPropertiesOfIdentifiedObjectIndex(
      IdentifiedObjectIndex eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.IdentifiedObjectIndex object =
        (org.obo.datamodel.IdentifiedObjectIndex) objectMap.get(eobject);

    if (object != null) {
    }

  }


  private static void passPropertiesOfIdentifiedObject(
      IdentifiedObject eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.IdentifiedObject object =
        (org.obo.datamodel.IdentifiedObject) objectMap.get(eobject);

    if (object != null) {

      // property typeExtension : NestedValue[?] { composes };
      object.setTypeExtension((org.obo.datamodel.NestedValue) getEquivalentObject(
          eobject.getTypeExtension(), objectMap));

      // property builtIn : Boolean; // NO CONSTRUTOR, não cria builtin
      // object.setBuiltIn(eobject.isBuiltIn());

      // property name : String;
      object.setName(eobject.getName());

      // property nameExtension : NestedValue[?] { composes };
      object.setNameExtension((org.obo.datamodel.NestedValue) getEquivalentObject(
          eobject.getNameExtension(), objectMap));

      // property idExtension : NestedValue[?] { composes };
      object.setIDExtension(
          (org.obo.datamodel.NestedValue) getEquivalentObject(eobject.getIdExtension(), objectMap));

      // property anonymousExtension : NestedValue[?] { composes };
      object.setAnonymousExtension((org.obo.datamodel.NestedValue) getEquivalentObject(
          eobject.getAnonymousExtension(), objectMap));

      // property propertyValues : PropertyValue[*] { composes };
      for (PropertyValue epv : eobject
          .getPropertyValues()) {
        object.addPropertyValue(
            (org.obo.datamodel.PropertyValue) getEquivalentObject(epv, objectMap));
      }



    }

  }


  private static void passPropertiesOfImpliable(
      Impliable eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.Impliable object = (org.obo.datamodel.Impliable) objectMap.get(eobject);
    if (object != null) {
      // property implied : Boolean; NÃO TEM SET?
      // object.setImplied(object.isImplied());
    }

  }


  @SuppressWarnings("rawtypes")
  private static void passPropertiesOfInstance(
      Instance eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.Instance object = (org.obo.datamodel.Instance) objectMap.get(eobject);

    if (object != null) {
      obodatamodelPackage.eINSTANCE
          .eClass();

      // property oboPropertiesValues : OboPropertyValue[*] {composes};
      for (OboPropertyValue pv : eobject
          .getOboPropertiesValues()) {
        object.addPropertyValue(
            (org.obo.datamodel.OBOProperty) getEquivalentObject(pv.getProperty(), objectMap),
            (org.obo.datamodel.Value) getEquivalentObject(pv.getValue(), objectMap));
      }

      object
          .setType((org.obo.datamodel.OBOClass) getEquivalentObject(eobject.getType(), objectMap));

    }

  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfLinkDatabase(
      LinkDatabase eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.LinkDatabase object = (org.obo.datamodel.LinkDatabase) objectMap.get(eobject);

    if (object != null) {
      // property objects : IdentifiedObject[*];
      object.getObjects()
          .addAll((Collection<org.obo.datamodel.IdentifiedObject>) getEquivalentCollection(
              eobject.getObjects(), objectMap));

      // property session#linkDatabase : OBOSession[?];
      // object.setSession((org.obo.datamodel.OBOSession)
      // getEquivalentObject(object.getSession(), objectMap));

      // property objects : IdentifiedObject[*] { unique };
      object.getObjects()
          .addAll((Collection<org.obo.datamodel.IdentifiedObject>) getEquivalentCollection(
              eobject.getObjects(), objectMap));

      // property properties : OBOProperty[*] { unique composes};
      object.getProperties()
          .addAll((Collection<org.obo.datamodel.OBOProperty>) getEquivalentCollection(
              eobject.getProperties(), objectMap));
    }

  }


  private static void passPropertiesOfLinkedObject(
      LinkedObject eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.LinkedObject object = (org.obo.datamodel.LinkedObject) objectMap.get(eobject);
    if (object != null) {
      // pass childrens
      for (Relationship elink : eobject
          .getChildren()) {
        object.addChild((org.obo.datamodel.Link) getEquivalentObject(elink, objectMap));
      }

      // pass parents
      for (Relationship elink : eobject
          .getParents()) {
        object.addParent((org.obo.datamodel.Link) getEquivalentObject(elink, objectMap));
      }
    }
  }


  private static void passPropertiesOfLink(
      Link eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.Link object = (org.obo.datamodel.Link) objectMap.get(eobject);

    if (object != null) {
      // property namespace : Namespace[?];
      object.setNamespace(
          (org.obo.datamodel.Namespace) getEquivalentObject(eobject.getNamespace(), objectMap));
    }

  }


  private static void passPropertiesOfLinkLinkedObject(
      LinkLinkedObject eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.LinkLinkedObject object =
        (org.obo.datamodel.LinkLinkedObject) objectMap.get(eobject);

    if (object != null) {
      // property link : Link[?];
      object.setLink((org.obo.datamodel.Link) getEquivalentObject(eobject.getLink(), objectMap));

    }

  }


  private static void passPropertiesOfModificationMetadataObject(
      ModificationMetadataObject eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.ModificationMetadataObject object =
        (org.obo.datamodel.ModificationMetadataObject) objectMap.get(eobject);

    if (object != null) {

      // property createdBy : String[?];
      object.setCreatedBy(eobject.getCreatedBy());

      // property createdByExtension : NestedValue[?] { composes };
      object.setCreatedByExtension((org.obo.datamodel.NestedValue) getEquivalentObject(
          eobject.getCreatedByExtension(), objectMap));

      // --property creationDate : Date[?] { composes };
      object.setCreationDate(eobject.getCreationDate());

      // --property creationDateExtension : NestedValue[?] { composes };
      object.setCreationDateExtension((org.obo.datamodel.NestedValue) getEquivalentObject(
          eobject.getCreationDateExtension(), objectMap));

      // property modifiedBy : String[?];
      object.setModifiedBy(eobject.getModifiedBy());

      // property creationModifiedByExtension : NestedValue[?] { composes };
      object.setModifiedByExtension((org.obo.datamodel.NestedValue) getEquivalentObject(
          eobject.getModifiedByExtension(), objectMap));

      // --property modificationDate : Date[?] { composes };
      object.setModificationDate(eobject.getModificationDate());

      // --property modificationDateExtension : NestedValue[?] { composes };
      object.setModificationDateExtension((org.obo.datamodel.NestedValue) getEquivalentObject(
          eobject.getModificationDateExtension(), objectMap));

    }

  }


  private static void passPropertiesOfMultiIDObject(
      MultiIDObject eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.MultiIDObject object =
        (org.obo.datamodel.MultiIDObject) objectMap.get(eobject);

    if (object != null) {

      // property secondaryIds : String[*] { composes unique };
      for (String s : eobject.getSecondaryIds()) {
        object.addSecondaryID(s);
      } ;

      // property secondaryIdExtension : StringToNestedValueMap[*] {composes};
      for (String secondaryId : object.getSecondaryIDs()) {
        NestedValue eNestedValue =
            eobject.getSecondaryIdExtension().get(secondaryId);
        if (eNestedValue != null) {
          object.addSecondaryIDExtension(secondaryId,
              (org.obo.datamodel.NestedValue) getEquivalentObject(eNestedValue, objectMap));
        }
      }

    }

  }


  private static void passPropertiesOfMutableLinkDatabase(
      MutableLinkDatabase eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.MutableLinkDatabase object =
        (org.obo.datamodel.MutableLinkDatabase) objectMap.get(eobject);

    if (object != null) {
      // property identifiedObjectIndex : IdentifiedObjectIndex[*] { composes };
      object.setIdentifiedObjectIndex((org.obo.datamodel.IdentifiedObjectIndex) getEquivalentObject(
          eobject.getIdentifiedObjectIndex(), objectMap));
      // não possui gets para isso no injetor
    }

  }


  private static void passPropertiesOfNamespacedObject(
      NamespacedObject eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.NamespacedObject object =
        (org.obo.datamodel.NamespacedObject) objectMap.get(eobject);

    if (object != null) {
      // property namespace : Namespace[?];
      object.setNamespace(
          (org.obo.datamodel.Namespace) getEquivalentObject(eobject.getNamespace(), objectMap));

      // property namespaceExtension : NestedValue[?] { composes };
      object.setNamespaceExtension((org.obo.datamodel.NestedValue) getEquivalentObject(
          eobject.getNamespaceExtension(), objectMap));
    }

  }



  private static void passPropertiesOfNamespace(
      Namespace eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.Namespace object = (org.obo.datamodel.Namespace) objectMap.get(eobject);

    if (object != null) {
      // id - string
      object.setID(eobject.getId());

      // path - string NAO POSSUI
      // object.ssetPath(eobject.getPath());

      // privateID - integer NAO POSSUI
      // object.setPrivateId(BigInteger.valueOf(object.getPrivateID()));

    }

  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfNestedValue(
      NestedValue eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.NestedValue object = (org.obo.datamodel.NestedValue) objectMap.get(eobject);

    if (object != null) {
      // name - string - NAO TEM
      // object.setName(eobject.getName());

      // propertyValues
      object.getPropertyValues()
          .addAll((Collection<org.obo.datamodel.PropertyValue>) getEquivalentCollection(
              eobject.getPropertyValues(), objectMap));

      // suggestedcomment - string
      object.setSuggestedComment(eobject.getSuggestedComment());

    }

  }


  private static void passPropertiesOfObjectFactory(
      ObjectFactory eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.ObjectFactory object =
        (org.obo.datamodel.ObjectFactory) objectMap.get(eobject);

    if (object != null) {
    }

  }


  private static void passPropertiesOfObjectField(
      ObjectField eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.ObjectField object = (org.obo.datamodel.ObjectField) objectMap.get(eobject);

    if (object != null) {
    }

  }


  private static void passPropertiesOfOBOClass(
      OBOClass eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.OBOClass object = (org.obo.datamodel.OBOClass) objectMap.get(eobject);

    if (object != null) {
    }

  }


  private static void passPropertiesOfOBOObject(
      OBOObject eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.OBOObject object = (org.obo.datamodel.OBOObject) objectMap.get(eobject);

    if (object != null) {
    }

  }


  @SuppressWarnings({"rawtypes"})
  private static void passPropertiesOfOBOProperty(
      OBOProperty eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.OBOProperty object = (org.obo.datamodel.OBOProperty) objectMap.get(eobject);

    if (object != null) {

      // property nonInheritable : Boolean - NAO TEM
      // object.setNonInheritable(eobject.isNonInheritable());


      // property ciclic : Boolean;
      object.setCyclic(eobject.isCyclic());

      // property symmetric : Boolean;
      object.setSymmetric(eobject.isSymmetric());

      // property transitive : Boolean;
      object.setTransitive(eobject.isTransitive());

      // property reflexive : Boolean;
      object.setReflexive(eobject.isReflexive());

      // property alwaysImpliesInverse : Boolean;
      object.setAlwaysImpliesInverse(eobject.isAlwaysImpliesInverse());

      // property dummy : Boolean; - NAO TEM
      // object.setDummy(eobject.isDummy());

      // property metadataTag : Boolean;
      object.setMetadataTag(eobject.isMetadataTag());

      // property cyclicExtension : NestedValue[?] { composes };
      object.setCyclicExtension((org.obo.datamodel.NestedValue) getEquivalentObject(
          eobject.getCyclicExtension(), objectMap));

      // property symmetricExtension : NestedValue[?] { composes };
      object.setSymmetricExtension((org.obo.datamodel.NestedValue) getEquivalentObject(
          eobject.getSymmetricExtension(), objectMap));

      // property transitiveExtension : NestedValue[?] { composes };
      object.setTransitiveExtension((org.obo.datamodel.NestedValue) getEquivalentObject(
          eobject.getTransitiveExtension(), objectMap));

      // property reflexiveExtension : NestedValue[?] { composes };
      object.setReflexiveExtension((org.obo.datamodel.NestedValue) getEquivalentObject(
          eobject.getReflexiveExtension(), objectMap));

      // property alwaysImpliesInverseExtension : NestedValue[?] { composes };
      object.setAlwaysImpliesInverseExtension((org.obo.datamodel.NestedValue) getEquivalentObject(
          eobject.getAlwaysImpliesInverseExtension(), objectMap));

      // property domainExtension : NestedValue[?] { composes };
      object.setDomainExtension((org.obo.datamodel.NestedValue) getEquivalentObject(
          eobject.getDomainExtension(), objectMap));

      // property rangeExtension : NestedValue[?] { composes };
      object.setRangeExtension((org.obo.datamodel.NestedValue) getEquivalentObject(
          eobject.getRangeExtension(), objectMap));

      // property range : Type[?];
      object.setRange((org.obo.datamodel.Type) getEquivalentObject(eobject.getRange(), objectMap));

      // property domain : Type[?];
      object
          .setDomain((org.obo.datamodel.Type) getEquivalentObject(eobject.getDomain(), objectMap));

      // property disjointOver : OBOProperty [?];
      if (getEquivalentObject(eobject.getDisjointOver(), objectMap) != null) {
        object.setDisjointOver((org.obo.datamodel.OBOProperty) getEquivalentObject(
            eobject.getDisjointOver(), objectMap));
      }
      // property transitiveOver : OBOProperty [?];
      if (getEquivalentObject(eobject.getTransitiveOver(), objectMap) != null) {
        object.setTransitiveOver((org.obo.datamodel.OBOProperty) getEquivalentObject(
            eobject.getTransitiveOver(), objectMap));
      }

      // property holdsOverChains : ListOfProperties[*] { composes };
      if (eobject.getHoldsOverChains() != null) {
        for (ListOfProperties elist : eobject
            .getHoldsOverChains()) {
          List<org.obo.datamodel.OBOProperty> list = new ArrayList<org.obo.datamodel.OBOProperty>();

          /*
           * ListOfProperties eList =
           * (ListOfProperties)
           * getEquivalentObject(list, objectMap);
           */
          for (OBOProperty p : elist
              .getProperties()) {
            list.add((org.obo.datamodel.OBOProperty) getEquivalentObject(p, objectMap));
          }
        }
      }
    }

  }



  @SuppressWarnings("unchecked")
  private static void passPropertiesOfOBORestriction(
      OBORestriction eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.OBORestriction object =
        (org.obo.datamodel.OBORestriction) objectMap.get(eobject);

    if (object != null) {
      // property cardinality : Integer;
      if (eobject.getCardinality() != null) {
        object.setCardinality(eobject.getCardinality().intValue());
      }
      // property maxCardinality : Integer;
      if (eobject.getMaxCardinality() != null) {
        object.setMaxCardinality(eobject.getMaxCardinality().intValue());
      }

      // property minCardinality : Integer;
      if (eobject.getMinCardinality() != null) {
        object.setMinCardinality(eobject.getMinCardinality().intValue());
      }

      // property completes : Boolean;
      object.setCompletes(eobject.isCompletes());

      // property inverseCompletes : Boolean;
      object.setInverseCompletes(eobject.isInverseCompletes());

      // property necessarilyTrue : Boolean;
      object.setNecessarilyTrue(eobject.isNecessarilyTrue());

      // property inverseNecessarilyTrue : Boolean;
      object.setInverseNecessarilyTrue(eobject.isInverseNecessarilyTrue());

      // property additionalArguments : LinkedObject[*] { !unique };
      if (object.getAdditionalArguments() != null) {
        object.getAdditionalArguments()
            .addAll((Collection<org.obo.datamodel.LinkedObject>) getEquivalentCollection(
                eobject.getAdditionalArguments(), objectMap));
      }

    }

  }


  private static void passPropertiesOfOBOSession(
      OBOSession eObject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.OBOSession object = (org.obo.datamodel.OBOSession) objectMap.get(eObject);

    if (object != null) {

      // pass identified objects
      for (IdentifiedObject io : eObject
          .getObjects()) {
        org.obo.datamodel.IdentifiedObject equivalentObject = 
            (org.obo.datamodel.IdentifiedObject) getEquivalentObject(io, objectMap);
        if (equivalentObject == null) {
          System.err.println("Equivalent object null for " + io);
        } else {
          object.addObject(equivalentObject);
        }
      }
      // pass LinkDataBase
      /*
       * object.setLinkDatabase((
       * LinkDatabase) getEquivalentObject(eObject.getLinkDatabase(),objectMap));
       */

      // defaultNamespace
      object.setDefaultNamespace((org.obo.datamodel.Namespace) getEquivalentObject(
          eObject.getDefaultNamespace(), objectMap));

      // namespaces
      for (Namespace en : eObject
          .getNamespaces()) {
        object.addNamespace((org.obo.datamodel.Namespace) getEquivalentObject(en, objectMap));
      }


      // currentFilenames - string
      object.setCurrentFilenames((Collection<String>) eObject.getCurrentFilenames());


      // propertyValues
      for (PropertyValue epv : eObject
          .getPropertyValues()) {
        object.addPropertyValue(
            (org.obo.datamodel.PropertyValue) getEquivalentObject(epv, objectMap));
      }

      // unknownStanzas
      for (UnknownStanza eus : eObject
          .getUnknownStanzas()) {
        object.addUnknownStanza(
            (org.obo.datamodel.UnknownStanza) getEquivalentObject(eus, objectMap));
      }

      // subsets
      for (TermSubset ets : eObject
          .getSubsets()) {
        object.addSubset((org.obo.datamodel.TermSubset) getEquivalentObject(ets, objectMap));
      }


      // categories - NAO TEM
      /*
       * for(TermSubset ets :
       * eObject.getCategories()){ object.((org.obo.datamodel.TermSubset) getEquivalentObject(ets,
       * objectMap)); }
       */

      // synonymTypes
      for (SynonymType est : eObject
          .getSynonymTypes()) {
        object.addSynonymType((org.obo.datamodel.SynonymType) getEquivalentObject(est, objectMap));
      }

      // synonymCategories NAO TEM
      /*
       * for(SynonymType est :
       * eObject.getSynonymCategories()){ object.get((org.obo.datamodel.SynonymType)
       * getEquivalentObject(est, objectMap)); }
       */

      // currentUser
      object.setCurrentUser(eObject.getCurrentUser());

      // idSpaces - string
      /*
       * object.getIDSpaces().addAll( eObject.getIdSpaces());
       */

      // idSpaces - string => string
      for (String s : eObject.getIdSpaces().keySet()) {
        object.addIDSpace(s, eObject.getIdSpaces().get(s));
      }


      object.setLoadRemark(eObject.getLoadRemark());


    }



  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfObsoletableObject(
      ObsoletableObject eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.ObsoletableObject object =
        (org.obo.datamodel.ObsoletableObject) objectMap.get(eobject);

    if (object != null) {
      // property obsolete : Boolean;
      object.setObsolete(eobject.isObsolete());

      // property replacedBy : ObsoletableObject[*] { unique };
      object.getReplacedBy()
          .addAll((Collection<org.obo.datamodel.ObsoletableObject>) getEquivalentCollection(
              eobject.getReplacedBy(), objectMap));

      // property considerReplacements : ObsoletableObject[*] { unique };
      object.getConsiderReplacements()
          .addAll((Collection<org.obo.datamodel.ObsoletableObject>) getEquivalentCollection(
              eobject.getConsiderReplacements(), objectMap));

      // property considerExtension : ObsoletableObjectToNestedValueMap[*] { composes };
      for (ObsoletableObject o : eobject
          .getConsiderReplacements()) {
        NestedValue eNestedValue =
            eobject.getConsiderExtension().get(o);
        if (eNestedValue != null) {
          object.addConsiderExtension(
              (org.obo.datamodel.ObsoletableObject) getEquivalentObject(o, objectMap),
              (org.obo.datamodel.NestedValue) getEquivalentObject(eNestedValue, objectMap));
        }
      }

      // property replacedByExtension : ObsoletableObjectToNestedValueMap[*] { composes };
      for (ObsoletableObject o : eobject
          .getReplacedBy()) {
        NestedValue eNestedValue =
            eobject.getReplacedByExtension().get(o);
        if (eNestedValue != null) {
          object.addReplacedByExtension(
              (org.obo.datamodel.ObsoletableObject) getEquivalentObject(o, objectMap),
              (org.obo.datamodel.NestedValue) getEquivalentObject(eNestedValue, objectMap));
        }
      }

      // property obsoleteExtension : NestedValue[?] { composes };
      object.setObsoleteExtension((org.obo.datamodel.NestedValue) getEquivalentObject(
          eobject.getObsoleteExtension(), objectMap));


    }

  }


  private static void passPropertiesOfPathCapable(
      PathCapable eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.PathCapable object = (org.obo.datamodel.PathCapable) objectMap.get(eobject);

    if (object != null) {
    }

  }


  private static void passPropertiesOfPropertyValue(
      PropertyValue eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.PropertyValue object =
        (org.obo.datamodel.PropertyValue) objectMap.get(eobject);

    if (object != null) {
      // property - string NAO TEM
      // object.setProperty(eobject.getProperty());

      // value - string NAO TEM
      // object.setValue(eobject.getValue());

      // lineNumber - integer NAO TEM
      // object.setLineNumber(eobject.getLineNumber().intValue());

      // filename - String NAO TEM
      // object.setFilename(eobject.getFilename());

      // line NAO TEM
      // object.setLine(eobject.getLine());
    }

  }


  private static void passPropertiesOfRelationship(
      Relationship eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.Relationship object = (org.obo.datamodel.Relationship) objectMap.get(eobject);

    if (object != null) {
      // type
      object.setType(
          (org.obo.datamodel.OBOProperty) getEquivalentObject(eobject.getType(), objectMap));

      // nestedValue
      object.setNestedValue(
          (org.obo.datamodel.NestedValue) getEquivalentObject(eobject.getNestedValue(), objectMap));
    }

  }



  private static void passPropertiesOfRootAlgorithm(
      RootAlgorithm eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.RootAlgorithm object =
        (org.obo.datamodel.RootAlgorithm) objectMap.get(eobject);

    if (object != null) {

    }

  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfSubsetObject(
      SubsetObject eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.SubsetObject object = (org.obo.datamodel.SubsetObject) objectMap.get(eobject);

    if (object != null) {
      // property subsets : TermSubset[*];
      object.getSubsets().addAll((Collection<org.obo.datamodel.TermSubset>) getEquivalentCollection(
          eobject.getSubsets(), objectMap));

      // property categories : TermSubset[*];
      // object.getCategories().addAll((Collection<TermSubset>)
      // getEquivalentCollection(object., objectMap));
      /*
       * for(TermSubset s : eobject.ge){
       * object.addCategory((org.obo.datamodel.TermSubset) getEquivalentObject(eObject, objectMap));
       * }
       */
      // property categoryExtensions : TermSubsetToNestedValueMap[*] { composes };
      for (TermSubset o : eobject
          .getSubsets()) {
        NestedValue eNestedValue =
            eobject.getCategoryExtensions().get(o);
        if (eNestedValue != null) {
          object.addCategoryExtension(
              (org.obo.datamodel.TermSubset) getEquivalentObject(o, objectMap),
              (org.obo.datamodel.NestedValue) getEquivalentObject(eNestedValue, objectMap));
        }
      }
    }

  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfSynonymedObject(
      SynonymedObject eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.SynonymedObject object =
        (org.obo.datamodel.SynonymedObject) objectMap.get(eobject);

    if (object != null) {
      // property synonyms : Synonym[*] { composes };
      object.getSynonyms().addAll((Collection<org.obo.datamodel.Synonym>) getEquivalentCollection(
          eobject.getSynonyms(), objectMap));
    }

  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfSynonym(
      Synonym eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.Synonym object = (org.obo.datamodel.Synonym) objectMap.get(eobject);

    if (object != null) {
      // property synonymType : SynonymType[?] { composes };
      object.setSynonymType(
          (org.obo.datamodel.SynonymType) getEquivalentObject(eobject.getSynonymType(), objectMap));

      // property nestedValue : NestedValue[?] { composes };
      object.setNestedValue(
          (org.obo.datamodel.NestedValue) getEquivalentObject(eobject.getNestedValue(), objectMap));

      // property scope : Integer;
      object.setScope(eobject.getScope().intValue());

      // property xrefs : Dbxref[?] { composes };
      object.getXrefs()
          .addAll((Collection<org.obo.datamodel.Dbxref>) getEquivalentCollection(eobject.getXrefs(),
              objectMap));

      // property text : String;
      object.setText(eobject.getText());
    }

  }


  private static void passPropertiesOfSynonymType(
      SynonymType eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.SynonymType object = (org.obo.datamodel.SynonymType) objectMap.get(eobject);

    if (object != null) {

      // attribute id : String;
      object.setID(eobject.getId());

      // attribute name : String;
      object.setName(eobject.getName());

      // attribute scope : Integer;
      object.setScope((eobject.getScope()).intValue());
    }

  }


  private static void passPropertiesOfTermSubset(
      TermSubset eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.TermSubset object = (org.obo.datamodel.TermSubset) objectMap.get(eobject);

    if (object != null) {
      // property name : String;
      object.setName(eobject.getName());
      // property desc : String;
      object.setDesc(eobject.getDesc());
    }

  }


  @SuppressWarnings("rawtypes")
  private static void passPropertiesOfType(
      Type eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.Type object = (org.obo.datamodel.Type) objectMap.get(eobject);

    if (object != null) {
    }

  }


  @SuppressWarnings("unchecked")
  private static void passPropertiesOfUnknownStanza(
      UnknownStanza eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.UnknownStanza object =
        (org.obo.datamodel.UnknownStanza) objectMap.get(eobject);


    if (object != null) {

      // namespace NAO TEM
      // object.setNamespace(
      // getEquivalentObject(eobject.getNamespace(), objectMap));

      // stanza - string NAO TEM
      // object.setStanza(eobject.getStanza());

      // propertyValues
      object.getPropertyValues()
          .addAll((Collection<org.obo.datamodel.PropertyValue>) getEquivalentCollection(
              eobject.getPropertyValues(), objectMap));

      // nestedValues
      // precisa pensar nesse

    }

  }



  private static void passPropertiesOfValueDatabase(
      ValueDatabase eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.ValueDatabase object =
        (org.obo.datamodel.ValueDatabase) objectMap.get(eobject);

    if (object != null) {
      /*
       * // public Collection<IdentifiedObject> getObjects();
       * eobject.getObjects().addAll((Collection<br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.
       * obodatamodel.IdentifiedObject>) getEquivalentCollection(object.getObjects(), objectMap));
       */}

  }


  @SuppressWarnings("rawtypes")
  private static void passPropertiesOfValue(
      Value eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.Value object = (org.obo.datamodel.Value) objectMap.get(eobject);

    if (object != null) {
      // property type : Type[?]; - NAO TEM
      // object.setType((Type)
      // getEquivalentObject(object.getType(), objectMap));
    }

  }


  @SuppressWarnings("rawtypes")
  private static void passPropertiesOfValueLink(
      ValueLink eobject,
      Map<EObject, Object> objectMap) {

    org.obo.datamodel.ValueLink object = (org.obo.datamodel.ValueLink) objectMap.get(eobject);

    if (object != null) {
      // property value : Value[?] { composes };
      object.setValue((org.obo.datamodel.Value) getEquivalentObject(eobject.getValue(), objectMap));
    }

  }



}
