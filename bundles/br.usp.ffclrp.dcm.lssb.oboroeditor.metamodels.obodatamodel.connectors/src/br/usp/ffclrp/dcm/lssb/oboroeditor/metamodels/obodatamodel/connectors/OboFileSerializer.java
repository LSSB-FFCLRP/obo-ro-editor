package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.bbop.dataadapter.DataAdapterException;
import org.obo.dataadapter.OBOFileAdapter.OBOAdapterConfiguration;
import org.obo.dataadapter.OBOAdapter;
import org.obo.dataadapter.OBOFileAdapter;
import org.obo.dataadapter.OBOParseException;
import org.obo.dataadapter.OBOSerializationEngine;
import org.obo.dataadapter.OBOSerializationEngine.FilteredPath;
import org.obo.dataadapter.OBOSerializer;
import org.obo.dataadapter.OBO_1_0_Serializer;
import org.obo.dataadapter.OBO_1_2_Serializer;

/**
 * A number of static methods for serializing and desserializing a org.obo.datamodel.OBOSession
 * to/from a OBOFFF file.
 * @author cawal
 *
 */
public class OboFileSerializer {

  private static final String AUTOGEN_STRING = "OBO-RO Editor";

  /**
   * Convenience method. Similar to calling serialize(session, file, OboFormatVersion.OBO1_2);
   * 
   * @param session the OBOSession to serialize
   * @param file the IFile to receive the session
   * @throws FileNotFoundException IFile does not present path information
   * @throws DataAdapterException problems during serialization
   */
  public static void serialize(org.obo.datamodel.OBOSession session, File file)
      throws FileNotFoundException, DataAdapterException {
    serialize(session, file, OboFormatVersion.OBO1_2);
  }

  /**
   * Serializes an ontology session using the OBO-RO Editor default parameters.
   * 
   * @param session
   * @param file
   * @param oboFormatVersion
   * @throws DataAdapterException
   */
  @SuppressWarnings("unchecked")
  public static void serialize(org.obo.datamodel.OBOSession session, File file,
      OboFormatVersion oboFormatVersion) throws DataAdapterException {

    OBOSerializer serializer = null;
    OBOAdapterConfiguration ioprofile = new OBOAdapterConfiguration();

    ioprofile.setWritePath(file.getAbsolutePath());
    ioprofile.setAllowDangling(true);
    ioprofile.setClosureforDangling(false);
    ioprofile.setFollowImports(false);
    ioprofile.setBasicSave(true);

    switch (oboFormatVersion) {
      case OBO1_0:
        ioprofile.setSerializer("OBO_1_0");
        serializer = new OBO_1_0_Serializer();
        break;

      case OBO1_2:
        ioprofile.setSerializer("OBO_1_2");
        serializer = new OBO_1_2_Serializer();
        break;

      default:
        ioprofile.setSerializer("OBO_1_2");
        serializer = new OBO_1_2_Serializer();
        break;
    }

    OBOSerializationEngine serializeEngine = new OBOSerializationEngine();
    serializeEngine.setUsername(session.getCurrentUser());
    serializeEngine.setAutogenString(AUTOGEN_STRING);
    serializeEngine.setCurrentProfile(session.getIDProfile());

    List<FilteredPath> filteredPaths = new LinkedList<FilteredPath>();
    if (ioprofile.getBasicSave()) {
      filteredPaths
          .add(new OBOSerializationEngine.FilteredPath(null, null, ioprofile.getWritePath()));
    } else {
      filteredPaths.addAll(ioprofile.getSaveRecords());
    }

    if (ioprofile.getAllowDangling()) {
      for (FilteredPath filteredPath : filteredPaths) {
        filteredPath.setAllowDangling(true);
        filteredPath.setIncludeXrefDesc(true);
      }
    }
    serializeEngine.serialize((org.obo.datamodel.OBOSession) session, serializer, filteredPaths);
  }

  
  
  public static org.obo.datamodel.OBOSession desserializeFromPaths(List<String> paths)
      throws IOException, OBOParseException, DataAdapterException {

    OBOFileAdapter adapter = new OBOFileAdapter();
    OBOFileAdapter.OBOAdapterConfiguration config = new OBOFileAdapter.OBOAdapterConfiguration();
    config.setAllowDangling(true);
    for (String path : paths) {
      config.getReadPaths().add(path);
    }
    org.obo.datamodel.OBOSession session =
        adapter.doOperation(OBOAdapter.READ_ONTOLOGY, config, null);

    return session;
  }
  
}
