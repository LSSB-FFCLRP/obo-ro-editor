package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.CharBuffer;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.bbop.dataadapter.DataAdapterException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceFactoryImpl;
import org.eclipse.m2m.atl.core.ATLCoreException;
import org.eclipse.m2m.atl.core.IInjector;
import org.eclipse.m2m.atl.core.IModel;
import org.eclipse.m2m.atl.core.emf.EMFInjector;
import org.eclipse.m2m.atl.core.emf.EMFModel;
import org.eclipse.m2m.atl.core.emf.EMFModelFactory;
import org.obo.dataadapter.OBOAdapter;
import org.obo.dataadapter.OBOFileAdapter;
import org.obo.dataadapter.OBOParseException;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession;



/**
 * This class implements operations that takes a OBOFFF ontology and transforms it in an ODM model
 * (injection operations).
 * 
 * @author cawal
 *
 */
public class OBOFileInjector extends EMFInjector implements IInjector {



  @Override
  public void inject(IModel targetModel, String source) throws ATLCoreException {
    inject(targetModel, source, Collections.emptyMap());
  }



  @Override
  public void inject(IModel targetModel, String source, Map<String, Object> options)
      throws ATLCoreException {

    String[] paths = {source};

    try {

      org.obo.datamodel.OBOSession session = readOntology(paths);
      OBOAPI2OBODatamodelTranslator translator = new OBOAPI2OBODatamodelTranslator(session);
      OBOSession translated = translator.translate();
      inject(targetModel, translated);


    } catch (IOException | OBOParseException | DataAdapterException e) {
      throw new ATLCoreException("Error while injecting " + source, e);
    }

  }



  @Override
  public void inject(IModel targetModel, InputStream source, Map<String, Object> options)
      throws ATLCoreException {
    
    Reader reader = new InputStreamReader(source);
    inject(targetModel,reader,options);


  }



  public void inject(IModel targetModel, Reader source, Map<String, Object> options)
      throws ATLCoreException {
    
    try {
      
      File tempFile = File.createTempFile("TemporaryOBOFile", "obo");
      FileWriter writer = new FileWriter(tempFile);
      
      while(source.ready()) {
        char[] buffer = new char[1024];
        source.read(buffer);
        writer.write(buffer);
      }
      
      source.close();
      writer.close();
      
      inject(targetModel,tempFile.getAbsolutePath(),options);
    } catch (ATLCoreException | IOException e) {
      if (e instanceof ATLCoreException) {
        ATLCoreException aux = (ATLCoreException) e;
        throw aux;
      } else {
        throw new ATLCoreException("Error while writing temp file",e);
      }
      
    }
    
  }

  private void inject(IModel targetModel, OBOSession translated) throws ATLCoreException {
    ResourceSet resSet = new ResourceSetImpl();
    resSet.getLoadOptions().put(XMIResource.OPTION_DEFER_IDREF_RESOLUTION, Boolean.TRUE);
    resSet.getLoadOptions().put(XMIResource.OPTION_ENCODING, "UTF-8");
    // Get the resource
    Resource resource = resSet.getResource(URI.createURI("new-model"), true);
    resource.getContents().add(translated);
    
    super.inject(targetModel, resource);

  }

  
  
  private static org.obo.datamodel.OBOSession readOntology(String[] paths)
      throws IOException, OBOParseException, DataAdapterException {

    OBOFileAdapter adapter = new OBOFileAdapter();
    OBOFileAdapter.OBOAdapterConfiguration config = new OBOFileAdapter.OBOAdapterConfiguration();
    for (String path : paths) {
      config.getReadPaths().add(path);
    }
    org.obo.datamodel.OBOSession session =
        adapter.doOperation(OBOAdapter.READ_ONTOLOGY, config, null);

    return session;
  }
}
