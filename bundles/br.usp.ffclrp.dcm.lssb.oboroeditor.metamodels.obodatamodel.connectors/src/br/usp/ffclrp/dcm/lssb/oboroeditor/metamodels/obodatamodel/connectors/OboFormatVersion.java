package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors;

/**
 * A ennumeration of the OBO formats supported by the API.
 * @author cawal
 *
 */
public enum OboFormatVersion {
  /** OBO Format 1.0*/
  OBO1_0,
  /** OBO Format 1.2*/
  OBO1_2
}
