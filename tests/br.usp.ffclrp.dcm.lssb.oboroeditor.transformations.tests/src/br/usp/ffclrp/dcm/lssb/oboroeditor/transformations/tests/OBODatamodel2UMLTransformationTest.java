package br.usp.ffclrp.dcm.lssb.oboroeditor.transformations.tests;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.m2m.atl.core.ATLCoreException;
import org.eclipse.m2m.atl.core.IExtractor;
import org.eclipse.m2m.atl.core.IInjector;
import org.eclipse.m2m.atl.core.IModel;
import org.eclipse.m2m.atl.core.IReferenceModel;
import org.eclipse.m2m.atl.core.ModelFactory;
import org.eclipse.m2m.atl.core.emf.EMFExtractor;
import org.eclipse.m2m.atl.core.emf.EMFInjector;
import org.eclipse.m2m.atl.core.emf.EMFModel;
import org.eclipse.m2m.atl.core.emf.EMFModelFactory;
import org.eclipse.m2m.atl.core.emf.EMFReferenceModel;
import org.eclipse.m2m.atl.core.launch.ILauncher;
import org.eclipse.m2m.atl.emftvm.EmftvmFactory;
import org.eclipse.m2m.atl.emftvm.ExecEnv;
import org.eclipse.m2m.atl.emftvm.Metamodel;
import org.eclipse.m2m.atl.emftvm.Model;
import org.eclipse.m2m.atl.emftvm.util.DefaultModuleResolver;
import org.eclipse.m2m.atl.emftvm.util.ModuleResolver;
import org.eclipse.m2m.atl.emftvm.util.TimingData;
import org.eclipse.m2m.atl.engine.emfvm.launch.EMFVMLauncher;
import org.junit.*;

public class OBODatamodel2UMLTransformationTest {


  final static String TEST_RESOURCES = "resources/";
  final static String OBODATAMODEL_2_UML_TRANSFORMATION_MODULE_URI = 
      "platform:resource/br.usp.ffclrp.dcm.lssb.oboroeditor.transformations"
      + "/resources/OBODatamodel2UML.emftvm";
  final static String UML_METAMODEL_URI = "http://www.eclipse.org/uml2/4.0.0/UML";

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {}

  @AfterClass
  public static void tearDownAfterClass() throws Exception {}

  @Before
  public void setUp() throws Exception {}

  @After
  public void tearDown() throws Exception {}

  @Test
  public void test() {
    try {
      /*
       * Initializations
       */
      ModelFactory modelFactory = new EMFModelFactory();
      IInjector injector = new EMFInjector();
      IExtractor extractor = new EMFExtractor();

      /*
       * Load metamodels
       */
      IReferenceModel odmMetamodel = modelFactory.newReferenceModel();
      URL url = new URL("http://dcm.ffclrp.usp.br/lssb/oboroeditor/metamodels/obodatamodel.ecore");
      injector.inject(odmMetamodel, new InputStreamReader(url.openConnection().getInputStream()),Collections.emptyMap()
          );

      /*
       * Load models
       */
      IModel odmModel = modelFactory.newModel(odmMetamodel);
      injector.inject(odmModel, TEST_RESOURCES + "/obodatamodel2uml/emptyOntology/OBOSession.xmi");

      /*
       * Run transformation
       */
      IModel umlModel = transform(odmModel, new NullProgressMonitor());

      /*
       * extract model
       */

      File file = File.createTempFile("test-from_obodatamodel", ".uml");
      extractor.extract(umlModel, file.getAbsolutePath());

      /*
       * Unload all models and metamodels (EMF-specific)
       */
      EMFModelFactory emfModelFactory = (EMFModelFactory) modelFactory;
      emfModelFactory.unload((EMFModel) umlModel);
      emfModelFactory.unload((EMFModel) odmModel);
      emfModelFactory.unload((EMFReferenceModel) odmMetamodel);

    } catch (ATLCoreException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  
  public static IModel transform(IModel inputOdmModel, IProgressMonitor progressMonitor)
      throws ATLCoreException, IOException {
  
          // Initializations
          EMFModelFactory modelFactory = new EMFModelFactory();
          ILauncher transformationLauncher = new EMFVMLauncher();
          IInjector injector = new EMFInjector();

          // Load the transformation definition
          URL url = new URL(OBODATAMODEL_2_UML_TRANSFORMATION_MODULE_URI);
          InputStream transformationInputStream = url.openConnection().getInputStream();

          // load the OBO-RO metamodel and create IModel
          IReferenceModel umlMetamodel = modelFactory.newReferenceModel();
          injector.inject(umlMetamodel, UML_METAMODEL_URI);
          IModel umlModel = modelFactory.newModel(umlMetamodel);

          // Run transformation
          transformationLauncher.initialize(new HashMap<String, Object>());
          transformationLauncher.addInModel(inputOdmModel, "IN", "OBO");
          transformationLauncher.addOutModel(umlModel, "OUT", "UML");
          transformationLauncher.launch(ILauncher.RUN_MODE, progressMonitor, new HashMap<String, Object>(),
                  transformationInputStream);

          // unload the metamodel
          modelFactory.unload((EMFReferenceModel) umlMetamodel);

          return umlModel;

}
  
  @Test
  public void test2() throws IOException {
    ExecEnv env = EmftvmFactory.eINSTANCE.createExecEnv();
    ResourceSet rs = new ResourceSetImpl();

    // Load metamodels
    Metamodel metaModel = EmftvmFactory.eINSTANCE.createMetamodel();
    metaModel.setResource(rs.getResource(URI.createURI("http://dcm.ffclrp.usp.br/lssb/oboroeditor/metamodels/obodatamodel.ecore"), true));
    env.registerMetaModel("METAMODEL", metaModel);

    // Load models
    Model inModel = EmftvmFactory.eINSTANCE.createModel();
    inModel.setResource(rs.getResource(URI.createURI("input.xmi", true), true));
    env.registerInputModel("IN", inModel);

    Model inOutModel = EmftvmFactory.eINSTANCE.createModel();
    inOutModel.setResource(rs.getResource(URI.createURI("inout.xmi", true), true));
    env.registerInOutModel("INOUT", inOutModel);

    Model outModel = EmftvmFactory.eINSTANCE.createModel();
    outModel.setResource(rs.createResource(URI.createFileURI("out.xmi")));
    env.registerOutputModel("OUT", outModel);

    // Load and run module
    ModuleResolver mr = new DefaultModuleResolver("platform:/plugin/my.plugin.id/transformations/", new ResourceSetImpl());
    TimingData td = new TimingData();
    env.loadModule(mr, "Module");
    td.finishLoading();
    env.run(td);
    td.finish();

    // Save models
    inOutModel.getResource().save(Collections.emptyMap());
    outModel.getResource().save(Collections.emptyMap());
  }
  
}
