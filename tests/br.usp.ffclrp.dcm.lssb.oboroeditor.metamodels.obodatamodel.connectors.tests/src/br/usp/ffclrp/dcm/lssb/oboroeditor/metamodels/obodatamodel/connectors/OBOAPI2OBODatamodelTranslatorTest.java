package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.Date;

import org.bbop.dataadapter.DataAdapterException;
import org.eclipse.ocl.ParserException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.obo.dataadapter.OBOParseException;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelFactory;

public class OBOAPI2OBODatamodelTranslatorTest extends TestBase {

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {}

  @AfterClass
  public static void tearDownAfterClass() throws Exception {}

  @Before
  public void setUp() throws Exception {}

  @After
  public void tearDown() throws Exception {}


  @Test
  public void oclExpressionTest() throws ParserException {

    OBOSession session = obodatamodelFactory.eINSTANCE.createOBOSession();
    String expression = "self.oclIsUndefined()";
    boolean result = (boolean) evaluateOCLExpressionInObject(session, expression);
    assertFalse(result);
  }

  @Test
  public void emptyOntologyToOBODatamodelTest() throws IOException, OBOParseException, DataAdapterException{

    // load source
    String[] paths = {TEST_RESOURCES + "emptyOntology/source.obo"};
    org.obo.datamodel.OBOSession source = null;
    source = getSession(paths);

    String targetPath = TEST_RESOURCES + "emptyOntology/target.obodatamodel";

    // load target
    OBOSession target = null;
    target = getOBODatamodelSession(targetPath);

    // transform source
    OBOAPI2OBODatamodelTranslator translator = new OBOAPI2OBODatamodelTranslator(source);
    OBOSession translated = translator.translate();
    
    // COMPARE TRANSLATED TO TARGET
    assertSimilar(target, translated,true);
  }


  @Test
  public void ontologyWithNamespaceAndObjects() throws IOException, OBOParseException, DataAdapterException {

    // load source
    String[] paths = {TEST_RESOURCES + "ontologyWithNamespaceAndObjects/source.obo"};
    org.obo.datamodel.OBOSession source = null;
    source = getSession(paths);

    String targetPath = TEST_RESOURCES + "emptyOntology/target.obodatamodel";

    // load target
    OBOSession target = null;
    target = getOBODatamodelSession(targetPath);

    // transform source
    OBOAPI2OBODatamodelTranslator translator = new OBOAPI2OBODatamodelTranslator(source);
    OBOSession translated = translator.translate();
    
    // COMPARE TRANSLATED TO TARGET
    assertSimilar(target, translated,true);
  }
  
  
  @Ignore
  @Test
  public void convertGO2OBO() throws IOException, OBOParseException, DataAdapterException {

    // load source
    System.out.println(new Date());
    String[] paths = {TEST_RESOURCES + "go/go.obo"};
    org.obo.datamodel.OBOSession source = null;
    source = getSession(paths);
    System.out.println(new Date());

    String targetPath = TEST_RESOURCES + "go/go.obodatamodel";


    // transform source
    OBOAPI2OBODatamodelTranslator translator = new OBOAPI2OBODatamodelTranslator(source);
    OBOSession translated = translator.translate();
    System.out.println(new Date());
    
    
    TestBase.saveOBODatamodel(translated, targetPath);
  }
  
  
  

  @Test
  public void convertDanglingReferences()  throws IOException, OBOParseException, DataAdapterException {

    // load source
    System.out.println(new Date());
    String[] paths = {TEST_RESOURCES + "danglingReferences/01.obo"};
    String targetPath = TEST_RESOURCES + "danglingReferences/01.obodatamodel";
    
    org.obo.datamodel.OBOSession source = null;
    source = getSession(paths);
    System.out.println(new Date());



    // transform source
    OBOAPI2OBODatamodelTranslator translator = new OBOAPI2OBODatamodelTranslator(source);
    OBOSession translated = translator.translate();
    System.out.println(new Date());
    
    
    TestBase.saveOBODatamodel(translated, targetPath);
  }
  
  
  
  
  @Test
  public void convertCL()  throws IOException, OBOParseException, DataAdapterException {

    // load source
    System.out.println(new Date());
    String[] paths = {TEST_RESOURCES + "cl/cl.obo"};
    org.obo.datamodel.OBOSession source = null;
    source = getSession(paths);
    System.out.println(new Date());

    String targetPath = TEST_RESOURCES + "cl/cl.obodatamodel";


    // transform source
    OBOAPI2OBODatamodelTranslator translator = new OBOAPI2OBODatamodelTranslator(source);
    OBOSession translated = translator.translate();
    System.out.println(new Date());
    
    
    TestBase.saveOBODatamodel(translated, targetPath);
  }
  
}
