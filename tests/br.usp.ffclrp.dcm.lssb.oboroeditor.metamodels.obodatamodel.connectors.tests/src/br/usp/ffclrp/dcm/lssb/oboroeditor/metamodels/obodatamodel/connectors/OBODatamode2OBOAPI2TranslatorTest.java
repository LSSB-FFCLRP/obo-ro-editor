package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;

import org.bbop.dataadapter.DataAdapterException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors.OBODatamodel2OBOAPITranslator;

public class OBODatamode2OBOAPI2TranslatorTest extends TestBase {

  
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {}

  @AfterClass
  public static void tearDownAfterClass() throws Exception {}

  @Before
  public void setUp() throws Exception {}

  @After
  public void tearDown() throws Exception {}

  @Ignore
  @Test
  public void test() {

    // load source
    String[] paths = {TEST_RESOURCES + "emptyOntology/source.obo"};
    org.obo.datamodel.OBOSession target = null;
    try {
      target = getSession(paths);
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      fail(e.toString());
    }

    System.out.println(target);

    String sourcePath = TEST_RESOURCES + "emptyOntology/target.obodatamodel";

    // load target
    OBOSession source = getOBODatamodelSession(sourcePath);
    System.out.println(source);

    // transform source
    OBODatamodel2OBOAPITranslator translator = new OBODatamodel2OBOAPITranslator(source);
    org.obo.datamodel.OBOSession translated = translator.translate();

    System.out.println(translated);

  }
  
  @Test
  public void transformGEXPO() throws IOException, DataAdapterException {
    
    String sourcePath = "/home/cawal/git/LSSB/OBORO-UML-Editor/br.usp.ffclrp.dcm.oboroeditor/tests/gexpo/gexpo-export.obodatamodel";

    // load target
    OBOSession source = getOBODatamodelSession(sourcePath);
    System.out.println(source);

    // transform source
    OBODatamodel2OBOAPITranslator translator = new OBODatamodel2OBOAPITranslator(source);
    org.obo.datamodel.OBOSession translated = translator.translate();

    System.out.println(translated);
    
    
    
    File file = new File("/home/cawal/git/LSSB/OBORO-UML-Editor/br.usp.ffclrp.dcm.oboroeditor/tests/gexpo/final.obo");
    
    file.createNewFile();
    OboFileSerializer.serialize(translated, file,OboFormatVersion.OBO1_2);
    

  }

}
