package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors;

import static org.junit.Assert.assertFalse;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.bbop.dataadapter.DataAdapterException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.match.DefaultComparisonFactory;
import org.eclipse.emf.compare.match.DefaultEqualityHelperFactory;
import org.eclipse.emf.compare.match.DefaultMatchEngine;
import org.eclipse.emf.compare.match.IComparisonFactory;
import org.eclipse.emf.compare.match.IMatchEngine;
import org.eclipse.emf.compare.match.eobject.IEObjectMatcher;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryImpl;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryRegistryImpl;
import org.eclipse.emf.compare.scope.DefaultComparisonScope;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.compare.utils.UseIdentifiers;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.ocl.OCL;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.Query;
import org.eclipse.ocl.ecore.CallOperationAction;
import org.eclipse.ocl.ecore.EcoreEnvironmentFactory;
import org.eclipse.ocl.ecore.SendSignalAction;
import org.eclipse.ocl.expressions.OCLExpression;
import org.eclipse.ocl.helper.OCLHelper;
import org.obo.dataadapter.OBOAdapter;
import org.obo.dataadapter.OBOFileAdapter;
import org.obo.dataadapter.OBOFileAdapter.OBOAdapterConfiguration;
import org.obo.dataadapter.OBOParseException;
import org.obo.dataadapter.OBOSerializationEngine;
import org.obo.dataadapter.OBOSerializationEngine.FilteredPath;
import org.obo.dataadapter.OBOSerializer;
import org.obo.dataadapter.OBO_1_0_Serializer;
import org.obo.dataadapter.OBO_1_2_Serializer;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

public class TestBase {

  final static String TEST_RESOURCES = "resources/";

  protected static final String AUTOGEN_STRING = "TESTE DE AUTOGEN STRING";



  public static org.obo.datamodel.OBOSession getSession(String[] paths)
      throws IOException, OBOParseException, DataAdapterException {

    OBOFileAdapter adapter = new OBOFileAdapter();
    OBOFileAdapter.OBOAdapterConfiguration config = new OBOFileAdapter.OBOAdapterConfiguration();
    config.setAllowDangling(true);
    for (String path : paths) {
      config.getReadPaths().add(path);
    }
    org.obo.datamodel.OBOSession session =
        adapter.doOperation(OBOAdapter.READ_ONTOLOGY, config, null);

    return session;
  }



  public static OBOSession getOBODatamodelSession(String filePath) {
    // Initialize the model
    obodatamodelPackage.eINSTANCE.eClass();

    // Register the XMI resource factory for the .xmi extension
    Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
    Map<String, Object> m = reg.getExtensionToFactoryMap();
    m.put("obodatamodel", new XMIResourceFactoryImpl());

    // Obtain a new resource set
    ResourceSet resSet = new ResourceSetImpl();
    resSet.getLoadOptions().put(XMIResource.OPTION_DEFER_IDREF_RESOLUTION, Boolean.TRUE);
    //resSet.getLoadOptions().put(XMIResource.OPTION_DEFER_ATTACHMENT,  Boolean.TRUE);
    resSet.getLoadOptions().put(XMIResource.OPTION_ENCODING, "UTF-8");
    // Get the resource
    Resource resource = resSet.getResource(URI.createURI(filePath), true);
    try {
      resource.load(resSet.getLoadOptions());
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    // Get the first model element and cast it to the right type, in my
    // example everything is hierarchical included in this first node
    OBOSession ontology = (OBOSession) resource.getContents().get(0);
    return ontology;
  }

  public static void saveOBODatamodel(OBOSession session, String filePath) throws IOException {
    OBODatamodelSerializer.serialize(session, filePath);
  }
  

  /**
   * Evaluates an OCL expression on EObject.
   * 
   * @param eobject
   * @param expression
   * @return The result must be casted for the expected type. E.g.:
   * 
   *         Collection<?> result = (Collection<?>)
   *         evaluateOCLExpressionInObject(eobject,expression);
   * @throws ParserException
   * @seeAlso evaluateOCLConstraintInObject(EObject, String)
   */
  public static Object evaluateOCLExpressionInObject(EObject eobject, String expression)
      throws ParserException {

    OCLExpression<EClassifier> query = null;

    // create an OCL instance for Ecore
    OCL<EPackage, EClassifier, EOperation, EStructuralFeature, EEnumLiteral, EParameter, EObject, CallOperationAction, SendSignalAction, org.eclipse.ocl.ecore.Constraint, EClass, EObject> ocl =
        null;

    ocl = OCL.newInstance(EcoreEnvironmentFactory.INSTANCE);

    // create an OCL helper object
    OCLHelper<EClassifier, EOperation, EStructuralFeature, org.eclipse.ocl.ecore.Constraint> helper =
        ocl.createOCLHelper();

    // set the OCL context classifier
    // helper.setContext(obodatamodelPackage.Literals.OBO_SESSION);
    helper.setContext(eobject.eClass());

    query = helper.createQuery(expression);

    // use the query expression parsed before to create a Query
    Query<EClassifier, EClass, EObject> eval = ocl.createQuery(query);

    // return results
    return eval.evaluate(eobject);

  }

  public static boolean evaluateOCLConstraintInObject(EObject eobject, String expression)
      throws ParserException {
    return (boolean) evaluateOCLExpressionInObject(eobject, expression);
  }
  


  public void load(String absolutePath, ResourceSet resourceSet) {
    URI uri = URI.createFileURI(absolutePath);

    resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi",
        new XMIResourceFactoryImpl());
    // Resource will be loaded within the resource set
    resourceSet.getLoadOptions().put(XMLResource.OPTION_DEFER_IDREF_RESOLUTION, Boolean.TRUE);

    resourceSet.getResource(uri, true);

  }
  
  
  /**
   * Assert if two OBOSession (Ecore models) are similar.
   * @param target
   * @param reference
   * @throws IOException
   */
  public void assertSimilar(OBOSession target, OBOSession reference, boolean ignoreLoadInformation) throws IOException {

    target = EcoreUtil.copy(target);
    reference = EcoreUtil.copy(reference);
    
    if(ignoreLoadInformation) {
      // remove load information
      removeLoadInformation(target);
      removeLoadInformation(reference);
    }
    
    Comparison comparison = compare(target, reference);
    
    /*Predicate<? super Diff> predicate = hasConflict(ConflictKind.REAL, ConflictKind.PSEUDO);
    // Filter out the differences that do not satisfy the predicate
    Iterable<Diff> conflictingDifferencesFromLeft = filter(comparison.getDifferences(), predicate);
    */
    
    assertFalse(comparison.getDifferences().size() != 0);

  }



  public void removeLoadInformation(OBOSession target) {
    target.setCurrentUser("");
    target.setLoadRemark("");
    target.getCurrentFilenames().clear();
  }

  public Comparison compare(EObject eobject1, EObject eobject2) throws IOException {
    // SAVE MODELS AS XMI TO COMPARE

    File file1 = File.createTempFile("test1", ".obodatamodel");
    File file2 = File.createTempFile("test2", ".obodatamodel");

    // Register the XMI resource factory for the extension
    Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
    Map<String, Object> m = reg.getExtensionToFactoryMap();

    m.put("obodatamodel", new XMIResourceFactoryImpl());

    // Obtain a new resource set
    ResourceSet resourceSet1 = new ResourceSetImpl();
    resourceSet1.getLoadOptions().put(XMIResource.OPTION_DEFER_IDREF_RESOLUTION, Boolean.TRUE);
    resourceSet1.getLoadOptions().put(XMIResource.OPTION_DEFER_ATTACHMENT,  Boolean.TRUE);
    resourceSet1.getLoadOptions().put(XMIResource.OPTION_ENCODING, "UTF-8");
    Resource resource1 = resourceSet1.createResource(URI.createURI(file1.getAbsolutePath()));
    resource1.getContents().add(eobject1);
    // now save the content.
    try {
      resource1.save(Collections.EMPTY_MAP);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }


    // now for model 2
    ResourceSet resourceSet2 = new ResourceSetImpl();
    resourceSet2.getLoadOptions().put(XMIResource.OPTION_DEFER_IDREF_RESOLUTION, Boolean.TRUE);
    resourceSet2.getLoadOptions().put(XMIResource.OPTION_DEFER_ATTACHMENT,  Boolean.TRUE);
    resourceSet2.getLoadOptions().put(XMIResource.OPTION_ENCODING, "UTF-8");
    
    Resource resource2 = resourceSet2.createResource(URI.createURI(file2.getAbsolutePath()));
    resource2.getContents().add(eobject2);

    // ... save the content.
    try {
      resource2.save(Collections.EMPTY_MAP);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }


    // Configure EMF Compare
    IEObjectMatcher matcher = DefaultMatchEngine.createDefaultEObjectMatcher(UseIdentifiers.ONLY);
    IComparisonFactory comparisonFactory =
        new DefaultComparisonFactory(new DefaultEqualityHelperFactory());
    IMatchEngine.Factory matchEngineFactory =
        new MatchEngineFactoryImpl(matcher, comparisonFactory);
    matchEngineFactory.setRanking(20);
    IMatchEngine.Factory.Registry matchEngineRegistry = new MatchEngineFactoryRegistryImpl();
    matchEngineRegistry.add(matchEngineFactory);
    EMFCompare comparator =
        EMFCompare.builder().setMatchEngineFactoryRegistry(matchEngineRegistry).build();

    // Compare the two models
    IComparisonScope scope = new DefaultComparisonScope(resourceSet1, resourceSet2, null);
    return comparator.compare(scope);
  }


}
