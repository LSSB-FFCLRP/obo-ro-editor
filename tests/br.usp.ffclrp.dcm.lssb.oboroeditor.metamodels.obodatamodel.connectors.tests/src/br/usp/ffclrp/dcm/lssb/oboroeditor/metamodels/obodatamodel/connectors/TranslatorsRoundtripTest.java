package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.connectors;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;

import org.bbop.dataadapter.DataAdapterException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.obo.dataadapter.OBOAdapter;
import org.obo.dataadapter.OBOFileAdapter;
import org.obo.dataadapter.OBOParseException;
import org.obo.history.HistoryGenerator;
import org.obo.history.HistoryList;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOSession;

public class TranslatorsRoundtripTest extends TestBase {


  
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {}

  @AfterClass
  public static void tearDownAfterClass() throws Exception {}

  @Before
  public void setUp() throws Exception {}

  @After
  public void tearDown() throws Exception {}

  
  @Test
  public void oboRoundtripMetaTest() throws DataAdapterException, IOException, OBOParseException {


    String[] testFiles = {TEST_RESOURCES + "roundtrip/metatest/testfile.1.0.obo",
        TEST_RESOURCES + "roundtrip/metatest/testfile.1.2.obo"};

    org.obo.datamodel.OBOSession session = getSession(testFiles);


    for (int i = 0; i < testFiles.length; i++) {

      // Load file
      OBOFileAdapter adapter = new OBOFileAdapter();
      OBOFileAdapter.OBOAdapterConfiguration config = new OBOFileAdapter.OBOAdapterConfiguration();
      config.getReadPaths().add(testFiles[i].toString());
      session = adapter.doOperation(OBOAdapter.READ_ONTOLOGY, config, null);

      // Save 1.0 input file as as 1.0 and 1.2 as 1.2
      File outFile = File.createTempFile("test", ".obo");
      if (testFiles[i].indexOf("1.0") > 0)
        OboFileSerializer.serialize(session, outFile, OboFormatVersion.OBO1_0);
      else if (testFiles[i].indexOf("1.2") > 0)
        OboFileSerializer.serialize(session, outFile, OboFormatVersion.OBO1_2);


      // Read in the round-tripped file
      String[] tempPath = {outFile.getAbsolutePath()};
      org.obo.datamodel.OBOSession session2 = getSession(tempPath);


      // get the history generator version of the changes
      HistoryList allChanges = HistoryGenerator.getHistory(session2, session);
      assertTrue("The file should be exactly the same going in as going out; in = " + testFiles[i]
          + ", out = " + outFile + ", allChanges = " + allChanges, allChanges.size() == 0);
      
      outFile.delete();
    }
  }


  
  
  
  @Test
  public void roundtripConnectorsTest() throws IOException, DataAdapterException {
    // load the OBO files
    String[] paths = {TEST_RESOURCES + "emptyOntology/source.obo"};
    org.obo.datamodel.OBOSession source = null;
    try {
      source = getSession(paths);
    } catch (Exception e) {
      e.printStackTrace();
      fail(e.toString());
    }
    
    // transform into OBO Datamodel model
    OBOAPI2OBODatamodelTranslator translator = new OBOAPI2OBODatamodelTranslator(source);
    OBOSession translated = translator.translate();
    
    // transform into org.obo.datamodel.OBOSession objects
    OBODatamodel2OBOAPITranslator translator2 = new OBODatamodel2OBOAPITranslator(translated);
    org.obo.datamodel.OBOSession roundtripped = translator2.translate();
    

    // save a temporary file to help manual checks
    File file = File.createTempFile("test-translated", ".obo");
    OboFileSerializer.serialize(roundtripped, file);
    
    // compare the initial and the roundtripped org.obo.datamodel.OBOSession objects
    HistoryList allChanges = HistoryGenerator.getHistory(roundtripped, source);
    assertTrue("The file should be exactly the same going in as going out; in = " + paths
        + ", out = " + roundtripped + ", allChanges = " + allChanges, allChanges.size() == 0);
    
    file.delete();
    
  }

  
  

  @Test
  public void roundtripROCoreConnectorsTest() throws IOException, DataAdapterException {
    // load the OBO files
    String[] paths = {TEST_RESOURCES + "/ROCore/core.obo"};
    //String[] paths = {TEST_RESOURCES + "/ROCore/core.owl"};
    org.obo.datamodel.OBOSession source = null;
    try {
      source = getSession(paths);
    } catch (Exception e) {
      e.printStackTrace();
      fail(e.toString());
    }
    
    source.getObjects().stream().forEach(item -> {System.out.println(item);});
    // transform into OBO Datamodel model
    OBOAPI2OBODatamodelTranslator translator = new OBOAPI2OBODatamodelTranslator(source);
    OBOSession translated = translator.translate();
    
    File obodatamodelFile = new File(TEST_RESOURCES + "/ROCore/core.obodatamodel");
    obodatamodelFile.createNewFile();
    TestBase.saveOBODatamodel(translated, obodatamodelFile.getAbsolutePath());
    
    // transform into org.obo.datamodel.OBOSession objects
    OBODatamodel2OBOAPITranslator translator2 = new OBODatamodel2OBOAPITranslator(translated);
    org.obo.datamodel.OBOSession roundtripped = translator2.translate();
    

    // save a temporary file to help manual checks
    File file = File.createTempFile("test-translated", ".obo");
    OboFileSerializer.serialize(roundtripped, file);
    
    // compare the initial and the roundtripped org.obo.datamodel.OBOSession objects
    HistoryList allChanges = HistoryGenerator.getHistory(roundtripped, source);
    
    allChanges.getHistoryItems().stream().forEach(System.out::println); 
    
    assertTrue("The file should be exactly the same going in as going out; in = " + paths
        + ", out = " + roundtripped + ", allChanges = " + allChanges, allChanges.size() == 0);
    
    //file.delete();
    
  }
  


  @Test
  public void roundtripPOConnectorsTest() throws IOException, DataAdapterException {
    // load the OBO files
    String[] paths = {TEST_RESOURCES + "/plantOntology/po.obo"};
    //String[] paths = {TEST_RESOURCES + "/ROCore/core.owl"};
    org.obo.datamodel.OBOSession source = null;
    try {
      source = getSession(paths);
    } catch (Exception e) {
      e.printStackTrace();
      fail(e.toString());
    }
    
    source.getObjects().stream().forEach(item -> {System.out.println(item);});
    // transform into OBO Datamodel model
    OBOAPI2OBODatamodelTranslator translator = new OBOAPI2OBODatamodelTranslator(source);
    OBOSession translated = translator.translate();
    
    File obodatamodelFile = new File(TEST_RESOURCES + "/plantOntology/po.obodatamodel");
    obodatamodelFile.createNewFile();
    TestBase.saveOBODatamodel(translated, obodatamodelFile.getAbsolutePath());
    
    // transform into org.obo.datamodel.OBOSession objects
    OBODatamodel2OBOAPITranslator translator2 = new OBODatamodel2OBOAPITranslator(translated);
    org.obo.datamodel.OBOSession roundtripped = translator2.translate();
    

    // save a temporary file to help manual checks
    File file = File.createTempFile("test-translated", ".obo");
    OboFileSerializer.serialize(roundtripped, file);
    
    // compare the initial and the roundtripped org.obo.datamodel.OBOSession objects
    HistoryList allChanges = HistoryGenerator.getHistory(roundtripped, source);
    
    allChanges.getHistoryItems().stream().forEach(System.out::println); 
    
    assertTrue("The file should be exactly the same going in as going out; in = " + paths
        + ", out = " + roundtripped + ", allChanges = " + allChanges, allChanges.size() == 0);
    
    //file.delete();
    
  }
  
}
