/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.tests;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DefinedObject;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Defined Object</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class DefinedObjectTest extends IdentifiedObjectTest {

  /**
   * Constructs a new Defined Object test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DefinedObjectTest(String name) {
    super(name);
  }

  /**
   * Returns the fixture for this Defined Object test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected DefinedObject getFixture() {
    return (DefinedObject)fixture;
  }

} //DefinedObjectTest
