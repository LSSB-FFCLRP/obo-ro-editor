/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.tests;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Relationship;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Relationship</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class RelationshipTest extends SerializableTest {

  /**
   * Constructs a new Relationship test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RelationshipTest(String name) {
    super(name);
  }

  /**
   * Returns the fixture for this Relationship test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected Relationship getFixture() {
    return (Relationship)fixture;
  }

} //RelationshipTest
