/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.tests;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Impliable;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Impliable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ImpliableTest extends TestCase {

  /**
   * The fixture for this Impliable test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected Impliable fixture = null;

  /**
   * Constructs a new Impliable test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImpliableTest(String name) {
    super(name);
  }

  /**
   * Sets the fixture for this Impliable test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void setFixture(Impliable fixture) {
    this.fixture = fixture;
  }

  /**
   * Returns the fixture for this Impliable test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected Impliable getFixture() {
    return fixture;
  }

} //ImpliableTest
