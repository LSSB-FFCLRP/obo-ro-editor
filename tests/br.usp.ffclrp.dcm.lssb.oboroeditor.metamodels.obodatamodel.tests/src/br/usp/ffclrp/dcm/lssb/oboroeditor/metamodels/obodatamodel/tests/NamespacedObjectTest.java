/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.tests;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NamespacedObject;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Namespaced Object</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class NamespacedObjectTest extends TestCase {

  /**
   * The fixture for this Namespaced Object test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected NamespacedObject fixture = null;

  /**
   * Constructs a new Namespaced Object test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NamespacedObjectTest(String name) {
    super(name);
  }

  /**
   * Sets the fixture for this Namespaced Object test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void setFixture(NamespacedObject fixture) {
    this.fixture = fixture;
  }

  /**
   * Returns the fixture for this Namespaced Object test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected NamespacedObject getFixture() {
    return fixture;
  }

} //NamespacedObjectTest
