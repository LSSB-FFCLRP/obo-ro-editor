/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.tests;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.TermSubset;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelFactory;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Map;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Term Subset To Nested Value Map</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class TermSubsetToNestedValueMapTest extends TestCase {

  /**
   * The fixture for this Term Subset To Nested Value Map test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected Map.Entry<TermSubset, NestedValue> fixture = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static void main(String[] args) {
    TestRunner.run(TermSubsetToNestedValueMapTest.class);
  }

  /**
   * Constructs a new Term Subset To Nested Value Map test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TermSubsetToNestedValueMapTest(String name) {
    super(name);
  }

  /**
   * Sets the fixture for this Term Subset To Nested Value Map test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void setFixture(Map.Entry<TermSubset, NestedValue> fixture) {
    this.fixture = fixture;
  }

  /**
   * Returns the fixture for this Term Subset To Nested Value Map test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected Map.Entry<TermSubset, NestedValue> getFixture() {
    return fixture;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see junit.framework.TestCase#setUp()
   * @generated
   */
  @Override
  @SuppressWarnings("unchecked")
  protected void setUp() throws Exception {
    setFixture((Map.Entry<TermSubset, NestedValue>)obodatamodelFactory.eINSTANCE.create(obodatamodelPackage.Literals.TERM_SUBSET_TO_NESTED_VALUE_MAP));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see junit.framework.TestCase#tearDown()
   * @generated
   */
  @Override
  protected void tearDown() throws Exception {
    setFixture(null);
  }

} //TermSubsetToNestedValueMapTest
