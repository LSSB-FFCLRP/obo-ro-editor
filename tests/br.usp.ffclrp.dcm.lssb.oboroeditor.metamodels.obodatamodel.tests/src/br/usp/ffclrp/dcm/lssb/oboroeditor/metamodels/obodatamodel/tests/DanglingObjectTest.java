/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.tests;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.DanglingObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Dangling Object</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class DanglingObjectTest extends LinkedObjectTest {

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static void main(String[] args) {
    TestRunner.run(DanglingObjectTest.class);
  }

  /**
   * Constructs a new Dangling Object test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DanglingObjectTest(String name) {
    super(name);
  }

  /**
   * Returns the fixture for this Dangling Object test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected DanglingObject getFixture() {
    return (DanglingObject)fixture;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see junit.framework.TestCase#setUp()
   * @generated
   */
  @Override
  protected void setUp() throws Exception {
    setFixture(obodatamodelFactory.eINSTANCE.createDanglingObject());
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see junit.framework.TestCase#tearDown()
   * @generated
   */
  @Override
  protected void tearDown() throws Exception {
    setFixture(null);
  }

} //DanglingObjectTest
