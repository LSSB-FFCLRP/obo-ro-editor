/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.tests;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.LinkedObject;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Linked Object</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class LinkedObjectTest extends IdentifiedObjectTest {

  /**
   * Constructs a new Linked Object test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LinkedObjectTest(String name) {
    super(name);
  }

  /**
   * Returns the fixture for this Linked Object test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected LinkedObject getFixture() {
    return (LinkedObject)fixture;
  }

} //LinkedObjectTest
