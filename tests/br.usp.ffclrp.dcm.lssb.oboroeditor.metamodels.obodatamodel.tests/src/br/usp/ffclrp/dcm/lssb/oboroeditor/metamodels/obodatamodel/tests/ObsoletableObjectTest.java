/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.tests;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Obsoletable Object</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ObsoletableObjectTest extends IdentifiedObjectTest {

  /**
   * Constructs a new Obsoletable Object test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ObsoletableObjectTest(String name) {
    super(name);
  }

  /**
   * Returns the fixture for this Obsoletable Object test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected ObsoletableObject getFixture() {
    return (ObsoletableObject)fixture;
  }

} //ObsoletableObjectTest
