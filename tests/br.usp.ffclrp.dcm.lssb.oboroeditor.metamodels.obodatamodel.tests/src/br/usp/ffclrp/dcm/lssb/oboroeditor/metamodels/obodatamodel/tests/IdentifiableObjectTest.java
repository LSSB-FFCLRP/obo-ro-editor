/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.tests;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiableObject;
import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Identifiable Object</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class IdentifiableObjectTest extends TestCase {

  /**
   * The fixture for this Identifiable Object test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected IdentifiableObject fixture = null;

  /**
   * Constructs a new Identifiable Object test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IdentifiableObjectTest(String name) {
    super(name);
  }

  /**
   * Sets the fixture for this Identifiable Object test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void setFixture(IdentifiableObject fixture) {
    this.fixture = fixture;
  }

  /**
   * Returns the fixture for this Identifiable Object test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected IdentifiableObject getFixture() {
    return fixture;
  }

} //IdentifiableObjectTest
