/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.tests;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.IdentifiedObject;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Identified Object</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class IdentifiedObjectTest extends ValueTest {

  /**
   * Constructs a new Identified Object test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IdentifiedObjectTest(String name) {
    super(name);
  }

  /**
   * Returns the fixture for this Identified Object test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected IdentifiedObject getFixture() {
    return (IdentifiedObject)fixture;
  }

} //IdentifiedObjectTest
