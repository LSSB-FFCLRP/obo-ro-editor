/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.tests;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBOObject;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>OBO Object</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class OBOObjectTest extends AnnotatedObjectTest {

  /**
   * Constructs a new OBO Object test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBOObjectTest(String name) {
    super(name);
  }

  /**
   * Returns the fixture for this OBO Object test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected OBOObject getFixture() {
    return (OBOObject)fixture;
  }

} //OBOObjectTest
