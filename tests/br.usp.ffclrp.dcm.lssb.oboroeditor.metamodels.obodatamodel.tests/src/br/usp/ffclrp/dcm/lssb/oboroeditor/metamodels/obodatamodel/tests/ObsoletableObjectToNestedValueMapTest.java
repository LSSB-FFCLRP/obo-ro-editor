/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.tests;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.NestedValue;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.ObsoletableObject;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelFactory;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelPackage;

import java.util.Map;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Obsoletable Object To Nested Value Map</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ObsoletableObjectToNestedValueMapTest extends TestCase {

  /**
   * The fixture for this Obsoletable Object To Nested Value Map test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected Map.Entry<ObsoletableObject, NestedValue> fixture = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static void main(String[] args) {
    TestRunner.run(ObsoletableObjectToNestedValueMapTest.class);
  }

  /**
   * Constructs a new Obsoletable Object To Nested Value Map test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ObsoletableObjectToNestedValueMapTest(String name) {
    super(name);
  }

  /**
   * Sets the fixture for this Obsoletable Object To Nested Value Map test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void setFixture(Map.Entry<ObsoletableObject, NestedValue> fixture) {
    this.fixture = fixture;
  }

  /**
   * Returns the fixture for this Obsoletable Object To Nested Value Map test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected Map.Entry<ObsoletableObject, NestedValue> getFixture() {
    return fixture;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see junit.framework.TestCase#setUp()
   * @generated
   */
  @Override
  @SuppressWarnings("unchecked")
  protected void setUp() throws Exception {
    setFixture((Map.Entry<ObsoletableObject, NestedValue>)obodatamodelFactory.eINSTANCE.create(obodatamodelPackage.Literals.OBSOLETABLE_OBJECT_TO_NESTED_VALUE_MAP));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see junit.framework.TestCase#tearDown()
   * @generated
   */
  @Override
  protected void tearDown() throws Exception {
    setFixture(null);
  }

} //ObsoletableObjectToNestedValueMapTest
