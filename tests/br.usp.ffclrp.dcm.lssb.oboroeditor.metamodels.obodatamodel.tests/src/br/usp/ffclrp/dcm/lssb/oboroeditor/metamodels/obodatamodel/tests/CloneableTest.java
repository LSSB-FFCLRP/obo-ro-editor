/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.tests;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Cloneable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class CloneableTest extends TestCase {

  /**
   * The fixture for this Cloneable test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable fixture = null;

  /**
   * Constructs a new Cloneable test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CloneableTest(String name) {
    super(name);
  }

  /**
   * Sets the fixture for this Cloneable test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void setFixture(br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable fixture) {
    this.fixture = fixture;
  }

  /**
   * Returns the fixture for this Cloneable test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Cloneable getFixture() {
    return fixture;
  }

} //CloneableTest
