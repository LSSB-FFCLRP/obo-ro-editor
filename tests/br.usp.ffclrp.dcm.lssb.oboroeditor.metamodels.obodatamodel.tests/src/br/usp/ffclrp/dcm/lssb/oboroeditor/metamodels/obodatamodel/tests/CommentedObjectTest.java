/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.tests;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.CommentedObject;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Commented Object</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class CommentedObjectTest extends IdentifiedObjectTest {

  /**
   * Constructs a new Commented Object test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommentedObjectTest(String name) {
    super(name);
  }

  /**
   * Returns the fixture for this Commented Object test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected CommentedObject getFixture() {
    return (CommentedObject)fixture;
  }

} //CommentedObjectTest
