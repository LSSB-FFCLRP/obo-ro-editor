/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.tests;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.MultiIDObject;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Multi ID Object</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class MultiIDObjectTest extends IdentifiedObjectTest {

  /**
   * Constructs a new Multi ID Object test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MultiIDObjectTest(String name) {
    super(name);
  }

  /**
   * Returns the fixture for this Multi ID Object test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected MultiIDObject getFixture() {
    return (MultiIDObject)fixture;
  }

} //MultiIDObjectTest
