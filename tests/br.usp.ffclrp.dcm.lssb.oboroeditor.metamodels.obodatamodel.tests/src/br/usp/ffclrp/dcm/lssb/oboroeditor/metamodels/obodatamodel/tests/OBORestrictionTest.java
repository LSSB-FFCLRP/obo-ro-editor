/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.tests;

import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.OBORestriction;
import br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.obodatamodelFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>OBO Restriction</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class OBORestrictionTest extends LinkTest {

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static void main(String[] args) {
    TestRunner.run(OBORestrictionTest.class);
  }

  /**
   * Constructs a new OBO Restriction test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OBORestrictionTest(String name) {
    super(name);
  }

  /**
   * Returns the fixture for this OBO Restriction test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected OBORestriction getFixture() {
    return (OBORestriction)fixture;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see junit.framework.TestCase#setUp()
   * @generated
   */
  @Override
  protected void setUp() throws Exception {
    setFixture(obodatamodelFactory.eINSTANCE.createOBORestriction());
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see junit.framework.TestCase#tearDown()
   * @generated
   */
  @Override
  protected void tearDown() throws Exception {
    setFixture(null);
  }

} //OBORestrictionTest
