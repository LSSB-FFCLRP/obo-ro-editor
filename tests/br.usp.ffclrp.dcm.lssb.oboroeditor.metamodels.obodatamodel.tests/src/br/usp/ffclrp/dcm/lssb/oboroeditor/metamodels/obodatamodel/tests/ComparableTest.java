/**
 */
package br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.tests;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Comparable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ComparableTest extends TestCase {

  /**
   * The fixture for this Comparable test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable fixture = null;

  /**
   * Constructs a new Comparable test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComparableTest(String name) {
    super(name);
  }

  /**
   * Sets the fixture for this Comparable test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void setFixture(br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable fixture) {
    this.fixture = fixture;
  }

  /**
   * Returns the fixture for this Comparable test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected br.usp.ffclrp.dcm.lssb.oboroeditor.metamodels.obodatamodel.Comparable getFixture() {
    return fixture;
  }

} //ComparableTest
