# Dependencies P2 Repository

Creates a P2 repository with the dependencies of the project wrapped as OSGI bundles using [p2-maven-plugin](https://github.com/reficio/p2-maven-plugin).


## Building

Retrieve all the dependencies and build the site with:
```
mvn p2:site
```

After a successful build, it's possible to get a running site with Jetty:
```
mvn jetty:run
```

